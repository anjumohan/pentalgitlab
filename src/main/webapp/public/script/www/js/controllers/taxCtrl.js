angular.module('starter.taxCtrl', [])
  .controller('taxCtrl', function($scope,$window, $state,Upload,inputService,sellerService,storeService,toaster,FILE_CONSTANT, MAIN_CONSTANT,$stateParams,$localStorage,loadingService) {
//alert($localStorage.token);
 $scope.imageUrl = FILE_CONSTANT.filepath;
if (!$localStorage.token||$localStorage.token===0) {
    $window.location.href ='#/login';
    }
    $window.scrollTo(0, 0);



 if ($state.current.name === 'admin.tax') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

      $scope.taxPageVo ={"countryId":0,
      "cityId":0,
      "taxId":0,
      "currentIndex":0,
      "rowPerPage":100,"stat":2}

// alert(33333333);
      $scope.gettax = function() {
        loadingService.isLoadin=true;

        sellerService.getalltaxes($scope.taxPageVo).then(function(response) {
          loadingService.isLoadin=false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.taxes = response.object;
            console.log($scope.taxes);



          } else {
           // alert(response.errorMessage);
          }
        },function(){
         // alert('Oops! somthing went wrong');
        });
      }
      $scope.gettax();


    }

//      $scope.gettax = function(tax) {
//    loadingService.isLoadin=true;
//       sellerService.getalltaxes(tax).then(function(response) {
//          loadingService.isLoadin= false;
//         console.log(response);
//         if (response.errorCode === 0) {
//           $scope.taxes = response.object;
//           console.log($scope.taxes);
//         }
//       });
//     };
//  $scope.gettax();
// }


 if ($state.current.name === 'admin.addtax') {
  
  // get country




$scope.getcountry = function(country) {
  loadingService.isLoadin=true;
      storeService.getcountries(country).then(function(response) {
        loadingService.isLoadin=false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.countries = response.object;
        }else { 
        loadingService.isLoadin=false;
      


        }
      },function(){
        loadingService.isLoadin=false;


      });
    };
$scope.getcountry();

 $scope.changeCountry=function(countryId){
  
  console.log(countryId);

    
     $scope.getcityByCountryId(countryId)

   };
$scope.getcityByCountryId = function(countryId) {
  // alert("entered getcityByCountryId");

  $scope.cityPageVo=
                {
                "countryId":countryId,
                "cityId":0,
                "rowPerPage":100,
                "currentIndex":0,
                 "citusStatus": 1,
                "cityName":"",
                "status":[]
              }
               $scope.citiesbycountries = [];

      storeService.getcitiesByCountryId($scope.cityPageVo,countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
 $scope.getcityByCountryId();

  $scope.tax = {};
      
       // $scope. category.imageName ="nothing";
         if ($stateParams.taxId!=='0') {
         $scope.tax=$localStorage.tax;
          $scope.taxtid=1;
     }else{
      $scope.taxtid=0;
     }

      $scope.addTax=function(getAddTaxDetails){

         if (!getAddTaxDetails.taxName) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Tax Name");
          } else {
            toaster.pop('info', "Please Enter Tax Name");
          }

        } else if (!getAddTaxDetails.countryId) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Select Country");
          } else {
            toaster.pop('info', "Please Select Country");
          }


        } else if (!getAddTaxDetails.cityId) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Select City");
          } else {
            toaster.pop('info', "Please Select City");
          }


        }  else if (!getAddTaxDetails.taxPercentage) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Tax Percentage");
          } else {
            toaster.pop('info', "Please Enter Tax Percentage");
          }


        } else if (getAddTaxDetails.status != 0 && getAddTaxDetails.status != 1) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Choose Status");
          } else {
            toaster.pop('info', "Please Choose Status");
          }

        }

        

         loadingService.isLoadin=true;

      console.log(getAddTaxDetails);

                

                inputService.submitAddTaxDetails(getAddTaxDetails).then(function(response) {
                   loadingService.isLoadin=false;
                    console.log(response);
                   
                        if (response.data.errorCode === 0) {
                          if ($stateParams.categoryId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added new Tax!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  The Tax!");

                             }
                           
                           // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
                           // console.log($scope.allProducts);
                            $window.location.href = '#/admin/tax';
                        } else  if (response.data.errorCode === 45||response.data.errorCode === 34) {
                        //  console.log( );
                 $window.location.href = '#/login';

                        }else{
                           toaster.pop('error', "Oops..!", response.data.errorMessage);
                          // alert(response.data.errorMessage);
                        }

                });
         

      /*/fd*/
    }


 }


 $scope.categorymanage=function(category){
   loadingService.isLoadin=true;
  console.log(category);
  $window.location.href='#/admin/addcategory/'+category.categoryId;


  if (category.categoryId) {
    // alert(category.categoryId);

  $localStorage.category=category;
   loadingService.isLoadin=false;
  //


}
};

$scope.viewcategory=function(category){
   loadingService.isLoadin=true;
  console.log(category);
  $window.location.href='#/admin/viewcategory/'+category.categoryId;


  if (category.categoryId) {
    // alert(category.categoryId);

  $localStorage.category=category;
   loadingService.isLoadin=false;
  //


}
};

 




// add category


    if ($state.current.name === 'admin.addtax') {
      $scope.category = {};
       $scope.category.color="#0000FF";
       // $scope. category.imageName ="nothing";
         if ($stateParams.categoryId!=='0') {
         $scope.category=$localStorage.category;
     }
     $scope.deleteimage = function(){
          $scope.category.imageName="";
          
          }


      // $scope.country.countryFlag = 'https://www.iconfinder.com/icons/22899/flag_icon';
      $scope.upload = function(file) {
      //$scope.isFileuploadStart=true;



      if (file) {
        

        $scope.clickimg = true;


        console.log(file);
        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/pentalupload",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {

          console.log(resp);

          //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
          if (resp.data.errorCode === 0) {
            $scope.category.imageName = resp.data.object;

            console.log($scope.category.imageName);

           
              // if ($scope.country.countryFlag) {
              //   console.log(resp);
              //   $scope.country.countryFlag = resp.data.object;
              //   $scope.country.countryFlag.isLoadin = false;

              // }
          


            // if (!$scope.country.countryFlag) {
            //   $scope.country.countryFlag = "";
            // }
          } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';

          } else {

          }





        }, function(resp) {
          $scope.progressPercentage = 0;
          // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
            if ($scope.category.imageName) {
              //splice i
              $scope.category.imageName;

            }
         

          console.log('Error status: ' + resp.status);


        }, function(evt) {
          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
            $scope.filecmplt = false;
          } else if ($scope.progressPercentage == 100) {
            $scope.filecmplt = true;

          } else {
            $scope.filenotUplaod = true;
          }
        });
      }
    };

      
      //loadingService.isLoadin = true;

      $scope.addCategory=function(getAddCategoryDetails){
        

         loadingService.isLoadin=true;

      console.log(getAddCategoryDetails);

                

                inputService.submitAddCategoryDetails(getAddCategoryDetails).then(function(response) {
                   loadingService.isLoadin=false;
                    console.log(response);
                   
                        if (response.data.errorCode === 0) {
                          if ($stateParams.categoryId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added new Category!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  The Category!");

                             }
                           
                           // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
                           // console.log($scope.allProducts);
                            $window.location.href = '#/admin/category';
                        } else  if (response.data.errorCode === 45||response.data.errorCode === 34) {
                        //  console.log( );
                 $window.location.href = '#/login';

                        }else{
                           toaster.pop('error', "Oops..!", response.data.errorMessage);
                          // alert(response.data.errorMessage);
                        }

                });
         

      /*/fd*/
    }


     }


// $scope.getcategory = function(maincat) {
//    loadingService.isLoadin=true;
//       inputService.getcategories(maincat).then(function(response) {
//          loadingService.isLoadin= false;
//       	console.log(response);
//         if (response.errorCode === 0) {
//           $scope.categories = response.object;
//           console.log($scope.categories);
//         }
//       });
//     };
//  $scope.getcategory();
$scope.viewtax=function(tax){
   loadingService.isLoadin=true;
  console.log(tax);
  $window.location.href='#/admin/viewtax/'+tax.taxId;


  if (tax.taxId) {
    // alert(category.categoryId);

  $localStorage.tax=tax;
   loadingService.isLoadin=false;
  //


}
};

$scope.taxmanage=function(tax){
   loadingService.isLoadin=true;
	console.log(tax);
	$window.location.href='#/admin/addtax/'+tax.taxId;


	if (tax.taxId) {
		// alert(category.categoryId);

	$localStorage.tax=tax;
   loadingService.isLoadin=false;
	//


}
};


//view category

if ($state.current.name === 'admin.viewtax') {
  if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.tax = {};
       // $scope. category.imageName ="nothing";
         if ($stateParams.taxId!=='0') {
         $scope.tax=$localStorage.tax;
     }
        

      
    

     }




   //  // add category


   //  if ($state.current.name === 'admin.addcategory') {
   //  	$scope.category = {};
   //  	 $scope. category.imageName ="nothing";
   //       if ($stateParams.categoryId!=='0') {
   //       $scope.category=$localStorage.category;
   //   }
        

    	
   //    //loadingService.isLoadin = true;

   //    $scope.addCategory=function(getAddCategoryDetails){
   //       loadingService.isLoadin=true;

  	// 	console.log(getAddCategoryDetails);

                

   //              inputService.submitAddCategoryDetails(getAddCategoryDetails).then(function(response) {
   //                 loadingService.isLoadin=false;
   //                  console.log(response);
                   
   //                      if (response.data.errorCode === 0) {
   //                        if ($stateParams.categoryId =='0') {
   //                        // toastrMsg(1,'Added new Category!','Successfull..!');
   //                         // $scope.pop = function(){
   //                            toaster.pop('success', "Successfull..!", "Added new Category!");
   //                            // toaster.pop('success', "Added new Category!", "Successfull..!");
   //                             // };
   //                           } else{
   //                             toaster.pop('success', "Successfull..!", "Updated  The Category!");

   //                           }
                           
   //                         // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
   //                         // console.log($scope.allProducts);
   //                          $window.location.href = '#/admin/category';
   //                      } else  if (response.data.errorCode === 45||response.data.errorCode === 34) {
   //                      //	console.log( );
			// 					 $window.location.href = '#/login';

   //                      }else{
   //                         toaster.pop('error', "Oops..!", response.data.errorMessage);
   //                      	// alert(response.data.errorMessage);
   //                      }

   //              });
         

  	// 	/*/fd*/
  	// }


   //   }




 



  });


