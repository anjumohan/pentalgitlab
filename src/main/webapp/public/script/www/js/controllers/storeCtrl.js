angular.module('starter.storeCtrl', [])
  .controller('storeCtrl', function($scope, $window, $state, inputService,toaster,storeService, FILE_CONSTANT,toaster, MAIN_CONSTANT, Upload, storeService, $stateParams, $localStorage,loadingService) {

if (!$localStorage.token||$localStorage.token===0) {
    $window.location.href ='#/login';
    }
    $window.scrollTo(0, 0);
    $scope.imageUrl = FILE_CONSTANT.filepath;

     $scope.pop = function(){
          toaster.pop('info', "title", "text");
      };


    $scope.store = {};
    $scope.store.featureOrPhotos = [];

    $scope.upload = function(file) {

      //$scope.isFileuploadStart=true;
$scope.storelogUploading=true;


      if (file) {

        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/pentalupload",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {
        $scope.store.imageName= resp.data.object;
        $scope.storelogUploading=false;

          console.log(resp);

         


        }, function(resp) {
          $scope.progressPercentage = 0;
         

        }, function(evt) {

          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          
        });
      }
    };










    if ($state.current.name === 'admin.storestatistics') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

      $scope.getstorestatistics = function() {
         loadingService.isLoadin=true;

        $scope.statisticsPageVo = {"rowPerPage":2,
        "currentIndex":0,
        "shopId":0,
        "countryId":0,
        "cityId":0,
        "fromDate":0,
        "toDate":0,
        "status":[]}
        storeService.getstorestatistics($scope.statisticsPageVo).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.storestatistics = response.object.Store;

          } else {
            alert(response.errorMessage);
          }
        },function(){
          //toaster.pop(response.errorMessage);
        });
      }
      $scope.getstorestatistics();

    }





/*    
// new added
$scope.orderPageVo = {
               "rowPerPage":15,
               "currentIndex":0,
               "customerId":0,
               "fromDate":0,
               "toDate":0,
               "orderId":0,
               "status":[2],
               "storeId":$scope.Id
               
       }


$scope.noPages = 1;
$scope.getNumber = function(num) {
    return new Array(num);   
}


    $scope.pageno=1;
// get store statistics*/

$scope.storestatistics= function(store) {
loadingService.isLoadin=true;
 console.log(store);
 console.log(store.storeId);

  $window.location.href='#/admin/listofstoreorder/'+ store.storeId;
  $localStorage.store=store;
   loadingService.isLoadin=false;

   // $scope.getallordersbyuserId(customer.userId);


 };




if ($state.current.name === 'admin.listofstoreorder') {
  if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

      $scope.pageno=1;
      $scope.noPages = 1;
$scope.getNumber = function(num) {
  console.log(num);
    return new Array(num);   
}
      // alert('323');
 
  console.log($stateParams.storeId);
  $scope.Id=$stateParams.storeId;
  console.log($scope.Id);
    $scope.orderPageVo = {
               "rowPerPage":15,
               "currentIndex":0,
               "customerId":0,
               "fromDate":0,
               "toDate":0,
               "orderId":0,
               "status":[2,4,6],
               "storeId":$scope.Id
               
       }

   $scope.getallordersbystoreId= function(pageno) {
    $scope.orderPageVo.currentIndex=(pageno-1)*  $scope.orderPageVo.rowPerPage;
   console.log($stateParams.storeId);
 


  
       console.log( $scope.orderPageVo);
        storeService. getallordersbystoreId( $scope.orderPageVo).then(function(response) {
          loadingService.isLoadin=false;
           $scope.noPages=Math.ceil(response.totalRecords/$scope.orderPageVo.rowPerPage);
        console.log(response);
        if (response.errorCode === 0) {
          $scope.ordersbystoreId = response.object;
          console.log($scope.ordersbystoreId);
        }
      },function(){
        loadingService.isLoadin=false;
      });
    };
   $scope.getallordersbystoreId($scope.pageno);

}

   



    if ($state.current.name === 'admin.store') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

      $scope.storePageVo = { "rowPerPage": 100, "currentIndex": 0, "shopId": 0, "status": [] }

// alert(33333333);
      $scope.allstore = function() {
        loadingService.isLoadin=true;

        storeService.getallstores($scope.storePageVo).then(function(response) {
          loadingService.isLoadin=false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.stores = response.object.Store;
            console.log($scope.stores);



          } else {
            alert(response.errorMessage);
          }
        },function(){
          alert('Oops! somthing went wrong');
        });
      }
      $scope.allstore();

    }

    // get cities

    if ($state.current.name === 'admin.city') {

      $scope.cityPageVo = {
        "countryId": 0,
        "cityId": 0,
        "rowPerPage": 100,
        "currentIndex": 0,
        "cityName": "",
        "status": []
      }

      $scope.getallcity = function() {
         loadingService.isLoadin=true;

        storeService.getallcities($scope.cityPageVo).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.cities = response.object;

          } else {
            alert(response.errorMessage);
          }
        });
      }
      $scope.getallcity();

    }


    $scope.ViewCity = function(city) {
       loadingService.isLoadin=true;

      console.log(city);
      $window.location.href = '#/admin/viewcity/' + city.cityId;


      if (city.cityId) {
        // alert(city.cityId);

        $localStorage.city = city;
        //


      }
    };


    //view city

    if ($state.current.name === 'admin.viewcity') {
       loadingService.isLoadin=false;
      $scope.city = {};
      // $scope. city.imageName ="nothing";
      if ($stateParams.cityId !== '0') {
        $scope.city = $localStorage.city;
      }


    }

    $scope.ViewCountry = function(country) {
       loadingService.isLoadin=true;
      console.log(country);
      $window.location.href = '#/admin/viewcountry/' + country.countryId;


      if (country.countryId) {
        // alert(country.countryId);

        $localStorage.country = country;
        //


      }
    };

    //view country

    if ($state.current.name === 'admin.viewcountry') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
       loadingService.isLoadin=false;
      $scope.country = {};
      // $scope. city.imageName ="nothing";
      if ($stateParams.countryId !== '0') {
        $scope.country = $localStorage.country;
      }


    }

    //counrty manage
    $scope.countrymanage = function(country) {
       loadingService.isLoadin=true;
      console.log(country);
      $window.location.href = '#/admin/addcountry/' + country.countryId;


      if (country.countryId) {
        // alert(country.countryId);

        $localStorage.country = country;
        //


      }
    };


    $scope.citymanage = function(city) {
       loadingService.isLoadin=true;
      console.log(city);
      $window.location.href = '#/admin/addcity/' + city.cityId;


      if (city.cityId) {
        // alert(city.cityId);

        $localStorage.city = city;
        //


      }
    };



    // $scope.getcountry = function(country) {
    //    loadingService.isLoadin=true;
    //   storeService.getcountries(country).then(function(response) {
    //      loadingService.isLoadin=false;
    //     console.log(response);
    //     if (response.errorCode === 0) {
    //       $scope.countries = response.object;
    //     }
    //   });
    // };
    // $scope.getcountry();


    // $scope.getstore = function(storeId) {

    //   $scope.storePageVo=
    //     {"rowPerPage":75,"currentIndex":0,"shopId":0,"status":[]}

    //       storeService.getstores($scope.storePageVo).then(function(response) {
    //         console.log(response);
    //  //        $scope.cities = {
    //  //    "countryId":0,"cityId":0,"rowPerPage":2,"currentIndex":0,"cityName":"","status":[]
    //  // }
    //         if (response.errorCode === 0) {
    //           $scope.stores = response.object;
    //         }
    //       });
    //     };
    // $scope.getstore();

    // //add city

    // if ($state.current.name === 'admin.addcity') {
    //   if (!$localStorage.token || $localStorage.token === 0) {
    //   $window.location.href = '#/login';
    // }
    //   $scope.city = {};

    //   // $scope. country.countryFlag ='1';
    //   if ($stateParams.cityId !== '0') {
    //     $scope.city = $localStorage.city;
    //   }

    //   $scope.addCity = function(getAddCityDetails) {
    //      loadingService.isLoadin=true;

    //     console.log(getAddCityDetails);



    //     storeService.submitAddCityDetails(getAddCityDetails).then(function(response) {
    //        loadingService.isLoadin=false;
           
    //       console.log(response);


    //       if (response.data.errorCode === 0) {
    //          if ($stateParams.cityId =='0') {
    //                       // toastrMsg(1,'Added new Category!','Successfull..!');
    //                        // $scope.pop = function(){
    //                           toaster.pop('success', "Successfull..!", "Added new City!");
    //                           // toaster.pop('success', "Added new Category!", "Successfull..!");
    //                            // };
    //                          } else{
    //                            toaster.pop('success', "Successfull..!", "Updated  City!");

    //                          }

    //         $window.location.href = '#/admin/city';
    //       } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
    //         //  console.log( );
    //         $window.location.href = '#/login';

    //       } else {
    //         // alert(response.data.errorMessage);
    //          toaster.pop('error', "Oops..!", response.data.errorMessage);
    //       }

    //     });



    //   }


    // }

    // add country


//     if ($state.current.name === 'admin.addcountry') {
//       if (!$localStorage.token || $localStorage.token === 0) {
//       $window.location.href = '#/login';
//     }
//       $scope.country = {};

//       // $scope.country.countryFlag = 'https://www.iconfinder.com/icons/22899/flag_icon';
//       $scope.upload = function(file) {
//       //$scope.isFileuploadStart=true;



//       if (file) {
        

//         $scope.clickimg = true;


//         console.log(file);
//         Upload.upload({
//           url: MAIN_CONSTANT.site_url + "/api/uploadtoaws",
//           headers: {
//             "Content-Type": 'multipart/*',
//             Bearer: $localStorage.token
//           },
//           /*data: { file: FILE_CONSTANT.site_url + file.name,
//             type: 'image/jpeg' }*/
//           data: {
//             file: file
//           }
//         }).then(function(resp) {

//           console.log(resp);

//           //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
//           if (resp.data.errorCode === 0) {
//             $scope.country.countryFlag = resp.data.object;

//             console.log($scope.country.countryFlag);

           
//               // if ($scope.country.countryFlag) {
//               //   console.log(resp);
//               //   $scope.country.countryFlag = resp.data.object;
//               //   $scope.country.countryFlag.isLoadin = false;

//               // }
          


//             // if (!$scope.country.countryFlag) {
//             //   $scope.country.countryFlag = "";
//             // }
//           } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
//             $localStorage.token = 0;
//             $window.location.href = '#/login';

//           } else {

//           }





//         }, function(resp) {
//           $scope.progressPercentage = 0;
//           // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
//             if ($scope.country.countryFlag) {
//               //splice i
//               $scope.country.countryFlag;

//             }
         

//           console.log('Error status: ' + resp.status);


//         }, function(evt) {
//           $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
//           /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
//           console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
//           // $scope.file = null;
//           if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
//             $scope.filecmplt = false;
//           } else if ($scope.progressPercentage == 100) {
//             $scope.filecmplt = true;

//           } else {
//             $scope.filenotUplaod = true;
//           }
//         });
//       }
//     };


//       // $scope. country.countryFlag ='1';
//       if ($stateParams.countryId !== '0') {
//         $scope.country = $localStorage.country;
//       }


// $scope.deleteimage = function(){
//           $scope.country.countryFlag="";
          
//           }
//       // $scope.deleteimage = function(name) {

//       //   console.log(name);

//       //    $scope.country = {};

//       //   if ($stateParams.countryId !== '0') {
//       //   $scope.country = $localStorage.country;
//       // }
//       //    // $scope.counrty;
//       //    // $scope.counrty.countryFlag;
//       //   // $scope.counrty.countryFlag = name;
//       //   console.log( $scope.counrty.countryFlag);
//       //   if ($scope.counrty.countryFlag) {
//       //     $scope.counrty.countryFlag = " ";

//       //   } 
//       //   //$scope.product.featureOrPhotos[i].data

//       // }

//       $scope.addCountry = function(getAddCountryDetails) {
//          loadingService.isLoadin=true;

//         console.log(getAddCountryDetails);

//         storeService.submitAddCountryDetails(getAddCountryDetails).then(function(response) {
//            loadingService.isLoadin=false;
//           console.log(response);


//           if (response.data.errorCode === 0) {
//              if ($stateParams.countryId =='0') {
//                           // toastrMsg(1,'Added new Category!','Successfull..!');
//                            // $scope.pop = function(){
//                               toaster.pop('success', "Successfull..!", "Added New Country!");
//                               // toaster.pop('success', "Added new Category!", "Successfull..!");
//                                // };
//                              } else{
//                                toaster.pop('success', "Successfull..!", "Updated  Country!");

//                              }

//             $window.location.href = '#/admin/country';
//           } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
//             //  console.log( );
//             $window.location.href = '#/login';

//           } else {
//             // alert(response.data.errorMessage);
//              toaster.pop('error', "Oops..!", response.data.errorMessage);
//           }

//         });

//       }


//     }
//     // add city


//     if ($state.current.name === 'admin.addcity') {
//       if (!$localStorage.token || $localStorage.token === 0) {
//       $window.location.href = '#/login';
//     }
//       $scope.city = {};

//       // $scope. country.countryFlag ='1';
//       if ($stateParams.cityId !== '0') {
//         $scope.city = $localStorage.city;
//       }

//       $scope.addCity = function(getAddCityDetails) {
//          loadingService.isLoadin=true;

//         console.log(getAddCityDetails);

//         storeService.submitAddCityDetails(getAddCityDetails).then(function(response) {
//            loadingService.isLoadin=false;
//           console.log(response);

//           if (response.data.errorCode === 0) {
//              if ($stateParams.cityId =='0') {
//                           // toastrMsg(1,'Added new Category!','Successfull..!');
//                            // $scope.pop = function(){
//                               toaster.pop('success', "Successfull..!", "Added new City!");
//                               // toaster.pop('success', "Added new Category!", "Successfull..!");
//                                // };
//                              } else{
//                                toaster.pop('success', "Successfull..!", "Updated  Cit!");

//                              }

//             $window.location.href = '#/admin/city';
//           } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
//             //  console.log( );
//             $window.location.href = '#/login';

//           } else {
//             // alert(response.data.errorMessage);
//              toaster.pop('error', "Oops..!", response.data.errorMessage);
//           }

//         });


//       }


//     }


    $scope.viewstore = function(store) {
       loadingService.isLoadin=true;
      console.log(store);
      $window.location.href = '#/admin/viewstore/' + store.storeId;


      if (store.storeId) {
        // alert(store.storeId);

        $localStorage.store = store;
        //


      }
    };
    //view store

    if ($state.current.name === 'admin.viewstore') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
       loadingService.isLoadin=false;
      $scope.store = {};
      $scope.store.imageName = "nothing";
      if ($stateParams.storeId !== '0') {
        $scope.store = $localStorage.store;
      }





    }

    $scope.storemanage = function(store) {
       loadingService.isLoadin=true;
      console.log(store);
      $window.location.href = '#/admin/addstore/' + store.storeId;


      if (store.storeId) {
        // alert(store.storeId);

        $localStorage.store = store;
        //


      }
    };


    // add store

    if ($state.current.name === 'admin.addstore') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }


      $scope.store.imageName = "nothing";
      if ($stateParams.storeId !== '0') {
        $scope.store = $localStorage.store;
      }

      $scope.addStore = function(getAddStoreDetails) {
         loadingService.isLoadin=true;
        $scope.featureVo = [{
          "featurePhotoId": 1,
          "modeType": 1,
          "userId": 1,
          "extraData": "qwert",
          "extraDataNum": "12",
          "productOrShop": 1,
          "status": 1,
          "lastUpdatedTime": ""
        }]


        console.log(getAddStoreDetails);

        storeService.submitAddStoredetails(getAddStoreDetails).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);

          if (response.data.errorCode === 0) {
            if ($stateParams.storeId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added new Store!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  Store!");

                             }

            $window.location.href = '#/admin/store';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {

            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
            toaster.pop('error', "Oops..!", response.data.errorMessage);
          }

        });



      }


    }


    $scope.changeCountry = function(countryId) {
      console.log(countryId);


      $scope.getcityByCountryId(countryId)

    };
    $scope.getcityByCountryId = function(countryId) {

      $scope.cityPageVo = {
        "countryId": countryId,
        "cityId": 0,
        "rowPerPage": 100,
        "currentIndex": 0,
        "cityName": "",
        "status": []
      }
      $scope.citiesbycountries=[];

      storeService.getcitiesByCountryId($scope.cityPageVo, countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
    // $scope.getcityByCountryId();














    //get all store item request



    if ($state.current.name === 'admin.storeitemrequests') {
      $scope.getallitemrequests = function(itemrequests) {
        loadingService.isLoadin=true;
        storeService.getallitemrequests(itemrequests).then(function(response) {
          loadingService.isLoadin=false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.itemrequests = response.object;
            console.log($scope.itemrequests);
          }
          else if(response.errorCode === 2){

            toaster.pop('warning', "Oops..!", "No Data to Show");


          }
        });
      };
      $scope.getallitemrequests();
    }
    //get all responsed item request



    if ($state.current.name === 'admin.responsedstorerequests') {
      $scope.getallitemrequests = function(itemrequests) {
        loadingService.isLoadin=true;
        storeService.getallitemrequests(itemrequests).then(function(response) {
          loadingService.isLoadin=false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.itemrequests = response.object;
            console.log($scope.itemrequests);
          }else if(response.errorCode === 2){

            toaster.pop('warning', "Oops..!", "No Data To Show");


          }
        });
      };
      $scope.getallitemrequests();



    }


    //get store

    if ($state.current.name === 'admin.store') {

      $scope.storePageVo = { "rowPerPage": 100, "currentIndex": 0, "shopId": 0, "status": [] }


      $scope.allstore = function() {
        loadingService.isLoadin=true;


        storeService.getallstores($scope.storePageVo).then(function(response) {
          loadingService.isLoadin=false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.stores = response.object.Store;
            console.log($scope.stores);



          } else {
            alert(response.errorMessage);
          }
        });
      }
      $scope.allstore();

    }



    $scope.itemrequestmanage = function(itemrequest) {
      console.log(itemrequest);
      // $window.location.href='#/admin/itemacceptance/'+itemrequest.requestItemId;



      if (itemrequest.requestItemId) {
        alert(itemrequest.requestItemId);

        $localStorage.itemrequest = itemrequest;
        //


      }
      $scope.addrequesteditem(itemrequest);
    };

    $scope.addrequesteditem = function(itemrequest) {
      loadingService.isLoadin=true;


      console.log(itemrequest);



      storeService.addrequesteditem(itemrequest).then(function(response) {
        loadingService.isLoadin=false;
        console.log(response);

        if (response.data.errorCode === 0) {

          // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
          // console.log($scope.allProducts);
          $window.location.href = '#/admin/storeitemrequests';
          // window.location.reload();
        } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
          //  console.log( );
          $window.location.href = '#/login';

        } else {
          alert(response.data.errorMessage);
        }

      });


      /*/fd*/
    }
// store search

// get country


$scope.getcountry = function(country) {
  loadingService.isLoadin=true;
      storeService.getcountries(country).then(function(response) {
        loadingService.isLoadin=false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.countries = response.object;
        }else { 
        loadingService.isLoadin=false;
      


        }
      },function(){
        loadingService.isLoadin=false;


      });
    };
$scope.getcountry();


// store  search
$scope.searchstorestatistics= function(search){
  loadingService.isLoadin=true;


  console.log(search);
  if (search.fromDate) {
  $scope.fromDate = new Date(search.fromDate).getTime();
}
  console.log($scope.fromDate);
  $scope.toDate = new Date(search.toDate).getTime();
  console.log($scope.toDate);
  console.log(search);
  $scope.storePageVo=
                     {
             "rowPerPage":2,
             "currentIndex":0,
             "customerId":0,
             "fromDate":0,
             "toDate":0,
             "orderId":0,
             "status":[1],
             "storeId":0
               
       }

                
                    // {
                    //           "rowPerPage":100,
                    //          "currentIndex":0,
                    //          "customerId":0,
                    //          "fromDate":$scope.fromDate,
                    //          "toDate":$scope.toDate,
                    //          "auctionId":0,
                    //          "auctionDetaiilsId":0,
                    //          "status":[1],
                    //          "cityId":search.cityId,
                    //          "countryId":search.countryId
                    //                                };
                                                   // alert("hiii");

               storeService.getallstorebysearch($scope.storePageVo).then(function(response) {
                  loadingService.isLoadin=false;
                    console.log(response);

                   

                    if (response.errorCode === 0) {
                       // $scope.shopList = $scope.shopList.concat(response.object.Product);

                         $scope.storestatistics = response.object;

                    } else {
                        $scope.storestatistics=[];
                        alert(response.errorMessage);
                        loadingService.isLoadin=false;
                    }
                },function(response){

                    $scope.storestatistics=[];
                    loadingService.isLoadin=false;



                });
            }
            
   $scope.changeCountry=function(countryId){
  console.log(countryId);

    
     $scope.getcityByCountryId(countryId)

   };
$scope.getcityByCountryId = function(countryId) {

  $scope.cityPageVo=
                {
                "countryId":countryId,
                "cityId":0,
                "rowPerPage":100,
                "currentIndex":0,
                "cityName":"",
                "status":[]
              }
               $scope.citiesbycountries = [];

      storeService.getcitiesByCountryId($scope.cityPageVo,countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
 $scope.getcityByCountryId();

 $scope.vieworderdetailsofstore = function(order) {
      // alert("01");
      loadingService.isLoadin = true;

      console.log(order);
      $window.location.href = '#/admin/customerorderdetails/' + order.orderId;


      if (order.orderId) {
        // alert(city.cityId);

        $localStorage.order = order;
        //


      }
    };

    if ($state.current.name === 'admin.customerorderdetails') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.order = {};
      // $scope.customer.password ="password123";
      // $scope. category.imageName ="nothing";


      if ($stateParams.orderId !== '0') {
        $scope.order = $localStorage.order;


      }
      $scope.Id = $scope.order.orderId;


      // console.log($stateParams.userId);
      // $scope.Id = $stateParams.userId;
      // console.log($scope.Id);
      $scope.getorderdetailsbyorderId = function(Id) {
        console.log(Id);
        $scope.orderPageVo = {
          "rowPerPage": 100,
          "currentIndex": 0,
          "customerId": 0,
          "fromDate": 0,
          "toDate": 0,
          "orderId": $scope.Id,
          "status": [],
          "storeId": 0

        }
        console.log($scope.orderPageVo);
        inputService.getorderdetailsbyorderId($scope.orderPageVo).then(function(response) {
          console.log(response);
          if (response.errorCode === 0) {
            // $scope.orderdetailsbyorderId = response.object;
            $scope.orderdetails = response.object[0].orderDetailsVo[0];
            // console.log($scope.ordersbyuserId);
            console.log( $scope.orderdetails);
          }
        });
      };
      $scope.getorderdetailsbyorderId();


    }


  });
