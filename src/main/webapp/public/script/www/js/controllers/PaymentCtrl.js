angular.module('starter.PaymentCtrl', [])
    .controller('PaymentCtrl', function($scope, $state, payrollService, $window, employeeService, toaster, loadingService) {



        // loadingService.isLoadin=true;
         $scope.ExpenseHead={};



        $scope.Payheadmaster = {};
        $scope.empsalary = { status: 1 };
        var date = new Date(); // some mock date
        var todaydate = date.getTime();
        console.log(todaydate)
        $scope.currentPages = 1;

        if($state.current.name=='admin.addincomehead'){

          

          $scope.openDeleteIncomeHeadModal = function(key,head) {
            if (key==0) {
              $("#myModal").modal();

            }else{
              $("#myModalAr").modal();

            }
        
        $scope.headData = head;
        
        console.log($scope.headData);
      }
      $scope.deleteincomedatahead = function() {
        $scope.headData.status=0;
                     $("#myModal").modal("hide");
                     $("#myModalAr").modal("hide");
                   
                     loadingService.isLoadin = true;
                    // $scope.IncomeHead.business_type = 0;
                    // $scope.IncomeHead.last_updated_time = todaydate;
              
                    
                    console.log( $scope.headData)
                    payrollService.addIncomeOrExpenseHead($scope.headData)
                        .then(function(res) {
                            console.log(res)
                            loadingService.isLoadin = false;
                            console.log(res);
                            if (res.data.errorCode == 0) {

                                $state.go($state.current, {}, { reload: true });

                                toaster.pop('success', "Successfully Deleted", );
                            } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                                $localStorage.token = 0;
                                 loadingService.isLoadin = false;
                                 $window.location.href = '#/login';

                            }
                        }, function(error) {
                             loadingService.isLoadin = false;
                            console.log(error)

                        });
            
            }



            $scope.IncomeHead = {status:1};
          
            $scope.addincomedatahead = function(form) {
                if (form.$valid) {     
                   
                     loadingService.isLoadin = true;
                    $scope.IncomeHead.business_type = 0;
                    $scope.IncomeHead.last_updated_time = todaydate;
                   // $scope.IncomeHead.empId = 0;
                    //$scope.IncomeHead.ispayable = 1;
                    $scope.IncomeHead.tranType=5;
                   // $scope.IncomeHead.schoolId = 0;
                    $scope.IncomeHead.operatingOfficer="";
                    $scope.IncomeHead.incomeOrExpense=7;
                    console.log( $scope.IncomeHead)
                    payrollService.addIncomeOrExpenseHead($scope.IncomeHead)
                        .then(function(res) {
                            console.log(res)
                            loadingService.isLoadin = false;
                            console.log(res);
                            if (res.data.errorCode == 0) {

                                $state.go($state.current, {}, { reload: true });

                                toaster.pop('success', " Income Head Add Successfully ", );
                            } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                                $localStorage.token = 0;
                                 loadingService.isLoadin = false;
                                 $window.location.href = '#/login';

                            }
                        }, function(error) {
                             loadingService.isLoadin = false;
                            console.log(error)

                        });
                }
            }
            $scope.getAllIncomeHead = function() {
                var incomeOrExpense=7;
            loadingService.isLoadin = true;
            payrollService.getAllIncomeOrExpenseHead(incomeOrExpense)
                .then(function(res) {
                    loadingService.isLoadin = false;
                    console.log(res);
                    if (res.data.errorCode == 0) {
                        $scope.allincomeHead = res.data.object;
                        console.log( $scope.allincomeHead );

                    }
                     else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                        $window.localStorage.token = 0;

                        $state.go('header_login.login');

                    } else {
                        toaster.pop('error', res.data.errorMessage);

                    }
                    //  alert('success');
                }, function(error) {
                    loadingService.isLoadin = false;
                    toaster.pop('error', " server error ");
                    // alert('error');
                });
            }
            $scope.getAllIncomeHead();

            $scope.editIncomeHead=function(incHead){
                console.log(incHead);
                $scope.UpdateIncomeHead={};
                // $scope.UpdateIncomeHead=incHead;
                 $scope.UpdateIncomeHead = angular.copy(incHead);
                 if ($scope.langval == 1) {
                     $('#updateIncomeHeadModalEn').modal();
                 }else{
                    $('#updateIncomeHeadModalAr').modal(); 
                 }
                

            }
              $scope.submitIncomeHeadUpdatedData=function(incHead){
                console.log(incHead);
                if ($scope.langval == 1) {
                    $('#updateIncomeHeadModalEn').modal('hide'); 
                 }else{
                    $('#updateIncomeHeadModalAr').modal('hide');                  
                 }
                
                  $scope.IncHeadData=angular.copy(incHead);




                  $scope.IncHeadData.business_type = 0;
                    $scope.IncHeadData.status = 1;
                    $scope.IncHeadData.last_updated_time = todaydate;
                   // $scope.IncomeHead.empId = 0;
                    //$scope.IncomeHead.ispayable = 1;
                    $scope.IncHeadData.tranType=5;
                   // $scope.IncomeHead.schoolId = 0;
                    $scope.IncHeadData.operatingOfficer="";
                    $scope.IncHeadData.incomeOrExpense=7;
                    console.log( $scope.IncHeadData)
                    payrollService.addIncomeOrExpenseHead($scope.IncHeadData)
                        .then(function(res) {
                            console.log(res)
                            loadingService.isLoadin = false;
                            console.log(res);
                            if (res.data.errorCode == 0) {

                                $state.go($state.current, {}, { reload: true });

                                toaster.pop('success', " Income Head Updated Successfully ", );
                            } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                                $localStorage.token = 0;
                                 loadingService.isLoadin = false;
                                 $window.location.href = '#/login';

                            }
                        }, function(error) {
                             loadingService.isLoadin = false;
                            console.log(error)

                        });

            }



        }

          if($state.current.name=='admin.addexpensehead'){

            

            $scope.openDeleteExpenseHeadModal = function(key,head) {
            if (key==0) {
              $("#myModal").modal();

            }else{
              $("#myModalAr").modal();

            }
        
        $scope.headData = head;
        
        console.log($scope.headData);
      }
      $scope.deleteexpensedatahead = function() {
      
                     $("#myModal").modal("hide");
                     $("#myModalAr").modal("hide");
                     $scope.headData.status=0;
                     loadingService.isLoadin = true;
                    // $scope.IncomeHead.business_type = 0;
                    // $scope.IncomeHead.last_updated_time = todaydate;
              
                    
                    console.log( $scope.headData)
                    payrollService.addIncomeOrExpenseHead($scope.headData)
                        .then(function(res) {
                            console.log(res)
                            loadingService.isLoadin = false;
                            console.log(res);
                            if (res.data.errorCode == 0) {

                                $state.go($state.current, {}, { reload: true });

                                toaster.pop('success', "Successfully Deleted", );
                            } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                                $localStorage.token = 0;
                                 loadingService.isLoadin = false;
                                 $window.location.href = '#/login';

                            }
                        }, function(error) {
                             loadingService.isLoadin = false;
                            console.log(error)

                        });
            
            }
            
             $scope.editExpenseHead=function(expHead){
                console.log(expHead);
                $scope.UpdateExpenseHead={};
                // $scope.UpdateIncomeHead=incHead;
                 $scope.UpdateExpenseHead = angular.copy(expHead);
                 if ($scope.langval == 1) {
                     $('#updateExpenseHeadModalEn').modal();
                 }else{
                    $('#updateExpenseHeadModalAr').modal(); 
                 }                

            }
              $scope.submitIncomeHeadUpdatedData=function(expHead){
                console.log(expHead);
                if ($scope.langval == 1) {
                    $('#updateExpenseHeadModalEn').modal('hide'); 
                 }else{
                    $('#updateExpenseHeadModalAr').modal('hide');                  
                 }
                
                  $scope.ExpHeadData=angular.copy(expHead);
                  $scope.ExpHeadData.business_type = 0;
                    $scope.ExpHeadData.status = 1;
                    $scope.ExpHeadData.last_updated_time = todaydate;
                   // $scope.IncomeHead.empId = 0;
                    //$scope.IncomeHead.ispayable = 1;
                    $scope.ExpHeadData.tranType=5;
                   // $scope.IncomeHead.schoolId = 0;
                    $scope.ExpHeadData.operatingOfficer="";
                    $scope.ExpHeadData.incomeOrExpense=8;
                    console.log( $scope.ExpHeadData)
                    payrollService.addIncomeOrExpenseHead($scope.ExpHeadData)
                        .then(function(res) {
                            console.log(res)
                            loadingService.isLoadin = false;
                            console.log(res);
                            if (res.data.errorCode == 0) {

                                $state.go($state.current, {}, { reload: true });

                                toaster.pop('success', " Expense Head Updated Successfully ", );
                            } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                                $localStorage.token = 0;
                                 loadingService.isLoadin = false;
                                 $window.location.href = '#/login';

                            }
                        }, function(error) {
                             loadingService.isLoadin = false;
                            console.log(error)

                        });

            }

                      $scope.addexpensedatahead = function(form) {
                        console.log(form);
                if (form.$valid) {
                     loadingService.isLoadin = true;
                     // $scope.ExpenseHead.business_type = 0;
                 //   $scope.ExpenseHead.last_updated_time = todaydate;
                   // $scope.ExpenseHead.empId = 0;
                   // $scope.ExpenseHead.ispayable = 1;
                  //  $scope.ExpenseHead.tranType=5;
                   // $scope.ExpenseHead.schoolId = 0;
                    // $scope.ExpenseHead.operatingOfficer="";
                    $scope.ExpenseHead.incomeOrExpense=8;
                    $scope.ExpenseHead.status=1;
                    console.log( $scope.ExpenseHead)

                    payrollService.addIncomeOrExpenseHead($scope.ExpenseHead)
                        .then(function(res) {
                            console.log(res)
                            loadingService.isLoadin = false;
                            console.log(res);
                            if (res.data.errorCode == 0) {

                                $state.go($state.current, {}, { reload: true });

                                toaster.pop('success', " Expense head Add Successfully ", );
                            } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                                $window.localStorage.token = 0;
                                 loadingService.isLoadin = false;
                                $state.go('header_login.login');

                            }
                        }, function(error) {
                             loadingService.isLoadin = false;
                            console.log(error)

                        });
                }
            }
            

              $scope.getAllExpenseHead = function() {
            // loadingService.isLoadin = true;
            // payrollService.getexpensehead()
                      var incomeOrExpense=8;
            loadingService.isLoadin = true;
            payrollService.getAllIncomeOrExpenseHead(incomeOrExpense)
                .then(function(res) {
                    loadingService.isLoadin = false;
                    console.log(res);
                    if (res.data.errorCode == 0) {
                        $scope.allexpenseHead = res.data.object;

                    } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                        $window.localStorage.token = 0;

                        $state.go('header_login.login');

                    } else {
                        toaster.pop('error', res.data.errorMessage);

                    }
                    //  alert('success');
                }, function(error) {
                    loadingService.isLoadin = false;
                    toaster.pop('error', " server error ");
                    // alert('error');
                });
        }
         $scope.getAllExpenseHead();  


          }

          //----------------------------------- ADD INCOME PAGE START-------------------------------

             if ($state.current.name == 'admin.addincome') {
            $scope.Income = {status:1};

                    $scope.getAllIncomeHead = function() {
                var incomeOrExpense=7;
            loadingService.isLoadin = true;
            payrollService.getAllIncomeOrExpenseHead(incomeOrExpense)
                .then(function(res) {
                    loadingService.isLoadin = false;
                    console.log(res);
                    if (res.data.errorCode == 0) {
                        $scope.allincomeHead = res.data.object;
                        console.log( $scope.allincomeHead );

                    }
                     else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                        $window.localStorage.token = 0;

                        $state.go('header_login.login');

                    } else {
                        toaster.pop('error', res.data.errorMessage);

                    }
                    //  alert('success');
                }, function(error) {
                    loadingService.isLoadin = false;
                    toaster.pop('error', " server error ");
                    // alert('error');
                });
        }
                $scope.getAllIncomeHead();

                          $scope.addincomedata = function(form) {
                if (form.$valid) {




                     loadingService.isLoadin = true;
                    $scope.Income.business_type = 1;
                    $scope.Income.last_updated_time = todaydate;
                     $scope.Income.date_income = todaydate;
                     $scope.Income.note = "DummyNote";


                  //  $scope.Income.empId = 0;
                    $scope.Income.ispayable = 0;
                   // $scope.Income.tranType=1;
                   // $scope.Income.schoolId = 0;
                    payrollService.addincomedata($scope.Income)
                        .then(function(res) {
                            console.log(res)
                            loadingService.isLoadin = false;
                            console.log(res);
                            if (res.data.errorCode == 0) {

                                $state.go($state.current, {}, { reload: true });

                                toaster.pop('success', " Income Add Successfully ", );
                            } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                                $window.localStorage.token = 0;
                                 loadingService.isLoadin = false;
                                $state.go('header_login.login');

                            }
                        }, function(error) {
                             loadingService.isLoadin = false;
                            console.log(error)

                        });
                }
            }

            $scope.getAllIncome = function() {
            loadingService.isLoadin = true;
            payrollService.getAllIncome()
                .then(function(res) {
                    loadingService.isLoadin = false;
                    console.log(res);
                    if (res.data.errorCode == 0) {
                        $scope.allincome = res.data.object;

                    } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                        $window.localStorage.token = 0;

                        $state.go('header_login.login');

                    } else {
                        toaster.pop('error', res.data.errorMessage);

                    }
                    //  alert('success');
                }, function(error) {
                    loadingService.isLoadin = false;
                    toaster.pop('error', " server error ");
                    // alert('error');
                });
        }

        $scope.getAllIncome();

}


          //------------------------------------ ADD INCOME PAGE END -------------------------------



          //-------------------------------------- ADD EXPENSE PAGE START-----------------------------

                 if ($state.current.name == 'admin.addexpense') {
            $scope.Expense = {status:1};

             $scope.getAllExpense = function() {
            loadingService.isLoadin = true;
            payrollService.getAllExpense()
                .then(function(res) {
                    loadingService.isLoadin = false;
                    console.log(res);
                    if (res.data.errorCode == 0) {
                        $scope.allexpense= res.data.object;

                    } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                        $window.localStorage.token = 0;

                        $state.go('header_login.login');

                    } else {
                        toaster.pop('error', res.data.errorMessage);

                    }
                    //  alert('success');
                }, function(error) {
                    loadingService.isLoadin = false;
                    toaster.pop('error', " server error ");
                    // alert('error');
                });
        }
         $scope.getAllExpense();

                      $scope.getAllExpenseHead = function() {
     
                      var incomeOrExpense=8;
            loadingService.isLoadin = true;
            payrollService.getAllIncomeOrExpenseHead(incomeOrExpense)
                .then(function(res) {
                    loadingService.isLoadin = false;
                    console.log(res);
                    if (res.data.errorCode == 0) {
                        $scope.allexpenseHead = res.data.object;

                    } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                        $window.localStorage.token = 0;

                        $window.location.href = '#/login';

                    } else {
                        toaster.pop('error', res.data.errorMessage);

                    }
                    //  alert('success');
                }, function(error) {
                    loadingService.isLoadin = false;
                    toaster.pop('error', " server error ");
                    // alert('error');
                });
        }
         $scope.getAllExpenseHead();  

                    $scope.addexpensedata = function(form) {
                if (form.$valid) {

                     loadingService.isLoadin = true;
                    $scope.Expense.business_type = 1;
                    $scope.Expense.last_updated_time = todaydate;
                    $scope.Expense.date_income = todaydate;
                    $scope.Expense.note= "DummyNote";
                    $scope.Expense.empId = 0;
                    $scope.Expense.ispayable = 1;
                    $scope.Expense.tranType=1;
                    $scope.Expense.schoolId = 0;
                    payrollService.addexpensedata($scope.Expense)
                        .then(function(res) {
                            console.log(res)
                            loadingService.isLoadin = false;
                            console.log(res);
                            if (res.data.errorCode == 0) {

                                $state.go($state.current, {}, { reload: true });

                                toaster.pop('success', " Expense Add Successfully ", );
                            } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
                                $window.localStorage.token = 0;
                                 loadingService.isLoadin = false;
                                $state.go('header_login.login');

                            }
                        }, function(error) {
                             loadingService.isLoadin = false;
                            console.log(error)

                        });
                }
            }
        }



          //--------------------------------------- ADD EXPENSE PAGE END -----------------------------

        

        //         if($state.current.name=='admin.addincomehead'){
        //               $scope.IncomeHead = {status:1};

        // $scope.getAllIncomeHead();


        //     $scope.addincomedatahead = function(form) {
        //         if (form.$valid) {

        
        //             // ###########################################
        //              loadingService.isLoadin = true;
        //             $scope.IncomeHead.business_type = 0;
        //             $scope.IncomeHead.last_updated_time = todaydate;
        //            // $scope.IncomeHead.empId = 0;
        //             //$scope.IncomeHead.ispayable = 1;
        //             $scope.IncomeHead.tranType=5;
        //             $scope.IncomeHead.schoolId = 0;
        //             $scope.IncomeHead.operatingOfficer="";
        //             $scope.IncomeHead.incomeOrExpense=7;
        //             console.log( $scope.IncomeHead)
        //             payrollService.addIncomeOrExpenseHead($scope.IncomeHead)
        //                 .then(function(res) {
        //                     console.log(res)
        //                     loadingService.isLoadin = false;
        //                     console.log(res);
        //                     if (res.data.errorCode == 0) {

        //                         $state.go($state.current, {}, { reload: true });

        //                         toaster.pop('success', " Income Head Add Successfully ", );
        //                     } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                         $window.localStorage.token = 0;
        //                          loadingService.isLoadin = false;
        //                         $state.go('header_login.login');

        //                     }
        //                 }, function(error) {
        //                      loadingService.isLoadin = false;
        //                     console.log(error)

        //                 });
        //         }
        //     }




        // }

        // $scope.getAllDesignation = function() {
        //     var status = 1;
        //     loadingService.isLoadin = true;
        //     employeeService.getAllDesignation(0, 100, $window.localStorage.schoolId, status)
        //         .then(function(res) {
        //             loadingService.isLoadin = false;
        //             console.log(res.data.object);
        //             if (res.data.errorCode == 0) {

        //                 $scope.alldesignation = res.data.object;
        //             } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                 $window.localStorage.token = 0;

        //                 $state.go('header_login.login');

        //             } else {
        //                 toaster.pop('error', res.data.errorMessage);

        //             }


        //             //  alert('success');
        //         }, function(error) {
        //             loadingService.isLoadin = false;
        //             toaster.pop('error', " server error ");
        //             // alert('error');
        //         });



        // }



        // $scope.getAllEmployeeBySchool = function() {
        //     loadingService.isLoadin = true;
        //     employeeService.getAllEmployee(0, 100, $window.localStorage.schoolId)
        //         .then(function(res) {
        //             loadingService.isLoadin = false;
        //             console.log(res.data.object);
        //             if (res.data.errorCode == 0) {

        //                 $scope.allemployee = res.data.object;
        //             } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                 $window.localStorage.token = 0;

        //                 $state.go('header_login.login');

        //             } else {
        //                 toaster.pop('error', res.data.errorMessage);

        //             }
        //         }, function(error) {
        //             loadingService.isLoadin = false;
        //             toaster.pop('error', " server error ");
        //             // alert('failed')
        //         });

        // }


        // $scope.getAllEmployeeSalary = function() {
        //     var empid = 0;



        //     $scope.numPerPagess = 5;
        //     $scope.paginatesalaryfull = function(value) {
        //         var begin, end, index;
        //         begin = ($scope.currentPages - 1) * $scope.numPerPagess;
        //         end = begin + $scope.numPerPagess;
        //         index = $scope.emp_salary.indexOf(value);
        //         return (begin <= index && index < end);
        //     };




        //     loadingService.isLoadin = true;
        //     payrollService.getAllEmployeeSalary(empid, $window.localStorage.schoolId)
        //         .then(function(res) {
        //             console.log(res);
        //             loadingService.isLoadin = false;
        //             if (res.data.errorCode == 0) {

        //                 $scope.emp_salary = res.data.object;
        //                 $scope.emp_salaryfulllength = $scope.emp_salary.length;
        //                 console.log($scope.emp_salaryfulllength);

        //             } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                 $window.localStorage.token = 0;

        //                 $state.go('header_login.login');

        //             } else {
        //                 toaster.pop('error', res.data.errorMessage);

        //             }

        //         }, function(error) {
        //             loadingService.isLoadin = false;
        //             toaster.pop('error', " server error ");
        //             // alert('failed')
        //         });






        // }





        // $scope.getAllEmployeeBySchool();
        // $scope.getAllDesignation();
        // $scope.getAllEmployeeSalary();

        // $scope.getallemployeebydesignation = function(designid) {
        //     loadingService.isLoadin = true;
        //     employeeService.getAllEmployeeByDesignation(designid)
        //         .then(function(res) {
        //             console.log(res);
        //             loadingService.isLoadin = false;
        //             if (res.data.errorCode == 0) {
        //                 $scope.allemployeebydesig = res.data.object;
        //             } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                 $window.localStorage.token = 0;

        //                 $state.go('header_login.login');

        //             } else {
        //                 toaster.pop('error', res.data.errorMessage);

        //             }

        //         }, function(error) {
        //             toaster.pop('error', " server error ");
        //             loadingService.isLoadin = false;
        //             // alert('failed')
        //         });
        // }

        // $scope.addPayheadType = function(form) {
        //     console.log(form.$valid);

        //     if (form.$valid) {
        //         console.log($scope.Payheadmaster);
        //         loadingService.isLoadin = true;

        //         $scope.Payheadmaster.last_updated_time = todaydate;
        //         $scope.Payheadmaster.schoolId = $window.localStorage.schoolId;
        //         $scope.Payheadmaster.status = "";

        //         $scope.Payheadmaster.payHeadId = "0";
        //         payrollService.saveOrUpdatePayheadType($scope.Payheadmaster)
        //             .then(function(res) {
        //                 console.log(res);
        //                 loadingService.isLoadin = false;
        //                 if (res.data.errorCode == 0) {
        //                     $state.go($state.current, {}, { reload: true });
        //                     toaster.pop('success', " Payhead Type Add Successfully ", );
        //                 } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                     $window.localStorage.token = 0;

        //                     $state.go('header_login.login');

        //                 } else {
        //                     toaster.pop('error', res.data.errorMessage);

        //                 }
        //                 // alert('success')
        //             }, function(error) {
        //                 loadingService.isLoadin = false;
        //                 toaster.pop('error', " server error ");
        //                 // alert('failed')
        //             });

        //     }

        // }

        // //getpalyhead

        // $scope.getAllPayHead = function() {
        //     loadingService.isLoadin = true;
        //     payrollService.getAllPayHead(0, 100, $window.localStorage.schoolId)
        //         .then(function(res) {
        //             loadingService.isLoadin = false;
        //             console.log(res.data.object);
        //             if (res.data.errorCode == 0) {
        //                 $scope.payhead = res.data.object;

        //             } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                 $window.localStorage.token = 0;

        //                 $state.go('header_login.login');

        //             } else {
        //                 toaster.pop('error', res.data.errorMessage);

        //             }
        //             //  alert('success');
        //         }, function(error) {
        //             loadingService.isLoadin = false;
        //             toaster.pop('error', " server error ");
        //             // alert('error');
        //         });
        // }

        // $scope.getAllPayHead();

        // $scope.saveOrUpdateemployeesalary = function() {
        //     loadingService.isLoadin = true;
        //     $scope.empsalary.schoolId = $window.localStorage.schoolId;
        //     $scope.empsalary.last_updated_time = todaydate;
        //     $scope.empsalary.to_who_id = 1;
        //     // $scope.empsalary.status = "";
        //     $scope.empsalary.date = todaydate;
        //     $scope.empsalary.transaction_id = 0;
        //     $scope.empsalary.net_amount = 100;
        //     $scope.empsalary.paired_id = 1;
        //     $scope.empsalary.section = "";
        //     $scope.empsalary.departmentId = "0";
        //     $scope.empsalary.issueDate = todaydate;
        //     console.log($scope.empsalary);
        //     payrollService.employeesalarycollection($scope.empsalary)
        //         .then(function(res) {

        //             // alert('success');
        //             console.log(res);
        //             loadingService.isLoadin = false;
        //             if (res.data.errorCode == 0) {
        //                 $state.go($state.current, {}, { reload: true });
        //                 toaster.pop('success', " Employee Salary Add Successfully ", );
        //             } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                 $window.localStorage.token = 0;

        //                 $state.go('header_login.login');

        //             } else {
        //                 toaster.pop('error', res.data.errorMessage);

        //             }
        //         }, function(error) {
        //             loadingService.isLoadin = false;
        //             toaster.pop('error', " server error ");

        //         });

        // }

        // $scope.predicate = 'name';
        // $scope.reverse = true;
        // $scope.currentPage = 1;
        // $scope.order = function(predicate) {
        //     $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        //     $scope.predicate = predicate;
        // };
        // // add payhead
        // $scope.payhead = [
        //     { id: 1, payhead_name: 'Salary', addorded: 'Addition' },
        //     { id: 2, payhead_name: 'PF', addorded: 'Addition' },
        //     { id: 3, payhead_name: 'null', addorded: 'Addition' },
        //     { id: 4, payhead_name: 'null', addorded: 'Addition' },
        // ];


        // $scope.totalpayhead = $scope.payhead.length;
        // $scope.numPerPage = 5;
        // $scope.paginatepayhead = function(value) {
        //     var begin, end, index;
        //     begin = ($scope.currentPage - 1) * $scope.numPerPage;
        //     end = begin + $scope.numPerPage;
        //     index = $scope.payhead.indexOf(value);
        //     return (begin <= index && index < end);
        // };







        /* $scope.totalsalarylength = $scope.emp_salary.length;
         $scope.numPerPage = 5;
         $scope.paginatesalary = function(value) {
             var begin, end, index;
             begin = ($scope.currentPage - 1) * $scope.numPerPage;
             end = begin + $scope.numPerPage;
             index = $scope.emp_salary.indexOf(value);
             return (begin <= index && index < end);
         };*/

        //    $scope.getAllIncomeHead = function() {
        //     loadingService.isLoadin = true;
        //     payrollService.getincomehead()
        //         .then(function(res) {
        //             loadingService.isLoadin = false;
        //             console.log(res);
        //             if (res.data.errorCode == 0) {
        //                 $scope.allincomeHead = res.data.object;

        //             }
        //              else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                 $window.localStorage.token = 0;

        //                 $state.go('header_login.login');

        //             } else {
        //                 toaster.pop('error', res.data.errorMessage);

        //             }
        //             //  alert('success');
        //         }, function(error) {
        //             loadingService.isLoadin = false;
        //             toaster.pop('error', " server error ");
        //             // alert('error');
        //         });
        // }

        // $scope.getAllExpenseHead = function() {
        //     loadingService.isLoadin = true;
        //     payrollService.getexpensehead()
        //         .then(function(res) {
        //             loadingService.isLoadin = false;
        //             console.log(res);
        //             if (res.data.errorCode == 0) {
        //                 $scope.allexpenseHead = res.data.object;

        //             } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                 $window.localStorage.token = 0;

        //                 $state.go('header_login.login');

        //             } else {
        //                 toaster.pop('error', res.data.errorMessage);

        //             }
        //             //  alert('success');
        //         }, function(error) {
        //             loadingService.isLoadin = false;
        //             toaster.pop('error', " server error ");
        //             // alert('error');
        //         });
        // }
        //  if($state.current.name=='app.addincomehead'){

        //     $scope.IncomeHead = {status:1};



        // $scope.getAllIncomeHead();


        //     $scope.addincomedatahead = function(form) {
        //         if (form.$valid) {
        //              loadingService.isLoadin = true;
        //             $scope.IncomeHead.business_type = 0;
        //             $scope.IncomeHead.last_updated_time = todaydate;
        //             $scope.IncomeHead.empId = 0;
        //             $scope.IncomeHead.ispayable = 1;
        //             $scope.IncomeHead.tranType=5;
        //             $scope.IncomeHead.schoolId = 0;
        //             $scope.IncomeHead.operatingOfficer="";
        //             $scope.IncomeHead.incomeOrExpense=7;
        //             console.log( $scope.IncomeHead)
        //             payrollService.addincomehead($scope.IncomeHead)
        //                 .then(function(res) {
        //                     console.log(res)
        //                     loadingService.isLoadin = false;
        //                     console.log(res);
        //                     if (res.data.errorCode == 0) {

        //                         $state.go($state.current, {}, { reload: true });

        //                         toaster.pop('success', " Income Head Add Successfully ", );
        //                     } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                         $window.localStorage.token = 0;
        //                          loadingService.isLoadin = false;
        //                         $state.go('header_login.login');

        //                     }
        //                 }, function(error) {
        //                      loadingService.isLoadin = false;
        //                     console.log(error)

        //                 });
        //         }
        //     }

        //  }
        //  if($state.current.name=='app.addexpensehead'){

        //     $scope.ExpenseHead = {status:1};


             

        // $scope.getAllExpenseHead();

        //      $scope.addexpensedatahead = function(form) {
        //         if (form.$valid) {
        //              loadingService.isLoadin = true;
        //               $scope.ExpenseHead.business_type = 0;
        //             $scope.ExpenseHead.last_updated_time = todaydate;
        //             $scope.ExpenseHead.empId = 0;
        //             $scope.ExpenseHead.ispayable = 1;
        //             $scope.ExpenseHead.tranType=5;
        //             $scope.ExpenseHead.schoolId = 0;
        //             $scope.ExpenseHead.operatingOfficer="";
        //             $scope.ExpenseHead.incomeOrExpense=8;
        //             console.log( $scope.ExpenseHead)
        //             payrollService.addexpensehead($scope.ExpenseHead)
        //                 .then(function(res) {
        //                     console.log(res)
        //                     loadingService.isLoadin = false;
        //                     console.log(res);
        //                     if (res.data.errorCode == 0) {

        //                         $state.go($state.current, {}, { reload: true });

        //                         toaster.pop('success', " Expense head Add Successfully ", );
        //                     } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                         $window.localStorage.token = 0;
        //                          loadingService.isLoadin = false;
        //                         $state.go('header_login.login');

        //                     }
        //                 }, function(error) {
        //                      loadingService.isLoadin = false;
        //                     console.log(error)

        //                 });
        //         }
        //     }
            
        //  }
        // if ($state.current.name == 'app.addincome') {
        //     $scope.Income = {status:1};

        //      $scope.getAllIncome = function() {
        //     loadingService.isLoadin = true;
        //     payrollService.getIncome()
        //         .then(function(res) {
        //             loadingService.isLoadin = false;
        //             console.log(res);
        //             if (res.data.errorCode == 0) {
        //                 $scope.allincome = res.data.object;

        //             } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                 $window.localStorage.token = 0;

        //                 $state.go('header_login.login');

        //             } else {
        //                 toaster.pop('error', res.data.errorMessage);

        //             }
        //             //  alert('success');
        //         }, function(error) {
        //             loadingService.isLoadin = false;
        //             toaster.pop('error', " server error ");
        //             // alert('error');
        //         });
        // }

        // $scope.getAllIncome();
        //  $scope.getAllIncomeHead();

        //     $scope.addincomedata = function(form) {
        //         if (form.$valid) {
        //              loadingService.isLoadin = true;
        //             $scope.Income.business_type = 1;
        //             $scope.Income.last_updated_time = todaydate;
        //             $scope.Income.empId = 0;
        //             $scope.Income.ispayable = 0;
        //             $scope.Income.tranType=1;
        //             $scope.Income.schoolId = 0;
        //             payrollService.addincomedata($scope.Income)
        //                 .then(function(res) {
        //                     console.log(res)
        //                     loadingService.isLoadin = false;
        //                     console.log(res);
        //                     if (res.data.errorCode == 0) {

        //                         $state.go($state.current, {}, { reload: true });

        //                         toaster.pop('success', " Income Add Successfully ", );
        //                     } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                         $window.localStorage.token = 0;
        //                          loadingService.isLoadin = false;
        //                         $state.go('header_login.login');

        //                     }
        //                 }, function(error) {
        //                      loadingService.isLoadin = false;
        //                     console.log(error)

        //                 });
        //         }
        //     }
        // }

        // if ($state.current.name == 'app.addexpense') {
        //     $scope.Expense = {status:1};

        //      $scope.getAllExpense = function() {
        //     loadingService.isLoadin = true;
        //     payrollService.getExpense()
        //         .then(function(res) {
        //             loadingService.isLoadin = false;
        //             console.log(res);
        //             if (res.data.errorCode == 0) {
        //                 $scope.allexpense= res.data.object;

        //             } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                 $window.localStorage.token = 0;

        //                 $state.go('header_login.login');

        //             } else {
        //                 toaster.pop('error', res.data.errorMessage);

        //             }
        //             //  alert('success');
        //         }, function(error) {
        //             loadingService.isLoadin = false;
        //             toaster.pop('error', " server error ");
        //             // alert('error');
        //         });
        // }

        // $scope.getAllExpense();
        //   $scope.getAllExpenseHead();

        //     $scope.addexpensedata = function(form) {
        //         if (form.$valid) {
        //              loadingService.isLoadin = true;
        //             $scope.Expense.business_type = 1;
        //             $scope.Expense.last_updated_time = todaydate;
        //             $scope.Expense.empId = 0;
        //             $scope.Expense.ispayable = 1;
        //             $scope.Expense.tranType=1;
        //             $scope.Expense.schoolId = 0;
        //             payrollService.addexpensedata($scope.Expense)
        //                 .then(function(res) {
        //                     console.log(res)
        //                     loadingService.isLoadin = false;
        //                     console.log(res);
        //                     if (res.data.errorCode == 0) {

        //                         $state.go($state.current, {}, { reload: true });

        //                         toaster.pop('success', " Expense Add Successfully ", );
        //                     } else if (res.data.errorCode == 45 || res.data.errorCode == 34) {
        //                         $window.localStorage.token = 0;
        //                          loadingService.isLoadin = false;
        //                         $state.go('header_login.login');

        //                     }
        //                 }, function(error) {
        //                      loadingService.isLoadin = false;
        //                     console.log(error)

        //                 });
        //         }
        //     }
        // }

        $(document).on('click', ':not(form)[data-confirm]', function(e){
    if(!confirm($(this).data('confirm'))){
      e.stopImmediatePropagation();
      e.preventDefault();
        }
});

$(document).on('submit', 'form[data-confirm]', function(e){
    if(!confirm($(this).data('confirm'))){
        e.stopImmediatePropagation();
      e.preventDefault();
        }
});

$(document).on('input', 'select', function(e){
    var msg = $(this).children('option:selected').data('confirm');
    if(msg != undefined && !confirm(msg)){
        $(this)[0].selectedIndex = 0;
    }
});



    });