angular.module('starter.offerCtrl', [])
  .controller('offerCtrl', function($scope,$window, $state,Upload,inputService,toaster,FILE_CONSTANT, MAIN_CONSTANT,$stateParams,$localStorage,loadingService) {
//alert($localStorage.token);
 $scope.imageUrl = FILE_CONSTANT.filepath;
if (!$localStorage.token||$localStorage.token===0) {
    $window.location.href ='#/login';
    }
    $window.scrollTo(0, 0);
// $scope.field1=0;
  $scope.field1=1;
  $scope.field2=1;


 if ($state.current.name === 'admin.offer') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }


     $scope.offerPageVo = {
"offerId":"",
"offerName":"",
"offerNameAr":"",
"status":2,"offerStarts":"","offerEnds":"",

"rowPerPage":"300","currentIndex":0

}
      

      $scope.getalloffer = function() {
         loadingService.isLoadin=true;

        inputService.getalloffers($scope.offerPageVo).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);


          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.offers = response.object;
  $scope.nooffers=0;

          }else if (response.errorCode===-3) {
             $scope.nooffers=1;

          } else {
            alert(response.errorMessage);
          }
        });
      }
      $scope.getalloffer();

 $scope.offermanage=function(offer){
   loadingService.isLoadin=true;
  console.log(offer);
  $window.location.href='#/admin/addoffer/'+offer.offerId;


  if (offer.offerId) {
    // alert(category.categoryId);

  $localStorage.offer=offer;
   loadingService.isLoadin=false;
  //


}
};

$scope.viewoffer=function(offer){
   loadingService.isLoadin=true;
  console.log(offer);
  $window.location.href='#/admin/viewoffer/'+offer.offerId;


  if (offer.offerId) {
    // alert(category.categoryId);

  $localStorage.offer=offer;
   loadingService.isLoadin=false;
  //


}
};

    }




// add category


    if ($state.current.name === 'admin.addoffer') {
      $scope.offer = {};
       // $scope.category.color="#111"+ new Date().getTime();
       // console.log( $scope.category.color);
       // $scope. category.imageName ="nothing";
         if ($stateParams.offerId!=='0') {
         $scope.offer=$localStorage.offer;
         $scope.ofid=1;
     }else{
      $scope.ofid=0;
     }
     $scope.deleteimage = function(){
          $scope.offer.imageName="";
          
          }


      // $scope.country.countryFlag = 'https://www.iconfinder.com/icons/22899/flag_icon';
      $scope.upload = function(file) {
      $scope.isFileuploadStart=true;



      if (file) {
        

        $scope.clickimg = true;


        console.log(file);
        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/pentalupload",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {

          console.log(resp);

          //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
          if (resp.data.errorCode === 0) {
             $scope.isFileuploadStart=false;
            $scope.offer.imageName = resp.data.object;

            console.log($scope.offer.imageName);

           
              // if ($scope.country.countryFlag) {
              //   console.log(resp);
              //   $scope.country.countryFlag = resp.data.object;
              //   $scope.country.countryFlag.isLoadin = false;

              // }
          


            // if (!$scope.country.countryFlag) {
            //   $scope.country.countryFlag = "";
            // }
          } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
             $scope.isFileuploadStart=false;
            $localStorage.token = 0;
            $window.location.href = '#/login';

          } else {

          }





        }, function(resp) {
           $scope.isFileuploadStart=false;
          $scope.progressPercentage = 0;
          // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
            if ($scope.offer.imageName) {
              //splice i
              $scope.offer.imageName;

            }
         

          console.log('Error status: ' + resp.status);


        }, function(evt) {
          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
            $scope.filecmplt = false;
          } else if ($scope.progressPercentage == 100) {
            $scope.filecmplt = true;

          } else {
            $scope.filenotUplaod = true;
          }
        });
      }
    };

      
      //loadingService.isLoadin = true;
      $scope.offer.rowPerPage=100;
      $scope.offer.currentIndex=0;
      $scope.offer.offerStarts=new Date($scope.offer.offerStarts);
       $scope.offer.offerEnds=new Date($scope.offer.offerEnds);

      $scope.addOffer=function(getAddOfferDetails){
        

         loadingService.isLoadin=true;

      console.log(getAddOfferDetails);
      getAddOfferDetails.offerStarts=new Date( getAddOfferDetails.offerStarts).getTime();
      console.log(getAddOfferDetails.offerStarts);
       getAddOfferDetails.offerEnds=new Date( getAddOfferDetails.offerEnds).getTime();
      console.log(getAddOfferDetails.offerEnds);
      // $scope.fromDate = new Date(search.fromDate).getTime();

                

                inputService.submitAddOfferDetails(getAddOfferDetails).then(function(response) {
                   loadingService.isLoadin=false;
                    console.log(response);
                   
                        if (response.data.errorCode === 0) {
                          if ($stateParams.offerId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added New Offer!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  The Offer!");

                             }
                           
                           // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
                           // console.log($scope.allProducts);
                            $window.location.href = '#/admin/offer';
                        } else  if (response.data.errorCode === 45||response.data.errorCode === 34) {
                        //  console.log( );
                 $window.location.href = '#/login';

                        }else{
                           toaster.pop('error', "Oops..!", response.data.errorMessage);
                          // alert(response.data.errorMessage);
                        }

                });
         

      /*/fd*/
    }


     }


//view category

if ($state.current.name === 'admin.viewoffer') {
  if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.offer = {};
       // $scope. offer.imageName ="nothing";
         // if ($stateParams.categoryId!=='0') {
         $scope.offer=$localStorage.offer;
     // }
        

      
    

     }



 $scope.selectone=function(data){
console.log(data);
if(!data){
  // alert("nodata");
  $scope.field1=1;
};
var arr = data;
if (arr.length>=1){
        $scope.field1=0;
    }else{
        $scope.field1=1;
    }

    console.log($scope.field1);

  // (offer.offerPercentage)


 
}
 $scope.selectone=function(data){
console.log(data);
if(!data){
  // alert("nodata");
  $scope.field1=1;
};
var arr = data;
if (arr.length>=1){
        $scope.field1=0;
    }else{
        $scope.field1=1;
    }

    console.log($scope.field1);

  // (offer.offerPercentage)


 
}
 $scope.selectonetwo=function(data){
console.log(data);
if(!data){
  // alert("nodata");
  $scope.field2=1;
};
var arra = data;
if (arra.length>=1){
        $scope.field2=0;
    }else{
        $scope.field2=1;
    }

    console.log($scope.field2);

  // (offer.offerPercentage)


 
}
$scope.listofproductstooffer=function(offerid){
  console.log(offerid);
  $localStorage.offerId=offerid;
  $window.location.href='#/admin/listofproducts';


}

if ($state.current.name === 'admin.listofproducts') {
  var oferid =  $localStorage.offerId;
  console.log(oferid);
$scope.productPageVo=
     {"rowPerPage": "15",
       "currentIndex": 0,
       "productName": "",
       "productId": "0",
       "subCategoryId": "0","countryId":0,"cityId":0,"userId":0,
       "categoryId": "0",
       "sellerId": "0",
       "stat": "2",
       "status": []
     }

      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getproduct = function(pageno) {
        loadingService.isLoadin = true;
        // $scope.productPageVo.currentIndex = (pageno - 1) * $scope.productPageVo.rowPerPage;
          $scope.productPageVo.currentIndex = (pageno - 1) * $scope.productPageVo.rowPerPage;



        inputService.getproducts($scope.productPageVo).then(function(response) {
          loadingService.isLoadin = false;
            $scope.noPages = Math.ceil(response.object.TOTALCOUNTS / $scope.productPageVo.rowPerPage);

          // $scope.noPages = Math.ceil(response.object.TOTALCOUNTS / $scope.productPageVo.rowPerPage);
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.products = response.object.Product;

            console.log($scope.products);
            // console.log($scope.productimages);
          }
        }, function() {
          alert("Oops! error code");
          loadingService.isLoadin = false
        });
      };
      $scope.getproduct($scope.pageno);
      $scope.assignoffertoproduct=function(product){

        console.log(product);
        var productId = product.productId;
        console.log(productId);
         console.log(oferid);
          inputService.assigningoffertoproduct(oferid,productId).then(function(response) {
         loadingService.isLoadin= false;
        console.log(response);
        if (response.errorCode === 0) {
           // $window.location.href='#/admin/listofproducts';
           $scope.getproduct(1);
           toaster.pop('success', "Successfull..!", "Assigned Offer to Product!");
        }
      });
      }
        $scope.removeoffertoproduct=function(product){

        console.log(product);
        var productId = product.productId;
        console.log(productId);
        
         // var oferid =0;
         //  console.log(oferid);
          inputService.removeingoffertoproduct(productId).then(function(response) {
         loadingService.isLoadin= false;
        console.log(response);
        if (response.errorCode === 0) {
           // $window.location.href='#/admin/listofproducts';
           $scope.getproduct(1);
           toaster.pop('success', "Successfull..!", "Removed Product from Offer !");
        }
      });
      }
}
  });


