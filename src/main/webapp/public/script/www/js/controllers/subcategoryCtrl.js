angular.module('starter.subcategoryCtrl', [])
  .controller('subcategoryCtrl', function($scope, $window, $state,Upload,FILE_CONSTANT, MAIN_CONSTANT, toaster,inputService, $stateParams, $localStorage) {
    //alert($localStorage.token);
$scope.imageUrl = FILE_CONSTANT.filepath;

    // //change category status
    // $scope.ChangeCategorystatus=function(category,categoryId){
    //   console.log(category);


    if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
    $window.scrollTo(0, 0);

 if ($state.current.name === 'admin.deactivatedsubcategories') {
  $window.scrollTo(0, 0);

    $scope.getsubcategory = function() {
      loadingService.isLoadin = true;
      inputService.getsubcategories().then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.subcategories = response.object;
          console.log($scope.subcategories);
        }
      });
    };
    $scope.getsubcategory();

    $scope.viewsubcategory = function(subcategory) {

      console.log(subcategory);
      $window.location.href = '#/admin/viewsubcategory/' + subcategory.subCategoryId;


      if (subcategory.subCategoryId) {
        // alert(subcategory.subCategoryId);

        $localStorage.subcategory = subcategory;
        //


      }
    };
  }


    //subcategories
    $scope.getactivecategory = function(maincat) {
      loadingService.isLoadin = true;
      inputService.getactivecategories(maincat).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.categories = response.object;
          console.log($scope.categories);
        }
      });
    };
    $scope.getactivecategory();
 if ($state.current.name === 'admin.subcategory') {
  $window.scrollTo(0, 0);

    $scope.getsubcategory = function() {
      loadingService.isLoadin = true;
      inputService.getsubcategories().then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.subcategories = response.object;
          console.log($scope.subcategories);
        }
      });
    };
    $scope.getsubcategory();

    $scope.viewsubcategory = function(subcategory) {

      console.log(subcategory);
      $window.location.href = '#/admin/viewsubcategory/' + subcategory.subCategoryId;


      if (subcategory.subCategoryId) {
        // alert(subcategory.subCategoryId);

        $localStorage.subcategory = subcategory;
        //


      }
    };
  }
    //view subcategory

    if ($state.current.name === 'admin.viewsubcategory') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.subcategory = {};
      // $scope. subcategory.imageName ="nothing";
      if ($stateParams.subCategoryId !== '0') {
        $scope.subcategory = $localStorage.subcategory;
      }


    }




    $scope.subcategorymanage = function(subcategory) {
      console.log(subcategory);
      $window.location.href = '#/admin/addsubcategory/' + subcategory.subCategoryId;


      if (subcategory.subCategoryId) {
        // alert(subcategory.subCategoryId);

        $localStorage.subcategory = subcategory;
        //


      }
    };



    // add subcategory


    if ($state.current.name === 'admin.addsubcategory') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.subcategory = {};
      // $scope.subcategory.imageName = "nothing";
      if ($stateParams.subCategoryId !== '0') {
        $scope.subcategory = $localStorage.subcategory;
        $scope.subcati=1;
      }else{
        $scope.subcati=0;
      }

      $scope.deleteimage = function(){
          $scope.subcategory.imageName="";
          
          }


      // $scope.country.countryFlag = 'https://www.iconfinder.com/icons/22899/flag_icon';
      $scope.upload = function(file) {
      $scope.isFileuploadStart=true;



      if (file) {
        

        $scope.clickimg = true;


        console.log(file);
        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/pentalupload",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {

          console.log(resp);

          //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
          if (resp.data.errorCode === 0) {
              $scope.isFileuploadStart=false;
            $scope.subcategory.imageName = resp.data.object;

            console.log($scope.subcategory.imageName);

           
              // if ($scope.country.countryFlag) {
              //   console.log(resp);
              //   $scope.country.countryFlag = resp.data.object;
              //   $scope.country.countryFlag.isLoadin = false;

              // }
          


            // if (!$scope.country.countryFlag) {
            //   $scope.country.countryFlag = "";
            // }
          } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
              $scope.isFileuploadStart=false;
            $localStorage.token = 0;
            $window.location.href = '#/login';

          } else {

          }





        }, function(resp) {
            $scope.isFileuploadStart=false;
          $scope.progressPercentage = 0;
          // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
            if ($scope.subcategory.imageName) {
              //splice i
              $scope.subcategory.imageName;

            }
         

          console.log('Error status: ' + resp.status);


        }, function(evt) {
          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
            $scope.filecmplt = false;
          } else if ($scope.progressPercentage == 100) {
            $scope.filecmplt = true;

          } else {
            $scope.filenotUplaod = true;
          }
        });
      }
    };

      $scope.selectcategory = function(categoryId) {
        console.log(categoryId);
        $scope.subcategory = { "categoryId": categoryId.categoryId };
      }


      //loadingService.isLoadin = true;

      $scope.addSubCategory = function(getAddSubCategoryDetails) {
        loadingService.isLoadin = true;
        getAddSubCategoryDetails.rank=1;

        console.log(getAddSubCategoryDetails);


          if (!getAddSubCategoryDetails.categoryId) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Select Category");
          } else {
           toaster.pop('info', "Please Select Category");
          }

        } else if (!getAddSubCategoryDetails.subCategoryName) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Subcategory Name In English");
          } else {
            toaster.pop('info', "Please Enter Subcategory Name In English");
          }


        } else if (!getAddSubCategoryDetails.subCategoryNameAr) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Subcategory Name In Arabic");
          } else {
            toaster.pop('info', "Please Enter Subcategory Name In Arabic");
          }


        }else if (!getAddSubCategoryDetails.discription) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Description In English");
          } else {
            toaster.pop('info', "Please Enter Description In English");
          }


        } else if (!getAddSubCategoryDetails.discriptionAr) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Description In Arabic");
          } else {
            toaster.pop('info', "Please Enter Description In Arabic");
          }

        } else if (!getAddSubCategoryDetails.imageName) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Upload Image");
          } else {
            toaster.pop('info', "Please Upload Image");
          }


        } else if (getAddSubCategoryDetails.status != 0 && getAddSubCategoryDetails.status != 1) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Choose Status");
          } else {
            toaster.pop('info', "Please Choose Status");
          }

        }else{
        inputService.submitAddSubCategoryDetails(getAddSubCategoryDetails).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          if (response.data.errorCode === 0) {

            if ($stateParams.subCategoryId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added New Subcategory!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  Subcategory!");

                             }
            // toaster.pop('success', "Successfull..!", "Added New SubCategory!");

            // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
            // console.log($scope.allProducts);
            $window.location.href = '#/admin/subcategory';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            toaster.pop('error', "Oops..!", response.data.errorMessage);
            // alert(response.data.errorMessage);
          }

        });


        }



        /*/fd*/
      }


    }





  });
