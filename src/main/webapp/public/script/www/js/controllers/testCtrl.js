angular.module('starter.testCtrl', [])
    .controller('testCtrl', function($scope, $timeout, addPostService, authService) {
        var authData = authService.getCurrentUser();
        if (authData) {
            console.log("Logged in as:", authData.uid);
        } else {
            console.log("Logged out");
        }
    });
