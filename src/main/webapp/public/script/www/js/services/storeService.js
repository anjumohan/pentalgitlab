angular.module('starter.storeService', [])
  .factory('storeService', storeService);

function storeService(TYPE_POST, $q, $http, MAIN_CONSTANT,$localStorage) {
$http.defaults.headers.common['Bearer'] = $localStorage.token;
return {
    test:function(){alert(2222)},
    changeorderstatus:changeorderstatus,
     
      getcountries: getcountries,
      getactivecountries:getactivecountries,
      getallorders:getallorders,
      // getstores:getstores,
      getallcities:getallcities,
      submitAddStoredetails:submitAddStoredetails,
      getstorestatistics:getstorestatistics,
      getcitiesByCountryId:getcitiesByCountryId,
      getallitemrequests:getallitemrequests,
      addrequesteditem:addrequesteditem,
      getallordersbystoreId:getallordersbystoreId,
      getactivetaxes:getactivetaxes,
      

     

     getallstores:getallstores,
     submitAddCountryDetails:submitAddCountryDetails,
     getlistofordersbystoreId:getlistofordersbystoreId,
     getallstorebysearch:getallstorebysearch,
     getalltaxes:getalltaxes,
     getactivecountries:getactivecountries,
     getcitiesByCountryId:getcitiesByCountryId,
     submitAddCityDetails:submitAddCityDetails
  };
   //get citiesby country
   function  getcitiesByCountryId(countryId) {
    var deffered = $q.defer();
    //adppostData
    console.log(countryId);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/getallcity",
      data: countryId
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
  //active countries

function getactivecountries() {
    var deffered = $q.defer();
    console.log();
      $http({
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/getallcountry?countryId=0&status=1",

    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }


//change order status

  function changeorderstatus(orderId,changeId) {
    console.log(orderId,changeId);
    // alert("reache77777777d");
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/order/changeorderstatus?orderId="+orderId +"&orderlyStatus="+changeId+"&orderDetailsIds[]=0",
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }



// get all stores by search
  function getallstorebysearch(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getshops",
      data: Data
    }).then(function mySucces(response) {
      
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }


    // get order by store

function  getlistofordersbystoreId(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallorders",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  // get order by store

function getallordersbystoreId(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallorders",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
 
 

  function getallitemrequests() {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getallitemrequest?requestItemId=0&userId=0&countryId=0&cityId=0&status=2&storeName=&currentIndex=0&rowPerPage=10" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

   function getalltaxes(taxData) {
    var deffered = $q.defer();
    //adppostData
    console.log(taxData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/order/getalltax",
      data: taxData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  // active taxes

   function getactivetaxes(taxData) {
    var deffered = $q.defer();
    //adppostData
    console.log(taxData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/order/getalltax",
      data: taxData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }


  function getallstores(storeData) {
    var deffered = $q.defer();
    //adppostData
    console.log(storeData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/user/getallsellers",
      data: storeData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }



function getstorestatistics(orderData) {
    var deffered = $q.defer();
    //adppostData
    console.log(orderData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getshops",
      data: orderData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }


  //get all orders

  function getallorders(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallorders",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

function getallcities(cityData) {
    var deffered = $q.defer();
    //adppostData
    console.log(cityData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getallcity",
      data: cityData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
   //get citiesby country
   function  getcitiesByCountryId(countryId) {
    var deffered = $q.defer();
    //adppostData
    console.log(countryId);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getallcity",
      data: countryId
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  // function getcitiesByCountryId(countryId) {
  // console.log();
  //   var deffered = $q.defer();
  //   //adppostData
    
  //   $http({
  //     method: "GET",
  //     url: MAIN_CONSTANT.site_url + "/api/input/getallcity" ,
  //   }).then(function mySucces(response) {
  //       console.log(response);
  //       if (response.data.errorCode === 0) {
  //         deffered.resolve(response.data);
  //       } else {
  //         deffered.resolve(response.data);

  //        // deffered.reject(response.data);
  //       }
  //     },
  //     function myError(response) {
  //       console.log("Please check your internet connection");
  //     });
  //   return deffered.promise;

  // }





  //end test

// function getcities(Data) {
//     var deffered = $q.defer();
//     console.log(cities);
    
//     $http({
//       method: "POST",
//       url: MAIN_CONSTANT.site_url + "/api/input/getallcity",
//       data:cities
//     }).then(function mySucces(response) {

//         console.log(response);
//         if (response.data.errorCode === 0) {
//           deffered.resolve(response.data);
//         } else {
//           deffered.reject(response.data);
//         }
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deffered.promise;

//   }

//ACTIVE COUNTRIES
function getactivecountries() {
    var deffered = $q.defer();
    console.log();
      $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getallcountry?countryId=0&status=1",

    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  //active countries

function getactivecountries() {
    var deffered = $q.defer();
    console.log();
      $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getallcountry?countryId=0&status=1",

    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }





  function getcountries() {
    var deffered = $q.defer();
    console.log();
      $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getallcountry?countryId=0&status=2",

    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
 // function getstores(Data) {
 //    var deffered = $q.defer();
 //    console.log();
 //    $http({
 //      method: "POST",
 //      url: MAIN_CONSTANT.site_url + "/api/input/getshops",
 //       data:stores

 //    }).then(function mySucces(response) {
 //        console.log(response);
 //        if (response.data.errorCode === 0) {
 //          deffered.resolve(response.data);
 //        } else {
 //          deffered.reject(response.data);
 //        }
 //      },
 //      function myError(response) {
 //        console.log("Please check your internet connection");
 //      });
 //    return deffered.promise;

 //  }
   function submitAddCityDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/addcity",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;

  }
  function submitAddCountryDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/addcountry",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;

  }

  function submitAddCityDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/addcity",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;

  }


   function submitAddStoredetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
     // alert("hai");
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/addsellerdetails",
      data: Data
     
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

  function addrequesteditem(itemrequest) {
    console.log(itemrequest);
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(itemrequest);
    $http({
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/updaterequestitems?status=0 &requestId="+itemrequest.requestItemId,
      data: itemrequest
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;

  }


 
}