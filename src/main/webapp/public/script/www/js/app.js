  // angular.module is a global place for creating, registering and retrieving Angular modules
  // 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
  // the 2nd parameter is an array of 'requires'
  // 'starter.controllers' is found in controllers.js

  angular.module('starter', [

      'ui.router',
      'common.btnLoading',
      'common.loading',

      'starter.homeCtrl',

      'starter.authCtrl',
      'starter.categoryCtrl',
      'starter.subcategoryCtrl',
      'starter.productCtrl',
      'starter.customerCtrl',
      'starter.attributeCtrl',
      'starter.auctionCtrl',
      'starter.locationCtrl',
      'starter.sellerCtrl',
      'starter.reportCtrl',
      'starter.taxCtrl',
      'starter.reviewCtrl',
      'starter.orderCtrl',
      'starter.offerCtrl',
      'starter.addCtrl',
      'starter.revenueCtrl',
      'starter.PaymentCtrl',
      'ngMap',

      'starter.authService',
      'starter.inputService',
      'starter.auctionService',
      'starter.employeeService',
      'starter.payrollService',
      'starter.storeCtrl',
      'starter.promocodeCtrl',
      'starter.loadingService',


      'starter.storeService',
      'starter.sellerService',
      'ngStorage',
      'textAngular',
      'ngMapAutocomplete',

      'ui.bootstrap',
      'ngFileUpload',
      'starter.inputCtrl',
      /*  'angularFileUpload',*/
      'ngTable',
      'constant',
      'toaster',
      // 'htmlToPdfSave',
      'chart.js',
      // 'angular-search-and-select'



    ])

    .run(function($templateCache, $rootScope, $state, loadingService) {})
    .filter('unique', function() {
      return function(collection, keyname) {
        var output = [],
          keys = [];

        angular.forEach(collection, function(item) {
          var key = item[keyname];
          if (keys.indexOf(key) === -1) {
            keys.push(key);
            output.push(item);
          }
        });
        return output;
      };
    })


    .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider


        .state('login', {
          url: '/login',
          templateUrl: 'public/script/www/templates/auth/login.html',
          controller: 'authCtrl'
        })


        .state('app', {
          url: '/app',

          templateUrl: 'public/script/www/templates/auth/login.html',
          controller: 'authCtrl'
        })
        .state('admin', {
          url: '/admin',
          abstract: true,

          templateUrl: 'public/script/www/templates/common/sidebar.html',
          controller: 'homeCtrl'
        })

        .state('admin.dashboard', {
          url: '/dashboard',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/dashboard/dashboard.html',
              controller: 'homeCtrl'
            }
          }

        })
        .state('admin.cmsdata', {
          url: '/cmsdata',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/cms-data/cms-data.html',
              controller: 'categoryCtrl'
            }
          }


        })

        //  .state('admin.advertisement', {
        //   url: '/advertisement',
        //   views: {
        //     'menuContent': {
        //       templateUrl: 'public/script/www/templates/advertisement.html',
        //       controller: 'advertisementCtrl'
        //     }
        //   }

        // })
        // REPORTS
        .state('admin.userreports', {
          url: '/userreports',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/user-reports.html',
              controller: 'reportCtrl'
            }
          }

        })
        .state('admin.commission-reports', {
          url: '/commission-reports',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/commision-report.html',
              controller: 'reportCtrl'
            }
          }

        })
        .state('admin.income-reports', {
          url: '/income-reports',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/income-report.html',
              controller: 'reportCtrl'
            }
          }

        })
        .state('admin.expense-reports', {
          url: '/expense-reports',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/expense-report.html',
              controller: 'reportCtrl'
            }
          }

        })

        .state('admin.tax-report', {
          url: '/tax-report',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/tax-reports.html',
              controller: 'reportCtrl'
            }
          }

        })
        .state('admin.lowstockreport', {
          url: '/lowstockreport',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/low-stock-report.html',
              controller: 'reportCtrl'
            }
          }

        })
        .state('admin.orderreports', {
          url: '/orderreports',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/order-reports.html',
              controller: 'reportCtrl'
            }
          }

        })
        .state('admin.productreport', {
          url: '/productreport',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/product-report.html',
              controller: 'reportCtrl'
            }
          }

        })
        .state('admin.best-purchased-productreport', {
          url: '/best-purchased-productreport',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/best-purchased-product-report.html',
              controller: 'reportCtrl'
            }
          }

        })
        .state('admin.sales-report', {
          url: '/sales-report',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/sales-report.html',
              controller: 'reportCtrl'
            }
          }

        })
        .state('admin.transaction-report', {
          url: '/transaction-report',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/transaction-report.html',
              controller: 'reportCtrl'
            }
          }

        })

        // Manage Revenue
        .state('admin.managerevenue', {
          url: '/managerevenue',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/revenue/manage-revenue.html',
              controller: 'revenueCtrl'
            }
          }

        })

        .state('admin.promocode', {
          url: '/promocode',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/promocodemanagement.html',
              controller: 'promocodeCtrl'
            }
          }

        })
        .state('admin.addpromocode', {
          url: '/addpromocode/:codeId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/add-promocode.html',
              controller: 'promocodeCtrl'
            }
          }

        })
        .state('admin.viewpromocode', {
          url: '/viewpromocode/:codeId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/view-promocode.html',
              controller: 'promocodeCtrl'
            }
          }

        })
        .state('admin.viewordersbypromocode', {
          url: '/viewordersbypromocode/:codeId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/view-ordersby-promocode.html',
              controller: 'promocodeCtrl'
            }
          }

        })
        .state('admin.viewpromoter', {
          url: '/viewpromoter/:customerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/view-promoter.html',
              controller: 'promocodeCtrl'
            }
          }

        })
        .state('admin.savepromoter', {
          url: '/savepromoter/:customerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/save-promoter.html',
              controller: 'promocodeCtrl'
            }
          }

        })

        .state('admin.category', {
          url: '/category',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/categorymanagement.html',
              controller: 'categoryCtrl'
            }
          }

        })
        .state('admin.addcategory', {
          url: '/addcategory/:categoryId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/addcategory.html',
              controller: 'categoryCtrl'
            }
          }

        })
        .state('admin.viewcategory', {
          url: '/viewcategory/:categoryId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/viewcategory.html',
              controller: 'categoryCtrl'
            }
          }

        })

        .state('admin.deactivatedcategories', {
          url: '/deactivatedcategories',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/deactivatedcategories.html',
              controller: 'categoryCtrl'
            }
          }

        })

        .state('admin.subcategory', {
          url: '/subcategory',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/itemtype.html',
              controller: 'subcategoryCtrl'
            }
          }


        })

        .state('admin.addsubcategory', {
          url: '/addsubcategory/:subCategoryId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/additemtype.html',
              controller: 'subcategoryCtrl'
            }
          }


        })


        .state('admin.viewsubcategory', {
          url: '/viewsubcategory/:subCategoryId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/viewsubcategory.html',
              controller: 'subcategoryCtrl'
            }
          }

        })

        .state('admin.deactivatedsubcategories', {
          url: '/deactivatedsubcategories',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/deactivatedsubcategories.html',
              controller: 'subcategoryCtrl'
            }
          }

        })


        .state('admin.country', {
          url: '/country',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/country/countrymanagement.html',
              controller: 'locationCtrl'
            }
          }

        })
        .state('admin.addcountry', {
          url: '/addcountry/:countryId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/country/addcountry.html',
              controller: 'locationCtrl'
            }
          }

        })

        .state('admin.viewcountry', {
          url: '/viewcountry/:countryId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/country/viewcountry.html',
              controller: 'locationCtrl'
            }
          }

        })




        .state('admin.city', {
          url: '/city',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/city/citymanagement.html',
              controller: 'locationCtrl'
            }
          }

        })
        .state('admin.addcity', {
          url: '/addcity/:cityId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/city/addcity.html',
              controller: 'locationCtrl'
            }
          }

        })

        .state('admin.viewcity', {
          url: '/viewcity/:cityId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/city/viewcity.html',
              controller: 'locationCtrl'
            }
          }

        })
        .state('admin.myaccountsettings', {
          url: '/myaccountsettings',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/account-settings/account-settings.html',
              controller: 'categoryCtrl'
            }
          }

        })

        // attribute

        .state('admin.attribute', {
          url: '/attribute',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/attribute/attributes.html',
              controller: 'attributeCtrl'
            }
          }

        })
        .state('admin.deactivatedattribute', {
          url: '/deactivatedattribute',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/attribute/deactivatedattributes.html',
              controller: 'attributeCtrl'
            }
          }

        })

        .state('admin.addattribute', {
          url: '/addattribute/:attributeId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/attribute/addattribute.html',
              controller: 'attributeCtrl'
            }
          }

        })

        .state('admin.viewattribute', {
          url: '/viewattribute/:attributeId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/attribute/viewattribute.html',
              controller: 'attributeCtrl'
            }
          }

        })
        .state('admin.reviews', {
          url: '/reviews/:productId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reviews/reviewsandratings.html',
              controller: 'reviewCtrl'
            }
          }

        })

        //product
        .state('admin.stock', {
          url: '/stock',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/productstock.html',
              controller: 'productCtrl'
            }
          }

        })
        .state('admin.addstock', {
          url: '/addstock/:productId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/addstock.html',
              controller: 'productCtrl'
            }
          }

        })
        .state('admin.product', {
          url: '/product',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/itemmanagement.html',
              controller: 'productCtrl'
            }
          }

        })

        .state('admin.addproduct', {
          url: '/addproduct/:productId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/additem.html',
              controller: 'productCtrl'
            }
          }

        })
        .state('admin.assignofferonproduct', {
          url: '/assignofferonproduct/:productId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/assign-offer-on-product.html',
              controller: 'productCtrl'
            }
          }

        })

        .state('admin.viewproduct', {
          url: '/viewproduct/:productId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/viewitem.html',
              controller: 'productCtrl'
            }
          }

        })

        //SELLER
        .state('admin.seller', {
          url: '/seller',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/sellermanagement.html',
              controller: 'sellerCtrl'
            }
          }

        })



        .state('admin.addseller', {
          url: '/addseller/:sellerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/addseller.html',
              controller: 'sellerCtrl'
            }
          }

        })
        .state('admin.viewseller', {
          url: '/viewseller/:sellerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/viewseller.html',
              controller: 'sellerCtrl'
            }
          }

        })
        .state('admin.editseller', {
          url: '/editseller',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/editseller.html',
              controller: 'sellerCtrl'
            }
          }

        })

        .state('admin.sellerstatistics', {
          url: '/sellerstatistics',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/sellerstatistics.html',
              controller: 'sellerCtrl'
            }
          }

        })



        // CUSTOMER
        .state('admin.customer', {
          url: '/customer',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/customeraccounts.html',
              controller: 'customerCtrl'
            }
          }

        })
        .state('admin.viewcustomer', {
          url: '/viewcustomer/:customerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/viewcustomer.html',
              controller: 'customerCtrl'
            }
          }

        })
        .state('admin.addcustomer', {
          url: '/addcustomer/:customerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/addcustomer.html',
              controller: 'customerCtrl'
            }
          }

        })
        .state('admin.editcustomer', {
          url: '/editcustomer/:customerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/editcustomer.html',
              controller: 'customerCtrl'
            }
          }

        })

        .state('admin.customerstatistics', {
          url: '/customerstatistics',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/customerstatistics.html',
              controller: 'customerCtrl'
            }
          }

        })

        .state('admin.customerreview', {
          url: '/customerreview',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/customerreview.html',
              controller: 'customerCtrl'
            }
          }

        })


        .state('admin.tax', {
          url: '/tax',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/tax/taxmanagement.html',
              controller: 'taxCtrl'
            }
          }

        })
        .state('admin.addtax', {
          url: '/addtax/:taxId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/tax/addtax.html',
              controller: 'taxCtrl'
            }
          }

        })
        .state('admin.viewtax', {
          url: '/viewtax/:taxId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/tax/viewtax.html',
              controller: 'taxCtrl'
            }
          }

        })
        // OFFER
        .state('admin.offer', {
          url: '/offer',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/offer/offer.html',
              controller: 'offerCtrl'
            }
          }

        })

        .state('admin.addoffer', {
          url: '/addoffer/:offerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/offer/addoffer.html',
              controller: 'offerCtrl'
            }
          }

        })
        .state('admin.viewoffer', {
          url: '/viewoffer/:offerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/offer/viewoffer.html',
              controller: 'offerCtrl'
            }
          }

        })
        .state('admin.listofproducts', {
          url: '/listofproducts',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/offer/listofproducts.html',
              controller: 'offerCtrl'
            }
          }

        })
        .state('admin.homedata', {
          url: '/homedata',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/homedata/homedatamanagement.html',
              controller: 'addCtrl'
            }
          }

        })
        .state('admin.addslider', {
          url: '/addslider/:addId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/homedata/addslider.html',
              controller: 'addCtrl'
            }
          }

        })
        .state('admin.addbanner', {
          url: '/addbanner/:addId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/homedata/addbanner.html',
              controller: 'addCtrl'
            }
          }

        })
        // RETURNS
        .state('admin.returns', {
          url: '/returns',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/returns/return-order.html',
              controller: 'orderCtrl'
            }
          }

        })
        // .state('admin.viewreturn', {
        //         url: '/viewreturn/:orderId',
        //         views: {
        //           'menuContent': {
        //             templateUrl: 'public/script/www/templates/returns/view-return.html',
        //             controller: 'orderCtrl'
        //           }
        //         }

        //       })

        .state('admin.viewreturn', {
          url: '/viewreturn/:orderId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/returns/view-returnorder.html',
              controller: 'orderCtrl'
            }
          }

        })


        // MANUFACTURE
        .state('admin.manufacture', {
          url: '/manufacture',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/manufacture/manufacture.html',
              controller: 'categoryCtrl'
            }
          }

        })
        .state('admin.addmanufacture', {
          url: '/addmanufacture/:manufacturerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/manufacture/addmanufacture.html',
              controller: 'categoryCtrl'
            }
          }

        })
        .state('admin.viewmanufacture', {
          url: '/viewmanufacture/:manufacturerId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/manufacture/viewmanufacture.html',
              controller: 'categoryCtrl'
            }
          }

        })
        //ORDER
        .state('admin.order', {
          url: '/order',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/order.html',
              controller: 'orderCtrl'
            }
          }

        })
        .state('admin.neworder', {
          url: '/neworder',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/order/neworders.html',
              controller: 'orderCtrl'
            }
          }

        })
        // .state('seller.neworder', {
        //           url: '/neworder',
        //           views: {
        //             'menuContent': {
        //               templateUrl: 'public/script/www/templates/order/neworders.html',
        //               controller: 'orderCtrl'
        //             }
        //           }

        //         })

        .state('admin.vieworder', {
          url: '/vieworder/:orderId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/order/vieworder.html',
              controller: 'orderCtrl'
            }
          }

        })
        .state('admin.editorder', {
          url: '/editorder/:orderId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/order/editorder.html',
              controller: 'orderCtrl'
            }
          }

        })

        .state('admin.milestoneorder', {
          url: '/milestoneorder/:orderId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/milestoneoforder.html',
              controller: 'orderCtrl'
            }
          }

        })
        .state('admin.deliveredorders', {
          url: '/deliveredorders',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/deliveredorders.html',
              controller: 'orderCtrl'
            }
          }

        })
        .state('admin.invoice', {
          url: '/invoice/:orderId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/invoice.html',
              controller: 'orderCtrl'
            }
          }

        })


        .state('admin.itemacceptance', {
          url: '/itemacceptance/:requestItemId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/itemacceptance.html',
              controller: 'storeCtrl'
            }
          }
        })

        .state('admin.itemdeactivation', {
          url: '/itemdeactivation',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/itemdeactivation.html',
              controller: 'storeCtrl'
            }
          }

        })
        .state('admin.responsedstorerequests', {
          url: '/responsedstorerequests',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/responsedstoreitemrequests.html',
              controller: 'storeCtrl'
            }
          }

        })
        .state('admin.listofcustomerorder', {
          url: '/listofcustomerorder/:userId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/listofordersbycustomer.html',
              controller: 'customerCtrl'
            }
          }

        })
        .state('admin.customerorderdetails', {
          url: '/customerorderdetails/:orderId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customerorderdetails.html',
              controller: 'customerCtrl'
            }
          }

        })
        .state('admin.listofstoreorder', {
          url: '/listofstoreorder/:storeId',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/listofordersbystore.html',
              controller: 'storeCtrl'
            }
          }

        })

        .state('admin.addincomehead', {
          url: '/addincomehead',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/IncomeAndExpense/add-income-head.html',
              controller: 'PaymentCtrl'
            }
          }
        })
        .state('admin.addexpensehead', {
          url: '/addexpensehead',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/IncomeAndExpense/add-expense-head.html',
              controller: 'PaymentCtrl'
            }
          }
        })
        .state('admin.addincome', {
          url: '/addincome',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/IncomeAndExpense/add-income.html',
              controller: 'PaymentCtrl'
            }
          }
        })
        .state('admin.addexpense', {
          url: '/addexpense',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/IncomeAndExpense/add-expense.html',
              controller: 'PaymentCtrl'
            }
          }
        })


        .state('app.signup', {
          url: '/signup',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/auth/signup.html',
              controller: 'authCtrl'
            }
          }

        })


        .state('app.home', {
          url: '/home?mode',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/common/sidebar.html',
              controller: 'homeCtrl'
            }
          }

        })


      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/login');
    });
