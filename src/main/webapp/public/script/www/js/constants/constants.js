angular.module('constant', [])
.constant('TYPE_POST', { business: 'b',job:'j',sellbuy:'s' } )
.constant('MAIN_CONSTANT',{
	
	 site_url:"http://13.127.113.83:3250",
	// site_url:"http://localhost:3250",
	site_name:'imp',

})

.constant('FILE_CONSTANT',{
     // filepath:"https://s3.ap-south-1.amazonaws.com/projectdatas/leansouq/",
    // filepath:"http://13.127.113.83:3250/c:pentalimage/",
     filepath:"http://13.127.113.83:3250/appfiles/",
	  site_name:'imp',

})
.constant('ROLLE',{
	admin:'ADMIN',
	user:'USER'
})
.constant('URL_CONSTANT',{
	template_url:"public/script/www/templates/",
	site_name:'imp',
	js_url:'public/script/www/js/',
	direct_url:'public/script/www/js/direct/',

});
