<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title></title>
    <!-- un-comment this code to enable service worker
    <script>
      if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('service-worker.js')
          .then(() => console.log('service worker installed'))
          .catch(err => console.log('Error', err));
      }
      
    </script>-->
    <!-- <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <!--Import materialize.css-->
   <!--  <link rel="stylesheet" type="text/css" href="public/script/www/css/themecss/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="public/script/www/css/themecss/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="public/script/www/css/themecss/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="public/script/www/css/themecss/main.css">
    <link rel="stylesheet" type="text/css" href="public/script/www/css/themecss/style.css">
    <link rel="stylesheet" type="text/css" href="public/script/www/css/themecss/responsive.css">
    <link rel="stylesheet" type="text/css" href="public/script/www/lib/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="public/script/www/lib/ng-table/dist/ng-table.min.css">
    <link rel="stylesheet" href="public/script/www//lib/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="public/script/www/lib/angucomplete-alt/angucomplete-alt.css">
    <link rel="stylesheet" href="public/script/www/lib/ngToast/dist/ngToast.min.css"> -->
    
    <!-- <script type="text/javascript" src="public/script/www/lib/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="public/script/www/lib/bootstrap/dist/js/bootstrap.min.js"></script> -->
    <link rel="stylesheet" type="text/css" href="public/script/www/css/bootstrap.min.css">
<link rel="stylesheet" href="public/script/www/lib/ngToast/dist/ngToast.min.css">

    <link rel="stylesheet" type="text/css" href="public/script/www/css/demo.css">
    <link rel="stylesheet" type="text/css" href="public/script/www/css/material-dashboard.css?v=1.2.0.css">
    <link rel="stylesheet" type="text/css" href="public/script/www/css/style.css">
 <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet" class="ng-scope">
 <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons" rel="stylesheet" type="text/css" class="ng-scope">
<link rel="stylesheet" href="public/script/www/lib/fullcalendar/dist/fullcalendar.css"/>
    <script type="text/javascript" src="public/script/www/lib/jquery/dist/jquery.min.js"></script>
 <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>


<script type="text/javascript" src="public/script/www/lib/moment/min/moment.min.js"></script>

<script type="text/javascript" src="public/script/www/lib/fullcalendar/dist/fullcalendar.min.js"></script>


<script type="text/javascript" src="public/script/www/lib/angular/angular.min.js"></script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script> -->
   <script src="//unpkg.com/@uirouter/angularjs@1.0.7/release/angular-ui-router.min.js"></script>
  <script type="text/javascript" src="public/script/www/lib/ngstorage/ngStorage.min.js"></script>
<!-- jquery, moment, and angular have to get included before fullcalendar -->

<script type="text/javascript" src="public/script/www/lib/angular-ui-calendar/src/calendar.js"></script>
<script type="text/javascript" src="public/script/www/lib/fullcalendar/dist/gcal.js"></script>
<script src="public/script/www/lib/ngmap/build/scripts/ng-map.min.js"></script>
   <script src="public/script/www/lib/ng-file-upload-shim/ng-file-upload-shim.min.js"></script> <!-- for no html5 browsers support -->
<script src="public/script/www/lib/ng-file-upload-shim/ng-file-upload.min.js"></script>
<script src="public/script/www/lib/angular-animate/angular-animate.min.js"></script>
<script src="public/script/www/lib/angular-sanitize/angular-sanitize.min.js"></script>
<script src="public/script/www/lib/ngToast/dist/ngToast.min.js"></script>
<script src="/public/script/www/js/direct/loading/index.js"></script>
    <script src="public/script/www/js/app.js"></script>
    <script src="public/script/www/js/constants/constants.js"></script>
   
    <script src="public/script/www/js/controllers/authCtrl.js"></script>
    <script src="public/script/www/js/controllers/usersCtrl.js"></script>
    <script src="public/script/www/js/controllers/homepageCtrl.js"></script>
    <script src="public/script/www/js/controllers/inputCtrl.js"></script>
    <script src="public/script/www/js/controllers/reportCtrl.js"></script>
    <script src="public/script/www/js/controllers/headerCtrl.js"></script>
    <script src="public/script/www/js/controllers/homeadminCtrl.js"></script>

    <script src="public/script/www/js/services/authService.js"></script>
    <script src="public/script/www/js/services/inputService.js"></script>
    <script src="public/script/www/js/services/loadingService.js"></script>
    <script src="public/script/www/js/services/userService.js"></script>
    <script src="public/script/www/js/services/reportService.js"></script>
    <script src="public/script/www/js/services/homeadminService.js"></script>
    
   
</head>

<body ng-app="starter">
    <toast></toast>
    <ui-view></ui-view>
    <loading-spinner></loading-spinner>
</body>
<style type="text/css">
.cursor-pointer {
    cursor: pointer;
}
</style>

</html>