angular.module('starter.auctionService', [])
  .factory('auctionService', auctionService);

function auctionService(TYPE_POST, $q, $http, MAIN_CONSTANT,$localStorage) {
$http.defaults.headers.common['Bearer'] = $localStorage.token;
//alert( $localStorage.token);

  return {
   
    // getproducts:getproducts
    getallauctions:getallauctions,
    submitAddAuctionDetails:submitAddAuctionDetails,
    getallauctionsbysearch:getallauctionsbysearch,
    getallauctiondetails:getallauctiondetails
    // categorymanage:categorymanage

  };
//add auction


// add category


 function submitAddAuctionDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/addauction",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


// get all auctions by search
  function getallauctionsbysearch(auctionData) {
    var deffered = $q.defer();
    //adppostData
    console.log(auctionData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallauction",
      data: auctionData
    }).then(function mySucces(response) {
      
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }


  // get all auctions
  function getallauctions(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallauction",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  //get all auction details
  

   function getallauctiondetails(auctiondetailsData) {
    var deffered = $q.defer();
    //adppostData
    console.log(auctiondetailsData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallauctionby",
      data: auctiondetailsData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }


//  function getcustomers() {
//     var deffered = $q.defer();
//     //adppostData
    
//     $http({
//       method: "GET",
//       url: MAIN_CONSTANT.site_url + "/api/user/getcustomers?customerId=0" ,
//     }).then(function mySucces(response) {
//         console.log(response);
//         if (response.data.errorCode === 0) {
//           deffered.resolve(response.data);
//         } else {
//           deffered.reject(response.data);
//         }
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deffered.promise;

//   }


//  function getcategories() {
//     var deffered = $q.defer();
//     //adppostData
    
//     $http({
//       method: "GET",
//       url: MAIN_CONSTANT.site_url + "/api/input/getcategory?categoryId=0&mainCategoryId=0" ,
//     }).then(function mySucces(response) {
//         console.log(response);
//         if (response.data.errorCode === 0) {
//           deffered.resolve(response.data);
//         } else {
//           deffered.reject(response.data);
//         }
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deffered.promise;

//   }


//   // getSubcategories




//   // getproducts

// function getproducts(products) {
//     var deffered = $q.defer();
//     console.log(products);
//     //adppostData
    
//     $http({
//       method: "POST",
//       url: MAIN_CONSTANT.site_url + "/api/input/getproducts" ,
//       data:products
//     }).then(function mySucces(response) {
//         console.log(response);
//         if (response.data.errorCode === 0) {
//           deffered.resolve(response.data);
//         } else {
//           deffered.resolve(response.data);


//           // deffered.reject(response.data);
//         }
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deffered.promise;

//   }

  



// // add category


//  function submitAddCategoryDetails(Data) {
  
//   console.log( $localStorage.token);
//     var deferred = $q.defer();
//     //adppostData
//     console.log(Data);
//     $http({
//       method: "POST",
//       headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json',
//         'Bearer':$localStorage.token
//       },
//       url: MAIN_CONSTANT.site_url + "/api/input/savecategory",
//       data: Data
//     }).then(function(data) {
//         deferred.resolve(data);
//         // alert(data);
//         console.log(data);
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deferred.promise;


//   }


// // get subcategories



//  function getsubcategories() {
//   console.log();
//     var deffered = $q.defer();
//     //adppostData
    
//     $http({
//       method: "GET",
//       url: MAIN_CONSTANT.site_url + "/api/input/getsubcategory?mainCategoryId=0&categoryId=0" ,
//     }).then(function mySucces(response) {
//         console.log(response);
//         if (response.data.errorCode === 0) {
//           deffered.resolve(response.data);
//         } else {
//           deffered.resolve(response.data);

//          // deffered.reject(response.data);
//         }
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deffered.promise;

//   }

//   //get subcategory by category

//   function getsubcategoriesByCategoryId(catId) {
//   console.log();
//     var deffered = $q.defer();
//     //adppostData
    
//     $http({
//       method: "GET",
//       url: MAIN_CONSTANT.site_url + "/api/input/getsubcategory?mainCategoryId=0&categoryId="+ catId ,
//     }).then(function mySucces(response) {
//         console.log(response);
//         if (response.data.errorCode === 0) {
//           deffered.resolve(response.data);
//         } else {
//           deffered.resolve(response.data);

//          // deffered.reject(response.data);
//         }
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deffered.promise;

//   }

 


//   //add subcategory

//   // add category


//  function submitAddSubCategoryDetails(Data) {
  
//   console.log( $localStorage.token);
//     var deferred = $q.defer();
//     //adppostData
//     console.log(Data);
//      // alert("hai");
//     $http({
//       method: "POST",
//       headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json',
//         'Bearer':$localStorage.token
//       },
//       url: MAIN_CONSTANT.site_url + "/api/input/savesubcategory",
//       data: Data
     
//     }).then(function(data) {
//         deferred.resolve(data);
//         // alert(data);
//         console.log(data);
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deferred.promise;


//   }

//   // add product


//  function submitAddProductDetails(Data) {
  
//   console.log( $localStorage.token);
//     var deferred = $q.defer();
//     //adppostData
//     console.log(Data);
//     $http({
//       method: "POST",
//       headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json',
//         'Bearer':$localStorage.token
//       },
//       url: MAIN_CONSTANT.site_url + "/api/input/saveproduct",
//       data: Data
//     }).then(function(data) {
//         deferred.resolve(data);
//         // alert(data);
//         console.log(data);
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deferred.promise;


//   }




//   // edit category


// function submitEditCategoryDetails(Data) {
  
//   console.log( $localStorage.token);
//     var deferred = $q.defer();
//     //adppostData
//     console.log(Data);
//     $http({
//       method: "POST",
//       headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json',
//         'Bearer':$localStorage.token
//       },
//       url: MAIN_CONSTANT.site_url + "/api/input/savecategory",
//       data: Data
//     }).then(function(data) {
//         deferred.resolve(data);
//         // alert(data);
//         console.log(data);
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deferred.promise;


//   }

// // get all subcategories



}




