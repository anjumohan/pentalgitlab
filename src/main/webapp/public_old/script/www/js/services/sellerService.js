angular.module('starter.sellerService', [])
  .factory('sellerService', sellerService);

function sellerService(TYPE_POST, $q, $http, MAIN_CONSTANT,$localStorage) {
$http.defaults.headers.common['Bearer'] = $localStorage.token;
return {
    
     
      getcountries: getcountries,
      // getsellers:getsellers,
      getallcities:getallcities,
      submitAddsellerdetails:submitAddsellerdetails,
      getsellerstatistics:getsellerstatistics,
      getcitiesByCountryId:getcitiesByCountryId,
      getallitemrequests:getallitemrequests,
      addrequesteditem:addrequesteditem,
      getallordersbysellerId:getallordersbysellerId,
      getactivecountries:getactivecountries,

     

     getallsellers:getallsellers,
     submitAddCountryDetails:submitAddCountryDetails,
     getlistofordersbysellerId:getlistofordersbysellerId,
     getallsellerbysearch:getallsellerbysearch,
     getinvoicebyorderId:getinvoicebyorderId,
     getalltaxes:getalltaxes,

     submitAddCityDetails:submitAddCityDetails
  };
//


function  getinvoicebyorderId(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallorderdetailsby",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

//
function getactivecountries() {
    var deffered = $q.defer();
    console.log();
      $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getallcountry?countryId=0&status=1",

    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
// get all sellers by search
  function getallsellerbysearch(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getshops",
      data: Data
    }).then(function mySucces(response) {
      
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }


    // get order by seller

function  getlistofordersbysellerId(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallorders",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  // get order by seller

function getallordersbysellerId(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallorders",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
 
 

  function getallitemrequests() {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getallitemrequest?requestItemId=0&userId=0&countryId=0&cityId=0&status=2&sellerName=&currentIndex=0&rowPerPage=10" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

   function getalltaxes(taxData) {
    var deffered = $q.defer();
    //adppostData
    console.log(taxData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/order/getalltax",
      data: taxData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }


  function getallsellers(sellerData) {
    var deffered = $q.defer();
    //adppostData
    console.log(sellerData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/user/getallsellers",
      data: sellerData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }



function getsellerstatistics(orderData) {
    var deffered = $q.defer();
    //adppostData
    console.log(orderData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getshops",
      data: orderData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

function getallcities(cityData) {
    var deffered = $q.defer();
    //adppostData
    console.log(cityData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getallcity",
      data: cityData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
   //get citiesby country
   function  getcitiesByCountryId(countryId) {
    var deffered = $q.defer();
    //adppostData
    console.log(countryId);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getallcity",
      data: countryId
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  // function getcitiesByCountryId(countryId) {
  // console.log();
  //   var deffered = $q.defer();
  //   //adppostData
    
  //   $http({
  //     method: "GET",
  //     url: MAIN_CONSTANT.site_url + "/api/input/getallcity" ,
  //   }).then(function mySucces(response) {
  //       console.log(response);
  //       if (response.data.errorCode === 0) {
  //         deffered.resolve(response.data);
  //       } else {
  //         deffered.resolve(response.data);

  //        // deffered.reject(response.data);
  //       }
  //     },
  //     function myError(response) {
  //       console.log("Please check your internet connection");
  //     });
  //   return deffered.promise;

  // }





  //end test

// function getcities(Data) {
//     var deffered = $q.defer();
//     console.log(cities);
    
//     $http({
//       method: "POST",
//       url: MAIN_CONSTANT.site_url + "/api/input/getallcity",
//       data:cities
//     }).then(function mySucces(response) {

//         console.log(response);
//         if (response.data.errorCode === 0) {
//           deffered.resolve(response.data);
//         } else {
//           deffered.reject(response.data);
//         }
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deffered.promise;

//   }

  function getcountries() {
    var deffered = $q.defer();
    console.log();
      $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getallcountry?countryId=0&status=2",

    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
 // function getsellers(Data) {
 //    var deffered = $q.defer();
 //    console.log();
 //    $http({
 //      method: "POST",
 //      url: MAIN_CONSTANT.site_url + "/api/input/getshops",
 //       data:sellers

 //    }).then(function mySucces(response) {
 //        console.log(response);
 //        if (response.data.errorCode === 0) {
 //          deffered.resolve(response.data);
 //        } else {
 //          deffered.reject(response.data);
 //        }
 //      },
 //      function myError(response) {
 //        console.log("Please check your internet connection");
 //      });
 //    return deffered.promise;

 //  }
   function submitAddCityDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/addcity",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;

  }
  function submitAddCountryDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/addcountry",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;

  }

  function submitAddCityDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/addcity",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;

  }


   function submitAddsellerdetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
     // alert("hai");
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/addsellerdetails",
      data: Data
     
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

  function addrequesteditem(itemrequest) {
    console.log(itemrequest);
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(itemrequest);
    $http({
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/updaterequestitems?status=0 &requestId="+itemrequest.requestItemId,
      data: itemrequest
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;

  }


 
}