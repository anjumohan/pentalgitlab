angular.module('starter.locationCtrl', [])
  .controller('locationCtrl', function($scope, $window, $state, inputService,toaster,storeService, FILE_CONSTANT,toaster, MAIN_CONSTANT, Upload, storeService, $stateParams, $localStorage,loadingService) {

if (!$localStorage.token||$localStorage.token===0) {
    $window.location.href ='#/login';
    }
    $scope.imageUrl = FILE_CONSTANT.filepath;

     // $scope.pop = function(){
     //      toaster.pop('info', "title", "text");
     //  };


//get country

 if ($state.current.name === 'admin.country') {
  if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

// get country


$scope.getcountry = function(country) {
  loadingService.isLoadin=false;
      storeService.getcountries(country).then(function(response) {
        loadingService.isLoadin=false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.countries = response.object;
        }else { 
        loadingService.isLoadin=false;
      


        }
      },function(){
        loadingService.isLoadin=false;


      });
    };
$scope.getcountry();



  }


 // get cities

    if ($state.current.name === 'admin.city') {

      $scope.cityPageVo = {
        "countryId": 0,
        "cityId": 0,
        "rowPerPage": 100,
        "currentIndex": 0,
        "cityName": "",
        "citusStatus": 2,
        "status": []
      }
      

      $scope.getallcity = function() {
         loadingService.isLoadin=true;

        storeService.getallcities($scope.cityPageVo).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.cities = response.object;

          } else {
            alert(response.errorMessage);
          }
        });
      }
      $scope.getallcity();

    }
    $scope.ViewCountry = function(country) {
       loadingService.isLoadin=true;
      console.log(country);
      $window.location.href = '#/admin/viewcountry/' + country.countryId;


      if (country.countryId) {
        // alert(country.countryId);

        $localStorage.country = country;
        //


      }
    };

    //view country

    if ($state.current.name === 'admin.viewcountry') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
       loadingService.isLoadin=false;
      $scope.country = {};
      // $scope. city.imageName ="nothing";
      if ($stateParams.countryId !== '0') {
        $scope.country = $localStorage.country;
      }


    }


    $scope.ViewCity = function(city) {
       loadingService.isLoadin=true;

      console.log(city);
      $window.location.href = '#/admin/viewcity/' + city.cityId;


      if (city.cityId) {
        // alert(city.cityId);

        $localStorage.city = city;
        //


      }
    };


    //view city

    if ($state.current.name === 'admin.viewcity') {
       loadingService.isLoadin=false;
      $scope.city = {};
      // $scope. city.imageName ="nothing";
      if ($stateParams.cityId !== '0') {
        $scope.city = $localStorage.city;
      }


    }

   

    //counrty manage
    $scope.countrymanage = function(country) {
       loadingService.isLoadin=true;
      console.log(country);
      $window.location.href = '#/admin/addcountry/' + country.countryId;


      if (country.countryId) {
        // alert(country.countryId);

        $localStorage.country = country;
        //


      }
    };


    $scope.citymanage = function(city) {
       loadingService.isLoadin=true;
      console.log(city);
      $window.location.href = '#/admin/addcity/' + city.cityId;


      if (city.cityId) {
        // alert(city.cityId);

        $localStorage.city = city;
        //


      }
    };




    //add city

    if ($state.current.name === 'admin.addcity') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
    //get active country
    $scope.getactivecountry = function(country) {
  loadingService.isLoadin=false;
      sellerService.getactivecountries(country).then(function(response) {
        loadingService.isLoadin=false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.countries = response.object;
        }else { 
        loadingService.isLoadin=false;
      


        }
      },function(){
        loadingService.isLoadin=false;


      });
    };
$scope.getactivecountry();
      $scope.city = {};

      // $scope. country.countryFlag ='1';
      if ($stateParams.cityId !== '0') {
        $scope.city = $localStorage.city;
      }

      $scope.addCity = function(getAddCityDetails) {
         loadingService.isLoadin=true;

        console.log(getAddCityDetails);



        storeService.submitAddCityDetails(getAddCityDetails).then(function(response) {
           loadingService.isLoadin=false;
           
          console.log(response);


          if (response.data.errorCode === 0) {
             if ($stateParams.cityId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added new City!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  City!");

                             }

            $window.location.href = '#/admin/city';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
             toaster.pop('error', "Oops..!", response.data.errorMessage);
          }

        });



      }


    }

    // add country


    if ($state.current.name === 'admin.addcountry') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.country = {};

      // $scope.country.countryFlag = 'https://www.iconfinder.com/icons/22899/flag_icon';
      $scope.upload = function(file) {
      //$scope.isFileuploadStart=true;



      if (file) {
        

        $scope.clickimg = true;


        console.log(file);
        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/uploadtoaws",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {

          console.log(resp);

          //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
          if (resp.data.errorCode === 0) {
            $scope.country.countryFlag = resp.data.object;

            console.log($scope.country.countryFlag);

           
              // if ($scope.country.countryFlag) {
              //   console.log(resp);
              //   $scope.country.countryFlag = resp.data.object;
              //   $scope.country.countryFlag.isLoadin = false;

              // }
          


            // if (!$scope.country.countryFlag) {
            //   $scope.country.countryFlag = "";
            // }
          } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';

          } else {

          }





        }, function(resp) {
          $scope.progressPercentage = 0;
          // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
            if ($scope.country.countryFlag) {
              //splice i
              $scope.country.countryFlag;

            }
         

          console.log('Error status: ' + resp.status);


        }, function(evt) {
          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
            $scope.filecmplt = false;
          } else if ($scope.progressPercentage == 100) {
            $scope.filecmplt = true;

          } else {
            $scope.filenotUplaod = true;
          }
        });
      }
    };


      // $scope. country.countryFlag ='1';
      if ($stateParams.countryId !== '0') {
        $scope.country = $localStorage.country;
      }


$scope.deleteimage = function(){
          $scope.country.countryFlag="";
          
          }
     
      $scope.addCountry = function(getAddCountryDetails) {
         loadingService.isLoadin=true;

        console.log(getAddCountryDetails);

        storeService.submitAddCountryDetails(getAddCountryDetails).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);


          if (response.data.errorCode === 0) {
             if ($stateParams.countryId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added New Country!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  Country!");

                             }

            $window.location.href = '#/admin/country';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
             toaster.pop('error', "Oops..!", response.data.errorMessage);
          }

        });

      }


    }
    // add city


    if ($state.current.name === 'admin.addcity') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.city = {};

      // $scope. country.countryFlag ='1';
      if ($stateParams.cityId !== '0') {
        $scope.city = $localStorage.city;
      }

      $scope.addCity = function(getAddCityDetails) {
         loadingService.isLoadin=true;

        console.log(getAddCityDetails);

        storeService.submitAddCityDetails(getAddCityDetails).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);

          if (response.data.errorCode === 0) {
             if ($stateParams.cityId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added new City!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  Cit!");

                             }

            $window.location.href = '#/admin/city';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
             toaster.pop('error', "Oops..!", response.data.errorMessage);
          }

        });


      }


    }


    $scope.changeCountry = function(countryId) {
      console.log(countryId);


      $scope.getcityByCountryId(countryId)

    };
    $scope.getcityByCountryId = function(countryId) {

      $scope.cityPageVo = {
        "countryId": countryId,
        "cityId": 0,
        "rowPerPage": 100,
        "currentIndex": 0,
        "cityName": "",
        "status": []
      }
      $scope.citiesbycountries=[];

      storeService.getcitiesByCountryId($scope.cityPageVo, countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
    // $scope.getcityByCountryId();







   $scope.changeCountry=function(countryId){
  console.log(countryId);

    
     $scope.getcityByCountryId(countryId)

   };
$scope.getcityByCountryId = function(countryId) {

  $scope.cityPageVo=
                {
                "countryId":countryId,
                "cityId":0,
                "rowPerPage":100,
                "currentIndex":0,
                "cityName":"",
                "status":[]
              }
               $scope.citiesbycountries = [];

      storeService.getcitiesByCountryId($scope.cityPageVo,countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
 // $scope.getcityByCountryId();

 


  });
