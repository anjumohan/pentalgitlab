angular.module('starter.customerCtrl', [])
  .controller('customerCtrl', function($scope, $window, $state,toaster, inputService, storeService,  FILE_CONSTANT,MAIN_CONSTANT, Upload,$stateParams, $localStorage) {
    //alert($localStorage.token);

    if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
     $scope.imageUrl = FILE_CONSTANT.filepath;


    $scope.customer = {};

      // $scope.country.countryFlag = 'https://www.iconfinder.com/icons/22899/flag_icon';
      $scope.upload = function(file) {
      //$scope.isFileuploadStart=true;



      if (file) {
        

        $scope.clickimg = true;


        console.log(file);
        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/uploadtoaws",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {

          console.log(resp);

          //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
          if (resp.data.errorCode === 0) {
            $scope.customer.profileImage = resp.data.object;

            console.log($scope.customer.profileImage);

           
              // if ($scope.country.countryFlag) {
              //   console.log(resp);
              //   $scope.country.countryFlag = resp.data.object;
              //   $scope.country.countryFlag.isLoadin = false;

              // }
          


            // if (!$scope.country.countryFlag) {
            //   $scope.country.countryFlag = "";
            // }
          } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';

          } else {

          }





        }, function(resp) {
          $scope.progressPercentage = 0;
          // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
            if ($scope.customer.profileImage) {
              //splice i
              $scope.customer.profileImage;

            }
         

          console.log('Error status: ' + resp.status);


        }, function(evt) {
          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
            $scope.filecmplt = false;
          } else if ($scope.progressPercentage == 100) {
            $scope.filecmplt = true;

          } else {
            $scope.filenotUplaod = true;
          }
        });
      }
    };
    $scope.deleteimage = function(){
          $scope.customer.profileImage="";
          
          }

if ($state.current.name === 'admin.customer') {


             $scope.customerPageVo = {"customerId":0,
             "cityId":0,
             "countryId":0,
             "userId":0,
             "orderBy":0,
             "fromDate":0,
             "toDate":0,
             "currentIndex":0,
             "rowPerPage":100,
             "status":2}

// alert(33333333);
      $scope.getallcustomers = function() {
        loadingService.isLoadin=true;

        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin=false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.customers = response.object;
            console.log($scope.customers);



          } else {
            alert(response.errorMessage);
          }
        },function(){
          alert('Oops! somthing went wrong');
        });
      };
      $scope.getallcustomers();

    }

    // CUSTOMER SEARCH


$scope.customerSearch= function(search){
  loadingService.isLoadin=true;

  $scope.customers=[];


  console.log(search);
  if (search.fromDate) {
  var fromDate = new Date(search.fromDate).getTime();
}else{
  var fromDate =0;

}
  console.log($scope.fromDate);
    if (search.toDate) {
  var toDate = new Date(search.toDate).getTime();
}else{
  var toDate =0;

}
if (search.countryId) {
  var countryId = search.countryId;
}else{
  var countryId =0;

}
if (search.cityId) {
  var cityId = search.cityId;
}else{
  var cityId =0;

}
if (search.phoneNo) {
  var phoneNo = search.phoneNo;
}else{
  var phoneNo =0;

}
if (search.emailid) {
  var emailid = search.emailid;
}else{
  var emailid =0;

}

  console.log(search);
   $scope.customerPageVo = {
                   "currentIndex": 0,
                   "rowPerPage": 100, 
                   "userId": 0,
                    "customerId": 0, 
                    "fromDate": fromDate,
                     "toDate":toDate, 
                     "countryId": countryId, 
                     "cityId":cityId,
                      "orderBy": 0 
                    }

                                                   // alert("hiii");

                inputService.getallcustomersbysearch($scope.customerPageVo).then(function(response) {
                  loadingService.isLoadin=false;
                    console.log(response);

                    // if (response.errorCode === -3){
                    //  $scope.testdata= true;
                    //     $scope.auctions=[];
                    //    // alert()


                    // }else{
                    //   $scope.testdata= false;
                    // }

                    if (response.errorCode === 0) {
                       // $scope.shopList = $scope.shopList.concat(response.object.Product);

                         $scope.customers = response.object;

                    } else {
                        $scope.customers=[];
                        alert(response.errorMessage);
                    }
                },function(response){

                    $scope.customers=[];
                   



                });
            } ;


            
    $scope.viewcustomer = function(customer) {
      // alert("01");
      loadingService.isLoadin = true;

      console.log(customer);
      $window.location.href = '#/admin/viewcustomer/' + customer.customerId;


      if (customer.customerId) {
        // alert(city.cityId);

        $localStorage.customer = customer;
        //


      }
    };
    //view customer

    if ($state.current.name === 'admin.viewcustomer') {

      loadingService.isLoadin = false;
      $scope.customer = {};
      // $scope. city.imageName ="nothing";
      if ($stateParams.customerId !== '0') {
        $scope.customer = $localStorage.customer;
      }


    }


    



    $scope.customermanage = function(customer) {

      console.log(customer);
      $window.location.href = '#/admin/editcustomer/' + customer.customerId;


      if (customer.customerId) {
        // alert(customer.customerId);

        $localStorage.customer = customer;



      }
    };

    if ($state.current.name === 'admin.editcustomer') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.customer = {};
      $scope.customer.password = "password123";
      // $scope. category.imageName ="nothing";


      if ($stateParams.customerId !== '0') {
        $scope.customer = $localStorage.customer;

      }

     $scope.addCustomer = function(getAddCustomerDetails) {
        
        // getAddCustomerDetails.password
        $scope.customer.password = "password123";
        console.log($scope.customer.password);
        loadingService.isLoadin = true;

        console.log(getAddCustomerDetails);



        inputService.submitAddCustomerDetails(getAddCustomerDetails).then(function(response) {
          loadingService.isLoadin = false;
          
          console.log(response);

          if (response.data.errorCode === 0) {
             toaster.pop('success', "Successfull..!", "Updated the Customer Details!");

            // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
            // console.log($scope.allProducts);
            $window.location.href = '#/admin/customer';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
             toaster.pop('error', "Oops..!", response.data.errorMessage);
          }

        });


        /*/fd*/
      }



    }


    // add customer


    if ($state.current.name === 'admin.addcustomer') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.customer = {};
      $scope.customer.password = "password123";
      // $scope. category.imageName ="nothing";


      if ($stateParams.customerId !== '0') {
        $scope.customer = $localStorage.customer;
         

      }

     //loadingService.isLoadin = true;

      $scope.addCustomer = function(getAddCustomerDetails) {
        
        // getAddCustomerDetails.password
        $scope.customer.password = "password123";
        console.log($scope.customer.password);
        loadingService.isLoadin = true;

        console.log(getAddCustomerDetails);



        inputService.submitAddCustomerDetails(getAddCustomerDetails).then(function(response) {
          loadingService.isLoadin = false;
         
          console.log(response);

          if (response.data.errorCode === 0) {
             toaster.pop('success', "Successfull..!", "Updated the Customer Details!");

            // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
            // console.log($scope.allProducts);
            $window.location.href = '#/admin/customer';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
             toaster.pop('error', "Oops..!", response.data.errorMessage);
          }

        });


        /*/fd*/
      }


    }

    // get customer reviews
    if ($state.current.name === 'admin.customerreview') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

      $scope.getcustomerreviews = function(customerreviews) {
        loadingService.isLoadin = true;
        $scope.reviewPageVo = {
          "productId": 0,
          "productOrShop": 0,
          "currentIndex": 0,
          "rowPerPage": 100
        }


        inputService.getcustomerreviews($scope.reviewPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.customerreviews = response.object.REVIEWS;
            console.log($scope.customerreviews);
          }
        });
      };
      $scope.getcustomerreviews();
    }
    // get country


    $scope.getcountry = function(country) {
      loadingService.isLoadin = true;
      storeService.getcountries(country).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.countries = response.object;
        }
      });
    };
    $scope.getcountry();

    //function for country change
    $scope.changeCountry = function(countryId) {
      console.log(countryId);


      $scope.getcityByCountryId(countryId)

    };
    $scope.getcityByCountryId = function(countryId) {

      $scope.cityPageVo = {
        "countryId": countryId,
        "cityId": 0,
        "rowPerPage": 100,
        "currentIndex": 0,
         "citusStatus":1,
        "cityName": "",
        "status": []
      }



$scope.citiesbycountries =[];
      storeService.getcitiesByCountryId($scope.cityPageVo, countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
    $scope.getcityByCountryId();

    //view customer order details

    // vieworderdetails

    $scope.vieworderdetails = function(order) {
      // alert("01");
      

      console.log(order);
      $window.location.href = '#/admin/customerorderdetails/' + order.orderId;
       


      if (order.orderId) {
        // alert(city.cityId);

        $localStorage.order = order;
        //


      }
    };

    if ($state.current.name === 'admin.customerorderdetails') {
      loadingService.isLoadin = true;
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.order = {};
      // $scope.customer.password ="password123";
      // $scope. category.imageName ="nothing";


      if ($stateParams.orderId !== '0') {
        $scope.order = $localStorage.order;


      }
      $scope.Id = $scope.order.orderId;


      // console.log($stateParams.userId);
      // $scope.Id = $stateParams.userId;
      // console.log($scope.Id);
      $scope.getorderdetailsbyorderId = function(Id) {
        console.log(Id);
        $scope.orderPageVo = {
          "rowPerPage": 100,
          "currentIndex": 0,
          "customerId": 0,
          "fromDate": 0,
          "toDate": 0,
          "orderId": $scope.Id,
          "status": [2,4,6],
          "storeId": 0

        }
        console.log($scope.orderPageVo);
        inputService.getorderdetailsbyorderId($scope.orderPageVo).then(function(response) {
           loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            // $scope.orderdetailsbyorderId = response.object;
            $scope.orderdetails = response.object[0].orderDetailsVo[0];
            // console.log($scope.ordersbyuserId);
            console.log( $scope.orderdetails);
          }
        },function(){
           loadingService.isLoadin = false;


        }
        );
      };
      $scope.getorderdetailsbyorderId();


    }


  });
