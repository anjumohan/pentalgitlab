package com.hidile.pental.dao.order;

import java.util.List;
import java.util.Map;

import com.hidile.pental.entity.input.City;
import com.hidile.pental.entity.input.Country;
import com.hidile.pental.entity.input.ReturnItems;
import com.hidile.pental.entity.order.CardDetails;
import com.hidile.pental.entity.order.Order;
import com.hidile.pental.entity.order.OrderDetails;
import com.hidile.pental.entity.order.ShippingDetails;
import com.hidile.pental.entity.order.TaxManagement;
import com.hidile.pental.entity.order.Transactions;
import com.hidile.pental.model.BasicResponse;
public interface OrderDao
{
	//public Map<String,Object> addOrder(Map<Long, List<OrderDetails>> orMap,Map<Long, Order> orders,Map<String, Object> tranMaps,Map<String, Object> onlineMap);

	//public  Map<String,Object> saveOrUpdateAuction(Auction auction,List<FeaturesAndPhotosAuction> featuresAndPhotos);
	public  Map<String,Object> saveOrUpdateTax(TaxManagement tax);
	public  Map<String,Object> getTax(long countryId,long cityId,long taxId,int rowPerPage,int currentIndex,int stat);
	public  Map<String,Object>  getAllShippingDetails(long customerId,long shippingId,int rowPerPage,int currentIndex,int stat);
	public Map<String,Object>    getCardDetails(long cardId,long customerId,int currentIndex,int rowPerPage);
	//public Map<String,Object>   saveOrUpdateCart(CartAndFavourites cartAndFavourites);
	//public Map<String,Object> saveAuctionDetails(AuctionDetails acDetails);
	public Map<String,Object> deleteTax(long taxId); 
	public Map<String,Object> deleteShipping(long shippingId);
	public Map<String,Object> deleteCard(long cardId);
	public Map<String, Object> saveProduct(Order order, List<OrderDetails> orderDetails);
	public  Map<String,Object> saveOrUpdateCity(City city);
	public  Map<String,Object> saveOrUpdateCountry(Country country);
	public  Map<String,Object> addShippingDetails(ShippingDetails shippingDetails);
	public  Map<String,Object> addCardDetails(CardDetails cardDetails);
	//public Map<String, Object> getPromo(String promoCode, int status);
//	public Map<String,Object> deleteAuction(long auctionId,long auctionDetailsId); 
	//public Map<String,Object> changeStatusOfOrder(int status,long orderId,long userId,List<Long> orList);
	//public Map<String,Object> addShippingDetails(int status,long orderId,long userId,List<Long> orList) throws Exception;
//	public Map<String,Object> changeStatusOfAuction(int status,long bookingId,long userId);
	public  Map<String,Object> addReturnProduct(List<ReturnItems> returnItems) throws Exception;
	 public Map<String,Object> getShippingByOrder(long orderId);
	 public Map<String, Object> changeStatusOfTransaction(long orderId);
	 public Map<String,Object> getAllReturnProducts(long returnItemId,long orderListId,int rowPerPage,int currentIndex,long storeId,int stat);
	 public Map<String, Object> changeStatusOfOrder(int status, long orderId, long userId, List<Long> orList,/*Map<String, Object> tranMaps*/Transactions transactions,long entryId)throws Exception ;
}
