package com.hidile.pental.dao.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.modelmapper.ModelMapper;
import org.omg.PortableInterceptor.SUCCESSFUL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.hidile.pental.constants.Constants;
import com.hidile.pental.entity.input.City;
import com.hidile.pental.entity.input.Country;
import com.hidile.pental.entity.input.ReturnItems;
import com.hidile.pental.entity.input.Review;
import com.hidile.pental.entity.order.CardDetails;
import com.hidile.pental.entity.order.Order;
import com.hidile.pental.entity.order.OrderDetails;
import com.hidile.pental.entity.order.ShippingDetails;
import com.hidile.pental.entity.order.TaxManagement;
import com.hidile.pental.entity.order.Transactions;
import com.hidile.pental.entity.user.CustomerDetails;
import com.hidile.pental.entity.user.LoginDetails;
import com.hidile.pental.entity.user.NotificationDetails;
import com.hidile.pental.entity.user.PromocodeDetails;
import com.hidile.pental.utils.TimeUtils;

@EnableTransactionManagement
@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
@Repository("orderDao")
public class OrderDaoImpl implements OrderDao, Constants {
	private static Logger logger = Logger.getLogger(OrderDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HibernateTemplate hibernateTemplate;

	@Autowired
	private ModelMapper modelMapper;

	private Session openDBsession() {
		Session session;
		session = sessionFactory.getCurrentSession();
		return session;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> getPromo(String promoCode, int status) {
	 * long totalCounts = 0; Map<String, Object> map = null; List<Country> data =
	 * null; try { Session session = openDBsession(); Criteria criteria =
	 * session.createCriteria(PromocodeDetails.class); if (countryId > 0) {
	 * criteria.add(Restrictions.eq("countryId", countryId)); } String countArry =
	 * ""; if (status == 0 || status == 1) { criteria.add(Restrictions.eq("status",
	 * status)); countArry = " where o.status=" + status; } data = criteria.list();
	 * session.flush(); StringBuffer strBfr = new
	 * StringBuffer("select count(*) from PromocodeDetails o" + countArry); Query
	 * qry = session.createQuery(strBfr.toString()); totalCounts = (long)
	 * qry.uniqueResult();
	 * 
	 * // closeDBSession(); } catch (HibernateException e) { // closeDBSession(); if
	 * (TESTING) { e.printStackTrace(); } logger.info(e.getMessage()); map = new
	 * HashMap<>(); map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE, e.getMessage());
	 * throw e; } if (data == null || data.isEmpty()) { map = new HashMap<>();
	 * map.put(ERROR_CODE, -3); map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR); return map; } else
	 * { map = new HashMap<>(); map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
	 * map.put("CountryList", data); map.put("TotalCounts", totalCounts); return
	 * map; } }
	 */
	/*
	 * @Override public Map<String, Object> addOrder(Map<Long,List<OrderDetails>>
	 * orMap, Map<Long, Order> orders,Map<String, Object> tranMaps,Map<String,
	 * Object> onlineMap){ Map<String, Object> map = new HashMap<String, Object>();
	 * try { logger.info(tranMaps+
	 * "tranMaps------------------------------------------------------------------")
	 * ; logger.info(onlineMap+
	 * "onlineMap------------------------------------------------------------------"
	 * );
	 * 
	 * Session session = openDBsession(); Transactions saletran = (Transactions)
	 * tranMaps.get("ORDER_TRAN"); logger.info(saletran+
	 * "saletran**************************************************************************"
	 * ); logger.info("ANJU MOHAN C K----------------------------------"+saletran.
	 * getTransactionId()); session.save(saletran); session.flush();
	 * 
	 * Transactions paymetTran = (Transactions) onlineMap.get("PAYMENT_TRAN");
	 * logger.info(paymetTran+
	 * "paymetTran**************************************************************************"
	 * ); logger.info("ANJU MOHAN C K----------------------------------"+paymetTran.
	 * getTransactionId());
	 * 
	 * paymetTran.setPairedId(saletran.getTransactionId());
	 * paymetTran.setToWhoId(saletran.getToWhoId()); session.save(paymetTran);
	 * session.flush();
	 * 
	 * 
	 * 
	 * 
	 * 
	 * Transactions payTran = (Transactions) tranMaps.get("ORDER_TRAN");
	 * logger.info("transaction id=="+saletran.getTransactionId());
	 * payTran.setPairedId(saletran.getTransactionId());
	 * payTran.setBankOrCash(TRAN_BANK); payTran.setCmd(1); payTran.setCompanyId(0);
	 * payTran.setInOrOut(CASH_IN); payTran.setDiscount(0);
	 * payTran.setTransactionMode(TRAN_MODE_DEBIT_ONLINE_PAY);
	 * payTran.setOperatingOfficer(saletran.getOperatingOfficer());
	 * payTran.setToWhoId(saletran.getToWhoId());
	 * payTran.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); payTran.setReciptNo("1");
	 * payTran.setNote("sale of item"); payTran.setIsCleared(NOT_CLEAR_TRAN);
	 * payTran.setStatus(ACTIVE); session.save(payTran); session.flush(); for
	 * (Entry<Long, Order> entry : orders.entrySet()) { Order order =
	 * entry.getValue(); order.setOrderDetails(null);
	 * order.setTransactionId(saletran.getTransactionId()); session.flush();
	 * 
	 * session.save(order); //-------------------------added
	 * 
	 * 
	 * String
	 * q7="update Transactions p set p.pairedId =:pairedId where p.transactionId =:transactionId "
	 * ; Query q4= session.createQuery(q7);
	 * 
	 * q4.setParameter("pairedId",order.getOrderId());
	 * q4.setParameter("transactionId",saletran.getTransactionId());
	 * logger.info(order.getOrderId()+
	 * "order.getOrderId()------------------------------------");
	 * logger.info(saletran.getTransactionId()+
	 * "saletran.getTransactionId()------------------------------------"); long
	 * r2=q4.executeUpdate(); session.flush(); //----------------
	 * 
	 * 
	 * List<OrderDetails> orDetails = orMap.get(entry.getValue().getSellerId()); for
	 * (OrderDetails or : orDetails) { or.setOrderId(order.getOrderId());
	 * or.setOrder(null); logger.info(or.getItemQuantity()
	 * +"or item qty^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"); long
	 * item=or.getItemQuantity();
	 * 
	 * 
	 * String
	 * q2="select p.productQuantity from  Product p  where p.productId =:productId "
	 * ; Query q1= session.createQuery(q2); logger.info(or.getProductId()+
	 * "or.getProductId()____________________________________________________________"
	 * ); q1.setParameter("productId", or.getProductId()); long original=(long)
	 * q1.uniqueResult(); logger.info(original+
	 * "productQty____________________________________________________________");
	 * long remain=original-item; String
	 * q="update Product p set p.numberOfItemsRemaining =:numberOfItemsRemaining where p.productId =:productId "
	 * ; Query q3= session.createQuery(q);
	 * 
	 * q3.setParameter("numberOfItemsRemaining",remain);
	 * q3.setParameter("productId", or.getProductId()); long r=q3.executeUpdate();
	 * session.save(or); session.flush(); } }
	 * 
	 * 
	 * } catch (HibernateException e) { e.printStackTrace(); logger.debug(e); throw
	 * e; } map = new HashMap<>(); map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
	 * map.put(ERROR_MESSAGE, MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC,
	 * "نجاح"); map.put("order", orders); return map; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getShippingByOrder(long orderId) {
		long shippingId = 0;
		long totalCounts = 0;
		Map<String, Object> map = null;
		List<ShippingDetails> taxList = null;
		List<Country> data = null;
		boolean flag = false;
		try {
			Session session = openDBsession();
			logger.info(orderId + "))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))");
			if (orderId == 0) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "orderId is may not be  0  ");
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}

			Criteria criteria = session.createCriteria(ShippingDetails.class);

			DetachedCriteria detachedCriteria2 = DetachedCriteria.forClass(Order.class);
			detachedCriteria2.add(Restrictions.eq("orderId", orderId));
			List<Order> dvList = (List<Order>) hibernateTemplate.findByCriteria(detachedCriteria2);
			if (dvList == null || dvList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "orderId " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}

			StringBuffer strBfr1 = new StringBuffer("select o.shippingId from Order o where o.orderId=" + orderId);

			Query qry = session.createQuery(strBfr1.toString());
			shippingId = (long) qry.uniqueResult();
			logger.info(shippingId + "_____________________________________________________________________");

			StringBuffer strBfr = new StringBuffer("select count(*) from ShippingDetails s");
			if (shippingId > 0) {
				flag = true;
				strBfr.append("  where s.shippingId=" + shippingId);
				criteria.add(Restrictions.eq("shippingId", shippingId));
			}

			Query qry1 = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));

			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(ShippingDetails.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}

			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data == null || data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("OrderList", data);
			map.put("TotalCounts", totalCounts);
			return map;
		}
	}

	@Override
	public Map<String, Object> saveOrUpdateTax(TaxManagement tax) {
		Map<String, Object> map = null;
		List<TaxManagement> taxList = null;
		try {
			Session session = openDBsession();
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TaxManagement.class);
			detachedCriteria.add(Restrictions.eq("taxId", tax.getTaxId()));
			taxList = (List<TaxManagement>) hibernateTemplate.findByCriteria(detachedCriteria);
			map = new HashMap<>();

			if (taxList == null || taxList.isEmpty()) {
				session.save(tax);
				session.flush();
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, "نجاح");
				return map;
			}

			else if (tax.getTaxId() > 0) {
				/*
				 * if (tax.getTaxName().equals(taxList.get(0).getTaxName())) {
				 * map.put(ERROR_CODE, -3); map.put(ERROR_MESSAGE, "DUBLICATE DATA");
				 * map.put(ERROR_MESSAGE_ARABIC, "بيانات متكررة"); return map; }
				 */
				detachedCriteria = DetachedCriteria.forClass(TaxManagement.class);
				detachedCriteria.add(Restrictions.eq("taxId", tax.getTaxId()));
				taxList = (List<TaxManagement>) hibernateTemplate.findByCriteria(detachedCriteria);
				if (taxList == null || taxList.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such tax");
					map.put(ERROR_MESSAGE_ARABIC, "لا يوجد بلد من هذا القبيل");
					return map;

				} else {

					session.merge(tax);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "نجاح");
					return map;

				}
			} else {
				map.put(ERROR_CODE, -3);
				map.put(ERROR_MESSAGE, "DUBLICATE DATA");
				map.put(ERROR_MESSAGE_ARABIC, "بيانات متكررة");
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			tax = null;
			throw e;
		}

		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, "نجاح");
		map.put("tax", tax);
		return map;

	}

	@Override
	public Map<String, Object> getTax(long countryId, long cityId, long taxId, int rowPerPage, int currentIndex,
			int stat) {
		Map<String, Object> map = null;
		List<TaxManagement> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(TaxManagement.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from TaxManagement o");
			if (countryId > 0) {
				flag = true;
				strBfr.append(" where o.countryId=" + countryId);
				criteria.add(Restrictions.eq("countryId", countryId));
			}

			if (cityId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.cityId=" + cityId);
				criteria.add(Restrictions.eq("cityId", cityId));
			}

			if (taxId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.taxId=" + taxId);
				criteria.add(Restrictions.eq("taxId", taxId));
			}

			if (stat == 1 || stat == 0) {
				if (flag == false) {
					strBfr.append(" where o.status=" + stat);
				} else {
					strBfr.append(" and o.status=" + stat);
				}
				criteria.add(Restrictions.eq("status", stat));
			}

			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(currentIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(TaxManagement.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			/*
			 * if (orderId > 0) { criteria = session.createCriteria(Review.class);
			 * criteria.add(Restrictions.eq("orderId", orderId));
			 * //criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
			 * criteria.list();
			 * 
			 * session.flush(); }
			 */
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			// logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {

			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");

			map.put("TaxList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@Override
	public Map<String, Object> getAllShippingDetails(long customerId, long shippingId, int rowPerPage, int currentIndex,
			int stat) {
		Map<String, Object> map = null;
		List<ShippingDetails> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(ShippingDetails.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from ShippingDetails o ");
			if (shippingId > 0) {
				flag = true;
				strBfr.append("  where o.shippingId=" + shippingId);
				criteria.add(Restrictions.eq("shippingId", shippingId));
			}
			if (customerId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.customerId=" + customerId);
				criteria.add(Restrictions.eq("customerId", customerId));
			}

			if (stat == 1 || stat == 0) {
				if (flag == false) {
					strBfr.append(" where o.status=" + stat);
				} else {
					strBfr.append(" and o.status=" + stat);
				}
				criteria.add(Restrictions.eq("status", stat));
			}

			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(currentIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(ShippingDetails.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			/*
			 * if (orderId > 0) { criteria = session.createCriteria(Review.class);
			 * criteria.add(Restrictions.eq("orderId", orderId));
			 * //criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
			 * criteria.list();
			 * 
			 * session.flush(); }
			 */
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {

			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");

			map.put("ShippingList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@Override
	public Map<String, Object> getCardDetails(long cardId, long customerId, int currentIndex, int rowPerPage) {
		Map<String, Object> map = null;
		List<CardDetails> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(CardDetails.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from CardDetails o ");
			if (cardId > 0) {
				flag = true;
				strBfr.append("  where o.cardId=" + cardId);
				criteria.add(Restrictions.eq("cardId", cardId));
			}
			if (customerId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.customerId=" + customerId);
				criteria.add(Restrictions.eq("customerId", customerId));
			}
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(currentIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(CardDetails.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			/*
			 * if (orderId > 0) { criteria = session.createCriteria(Review.class);
			 * criteria.add(Restrictions.eq("orderId", orderId));
			 * //criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
			 * criteria.list();
			 * 
			 * session.flush(); }
			 */
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {

			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");

			map.put("CardList", data);
			// map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	/*
	 * @Override public Map<String, Object> saveOrUpdateAuction(Auction auction,
	 * List<FeaturesAndPhotosAuction> featuresAndPhotos) {
	 * 
	 * Map<String, Object> map = null; try { Session session = openDBsession();
	 * 
	 * if (auction.getAuctionId() <= 0) { auction.setAuctionDetails(null);
	 * session.save(auction); session.flush(); } else { DetachedCriteria
	 * detachedCriteria = DetachedCriteria.forClass(Auction.class);
	 * detachedCriteria.add(Restrictions.eq("auctionId", auction.getAuctionId()));
	 * List<Auction> auctionList = (List<Auction>)
	 * hibernateTemplate.findByCriteria(detachedCriteria); if (auctionList == null
	 * || auctionList.isEmpty()) { map = new HashMap<>(); map.put(ERROR_CODE, -4);
	 * map.put(ERROR_MESSAGE, "No such auction"); map.put(ERROR_MESSAGE_ARABIC,
	 * "Ù„Ø§ ÙŠÙˆØ¬Ø¯ Ù…Ø«Ù„ Ù‡Ø°Ø§ Ø§Ù„Ù…Ø²Ø§Ø¯"); return map; } else { Query qry =
	 * session
	 * .createQuery("delete from FeaturesAndPhotosAuction fp where fp.auctionId=:prodId"
	 * ); qry.setParameter("prodId", auction.getAuctionId()); int result =
	 * qry.executeUpdate(); if (result < 0) { map = new HashMap<>();
	 * map.put(ERROR_CODE, -4); map.put(ERROR_MESSAGE, "Features updation failed");
	 * map.put(ERROR_MESSAGE_ARABIC,
	 * "Ã™ï¿½Ã˜Â´Ã™â€ž Ã˜ÂªÃ˜Â­Ã˜Â¯Ã™Å Ã˜Â« Ã˜Â§Ã™â€žÃ™â€¦Ã™Å Ã˜Â²Ã˜Â§Ã˜Âª"); return
	 * map; } session.flush(); auction.setAuctionDetails(null);
	 * session.merge(auction); session.flush(); } } for (FeaturesAndPhotosAuction
	 * fePhotos : featuresAndPhotos) {
	 * fePhotos.setAuctionId(auction.getAuctionId()); fePhotos.setAuction(null);
	 * session.save(fePhotos); session.flush(); } map = new HashMap<>();
	 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­"); return map; }
	 * catch (HibernateException e) { e.printStackTrace(); logger.debug(e); throw e;
	 * } }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveOrUpdateCountry(Country country)

	{

		Map<String, Object> map = null;
		List<Country> countryList = null;
		try {
			Session session = openDBsession();
			DetachedCriteria detachedCriteria = null;
			if (country.getCountryId() <= 0) {
				detachedCriteria = DetachedCriteria.forClass(Country.class);
				detachedCriteria.add(Restrictions.eq("countryName", country.getCountryName()));
				countryList = (List<Country>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();

				if (countryList == null || countryList.isEmpty()) {
					session.save(country);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ã™â€ Ã˜Â¬Ã˜Â§Ã˜Â­ Ã™Ë†Ã˜Â¸Ã˜Â§Ã˜Â¦Ã™ï¿½");
					return map;
				} else {
					map.put(ERROR_CODE, -3);
					map.put(ERROR_MESSAGE, "DUBLICATE DATA");
					map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â¨Ã™Å Ã˜Â§Ã™â€ Ã˜Â§Ã˜Âª Ã™â€¦Ã˜ÂªÃ™Æ’Ã˜Â±Ã˜Â±Ã˜Â©");
					return map;
				}
			} else {
				detachedCriteria = DetachedCriteria.forClass(Country.class);
				detachedCriteria.add(Restrictions.eq("countryId", country.getCountryId()));
				countryList = (List<Country>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();
				if (countryList == null || countryList.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such country");
					map.put(ERROR_MESSAGE_ARABIC,
							"Ã™â€žÃ˜Â§ Ã™Å Ã™Ë†Ã˜Â¬Ã˜Â¯ Ã˜Â¨Ã™â€žÃ˜Â¯ Ã™â€¦Ã™â€  Ã™â€¡Ã˜Â°Ã˜Â§ Ã˜Â§Ã™â€žÃ™â€šÃ˜Â¨Ã™Å Ã™â€ž");
					return map;
				} else {
					session.merge(country);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ã™â€ Ã˜Â¬Ã˜Â§Ã˜Â­ Ã™Ë†Ã˜Â¸Ã˜Â§Ã˜Â¦Ã™ï¿½");
					map.put("country", country);
					return map;
				}
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			country = null;
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveOrUpdateCity(City city) {

		Map<String, Object> map = null;
		List<City> cityList = null;
		try {
			Session session = openDBsession();
			if (city.getCityId() <= 0) {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(City.class);
				detachedCriteria.add(Restrictions.eq("cityName", city.getCityName()));
				cityList = (List<City>) hibernateTemplate.findByCriteria(detachedCriteria);

				map = new HashMap<>();
				if (cityList == null || cityList.isEmpty()) {
					logger.info(city.getCountryId() + "++++++++++++++++++++++++++++++++");
					city.setCountry(null);
					session.save(city);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				} else {
					map.put(ERROR_CODE, -3);
					map.put(ERROR_MESSAGE, "DUBLICATE DATA");
					map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â¨Ã™Å Ã˜Â§Ã™â€ Ã˜Â§Ã˜Âª Ã™â€¦Ã˜ÂªÃ™Æ’Ã˜Â±Ã˜Â±Ã˜Â©");
				}
			} else {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(City.class);
				detachedCriteria.add(Restrictions.eq("cityId", city.getCityId()));
				cityList = (List<City>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();
				if (cityList == null || cityList.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such city");
					map.put(ERROR_MESSAGE_ARABIC,
							"Ã™â€žÃ˜Â§ Ã™Å Ã™Ë†Ã˜Â¬Ã˜Â¯ Ã˜Â¨Ã™â€žÃ˜Â¯ Ã™â€¦Ã™â€  Ã™â€¡Ã˜Â°Ã˜Â§ Ã˜Â§Ã™â€žÃ™â€šÃ˜Â¨Ã™Å Ã™â€ž");
				} else {
					city.setCountry(null);
					session.merge(city);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ã™â€ Ã˜Â¬Ã˜Â§Ã˜Â­ Ã™Ë†Ã˜Â¸Ã˜Â§Ã˜Â¦Ã™ï¿½");
					map.put("city", city);
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			city = null;
			throw e;
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveProduct(Order order, List<OrderDetails> orderDetails) {
		Map<String, Object> map = null;
		List<Order> servicesList = null;
		try {
			Session session = openDBsession();
			if (order.getOrderId() > 0) {
				Query qry = session.createQuery("delete from OrderDetails fp where fp.orderId=:orderId");
				qry.setParameter("orderId", order.getOrderId());
				int result = qry.executeUpdate();
				session.flush();
				session.update(order);
				session.flush();
			} else {
				session.save(order);
				session.flush();
			}
			for (OrderDetails fePhotos : orderDetails) {
				fePhotos.setOrderId(order.getOrderId());
				session.save(fePhotos);
				session.flush();
			}
			map = new HashMap<String, Object>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			/*
			 * Criteria criteria = session.createCriteria(Product.class);
			 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); servicesList =
			 * criteria.list(); session.flush(); map.put("ServiceList", servicesList);
			 */
			// closeDBSession();
		} catch (Exception e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> changeStatusOfTransaction(long orderId) {
		Map<String, Object> map = null;
		Transactions transactions = new Transactions();
		List<Transactions> orderList = null;
		try {
			Session session = openDBsession();
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Transactions.class);
			detachedCriteria.add(Restrictions.eq("pairedId", orderId));

			orderList = (List<Transactions>) hibernateTemplate.findByCriteria(detachedCriteria);
			if (orderList == null || orderList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "OrderId " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ø·Ù„Ø¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			} else {
				Query qry = session.createQuery(
						"update  Transactions set status=1,lastUpdatedTime=:lastUpdatedTime where pairedId=:orderId");
				qry.setParameter("orderId", orderId);
				qry.setParameter("lastUpdatedTime", TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
				int result = qry.executeUpdate();
				session.flush();

			}

			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, "SUCCESSFUL");
			map.put(ERROR_MESSAGE_ARABIC, "Ø·Ù„Ø¨");
			return map;
		} catch (Exception e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> changeStatusOfOrder(int status, long orderId, long userId, List<Long> orList,
			/* Map<String, Object> tranMaps */Transactions transactions, long entryId) throws Exception {
		Map<String, Object> map = null;
		// Map<String, Object> tranMap = new HashMap<String,Object>();
		double amt = 0;
		double shipping = 0;
		double instalation = 0;
		long itemQty = 0;
		List<Transactions> tranList = null;
		// Transactions saletran = (Transactions) tranMaps.get("ORDER_TRAN");

		List<Object[]> rows;
		try {
			Session session = openDBsession();
			if (orderId > 0) {
				StringBuffer stBuffer = new StringBuffer("from Order where");
				switch (status) {
				case DELIVERED:
					stBuffer.append(" orderstatus=" + READY_TO_DELIVERY);
					break;
				case READY_TO_DELIVERY:
					stBuffer.append(" orderstatus=" + CONFIRMED);
					break;
				case PICKED:
					stBuffer.append(" orderstatus=" + READY_TO_DELIVERY);
					break;
				case CANCELED:
					stBuffer.append(" orderstatus=" + ORDERED);
					break;
				case REJECTED:
					stBuffer.append(" orderstatus=" + ORDERED);
					break;
				case CONFIRMED:
					stBuffer.append(" orderstatus=" + ORDERED);
					break;
				default:
					break;
				}
				stBuffer.append(" and orderId=:odId");
				Query query = session.createQuery(stBuffer.toString());
				query.setParameter("odId", orderId);
				Order order = (Order) query.uniqueResult();
				if (order == null) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, "Change to this status not allowed");
					map.put(ERROR_MESSAGE_ARABIC, "Ø·Ù„Ø¨" + MESSAGE_NOT_FOUND_AR);
					return map;
				}
			}
			List<Order> orderList = null;
			if (orderId > 0) {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Order.class);
				detachedCriteria.add(Restrictions.eq("orderId", orderId));
				detachedCriteria.add(Restrictions.eq("status", ACTIVE));
				orderList = (List<Order>) hibernateTemplate.findByCriteria(detachedCriteria);// this contain
				if (orderList == null || orderList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, "Order " + MESSAGE_NOT_FOUND);
					map.put(ERROR_MESSAGE_ARABIC, "Ø·Ù„Ø¨" + MESSAGE_NOT_FOUND_AR);
					return map;
				}
			} else {
				Query qry = session.createQuery("select orderId from OrderDetails where orderListId=" + orList.get(0));
				long ordId = (long) qry.uniqueResult();
				session.flush();
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Order.class);
				detachedCriteria.add(Restrictions.eq("orderId", ordId));
				detachedCriteria.add(Restrictions.eq("status", ACTIVE));
				orderList = (List<Order>) hibernateTemplate.findByCriteria(detachedCriteria);// this contain
				if (orderList == null || orderList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, "Order " + MESSAGE_NOT_FOUND);
					map.put(ERROR_MESSAGE_ARABIC, "Ø·Ù„Ø¨" + MESSAGE_NOT_FOUND_AR);
					return map;
				}
			}
			StringBuffer sBuffer = new StringBuffer("from LoginDetails lg where lg.userId=? and ");

			if (status == CONFIRMED || status == DELIVERED || status == REJECTED || status == READY_TO_DELIVERY) {
				sBuffer.append("(role=" + ROLL_ID_SELLER + " or role=" + ROLL_ID_ADMIN + ")");
			} else if (status == CANCELED || status == ORDERED) 
			{
				sBuffer.append("role=" + ROLL_ID_CUSTOMER);
			}
			Query query = session.createQuery(sBuffer.toString());
			query.setParameter(0, userId);
			List<LoginDetails> logins = query.list();
			if (logins == null || logins.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -25);
				map.put(ERROR_MESSAGE, MESSAGE_NO_SUCH_USER);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_SUCH_USER_AR);
				return map;
			}
			String strQuery = null;
			long currentTime = TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);
			if (status == DELIVERED) {
				strQuery = "update from Order o set o.orderstatus=:STAT,o.operatingOfficerId=:USERID,o.deliveryDate="
						+ currentTime + " where o.orderId=:ID";
				Query qry = session.createQuery(strQuery);
				qry.setParameter("STAT", status);
				qry.setParameter("ID", orderId);
				qry.setParameter("USERID", userId);
				int result = qry.executeUpdate();
				if (result <= 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
					map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
					return map;
				}
				if (orderId > 0) {
					strQuery = "update from OrderDetails o set o.status=:STAT,o.deliveryDate=" + currentTime
							+ " where o.orderId=:ID";
					qry = session.createQuery(strQuery);
					qry.setParameter("STAT", status);
					qry.setParameter("ID", orderId);
					result = qry.executeUpdate();
					if (result <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -6);
						map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
						map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
						return map;
					}

				}

				else {
					strQuery = "update from OrderDetails o set o.status=:STAT,o.deliveryDate=" + currentTime
							+ " where o.orderListId in (:ID)";
					qry = session.createQuery(strQuery);
					qry.setParameter("STAT", status);
					qry.setParameterList("ID", orList);
					result = qry.executeUpdate();
					if (result <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -6);
						map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
						map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
						return map;
					}
				}
				if (orderList.get(0).getCod() == 2) {

					strQuery = "select transactionId  from Order o  where o.orderId=:ID";
					qry = session.createQuery(strQuery);
					qry.setParameter("ID", orderId);
					long transactionId = (long) qry.uniqueResult();
					
					
					//added
					
					String q31111 = " select count(*) from  Transactions where status=1";
                    Query q4211 = session.createQuery(q31111);
                    long count = (long) q4211.uniqueResult();
                    String receptNum=null;

                    if (count > 0)

                    {

                        String q3111 = " select MAX(lastUpdatedTime) from  Transactions where status=1 and transactionMode=20";
                        Query q421 = session.createQuery(q3111);
                        long lastUpdatedTimemax1 = (long) q421.uniqueResult();

                        String q3112 = " select t.reciptNo from  Transactions t where t.lastUpdatedTime =:lastUpdatedTime and t.status=1 and t.transactionMode=20 ";
                        Query q411 = session.createQuery(q3112);
                        q411.setParameter("lastUpdatedTime", lastUpdatedTimemax1);
                        String reciptNo1 = (String) q411.uniqueResult();
                       // logger.info(reciptNo1 + "reciptNo1");
                        StringBuilder sb = new StringBuilder(reciptNo1);

                        sb.delete(0, 3);
                        String reciptNo2 = sb.toString();
                       // logger.info(reciptNo2 + "reciptNo2__________________reciptNo2_________________reciptNo2");
                        long rec = Long.parseLong(reciptNo2);

                        String recp = Long.toString(rec + 1);

                        receptNum="PNT" + recp;
                    } else {
                    	 receptNum="PNT1";
                    }		
					
					
					
					
					
					
					
					
//added
					strQuery = "update from Transactions o set o.status=:STAT,o.reciptNo=:reciptNo,o.lastUpdatedTime=:time,o.bankOrCash=:mode where o.transactionId=:ID";
					qry = session.createQuery(strQuery);
					qry.setParameter("STAT", ACTIVE);
					qry.setParameter("ID", transactionId);
					qry.setParameter("reciptNo", receptNum);
					qry.setParameter("time", TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
					qry.setParameter("mode", TRAN_CASH);
					result = qry.executeUpdate();
					
					strQuery = "update from Transactions o set o.status=:STAT,o.reciptNo=:reciptNo,o.lastUpdatedTime=:time where o.pairedId=:ID and transactionMode=:mode";
					qry = session.createQuery(strQuery);
					qry.setParameter("STAT", ACTIVE);
					qry.setParameter("ID", transactionId);
					qry.setParameter("reciptNo", receptNum);
					qry.setParameter("time", TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
					qry.setParameter("mode", TRAN_MODE_COMMISSION_PAY);
					result = qry.executeUpdate();

					transactions.setBankOrCash(TRAN_CASH);
					transactions.setTransactionMode(TRAN_MODE_DEBIT_CASH_PAY);
					transactions.setCmd(1);
					transactions.setOperatingOfficer(userId);
					transactions.setToWhoId(orderList.get(0).getCustomerId());
					transactions.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));

					

					transactions.setReciptNo(receptNum);
					transactions.setNote("CASH PAY");
					transactions.setNetAmount(orderList.get(0).getTransaction().getNetAmount());
					transactions.setInOrOut(CASH_IN);
					transactions.setIsCleared(NOT_CLEAR_TRAN);
					transactions.setStatus(ACTIVE);
					transactions.setPairedId(transactionId);
					session.save(transactions);
					session.flush();
					map = new HashMap<>();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCCESS);
					map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
					return map;
				}

				map = new HashMap<>();
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
				return map;
			}

			else if (status == CANCELED || status == REJECTED) 
			{
				// if all cancel reject is selected go to this control
				double refundAmt = orderList.get(0).getNetPrice();
				if (orderId > 0 || orderList.size() == orList.size()) {
					strQuery = "update from Order o set o.orderstatus=:STAT,o.operatingOfficerId=:USERID,o.netPrice=0 where o.orderId=:ID";
					Query qry = session.createQuery(strQuery);
					qry.setParameter("STAT", status);
					qry.setParameter("ID", orderId);
					qry.setParameter("USERID", userId);
					int result = qry.executeUpdate();
					if (result <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -1);
						map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
						map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
						return map;
					}
					strQuery = "select  o.transactionId from  Order o where o.orderId=:ID";
					qry = session.createQuery(strQuery);

					qry.setParameter("ID", orderId);
					long tranId = (long) qry.uniqueResult();

					strQuery = "update  Transactions  o set o.status=:STAT where o.transactionId=:ID";
					qry = session.createQuery(strQuery);
					qry.setParameter("STAT", INACTIVE);
					qry.setParameter("ID", tranId);
					result = qry.executeUpdate();

					
					strQuery = "update from OrderDetails o set o.status=:STAT,o.operatingOfficerId=:USERID,o.cancellationDate="
							+ currentTime + " where o.orderId=:ID)";
					qry = session.createQuery(strQuery);
					qry.setParameter("STAT", status);
					qry.setParameter("ID", orderId);
					qry.setParameter("USERID", userId);
					result = qry.executeUpdate();
					if (result <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -6);
						map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
						map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
						return map;
					}
				} else {
					// if some of the items are cancelled or rejected
					if (orList != null && !orList.isEmpty()) {
						StringBuffer stBuffer = new StringBuffer("from OrderDetails where");
						stBuffer.append(" status=1 and orderListId in (:odId)");
						query = session.createQuery(stBuffer.toString());
						query.setParameterList("odId", orList);
						List<OrderDetails> orDetails = query.list();
						if (orDetails.size() != orList.size()) {
							map = new HashMap<>();
							map.put(ERROR_CODE, -1);
							map.put(ERROR_MESSAGE, "Change to this status not allowed");
							map.put(ERROR_MESSAGE_ARABIC, "Ø·Ù„Ø¨" + MESSAGE_NOT_FOUND_AR);
							return map;
						}
						StringBuffer detQuery = new StringBuffer(
								"update from OrderDetails od set od.status=:STAT,od.cancellationDate=" + currentTime
										+ " where od.orderListId in (:odids)");
						Query qry = session.createQuery(detQuery.toString());
						qry.setParameter("STAT", status);
						qry.setParameterList("odids", orList);
						int result = qry.executeUpdate();
						if (result <= 0) {
							throw new Exception();
						} else {
							Order orders = orDetails.get(0).getOrder();
							/*
							 * detQuery = new
							 * StringBuffer("select sum(od.itemNetPrice) from OrderDetails od where od.orderListId in (:odids)"
							 * ); qry = session.createQuery(detQuery.toString());
							 * qry.setParameterList("odids", orList); amt = (double) qry.uniqueResult();
							 * logger.info(amt);
							 */
							session.flush();
							detQuery = new StringBuffer(
									"select count(od) from OrderDetails od where od.status=1 and od.orderId="
											+ orders.getOrderId());
							qry = session.createQuery(detQuery.toString());
							long items = (long) qry.uniqueResult();
							logger.info(items);
							session.flush();

							detQuery = new StringBuffer(
									"select od.shippingCharge from OrderDetails od where od.orderListId in (:odids)");
							qry = session.createQuery(detQuery.toString());
							qry.setParameterList("odids", orList);
							shipping = (double) qry.uniqueResult();

							detQuery = new StringBuffer(
									"select od.installationCharge from OrderDetails od where od.orderListId in (:odids)");
							qry = session.createQuery(detQuery.toString());
							qry.setParameterList("odids", orList);
							instalation = (double) qry.uniqueResult();

							detQuery = new StringBuffer(
									"select od.itemQuantity from OrderDetails od where od.orderListId in (:odids)");
							qry = session.createQuery(detQuery.toString());
							qry.setParameterList("odids", orList);
							itemQty = (long) qry.uniqueResult();

							double totalShipping = shipping * itemQty;
							double totalInstallation = instalation * itemQty;

							detQuery = new StringBuffer(
									"select sum(od.itemNetPrice) from OrderDetails od where od.orderListId in (:odids)");
							qry = session.createQuery(detQuery.toString());
							qry.setParameterList("odids", orList);
							amt = (double) qry.uniqueResult() + totalShipping + totalInstallation;
							logger.info(amt);

							if (items == 0) {
								orders.setOrderstatus(status);
							}
							if (amt == 0.0) {
								orders.setOrderstatus(status);
							} else {
								amt = orders.getNetPrice() - amt;
								orders.setNetPrice(amt);
							}
							session.merge(orders);
							session.flush();
							refundAmt = amt;
						}
					} else {
						map = new HashMap<>();
						map.put(ERROR_CODE, -8);
						map.put(ERROR_MESSAGE, "Orderlistids is empty or null");
						map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
						return map;
					}
				}
				Criteria criteria = session.createCriteria(OrderDetails.class);
				if (orderId > 0) 
				{
					criteria.add(Restrictions.eq("orderId", orderId));
				} else {
					criteria.add(Restrictions.in("orderListId", orList));
				}
				//logger.info(orList.size());
				logger.info(orderId);
				List<OrderDetails> orDetails = criteria.list();
				logger.info(orDetails.size()+"**************&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*************************************");
				
				if(orderId >0)
				{
					String q = "select o.productId from OrderDetails o where o.orderId=:orderId";
					Query qry1 = session.createQuery(q);
					qry1.setParameter("orderId", orderId);
					long prod = (long) qry1.uniqueResult();

					

					Query qry2 = session.createQuery(
							"update Product p set p.numberOfItemsRemaining=p.numberOfItemsRemaining+:ite where p.productId=:odId");
					qry2.setParameter("ite", orDetails.get(0).getItemQuantity());
					qry2.setParameter("odId", prod);
					int outcome = qry2.executeUpdate();
					logger.info(outcome);
					logger.info(outcome+ "outcome-----------------------------------------------------------------------------");
					if (outcome <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -4);
						map.put(ERROR_MESSAGE, "product update failed");
						map.put(ERROR_MESSAGE_ARABIC, "نجاح");
						return map;
					}
				}
				
				else{
				
				
				for (OrderDetails or : orDetails) 
				{
					String q = "select o.productId from OrderDetails o where o.orderListId=:orderListId";
					Query qry1 = session.createQuery(q);
					qry1.setParameter("orderListId", or.getOrderListId());
					long prod = (long) qry1.uniqueResult();

					logger.info(prod + "prod_________________________________________________________"+or.getItemQuantity());

					Query qry2 = session.createQuery(
							"update Product p set p.numberOfItemsRemaining=p.numberOfItemsRemaining+:ite where p.productId=:odId");
					qry2.setParameter("ite", or.getItemQuantity());
					qry2.setParameter("odId", prod);
					int outcome = qry2.executeUpdate();
					logger.info(outcome);
					//logger.info(outcome+ "outcome-----------------------------------------------------------------------------");
					if (outcome <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -4);
						map.put(ERROR_MESSAGE, "product update failed");
						map.put(ERROR_MESSAGE_ARABIC, "نجاح");
						return map;
					}
				}
			}
				if (orderList.get(0).getOnline() == 1) {
					transactions.setBankOrCash(TRAN_CASH);
					transactions.setTransactionMode(TRAN_MODE_CREDIT_CANCEL_PAY);
					transactions.setCmd(1);
					transactions.setOperatingOfficer(userId);
					transactions.setToWhoId(userId);
					transactions.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
					String q31111 = " select count(*) from  Transactions";
					Query q4211 = session.createQuery(q31111);
					long count = (long) q4211.uniqueResult();

					if (count > 0)

					{

						String q3111 = " select MAX(lastUpdatedTime) from  Transactions where status=1 and transactionMode=20";
						Query q421 = session.createQuery(q3111);
						long lastUpdatedTimemax1 = (long) q421.uniqueResult();

						String q3112 = " select t.reciptNo from  Transactions t where t.lastUpdatedTime =:lastUpdatedTime and  t.status=1 and t.transactionMode=20";
						Query q411 = session.createQuery(q3112);
						q411.setParameter("lastUpdatedTime", lastUpdatedTimemax1);
						String reciptNo1 = (String) q411.uniqueResult();
						logger.info(reciptNo1 + "reciptNo1");
						StringBuilder sb = new StringBuilder(reciptNo1);

						sb.delete(0, 3);
						String reciptNo2 = sb.toString();
						logger.info(reciptNo2 + "reciptNo2__________________reciptNo2_________________reciptNo2");
						long rec = Long.parseLong(reciptNo2);

						String recp = Long.toString(rec + 1);

						transactions.setReciptNo("PNT" + recp);
					} else {
						transactions.setReciptNo("PNT1");
					}
					transactions.setNote("Cancell of item");
					transactions.setNetAmount(refundAmt);
					transactions.setInOrOut(CASH_IN);
					transactions.setIsCleared(NOT_CLEAR_TRAN);
					transactions.setStatus(ACTIVE);
					transactions.setPairedId(orderList.get(0).getTransactionId());
					session.save(transactions);
					session.flush();
					map = new HashMap<>();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE,
							"Your payment amount" + transactions.getNetAmount() + "will be credited shortly");
					map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
					return map;
				} else if (orderList.get(0).getOnline() == 2) {
					for (OrderDetails or : orDetails) {

						strQuery = "update from Transactions o set o.netAmount=:netAmount,o.status=:STAT where o.pairedId=:ID";
						Query qry = session.createQuery(strQuery);
						qry.setParameter("STAT", ACTIVE);
						qry.setParameter("netAmount", refundAmt);
						qry.setParameter("ID", or.getOrderId());
						int result = qry.executeUpdate();
						if (result <= 0) {
							map = new HashMap<>();
							map.put(ERROR_CODE, -22);
							map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
							map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
							return map;
						}
					}
				}

				map = new HashMap<>();
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
				return map;
			} else {
				strQuery = "update from Order o set o.orderstatus=:STAT,o.operatingOfficerId=:USERID where o.orderId=:ID";
				Query qry = session.createQuery(strQuery);
				qry.setParameter("STAT", status);
				qry.setParameter("ID", orderId);
				qry.setParameter("USERID", userId);
				int result = qry.executeUpdate();
				if (result <= 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -22);
					map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
					map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
					return map;
				}
			}

			// closeDBSession();

			/// ADDED NOTIFICATION DETAILS
			NotificationDetails notifyDetails = new NotificationDetails();
			notifyDetails.setUserId(userId);
			// notifyDetails.setDescription("ORDERED SUCCESSFULLY");
			notifyDetails.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
			notifyDetails.setOrderStatus(status);

			if (status == 2) {
				notifyDetails.setDescription("ORDERED CONFIRMED");
			} else if (status == 6) {
				notifyDetails.setDescription("ORDERE IS READY_TO_DELIVERY");
			}

			else if (status == 3) {
				notifyDetails.setDescription("ORDERE REJECTED");
			} else if (status == 4) {
				notifyDetails.setDescription("ORDERE DELIVERED");
			} else if (status == 5) {
				notifyDetails.setDescription("ORDERE CANCELED");
			} else if (status == 7) {
				notifyDetails.setDescription("ORDERE PICKED");
			}

			notifyDetails.setReadStatus(0);
			session.save(notifyDetails);
			session.flush();

			/// ADDED NOTIFICATION
			/// DETAILS-----------------------------------------*****************_______________

			// closeDBSession();
		} catch (Exception e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> addShippingDetails(ShippingDetails shippingDetails) {
		Map<String, Object> map = null;
		List<ShippingDetails> shippingList = null;
		List<CustomerDetails> customerList = null;
		try {
			logger.info(shippingDetails.getCustomerId() + "id--------------------------------");
			Session session = openDBsession();
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(CustomerDetails.class);
			detachedCriteria.add(Restrictions.eq("customerId", shippingDetails.getCustomerId()));
			customerList = (List<CustomerDetails>) hibernateTemplate.findByCriteria(detachedCriteria);
			if (customerList == null || customerList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -4);
				map.put(ERROR_MESSAGE, "No such userId");
				map.put(ERROR_MESSAGE_ARABIC, "");
				return map;
			}

			if (shippingDetails.getShippingId() <= 0) {
				map = new HashMap<>();
				shippingDetails.setCustomerId(shippingDetails.getCustomerId());
				session.save(shippingDetails);
				session.flush();
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, "");
				map.put("shippingDetails", shippingDetails);
				return map;
			} else {
				detachedCriteria = DetachedCriteria.forClass(ShippingDetails.class);
				detachedCriteria.add(Restrictions.eq("shippingId", shippingDetails.getShippingId()));
				shippingList = (List<ShippingDetails>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();
				if (shippingList == null || shippingList.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such shippind id");
					map.put(ERROR_MESSAGE_ARABIC, "Ù„Ø§ ÙŠÙˆØ¬Ø¯ Ù…Ø«Ù„ Ù‡Ø°Ù‡ Ø§Ù„Ù�Ø¦Ø©");
					return map;
				} else {
					// category.setCategory(null);
					session.merge(shippingDetails);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ø¬Ø§Ø­");
					map.put("shippingDetails", shippingDetails);
					return map;
				}
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			shippingDetails = null;
			throw e;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> addCardDetails(CardDetails cardDetails) {
		Map<String, Object> map = null;
		List<CardDetails> shippingList = null;
		List<CustomerDetails> customerList = null;
		CustomerDetails cd = new CustomerDetails();
		try {

			logger.info(cd.getUserId() + "cd.getUserId()");

			Session session = openDBsession();
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(CustomerDetails.class);
			detachedCriteria.add(Restrictions.eq("customerId", cardDetails.getCustomerId()));
			customerList = (List<CustomerDetails>) hibernateTemplate.findByCriteria(detachedCriteria);
			if (customerList == null || customerList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -4);
				map.put(ERROR_MESSAGE, "No such userId");
				map.put(ERROR_MESSAGE_ARABIC, "");
				return map;
			}

			if (cardDetails.getCardId() <= 0) {
				map = new HashMap<>();

				session.save(cardDetails);
				session.flush();
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, "");
				map.put("shippingDetails", cardDetails);
				return map;
			} else {
				detachedCriteria = DetachedCriteria.forClass(CardDetails.class);
				detachedCriteria.add(Restrictions.eq("cardId", cardDetails.getCardId()));
				shippingList = (List<CardDetails>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();
				if (shippingList == null || shippingList.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such cardId ");
					map.put(ERROR_MESSAGE_ARABIC, "Ù„Ø§ ÙŠÙˆØ¬Ø¯ Ù…Ø«Ù„ Ù‡Ø°Ù‡ Ø§Ù„Ù�Ø¦Ø©");
					return map;
				} else {
					// category.setCategory(null);
					session.merge(cardDetails);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ø¬Ø§Ø­");
					map.put("shippingDetails", cardDetails);
					return map;
				}
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			cardDetails = null;
			throw e;
		}
	}

	@Override
	public Map<String, Object> deleteTax(long taxId) {
		Map<String, Object> map = null;
		Session session = openDBsession();
		if (taxId > 0) {
			Criteria criteria = session.createCriteria(TaxManagement.class);
			criteria.add(Restrictions.eq("taxId", taxId));
			TaxManagement tax = (TaxManagement) criteria.uniqueResult();
			session.flush();
			if (tax != null) {
				Query qry = session.createQuery(" delete  from TaxManagement where taxId=? ");
				qry.setParameter(0, taxId);
				int result = qry.executeUpdate();
				logger.info(result + "___________________________________________________________________");
				if (result < 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -3);
					map.put(ERROR_MESSAGE, "Message deleteing failed");
					map.put(ERROR_MESSAGE_ARABIC, "أخفق حذف الرسالة");
					return map;
				}

				session.delete(tax);

				session.flush();
				map = new HashMap<>();
				map.put(ERROR_CODE, 0);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				return map;

			} else {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "No such tax found with this tax id");
				map.put(ERROR_MESSAGE_ARABIC, "لم يتم العثور على مثل هذا المزاد مع هذا المزاد إد");
				return map;
			}
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, 0);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		return map;
	}

	@Override
	public Map<String, Object> deleteShipping(long shippingId) {
		Map<String, Object> map = null;
		Session session = openDBsession();
		if (shippingId > 0) {
			Criteria criteria = session.createCriteria(ShippingDetails.class);
			criteria.add(Restrictions.eq("shippingId", shippingId));
			ShippingDetails tax = (ShippingDetails) criteria.uniqueResult();
			session.flush();
			if (tax != null) {
				Query qry = session.createQuery(" delete ShippingDetails s  where s.shippingId =:shippingId ");
				qry.setParameter("shippingId", shippingId);
				int result = qry.executeUpdate();
				logger.info(result + "___________________________________________________________________");
				if (result < 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -3);
					map.put(ERROR_MESSAGE, "Message deleteing failed");
					map.put(ERROR_MESSAGE_ARABIC, "أخفق حذف الرسالة");
					return map;
				}
				map = new HashMap<>();
				map.put(ERROR_CODE, 0);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				return map;

			} else {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "No such shipping address found with this shipping id");
				map.put(ERROR_MESSAGE_ARABIC, "لم يتم العثور على مثل هذا المزاد مع هذا المزاد إد");
				return map;
			}
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, 0);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> deleteCard(long cardId) {
		Map<String, Object> map = null;
		Session session = openDBsession();
		if (cardId > 0) {
			Criteria criteria = session.createCriteria(CardDetails.class);
			criteria.add(Restrictions.eq("cardId", cardId));
			CardDetails tax = (CardDetails) criteria.uniqueResult();
			session.flush();
			if (tax != null) {
				Query qry = session.createQuery(" delete CardDetails s  where s.cardId =:cardId ");
				qry.setParameter("cardId", cardId);
				int result = qry.executeUpdate();
				logger.info(result + "___________________________________________________________________");
				if (result < 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -3);
					map.put(ERROR_MESSAGE, "Message deleteing failed");
					map.put(ERROR_MESSAGE_ARABIC, "أخفق حذف الرسالة");
					return map;
				}
				map = new HashMap<>();
				map.put(ERROR_CODE, 0);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				return map;

			} else {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "No such shipping address found with this shipping id");
				map.put(ERROR_MESSAGE_ARABIC, "لم يتم العثور على مثل هذا المزاد مع هذا المزاد إد");
				return map;
			}
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, 0);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllReturnProducts(long returnItemId, long orderListId, int rowPerPage,
			int currentIndex, long storeId, int stat) {

		Map<String, Object> map = null;
		List<ReturnItems> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();

			boolean flag = false;
			StringBuffer strBfr = new StringBuffer(" select count(*)  from ReturnItems r ");
			Criteria criteria = session.createCriteria(ReturnItems.class);
			if (storeId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" left join  r.orderDetails od left join od.product p where ");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.sellerId=" + storeId);
				Criteria criteria2 = criteria.createCriteria("orderDetails");
				Criteria criteria3 = criteria2.createCriteria("product");
				criteria3.add(Restrictions.eq("sellerId", storeId));
			}

			if (returnItemId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append("  where ");
				} else {
					strBfr.append(" and ");
				}
				strBfr.append(" r.returnItemId =" + returnItemId);
				criteria.add(Restrictions.eq("returnItemId", returnItemId));

			}

			if (orderListId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append("  where ");
				} else {
					strBfr.append(" and ");
				}
				strBfr.append(" r.orderListId =" + orderListId);
				criteria.add(Restrictions.eq("orderListId", orderListId));

			}

			if (stat > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where ");
				} else {
					strBfr.append(" and ");
				}
				strBfr.append(" r.status = " + stat);
				criteria.add(Restrictions.eq("status", stat));

			}
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			logger.info(qry + "qry------------------------------------------------------------");
			logger.info(totalCounts + "totalCounts-------------------------------------------------------------");
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(currentIndex);
			}
			List<Long> ids = criteria.list();
			logger.info(ids + "ids********************************************************");
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(ReturnItems.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {

			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");
			map.put("ReturnItemList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			map.put("TotalCounts", totalCounts);
			return map;
		}
	}

	@Override
	public Map<String, Object> addReturnProduct(List<ReturnItems> returnItems) throws Exception {
		String q = null;
		Map<String, Object> map = new HashMap<>();
		try {
			Session session = openDBsession();
			OrderDetails orderDetails2 = null;
			for (ReturnItems reItem : returnItems) {

				Criteria criteria = session.createCriteria(OrderDetails.class);
				criteria.add(Restrictions.eq("orderListId", reItem.getOrderListId()));
				OrderDetails orderDetails = (OrderDetails) criteria.uniqueResult();
				session.flush();
				if (orderDetails.getItemQuantity() < reItem.getItemQuantity()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, orderDetails.getItemQuantity() + " is only available");
					map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
					return map;
				} else if (orderDetails.getItemQuantity() > reItem.getItemQuantity()) {
					logger.info("inside this");
					orderDetails2 = new OrderDetails();
					orderDetails2.setCancellationDate(0);
					orderDetails2.setCompanyId(orderDetails.getCompanyId());
					orderDetails2.setDeliveryDate(orderDetails.getDeliveryDate());
					orderDetails2.setDescription(orderDetails.getDescription());
					orderDetails2.setImageName(orderDetails.getImageName());
					orderDetails2.setItemName(orderDetails.getItemName());
					orderDetails2.setItemNameAr(orderDetails.getItemNameAr());
					orderDetails2.setItemPurchasePrice(orderDetails.getItemPurchasePrice());
					orderDetails2.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
					orderDetails2.setOfferId(orderDetails.getOfferId());
					orderDetails2.setOperatingOfficerId(orderDetails.getOperatingOfficerId());
					orderDetails2.setOrderId(orderDetails.getOrderId());
					orderDetails2.setProductId(orderDetails.getProductId());
					orderDetails2.setReturnTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
					orderDetails2.setShippingCharge(orderDetails.getShippingCharge());
					orderDetails2.setTax(orderDetails.getTax());
					orderDetails2.setTaxId(orderDetails.getTaxId());
					orderDetails2.setItemPrice(orderDetails.getItemPrice());
					orderDetails2.setTotalAmountIncludingOffer(orderDetails.getTotalAmountIncludingOffer());
					orderDetails2.setStatus(RETURN_REQUESTED);
					orderDetails2.setOrderListId(0);
					orderDetails2.setItemQuantity(reItem.getItemQuantity());
					orderDetails2.setItemNetPrice(reItem.getItemQuantity() * orderDetails.getItemPrice());
					session.save(orderDetails2);
					session.flush();
					reItem.setOrderListId(orderDetails2.getOrderListId());
					reItem.setOldOrderListId(orderDetails.getOrderListId());
					long itemQty = orderDetails.getItemQuantity() - orderDetails2.getItemQuantity();
					orderDetails.setItemQuantity(itemQty);
					orderDetails.setItemNetPrice(orderDetails.getItemPrice() * orderDetails.getItemQuantity());
					orderDetails.setStatus(DELIVERED);
					session.merge(orderDetails);
					session.flush();
					Query qry = session
							.createQuery("update Order o set o.netPrice=o.netPrice-:price where o.orderId=:odId");
					qry.setParameter("odId", orderDetails.getOrderId());
					qry.setParameter("price", orderDetails2.getItemNetPrice());
					reItem.setOldOrderListId(orderDetails.getOrderListId());
					int result = qry.executeUpdate();
					logger.info(result);
					session.flush();
					if (result <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -2);
						map.put(ERROR_MESSAGE, "Order " + MESSAGE_UPDATE_FAILED);
						map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
						return map;
					}
				} else {
					Query qry = session
							.createQuery("update OrderDetails od set od.status=:stat where od.orderListId=:odId");
					qry.setParameter("odId", orderDetails.getOrderListId());
					qry.setParameter("stat", RETURN_REQUESTED);
					reItem.setOldOrderListId(orderDetails.getOrderListId());
					int result = qry.executeUpdate();
					logger.info(result);
					session.flush();
					if (result <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -3);
						map.put(ERROR_MESSAGE, "Order " + MESSAGE_UPDATE_FAILED);
						map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
						return map;
					}
					qry = session.createQuery("update Order o set o.netPrice=o.netPrice-:price where o.orderId=:odId");
					qry.setParameter("odId", orderDetails.getOrderId());
					qry.setParameter("price", orderDetails.getItemNetPrice());
					reItem.setOldOrderListId(orderDetails.getOrderListId());
					result = qry.executeUpdate();
					logger.info(result);
					session.flush();
					if (result <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -4);
						map.put(ERROR_MESSAGE, "Order " + MESSAGE_UPDATE_FAILED);
						map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
						return map;
					}
				}
				logger.info("befor adding reitem");
				session.save(reItem);
				session.flush();
				if (reItem.getOldOrderListId() <= 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -5);
					map.put(ERROR_MESSAGE, "Return item" + MESSAGE_UPDATE_FAILED);
					map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
					return map;
				}
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			returnItems = null;
			throw e;
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, "نجاح");
		return map;
	}
}
