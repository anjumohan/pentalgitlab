package com.hidile.pental.dao;

import java.util.List;
import java.util.Map;

import com.hidile.pental.entity.input.Attributes;
import com.hidile.pental.entity.input.Offer;
import com.hidile.pental.entity.input.SellerDetails;
import com.hidile.pental.entity.order.TaxManagement;
import com.hidile.pental.entity.user.CompanyDetails;
import com.hidile.pental.entity.user.CustomerDetails;
import com.hidile.pental.entity.user.DeliveryBoy;
import com.hidile.pental.entity.user.LoginDetails;
import com.hidile.pental.entity.user.PromocodeDetails;
import com.hidile.pental.model.BasicResponse;
import com.hidile.pental.model.DeliveryBoyVo;

public interface GeneralDao {

	public Map<String,Object> saveCustomer(CustomerDetails userMaster,LoginDetails login);
	public Map<String,Object> savePromoters(CustomerDetails userMaster,LoginDetails login);
	public Map<String,Object> savePromocode(PromocodeDetails promoDetails);
	public Map<String, Object> checkWeatherEmailPresent(String email,int role,int phoneOrWeb);
	public CustomerDetails getCustomerFrom(String userName, String phoneNo);
	public Map<String,Object>  getAllPromoters(long customerId,long cityId,long countryId,long userId,int status,long fromDate,long toDate,int currentIndex,int rowPerPage,long companyId,String customerName,String emailid,String phoneNo,String customerNameAr);
//	public UserMaster getLoginInfo(String email);
	public Map<String, Object> saveOrUpdateSellerDetails(SellerDetails sellerDetails, LoginDetails login);
//	public UserMaster getUserById(long userId);
	// public Map<String,Object> getAllCustomers(long customerId);
//	public Map<String, Object> saveOrUpdateUser(UserMaster userMaster);
	public Map<String,Object> getAllCustomers(long customerId,long cityId,long userId,long fromDate,long toDate,long countryId,int orderBy,int currentIndex,int rowPerPage,int status);
	public Map<String,Object> getPromocodeDetails(long codeId,long userId,int status,long validFrom,long validTo,int currentIndex,int rowPerPage,String promocode);	
	public Map<String,Object> validatePromocode(String promocode,double orderPrice);
	
	//public Map<String,Object> getAllCustomers(long customerId,long cityId,long userId,long fromDate,long toDate,long countryId,int status,int currentIndex,int rowPerPage,String customerName);		
	public Map<String,Object> getAllCustomers(long customerId,long cityId,long userId,long fromDate,long toDate,long countryId,int status,int currentIndex,int rowPerPage,long companyId,String customerName,String emailid,String phoneNo,String customerNameAr);		
	public Map<String, Object> getAllCustomers(long customerId,long userId,long fromDate,long toDate,int status,int firstIndex,int rowPerPage);		
	public Map<String, Object> getAllUsers(long userId);	
	//public Map<String, Object> sellerLogin(String email,String password);
	//public Map<String, Object> saveOrUpdateDeliveryBoy(DeliveryBoy deliveryBoy,LoginDetails login);
	
	//public Map<String, Object> getAllNotification(int firstIndex, int rowPerPage,int orderStatus,int readStatus,long userId,long notificationId);
	public Map<String, Object> saveComapnay(CompanyDetails companyDetails,LoginDetails login);

	public Map<String, Object> aunthenticate(String userName, String password,int role);

	public CustomerDetails getCustomerFromId(long customerId);
	public TaxManagement getTax(long taxId);
	public Offer getOffer(long offerId);
	public SellerDetails getSellerFromId(long customerId);
	public Map<String, Object> getAllNotification(int firstIndex, int rowPerPage,int orderStatus[],int readStatus,long userId,long notificationId);
	//public Map<String, Object> getAllUsers(long userId);

	//public Map<String, Object> getAllCustomers(long customerId,long userId,long fromDate,long toDate,int status,int firstIndex,int rowPerPage);
	
	public Map<String, Object> getGeneralDatas();
	
	//public Map<String, Object> activateAccount(long loginId,int role);
	public Map<String, Object> activateAccount(long loginId,int role,int status);
	public Map<String, Object>  validateCustomer(long loginId,int role,int status);
	public Map<String, Object> changePassword(String oldPass, String newPass, long userId);
	
	public Map<String,Object> saveAdmin(LoginDetails login,SellerDetails sellerDetails,LoginDetails login2,List<Attributes> attriList);
	
	public Map<String, Object> getAllDeliveryBoy(String address,long deliveryBoyId,String deliveryBoyName, int firstIndex, int rowPerPage,int status[],String phoneNumber);

	//public Map<String, Object>	getAllSeller(long sellerId,int firstIndex, int rowPerPage,int status);
	public Map<String, Object> assignOffer(long offerId,long productId);
	public Map<String, Object> changeDeliveryStatus(long orderId, long deliveryBoyId,int status);
	public Map<String, Object> cancellDeriveryBoy(long orderId);
	public Map<String, Object> changeSellerStatus(long sellerId,int status);
	public Map<String, Object>	getAllSeller(long sellerId,int firstIndex, int rowPerPage,int stat,String sellerName,String phoneNumber,long companyId);
	
	public Map<String, Object> changeCustomerStatus(long customerId,int status);
	 public Map<String, Object> changePromocodeStatus(long codeId,int status);
}
