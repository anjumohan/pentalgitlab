package com.hidile.pental.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.DoubleType;
import org.hibernate.type.LongType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hidile.pental.constants.Constants;
import com.hidile.pental.entity.input.Attributes;
import com.hidile.pental.entity.input.Category;
import com.hidile.pental.entity.input.Offer;
import com.hidile.pental.entity.input.OfferDetails;
import com.hidile.pental.entity.input.Product;
import com.hidile.pental.entity.input.Review;
import com.hidile.pental.entity.input.SellerDetails;
import com.hidile.pental.entity.order.Order;
import com.hidile.pental.entity.order.TaxManagement;
import com.hidile.pental.entity.user.CompanyDetails;
import com.hidile.pental.entity.user.CustomerDetails;
import com.hidile.pental.entity.user.DeliveryBoy;
import com.hidile.pental.entity.user.LoginDetails;
import com.hidile.pental.entity.user.NotificationDetails;
import com.hidile.pental.entity.user.PromocodeDetails;
import com.hidile.pental.utils.TimeUtils;
import com.hidile.pental.utils.Validation;

@Transactional
@Repository("generalDao")
public class GeneralDaoImpl implements GeneralDao, Constants {
	private static Logger logger = Logger.getLogger(GeneralDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HibernateTemplate hibernateTemplate;

	private Session openDBsession() {
		/*
		 * if(session == null ){ session=sessionFactory.openSession(); }else
		 * if(session.isOpen()){
		 */
		Session session = sessionFactory.getCurrentSession();
		/*
		 * }else{ session=sessionFactory.openSession(); }
		 */
		return session;
	}

	/*
	 * private void closeDBSession(){ if(session.isOpen()){ session.flush();
	 * session.close(); } }
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllUsers(long userId) {
		Map<String, Object> map = null;
		DetachedCriteria detCriteria = DetachedCriteria.forClass(LoginDetails.class);
		if (userId > 0) {
			detCriteria.add(Restrictions.eq("userId", userId));
		}
		List<LoginDetails> loginList = (List<LoginDetails>) hibernateTemplate.findByCriteria(detCriteria);

		if (loginList == null || loginList.isEmpty()) {

			map = new HashMap<>();
			map.put(ERROR_MESSAGE, MESSAGE_NO_SUCH_USER);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_SUCH_USER_AR);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put(DATA, loginList);
			return map;
		}
	}
	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> getAllCustomers(long customerId, long
	 * userId, long fromDate, long toDate, long cityId, long countryId, int status,
	 * int currentIndex, int rowPerPage,String customerName) { Map<String, Object>
	 * map = null; try { Session session = openDBsession(); String
	 * statData=" where (cd.status=1 or cd.status=0)"; if(status == 1 || status ==
	 * 0) { statData=" where cd.status="+status; } StringBuffer sBuffer = new
	 * StringBuffer(
	 * "select cd.*,l.CITY cityId,l.COUNTRY countryId,COALESCE(sum(case when ot.ORDER_STATUS = 2 then ot.netPrice else 0 end),0) as sumofprice from CS_CUSTOMER_DETAILS cd inner join LOGIN l on l.USER_ID=cd.USER_ID left join ORDER_TABLE ot on ot.customer_id=cd.USER_ID"
	 * +statData); if (cityId > 0) { sBuffer.append(" and l.CITY=" + cityId); } if
	 * (countryId > 0) { sBuffer.append(" and l.COUNTRY=" + countryId); } if (userId
	 * > 0) { sBuffer.append(" and l.USER_ID=" + userId); } if (customerId > 0) {
	 * sBuffer.append(" and cd.CUSTOMER_ID=" + customerId); } if
	 * (!customerName.equals("")) {
	 * logger.info(customerName+"___________________________________________");
	 * 
	 * sBuffer.append(" and lower(cd.customerName) like '%" +
	 * customerName.toLowerCase() + "%'");
	 * //criteria.add(Restrictions.ilike("sellerName",sellerName )); } if (fromDate
	 * > 0) { if (toDate == 0) { toDate =
	 * TimeUtils.instance().getEndTimeOfGiveTime(fromDate); }
	 * sBuffer.append(" and cd.CREATED_DATE between " + fromDate + " and " +
	 * toDate); } sBuffer.
	 * append(" group by cd.CUSTOMER_ID,l.USER_ID order by cd.CREATED_DATE desc");
	 * logger.info(sBuffer.toString()); SQLQuery qry =
	 * session.createSQLQuery(sBuffer.toString()); if (rowPerPage > 0) {
	 * qry.setFirstResult(currentIndex); qry.setMaxResults(rowPerPage); }
	 * qry.addScalar("sumofprice", DoubleType.INSTANCE).addEntity("cd",
	 * CustomerDetails.class) .addScalar("cityId",
	 * LongType.INSTANCE).addScalar("countryId", LongType.INSTANCE); //
	 * qry.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); List<Object[]> obj =
	 * qry.list(); session.flush(); if (obj != null && !obj.isEmpty()) { map = new
	 * HashMap<>(); logger.info(obj); map.put("customerList", obj);
	 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); } else { map = new HashMap<>(); map.put(ERROR_CODE, -2);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW); }
	 * 
	 * sBuffer.replace(7, 134, " COUNT(*) OVER () AS TotalRecords"); //
	 * strBfr.replace(109, strBfr.length(), ""); logger.info(sBuffer.toString());
	 * qry = session.createSQLQuery(sBuffer.toString()).addScalar("TotalRecords",
	 * LongType.INSTANCE); Long totalCounts = (Long) qry.uniqueResult();
	 * if(totalCounts == null) { totalCounts=0l; } map.put(TOTAL_COUNT,
	 * totalCounts); // closeDBSession(); } catch (HibernateException e) { //
	 * closeDBSession(); if (TESTING) { e.printStackTrace(); }
	 * logger.info(e.getMessage()); map = new HashMap<>(); map.put(ERROR_CODE, -6);
	 * map.put(ERROR_MESSAGE, e.getMessage()); throw e; } return map; }
	 * 
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveCustomer(CustomerDetails customer, LoginDetails login) {
		Map<String, Object> map = null;
		String pass = null;
		try {
			map = new HashMap<>();
			Session session = openDBsession();
			String qurey = null;
			Query qry = null;
			List<Long> logId = null;

			qurey = "select um.userId from LoginDetails um where um.phoneNo=:pNo or um.email=:eMail";
			qry = session.createQuery(qurey);
			qry.setParameter("pNo", customer.getPhoneNo());
			qry.setParameter("eMail", customer.getEmailId());
			logId = (List<Long>) qry.list();

			if (customer.getCustomerId() > 0) {
				qurey = "select um.userId from CustomerDetails um where um.customerId=:cid";
				qry = session.createQuery(qurey);
				qry.setParameter("cid", customer.getCustomerId());
				List<Long> custId = (List<Long>) qry.list();
				if (custId == null || custId.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "MESSAGE_NO_SUCH_CUSTOMER");
					map.put(ERROR_MESSAGE_ARABIC, "MESSAGE_NO_SUCH_CUSTOMER_AR");
					return map;
				}

			}

			if (logId == null || logId.isEmpty())

			{

				if (customer.getCustomerId() > 0) {
					customer.setUserId(logId.get(0));
					session.update(customer);
					session.flush();
					logger.info(login
							+ "login-----------------------------------------------------------------------------");
					if (login != null) {
						logger.info(pass + "pass____________________________________________________");

						String qurey1 = "UPDATE LoginDetails l SET l.city=:city, l.country=:country,l.email=:email,l.phoneNo=:phone_no WHERE l.userId=:user_id ";
						Query qry1 = session.createQuery(qurey1);
						logger.info(qry1 + "qry1________________________________________________________________");
						qry1.setParameter("city", login.getCity());
						qry1.setParameter("country", login.getCountry());
						qry1.setParameter("email", login.getEmail());
						qry1.setParameter("phone_no", login.getPhoneNo());
						qry1.setParameter("user_id", customer.getUserId());
						Integer result = qry1.executeUpdate();

					}
				} else {
					session.save(login);
					session.flush();
					customer.setUserId(login.getUserId());

					session.save(customer);
					session.flush();
					login.setEntryId(customer.getCustomerId());
					session.merge(login);
					session.flush();
				}
			} else if (customer.getCustomerId() > 0) {
				customer.setUserId(logId.get(0));
				session.update(customer);
				session.flush();
				logger.info(
						login + "login-----------------------------------------------------------------------------");
				if (login != null) {
					logger.info(pass + "pass____________________________________________________");

					String qurey1 = "UPDATE LoginDetails l SET l.city=:city, l.country=:country,l.email=:email,l.phoneNo=:phone_no WHERE l.userId=:user_id ";
					Query qry1 = session.createQuery(qurey1);
					logger.info(qry1 + "qry1________________________________________________________________");
					qry1.setParameter("city", login.getCity());
					qry1.setParameter("country", login.getCountry());
					qry1.setParameter("email", login.getEmail());
					qry1.setParameter("phone_no", login.getPhoneNo());
					qry1.setParameter("user_id", customer.getUserId());
					Integer result = qry1.executeUpdate();

				}
			}

			else {
				map.put(ERROR_CODE, -4);
				map.put(ERROR_MESSAGE, "Already registered phone number or email ||||||||||||||||||||||||||||||||||");
				map.put(ERROR_MESSAGE_ARABIC, "مسجل بالفعل رقم الهاتف أو البريد الإلكتروني");
				return map;
			}
			// closeDBSession();

			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("CUSTMOER", customer);
			map.put("LOGIN", login);

		} catch (HibernateException e) {
			// closeDBSession();
			map = new HashMap<>();
			logger.info(e.getMessage());
			if (TESTING) {
				e.printStackTrace();
			}
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> savePromoters(CustomerDetails customer, LoginDetails login) {
		Map<String, Object> map = null;
		String pass = null;
		try {
			map = new HashMap<>();
			Session session = openDBsession();
			String qurey = "select um.userId from LoginDetails um where um.phoneNo=:pNo or um.email=:eMail and roleId=54";
			Query qry = session.createQuery(qurey);
			qry.setParameter("pNo", customer.getPhoneNo());
			qry.setParameter("eMail", customer.getEmailId());
			List<Long> logId = (List<Long>) qry.list();
		//	logger.info(logId + "logId_____________________________________________________________________");
			if (customer.getCustomerId() > 0) 
			{
				qurey = "select um.userId from CustomerDetails um where um.customerId=:cid";
				qry = session.createQuery(qurey);
				qry.setParameter("cid", customer.getCustomerId());
				List<Long> custId = (List<Long>) qry.list();
				if (custId == null || custId.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "MESSAGE_NO_SUCH_CUSTOMER");
					map.put(ERROR_MESSAGE_ARABIC, "MESSAGE_NO_SUCH_CUSTOMER_AR");
					return map;
				}
			}

			if (customer.getCustomerId() > 0)
			{customer.setUserId(logId.get(0));
				/*
				
				customer.setCustomerId(customer.getCustomerId());
				logger.info(customer.getCustomerId()+"customer.getCustomerId()customer.getCustomerId()customer.getCustomerId()customer.getCustomerId()");
				logger.info(customer.getStatus()+"________________&&&&&&&&&&^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"+logId.get(0));
				customer.setStatus(customer.getStatus());
				customer.setEmailId(customer.getEmailId());
				customer.setPhoneNo(customer.getPhoneNo());
				customer.setProfileImage(customer.getProfileImage());
				customer.setCustomerName(customer.getCustomerName());//customerName
				session.update(customer);
				session.flush();*/
				String qurey12 = "UPDATE CustomerDetails l SET l.status=:status, l.customerName=:customerName,l.customerNameAr=:customerNameAr,l.address=:address,l.addressAr=:addressAr,l.emailId=:emailId,l.phoneNo=:phoneNo,l.profileImage=:profileImage,l.validate=:validate WHERE l.customerId=:customerId ";
				Query qry12 = session.createQuery(qurey12);
				qry12.setParameter("validate", customer.getValidate());
				qry12.setParameter("profileImage", customer.getProfileImage());
				qry12.setParameter("phoneNo", customer.getPhoneNo());
				qry12.setParameter("emailId", customer.getEmailId());
				qry12.setParameter("addressAr", customer.getAddressAr());
				qry12.setParameter("address", customer.getAddress());
				qry12.setParameter("customerNameAr", customer.getCustomerNameAr());
				qry12.setParameter("customerName", customer.getCustomerName());
				qry12.setParameter("status", customer.getStatus());
				qry12.setParameter("customerId",customer.getCustomerId() );
				Integer result1 = qry12.executeUpdate();
				logger.info(result1+"PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP__________________"+customer.getUserId());
				
				String qurey123 = "UPDATE CustomerDetails l SET l.status=:status  WHERE l.customerId=:customerId ";
				Query qry123 = session.createQuery(qurey123);
				logger.info(customer.getStatus()+"statussssssssssssssssssssssssss-----------");
				qry123.setParameter("status", customer.getStatus());
				qry123.setParameter("customerId", customer.getCustomerId());
				Integer result12 = qry123.executeUpdate();
				
				logger.info(result12+"PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP__________________");
				
				if (login != null) 
				{
					
					String qurey1 = "UPDATE LoginDetails l SET l.city=:city, l.country=:country,l.email=:email,l.phoneNo=:phone_no,l.roleId=:role_id,l.status=:status WHERE l.userId=:user_id ";
					Query qry1 = session.createQuery(qurey1);
					logger.info(qry1 + "qry1________________________________________________________________");
					qry1.setParameter("city", login.getCity());
					qry1.setParameter("country", login.getCountry());
					qry1.setParameter("email", login.getEmail());
					qry1.setParameter("phone_no", login.getPhoneNo());
					qry1.setParameter("user_id", customer.getUserId());
					qry1.setParameter("role_id", 54);
					qry1.setParameter("status", customer.getStatus());
				
					
					
					Integer result = qry1.executeUpdate();

					
				}
			} else {
				pass = login.getPassword();
				session.save(login);
				session.flush();
				customer.setUserId(login.getUserId());
				session.save(customer);
				session.flush();
				login.setEntryId(customer.getCustomerId());
				session.merge(login);
				session.flush();
			}
			// closeDBSession();

			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("CUSTMOER", customer);
		} catch (HibernateException e) {
			// closeDBSession();
			map = new HashMap<>();
			logger.info(e.getMessage());
			if (TESTING) {
				e.printStackTrace();
			}
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		return map;
	}

	@Override
	public Map<String, Object> savePromocode(PromocodeDetails promoDetails) {
		Map<String, Object> map = null;
		map = new HashMap<>();
		List<PromocodeDetails> promoList = null;
		Session session = openDBsession();
		if (promoDetails.getCodeId() > 0) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(PromocodeDetails.class);
			detachedCriteria.add(Restrictions.eq("codeId", promoDetails.getCodeId()));
			promoList = (List<PromocodeDetails>) hibernateTemplate.findByCriteria(detachedCriteria);
			logger.info(promoList + "promoList------------------------------------------------------");
			if (promoList == null || promoList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -4);
				map.put(ERROR_MESSAGE, "No such promocode id found");
				map.put(ERROR_MESSAGE_ARABIC, "Ù„Ø§ ÙŠÙˆØ¬Ø¯ Ù…Ø«Ù„ Ù‡Ø°Ù‡ Ø§Ù„Ù�Ø¦Ø©");
				return map;
			} else {
				session.merge(promoDetails);
				session.flush();
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			}
		}

		else {

			session.save(promoDetails);
			session.flush();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			return map;
		}
		return map;
	}

	@Override
	public Map<String, Object> changeSellerStatus(long sellerId, int status) {
		Map<String, Object> map = null;// 4 delivered
		int result = 0;
		try {

			Session session = openDBsession();

			DetachedCriteria detachedCriteria2 = DetachedCriteria.forClass(SellerDetails.class);
			detachedCriteria2.add(Restrictions.eq("sellerId", sellerId));
			List<SellerDetails> dvList = (List<SellerDetails>) hibernateTemplate.findByCriteria(detachedCriteria2);
			if (dvList == null || dvList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "sellerId " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}

			Query qry2 = session.createQuery("update SellerDetails set status=:status where sellerId=:sellerId");
			qry2.setParameter("status", status);
			qry2.setParameter("sellerId", sellerId);
			result = qry2.executeUpdate();

		}

		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		map = new HashMap<>();
		if (result <= 0) {
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
		} else {
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		}
		return map;
	}

	@Override
	public Map<String, Object> changeCustomerStatus(long customerId, int status) {
		Map<String, Object> map = null;// 4 delivered
		int result = 0;
		try {

			Session session = openDBsession();

			DetachedCriteria detachedCriteria2 = DetachedCriteria.forClass(CustomerDetails.class);
			detachedCriteria2.add(Restrictions.eq("customerId", customerId));
			List<CustomerDetails> dvList = (List<CustomerDetails>) hibernateTemplate.findByCriteria(detachedCriteria2);
			if (dvList == null || dvList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "sellerId " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}
			Query qry2 = session.createQuery("update CustomerDetails set status=:status where customerId=:customerId");
			qry2.setParameter("status", status);
			qry2.setParameter("customerId", customerId);
			result = qry2.executeUpdate();

			Query qry21 = session.createQuery("update LoginDetails set status=:status where entryId=:customerId");
			qry21.setParameter("status", status);
			qry21.setParameter("customerId", customerId);
			result = qry21.executeUpdate();

		}

		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		map = new HashMap<>();
		if (result <= 0) {
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
		} else {
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		}
		return map;
	}

	@Override
	public Map<String, Object> changePromocodeStatus(long codeId, int status) {
		Map<String, Object> map = null;// 4 delivered
		int result = 0;
		try {

			Session session = openDBsession();

			DetachedCriteria detachedCriteria2 = DetachedCriteria.forClass(PromocodeDetails.class);
			detachedCriteria2.add(Restrictions.eq("codeId", codeId));
			List<PromocodeDetails> dvList = (List<PromocodeDetails>) hibernateTemplate
					.findByCriteria(detachedCriteria2);
			if (dvList == null || dvList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "prmocode id " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}
			Query qry2 = session.createQuery("update PromocodeDetails set status=:status where codeId=:codeId");
			qry2.setParameter("status", status);
			qry2.setParameter("codeId", codeId);
			result = qry2.executeUpdate();

		}

		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		map = new HashMap<>();
		if (result <= 0) {
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
		} else {
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllCustomers(long customerId, long userId, long fromDate, long toDate, int status,
			int firstIndex, int rowPerPage) {
		Map<String, Object> map = null;
		Session session = openDBsession();
		Criteria detCriteria = session.createCriteria(CustomerDetails.class);
		boolean flag = false;
		Long count = 0l;
		StringBuffer sBuffer = new StringBuffer("select count(*) from CustomerDetails cd");
		if (customerId > 0) {
			detCriteria.add(Restrictions.eq("customerId", customerId));
		} else if (userId > 0) {
			/*
			 * if(flag == false) { flag=true; sBuffer.append(" where"); }else {
			 * sBuffer.append(" and"); }
			 */
			detCriteria.add(Restrictions.eq("userId", userId));
			// sBuffer.append(" cd.userId="+userId);
		} else {
			if (fromDate > 0) {
				if (flag == false) {
					flag = true;
					sBuffer.append(" where");
				} else {
					sBuffer.append(" and");
				}
				detCriteria.add(Restrictions.between("createdDate", fromDate, toDate));
				sBuffer.append(" cd.createdDate between " + fromDate + " and " + toDate);
			}
			if (status > 0) {
				if (flag == false) {
					flag = true;
					sBuffer.append(" where");
				} else {
					sBuffer.append(" and");
				}
				detCriteria.add(Restrictions.eq("status", ACTIVE));
				sBuffer.append(" cd.status=" + ACTIVE);
			}
			if (rowPerPage > 0) {
				detCriteria.setMaxResults(rowPerPage);
			}
			detCriteria.setFirstResult(firstIndex);
			Query totalQry = session.createQuery(sBuffer.toString());
			count = (Long) totalQry.uniqueResult();
			session.flush();
		}
		List<CustomerDetails> loginList = (List<CustomerDetails>) detCriteria.list();
		session.flush();

		if (loginList == null || loginList.isEmpty()) {

			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("customerList", loginList);
			map.put("TotalCounts", count);
			return map;
		}
	}

	/*
	 * @Override public Map<String,Object> getAllCustomers(long customerId) { long
	 * totalCounts = 0; Map<String, Object> map = null; List<CustomerDetails> data =
	 * null; try { Session session = openDBsession(); Criteria criteria =
	 * session.createCriteria(CustomerDetails.class); if (customerId > 0) {
	 * criteria.add(Restrictions.eq("customerId", customerId)); } data =
	 * criteria.list(); session.flush(); StringBuffer strBfr = new
	 * StringBuffer("select count(*) from CustomerDetails o"); Query qry =
	 * session.createQuery(strBfr.toString()); totalCounts = (long)
	 * qry.uniqueResult();
	 * 
	 * // closeDBSession(); } catch (HibernateException e) { // closeDBSession(); if
	 * (TESTING) { e.printStackTrace(); } logger.info(e.getMessage()); map = new
	 * HashMap<>(); map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE, e.getMessage());
	 * throw e; } if (data == null || data.isEmpty()) { map = new HashMap<>();
	 * map.put(ERROR_CODE, -3); map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR); return map; } else
	 * { map = new HashMap<>(); map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
	 * map.put("CustomerList", data); map.put("TotalCounts", totalCounts); return
	 * map; } }
	 */
	@Override
	public Map<String, Object> getAllCustomers(long customerId, long userId, long fromDate, long toDate, long cityId,
			long countryId, int orderBy, int currentIndex, int rowPerPage, int status) {
		Map<String, Object> map = null;
		try {
			Session session = openDBsession();
			StringBuffer sBuffer = new StringBuffer(
					"select cd.*,l.CITY cityId,l.COUNTRY countryId,COALESCE(sum(case when ot.ORDER_STATUS = 2 then ot.netPrice else 0 end),0) as sumofprice from CS_CUSTOMER_DETAILS cd inner join LOGIN l on l.USER_ID=cd.USER_ID left join ORDER_TABLE ot on ot.customer_id=cd.USER_ID where cd.status=1");
			if (cityId > 0) {
				sBuffer.append(" and");
				sBuffer.append(" l.CITY=" + cityId);
			}
			if (countryId > 0) {
				sBuffer.append(" l.COUNTRY=" + countryId);
			}
			if (userId > 0) {
				sBuffer.append(" and");
				sBuffer.append(" l.USER_ID=" + userId);
			}
			if (customerId > 0) {
				sBuffer.append(" and");
				sBuffer.append(" cd.CUSTOMER_ID=" + customerId);
			}

			/*
			 * if (!customerName.equals("")) {
			 * 
			 * sBuffer.append(" and");
			 * 
			 * sBuffer.append("  lower(cd.CUSTOMER_NAME) like '%" +
			 * customerName.toLowerCase() + "%'");
			 * 
			 * } if (!phoneNo.equals("")) {
			 * 
			 * sBuffer.append(" and");
			 * 
			 * sBuffer.append("  lower(cd.PHONE_NO) like '%" + phoneNo.toLowerCase() +
			 * "%'");
			 * 
			 * }
			 * 
			 * if (!emailId.equals("")) {
			 * 
			 * sBuffer.append(" and");
			 * 
			 * sBuffer.append("  lower(cd.EMAIL_ID) like '%" + emailId.toLowerCase() +
			 * "%'");
			 * 
			 * }
			 */

			if (fromDate > 0) {
				if (toDate == 0) {
					toDate = TimeUtils.instance().getEndTimeOfGiveTime(fromDate);
				}
				sBuffer.append(" and");
				sBuffer.append(" cd.CREATED_DATE between " + fromDate + " and " + toDate);
			}
			if (status > 0) {

				sBuffer.append(" and");
				sBuffer.append(" cd.STATUS=" + status);
			}

			sBuffer.append(" group by cd.CUSTOMER_ID,l.USER_ID");
			SQLQuery qry = session.createSQLQuery(sBuffer.toString());
			if (rowPerPage > 0) {
				qry.setFirstResult(currentIndex);
				qry.setMaxResults(rowPerPage);
			}
			qry.addScalar("sumofprice", DoubleType.INSTANCE).addEntity("cd", CustomerDetails.class)
					.addScalar("cityId", LongType.INSTANCE).addScalar("countryId", LongType.INSTANCE);
			// qry.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List<Object[]> obj = qry.list();
			session.flush();
			if (obj != null && !obj.isEmpty()) {
				map = new HashMap<>();
				logger.info(obj);
				map.put("customerList", obj);
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			} else {
				map = new HashMap<>();
				map.put(ERROR_CODE, -2);
				map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			}
			sBuffer.replace(7, 134, " COUNT(*) OVER () AS TotalRecords");
			// strBfr.replace(109, strBfr.length(), "");
			logger.info(sBuffer.toString());
			qry = session.createSQLQuery(sBuffer.toString()).addScalar("TotalRecords", LongType.INSTANCE);
			long totalCounts = (long) qry.uniqueResult();
			map.put(TOTAL_COUNT, totalCounts);
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -6);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		return map;

	}

	@Override
	public Map<String, Object> checkWeatherEmailPresent(String email, int role, int phoneOrWeb) {
		Map<String, Object> map = new HashMap<>();
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(LoginDetails.class);
			if (phoneOrWeb == 2) {
				criteria.add(Restrictions.eq("email", email));
			} else if (phoneOrWeb == 1) {
				criteria.add(Restrictions.eq("phoneNo", email));
			}
			criteria.add(Restrictions.eq("roleId", role));
			// criteria.add(Restrictions.eq("status", ACTIVE));

			LoginDetails user = (LoginDetails) criteria.uniqueResult();
			if (user == null) {
				map.put(ERROR_CODE, -4);
				map.put(ERROR_MESSAGE, "Invalid mail");
				map.put(ERROR_MESSAGE_ARABIC,"معرف البريد الإلكتروني غير صالح" );
			} else {
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				map.put(DATA, user);
			}
		} catch (HibernateException e) {
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map.put(ERROR_CODE, -5);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		return map;
	}

	@Override
	public Map<String, Object> saveOrUpdateSellerDetails(SellerDetails sellerDetails, LoginDetails login)

	{

		Map<String, Object> map = null;

		try {
			map = new HashMap<>();
			Session session = openDBsession();
			String qurey = "select um.userId from LoginDetails um where um.phoneNo=:pNo or um.email=:eMail";
			Query qry = session.createQuery(qurey);
			qry.setParameter("pNo", sellerDetails.getPhoneNumber());
			qry.setParameter("eMail", login.getEmail());
			List<Long> logId = (List<Long>) qry.list();
			if (sellerDetails.getSellerId() > 0) {
				qurey = "select um.userId from SellerDetails um where um.sellerId=:cid";
				qry = session.createQuery(qurey);
				qry.setParameter("cid", sellerDetails.getSellerId());
				List<Long> custId = (List<Long>) qry.list();
				if (custId == null || custId.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such seller exist in seller table");
					map.put(ERROR_MESSAGE_ARABIC, "لا يوجد مثل هذا العميل في جدول العملاء");
					return map;
				}
			}
			if (logId == null || logId.isEmpty()) {

				session.save(login);
				session.flush();
				sellerDetails.setUserId(login.getUserId());
				session.save(sellerDetails);
				login.setEntryId(sellerDetails.getSellerId());
				logger.info(login.getEntryId() + "entryId----------------------------------------");
				session.flush();
			} else if (sellerDetails.getSellerId() > 0) {
				session.update(sellerDetails);
				session.flush();
				login.setUserId(logId.get(0));
				session.update(login);
				session.flush();
			} else {
				map.put(ERROR_CODE, -4);
				map.put(ERROR_MESSAGE, "Already registered phone number or email");
				map.put(ERROR_MESSAGE_ARABIC, "مسجل بالفعل رقم الهاتف أو البريد الإلكتروني");
				return map;
			}
			// closeDBSession();

			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("SELLER", sellerDetails);
		} catch (HibernateException e) {
			// closeDBSession();
			map = new HashMap<>();
			logger.info(e.getMessage());
			if (TESTING) {
				e.printStackTrace();
			}
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		return map;
	}

	public Map<String, Object> saveComapnay(CompanyDetails companyDetails, LoginDetails login) {
		Map<String, Object> map = null;

		try {
			map = new HashMap<>();
			Session session = openDBsession();
			String qurey = "select um.userId from LoginDetails um where um.phoneNo=:pNo or um.email=:eMail";
			Query qry = session.createQuery(qurey);
			qry.setParameter("pNo", companyDetails.getPhone());
			qry.setParameter("eMail", login.getEmail());
			List<Long> logId = (List<Long>) qry.list();
			if (companyDetails.getCompanyId() > 0) {
				qurey = "select um.userId from CompanyDetails um where um.companyId=:cid";
				qry = session.createQuery(qurey);
				qry.setParameter("cid", companyDetails.getCompanyId());
				List<Long> custId = (List<Long>) qry.list();
				if (custId == null || custId.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such company exist in company table");
					map.put(ERROR_MESSAGE_ARABIC, "لا يوجد مثل هذا العميل في جدول العملاء");
					return map;
				}
			}
			if (logId == null || logId.isEmpty()) {
				session.save(companyDetails);
				session.flush();
				login.setCompanyId(companyDetails.getCompanyId());
				session.save(login);
				session.flush();

			} else if (companyDetails.getCompanyId() > 0) {
				session.update(companyDetails);
				session.flush();
				login.setUserId(logId.get(0));
				session.update(login);
				session.flush();
			} else {
				map.put(ERROR_CODE, -4);
				map.put(ERROR_MESSAGE, "Already registered phone number or email");
				map.put(ERROR_MESSAGE_ARABIC, "مسجل بالفعل رقم الهاتف أو البريد الإلكتروني");
				return map;
			}
			// closeDBSession();

			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("COMPANY", companyDetails);
		} catch (HibernateException e) {
			// closeDBSession();
			map = new HashMap<>();
			logger.info(e.getMessage());
			if (TESTING) {
				e.printStackTrace();
			}
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		return map;
	}

	@Override
	public CustomerDetails getCustomerFrom(String userName, String phoneNo) {
		List<CustomerDetails> userList;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(CustomerDetails.class);
			criteria.add(Restrictions.eq("customerName", userName));
			criteria.add(Restrictions.eq("phoneNo", phoneNo));
			userList = criteria.list();
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			throw e;
		}
		if (userList == null || userList.isEmpty()) {
			return null;
		}
		return userList.get(0);
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public UserMaster getLoginInfo(String email) {
	 * 
	 * DetachedCriteria detCriteria = DetachedCriteria .forClass(UserMaster.class);
	 * detCriteria.add(Restrictions.eq("email", email)); List<UserMaster> loginList
	 * = (List<UserMaster>) hibernateTemplate .findByCriteria(detCriteria);
	 * logger.info(loginList+"qqqqqqqqq");
	 * 
	 * if (loginList != null && !loginList.isEmpty()) {
	 * 
	 * return loginList.get(0);
	 * 
	 * } else { return null; }
	 * 
	 * }
	 */

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public UserMaster getUserById(long userId) {
	 * 
	 * DetachedCriteria detCriteria = DetachedCriteria .forClass(UserMaster.class);
	 * detCriteria.add(Restrictions.eq("userId", userId)); List<UserMaster>
	 * loginList = (List<UserMaster>) hibernateTemplate
	 * .findByCriteria(detCriteria);
	 * 
	 * if (loginList != null && !loginList.isEmpty()) {
	 * 
	 * logger.info(loginList); return loginList.get(0);
	 * 
	 * } else { return null; } }
	 */

	/*
	 * @Override public Map<String,Object> saveOrUpdateUser(UserMaster userMaster) {
	 * Map<String, Object> map = new HashMap<>(); if (userMaster != null) { try {
	 * openDBsession(); logger.info(userMaster.getUserId()); String
	 * qurey="select userId from UserMaster um where um.userName=:uName or um.phoneNo=:pNo or um.email=:eMail"
	 * ; Query qry=session.createQuery(qurey); qry.setParameter("uName",
	 * userMaster.getUserName()); qry.setParameter("pNo", userMaster.getPhoneNo());
	 * qry.setParameter("eMail", userMaster.getEmail()); Long userId=(Long)
	 * qry.uniqueResult(); session.flush();
	 * logger.info("get user id is"+userMaster.getUserId()); if(userId == null){
	 * session.save(userMaster); session.flush(); }else if(userId ==
	 * userMaster.getUserId()){ session.update(userMaster); session.flush(); }else{
	 * map.put(ERROR_MESSAGE,
	 * "DUBLICATE DATA USERNAME,PHONE NUMBER AND EMAIL SHOULD BE UNIQUE");
	 * map.put(ERROR_CODE, -7); return map; }
	 * 
	 * //closeDBSession(); map.put(ERROR_MESSAGE, "Success"); map.put(ERROR_CODE,
	 * ERROR_CODE_NO_ERROR); return map;
	 * 
	 * } catch (HibernateException e) { //closeDBSession(); if(TESTING){
	 * e.printStackTrace(); } logger.error(e.getMessage()); map.put(ERROR_MESSAGE,
	 * e.getMessage()); map.put(ERROR_CODE, -3); return map;
	 * 
	 * } }
	 * 
	 * return null; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllPromoters(long customerId, long cityId, long countryId, long userId, int status,
			long fromDate, long toDate, int currentIndex, int rowPerPage, long companyId, String customerName,
			String emailid, String phoneNo, String customerNameAr) {
		Map<String, Object> map = null;
		try {
			Session session = openDBsession();
			/*
			 * long currTime=TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);
			 * StringBuffer sBuffer1 = new
			 * StringBuffer("update PROMOCODE_DETAILS   set status=0 where valid_To <= :currTime  or  valid_From >= :currTime"
			 * ); SQLQuery qry1 = session.createSQLQuery(sBuffer1.toString());
			 * qry1.setParameter("currTime", currTime); int r=qry1.executeUpdate();
			 * logger.info(r+"__________________________________r");
			 */

			// Criteria criteria = session.createCriteria(CustomerDetails.class);
			String statData = " and (cd.status=1 or cd.status=0)";
			if (status == 1 || status == 0) {
				statData = " and cd.status=" + status;
			}
//			StringBuffer sBuffer = new StringBuffer(
//					"select cd.*,l.CITY cityId,l.COUNTRY countryId,COALESCE(sum(case when ot.ORDER_STATUS = 2 then ot.netPrice else 0 end),0) as sumofprice from CS_CUSTOMER_DETAILS cd inner join LOGIN l on l.USER_ID=cd.USER_ID left join ORDER_TABLE ot on ot.customer_id=cd.USER_ID"
//							+ statData);
			StringBuffer sBuffer = new StringBuffer(
					"select  cd.* from CS_CUSTOMER_DETAILS cd inner join LOGIN l on l.USER_ID=cd.USER_ID where l.ROLE=54 "+ statData);
			logger.info(sBuffer + "sBuffer__________________________sBuffer__________________");

			if (cityId > 0) {
				sBuffer.append(" and l.CITY=" + cityId);
			}
			if (countryId > 0) {
				sBuffer.append(" and l.COUNTRY=" + countryId);
			}
			if (userId > 0) {

				sBuffer.append(" and l.USER_ID=" + userId);
			}
			if (customerId > 0) {
				sBuffer.append(" and cd.CUSTOMER_ID=" + customerId);
			}
			if (companyId > 0) {
				sBuffer.append(" and cd.COMPANY_ID=" + companyId);
			}

			/*
			 * if (customerName != null && !customerName.equals("")) {
			 * logger.info(customerName +
			 * "proudctName_________________________________________");
			 * 
			 * sBuffer.append(" and");
			 * 
			 * sBuffer.append(" lower(cd.CUSTOMER_NAME) like '%" +
			 * customerName.toLowerCase() + "%'"); logger.info(sBuffer +
			 * "_______________________________________________");
			 * criteria.add(Restrictions.ilike("customerName", "%" + customerName + "%"));
			 * 
			 * } if (phoneNo != null && !phoneNo.equals("")) { logger.info(phoneNo +
			 * "proudctName_________________________________________");
			 * 
			 * sBuffer.append(" and");
			 * 
			 * sBuffer.append(" lower(cd.PHONE_NO) like '%" + phoneNo.toLowerCase() + "%'");
			 * logger.info(sBuffer + "_______________________________________________");
			 * criteria.add(Restrictions.ilike("phoneNo", "%" + phoneNo + "%"));
			 * 
			 * }
			 * 
			 * if (emailid != null && !emailid.equals("")) { logger.info(emailid +
			 * "proudctName_________________________________________");
			 * 
			 * sBuffer.append(" and");
			 * 
			 * sBuffer.append(" lower(cd.EMAIL_ID) like '%" + emailid.toLowerCase() + "%'");
			 * logger.info(sBuffer + "_______________________________________________");
			 * criteria.add(Restrictions.ilike("emailid", "%" + emailid + "%"));
			 * 
			 * }
			 * 
			 * if (customerNameAr != null && !customerNameAr.equals("")) {
			 * logger.info(customerNameAr +
			 * "customerNameAr________________________________________");
			 * 
			 * sBuffer.append(" and");
			 * 
			 * sBuffer.append(" lower(cd.CUSTOMER_NAME_Ar) like '%" +
			 * customerNameAr.toLowerCase() + "%'"); logger.info(sBuffer +
			 * "_______________________________________________");
			 * criteria.add(Restrictions.ilike("customerNameAr", "%" + customerNameAr +
			 * "%"));
			 * 
			 * }
			 */

			logger.info(sBuffer.toString());
			SQLQuery qry = session.createSQLQuery(sBuffer.toString());
			logger.info(qry + "qry__________________________qryr__________________");
			if (rowPerPage > 0) {
				qry.setFirstResult(currentIndex);
				qry.setMaxResults(rowPerPage);
			}
			qry.addEntity("cd", CustomerDetails.class);

			qry.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List<Object[]> obj = qry.list();
			logger.info(obj + "obj-------------------------------------------------------------------8888");
			session.flush();
			if (obj != null && !obj.isEmpty()) {
				map = new HashMap<>();
				logger.info(obj.size()
						+ "size of obj _______________________________________________________________________________________");
				map.put("customerList", obj);
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			} else {
				map = new HashMap<>();
				map.put(ERROR_CODE, -2);
				map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			}

			sBuffer.replace(8, 12, " COUNT(*) OVER () AS TotalRecords");
			// strBfr.replace(109, strBfr.length(), "");
			logger.info(sBuffer.toString());
			qry = session.createSQLQuery(sBuffer.toString()).addScalar("TotalRecords", LongType.INSTANCE);
			Long totalCounts = (Long) qry.uniqueResult();
			if (totalCounts == null) {
				totalCounts = 0l;
			}
			logger.info(totalCounts + "totalCounts-----------------------------------------3333");
			map.put(TOTAL_COUNT, totalCounts);
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -6);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllCustomers(long customerId, long userId, long fromDate, long toDate, long cityId,
			long countryId, int status, int currentIndex, int rowPerPage, long companyId, String customerName,
			String emailid, String phoneNo, String customerNameAr) {
		Map<String, Object> map = null;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(CustomerDetails.class);
			String statData = " where (cd.status=1 or cd.status=0)";
			if (status == 1 || status == 0) {
				statData = " where cd.status=" + status;
			}
			StringBuffer sBuffer = new StringBuffer(
					"select cd.*,l.CITY cityId,l.COUNTRY countryId,COALESCE(sum(case when ot.ORDER_STATUS = 4 then ot.netPrice else 0 end),0) as sumofprice from CS_CUSTOMER_DETAILS cd inner join LOGIN l on l.USER_ID=cd.USER_ID left join ORDER_TABLE ot on ot.customer_id=cd.USER_ID"
							+ statData);
			if (cityId > 0) {
				sBuffer.append(" and l.CITY=" + cityId);
			}
			if (countryId > 0) {
				sBuffer.append(" and l.COUNTRY=" + countryId);
			}
			if (userId > 0) {

				sBuffer.append(" and l.USER_ID=" + userId);
			}
			if (customerId > 0) {
				sBuffer.append(" and cd.CUSTOMER_ID=" + customerId);
			}
			if (companyId > 0) {
				sBuffer.append(" and cd.COMPANY_ID=" + companyId);
			}

			if (customerName != null && !customerName.equals("")) {
				logger.info(customerName + "proudctName_________________________________________");

				sBuffer.append(" and");

				sBuffer.append(" lower(cd.CUSTOMER_NAME) like '%" + customerName.toLowerCase() + "%'");
				logger.info(sBuffer + "_______________________________________________");
				criteria.add(Restrictions.ilike("customerName", "%" + customerName + "%"));

			}
			if (phoneNo != null && !phoneNo.equals("")) {
				logger.info(phoneNo + "proudctName_________________________________________");

				sBuffer.append(" and");

				sBuffer.append(" lower(cd.PHONE_NO) like '%" + phoneNo.toLowerCase() + "%'");
				logger.info(sBuffer + "_______________________________________________");
				criteria.add(Restrictions.ilike("phoneNo", "%" + phoneNo + "%"));

			}

			if (emailid != null && !emailid.equals("")) {
				logger.info(emailid + "proudctName_________________________________________");

				sBuffer.append(" and");

				sBuffer.append(" lower(cd.EMAIL_ID) like '%" + emailid.toLowerCase() + "%'");
				logger.info(sBuffer + "_______________________________________________");
				criteria.add(Restrictions.ilike("emailid", "%" + emailid + "%"));

			}

			if (customerNameAr != null && !customerNameAr.equals("")) {
				logger.info(customerNameAr + "customerNameAr________________________________________");

				sBuffer.append(" and");

				sBuffer.append(" lower(cd.CUSTOMER_NAME_Ar) like '%" + customerNameAr.toLowerCase() + "%'");
				logger.info(sBuffer + "_______________________________________________");
				criteria.add(Restrictions.ilike("customerNameAr", "%" + customerNameAr + "%"));

			}
			if (fromDate > 0) {
				if (toDate == 0) {
					toDate = TimeUtils.instance().getEndTimeOfGiveTime(fromDate);
				}
				sBuffer.append(" and cd.CREATED_DATE between " + fromDate + " and " + toDate);
			}
			sBuffer.append(" group by cd.CUSTOMER_ID,l.USER_ID order by cd.CREATED_DATE desc");
			logger.info(sBuffer.toString());
			SQLQuery qry = session.createSQLQuery(sBuffer.toString());
			if (rowPerPage > 0) {
				qry.setFirstResult(currentIndex);
				qry.setMaxResults(rowPerPage);
			}
			qry.addScalar("sumofprice", DoubleType.INSTANCE).addEntity("cd", CustomerDetails.class)
					.addScalar("cityId", LongType.INSTANCE).addScalar("countryId", LongType.INSTANCE);
			// qry.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List<Object[]> obj = qry.list();
			session.flush();
			if (obj != null && !obj.isEmpty()) {
				map = new HashMap<>();
				logger.info(obj);
				map.put("customerList", obj);
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			} else {
				map = new HashMap<>();
				map.put(ERROR_CODE, -2);
				map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			}

			sBuffer.replace(7, 134, " COUNT(*) OVER () AS TotalRecords");
			// strBfr.replace(109, strBfr.length(), "");
			logger.info(sBuffer.toString());
			qry = session.createSQLQuery(sBuffer.toString()).addScalar("TotalRecords", LongType.INSTANCE);
			
			 long  totalCounts = (long) qry.uniqueResult(); 
			 logger.info(totalCounts+"totalCountstotalCountstotalCountstotalCountstotalCounts");
			 if(totalCounts == 0) 
			 {
			 totalCounts=0;
			 }
			 
			map.put("TOTALCOUNT", totalCounts);
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -6);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> aunthenticate(String userName, String password, int role) {
		Map<String, Object> map = null;
		Session session = openDBsession();
		Criteria detCriteria = session.createCriteria(LoginDetails.class);
		logger.info(userName);
		if (Validation.instance().emailValidator(userName)) {
			detCriteria.add(Restrictions.eq("email", userName).ignoreCase());
		} else {
			detCriteria.add(Restrictions.eq("phoneNo", userName));
		}
		detCriteria.add(Restrictions.eq("password", password));
		detCriteria.add(Restrictions.eq("roleId", role));
		detCriteria.add(Restrictions.eq("status", ACTIVE));
		detCriteria.add(Restrictions.eq("emailVerified", VERIFIED_EMAIL));
		List<LoginDetails> loginList = (List<LoginDetails>) detCriteria.list();

		if (loginList == null || loginList.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, "Invalid phone number,email or password");
			map.put(ERROR_MESSAGE_ARABIC,"رقم الهاتف غير صحيح أو البريد الإلكتروني أو كلمة المرور");
			return map;
		} else {
			map = new HashMap<>();
			map.put("loggedUser", loginList.get(0));
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomerDetails getCustomerFromId(long customerId) {
		logger.info(customerId + "userId");
		DetachedCriteria detCriteria = DetachedCriteria.forClass(CustomerDetails.class);
		detCriteria.add(Restrictions.eq("userId", customerId));
		List<CustomerDetails> loginList = (List<CustomerDetails>) hibernateTemplate.findByCriteria(detCriteria);

		if (loginList != null && !loginList.isEmpty()) {

			return loginList.get(0);

		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public TaxManagement getTax(long taxId) {
		DetachedCriteria detCriteria = DetachedCriteria.forClass(TaxManagement.class);
		detCriteria.add(Restrictions.eq("taxId", taxId));
		List<TaxManagement> loginList = (List<TaxManagement>) hibernateTemplate.findByCriteria(detCriteria);

		if (loginList != null && !loginList.isEmpty()) {

			logger.info(loginList);
			return loginList.get(0);

		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Offer getOffer(long offerId) {
		DetachedCriteria detCriteria = DetachedCriteria.forClass(Offer.class);
		detCriteria.add(Restrictions.eq("offerId", offerId));
		List<Offer> loginList = (List<Offer>) hibernateTemplate.findByCriteria(detCriteria);

		if (loginList != null && !loginList.isEmpty()) {

			logger.info(loginList);
			return loginList.get(0);

		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public SellerDetails getSellerFromId(long customerId) {
		DetachedCriteria detCriteria = DetachedCriteria.forClass(SellerDetails.class);
		detCriteria.add(Restrictions.eq("sellerId", customerId));
		List<SellerDetails> loginList = (List<SellerDetails>) hibernateTemplate.findByCriteria(detCriteria);

		if (loginList != null && !loginList.isEmpty()) {

			logger.info(loginList);
			return loginList.get(0);

		} else {
			return null;
		}
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> getAllUsers(long userId) { Map<String,
	 * Object> map =null; DetachedCriteria detCriteria = DetachedCriteria
	 * .forClass(LoginDetails.class); if(userId > 0){
	 * detCriteria.add(Restrictions.eq("userId", userId)); } List<LoginDetails>
	 * loginList = (List<LoginDetails>) hibernateTemplate
	 * .findByCriteria(detCriteria);
	 * 
	 * if (loginList == null || loginList.isEmpty()) {
	 * 
	 * map=new HashMap<>(); map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE,
	 * MESSAGE_NOT_FOUND); return map; }else{ map=new HashMap<>();
	 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put("userList", loginList); return map; } }
	 */

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String,Object> getAllCustomers(long customerId,long
	 * cityId,long userId,long fromDate,long toDate,long countryId,int orderBy,int
	 * currentIndex,int rowPerPage,int status) { Map<String, Object> map = null; try
	 * { Session session = openDBsession(); String
	 * statData=" where (s.status=1 or s.status=0)"; if(status == 1 || status == 0)
	 * { statData=" where s.status="+status; } StringBuffer sBuffer = new
	 * StringBuffer(
	 * "select cd.*,l.CITY cityId,l.COUNTRY countryId,COALESCE(sum(case when ot.ORDER_STATUS = 2 then ot.netPrice else 0 end),0) as sumofprice from JW_CUSTOMER_DETAILS cd inner join LOGIN l on l.USER_ID=cd.USER_ID left join ORDER_TABLE ot on ot.customer_id=cd.USER_ID"
	 * +statData); if (cityId > 0) { sBuffer.append(" and l.CITY=" + cityId); } if
	 * (countryId > 0) { sBuffer.append(" and l.COUNTRY=" + countryId); } if (userId
	 * > 0) { sBuffer.append(" and l.USER_ID=" + userId); } if (customerId > 0) {
	 * sBuffer.append(" and cd.CUSTOMER_ID=" + customerId); } if (fromDate > 0) { if
	 * (toDate == 0) { toDate = TimeUtils.instance().getEndTimeOfGiveTime(fromDate);
	 * } sBuffer.append(" and cd.CREATED_DATE between " + fromDate + " and " +
	 * toDate); } sBuffer.
	 * append(" group by cd.CUSTOMER_ID,l.USER_ID order by cd.CREATED_DATE desc");
	 * logger.info(sBuffer.toString()); SQLQuery qry =
	 * session.createSQLQuery(sBuffer.toString()); if (rowPerPage > 0) {
	 * qry.setFirstResult(currentIndex); qry.setMaxResults(rowPerPage); }
	 * qry.addScalar("sumofprice", DoubleType.INSTANCE).addEntity("cd",
	 * CustomerDetails.class) .addScalar("cityId",
	 * LongType.INSTANCE).addScalar("countryId", LongType.INSTANCE); //
	 * qry.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); List<Object[]> obj =
	 * qry.list(); session.flush(); if (obj != null && !obj.isEmpty()) { map = new
	 * HashMap<>(); logger.info(obj); map.put("customerList", obj);
	 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); } else { map = new HashMap<>(); map.put(ERROR_CODE, -2);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW); }
	 * 
	 * sBuffer.replace(7, 134, "DISTINCT COUNT(*) OVER () AS TotalRecords"); //
	 * strBfr.replace(109, strBfr.length(), ""); logger.info(sBuffer.toString());
	 * qry = session.createSQLQuery(sBuffer.toString()).addScalar("TotalRecords",
	 * LongType.INSTANCE); Long totalCounts = (Long) qry.uniqueResult();
	 * if(totalCounts == null) { totalCounts=0l; } map.put(TOTAL_COUNT,
	 * totalCounts); // closeDBSession(); } catch (HibernateException e) { //
	 * closeDBSession(); if (TESTING) { e.printStackTrace(); }
	 * logger.info(e.getMessage()); map = new HashMap<>(); map.put(ERROR_CODE, -6);
	 * map.put(ERROR_MESSAGE, e.getMessage()); throw e; } return map; }
	 */
	/*
	 * @Override public Map<String, Object> activateAccount(long loginId, int role)
	 * { Map<String, Object> map=null; try { Session session=openDBsession(); String
	 * qry="update LoginDetails l set l.status=:stat,l.emailVerified=:emVerify where l.userId=:logId"
	 * ; Query query=session.createQuery(qry); query.setParameter("stat", ACTIVE);
	 * query.setParameter("emVerify",VERIFIED_EMAIL); query.setParameter("logId",
	 * loginId); int result = query.executeUpdate(); if(result > 0) { map=new
	 * HashMap<>(); map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC,MESSAGE_SUCSESS_AR); return
	 * map; }else { map=new HashMap<>(); map.put(ERROR_CODE, -1);
	 * map.put(ERROR_MESSAGE, MESSAGE_NOT_FOUND);
	 * map.put(ERROR_MESSAGE_ARABIC,MESSAGE_NOT_FOUND_AR); return map; } } catch
	 * (Exception e) { e.printStackTrace(); logger.info(e.getMessage()); throw e; }
	 * }
	 */
	@Override
	public Map<String, Object> validateCustomer(long loginId, int role, int status) {
		Map<String, Object> map = null;
		Session session = openDBsession();
		String qry = "update LoginDetails l set l.validate=:stat,l.emailVerified=:emVerify where l.userId=:logId and l.roleId=:rol";
		Query query = session.createQuery(qry);
		query.setParameter("stat", status);
		query.setParameter("emVerify", VERIFIED_EMAIL);
		query.setParameter("logId", loginId);
		query.setParameter("rol", role);
		int result = query.executeUpdate();
		if (result > 0) {
			if (role == ROLL_ID_CUSTOMER) 
			{
				
				qry = "update CustomerDetails set validate=:stat where userId=:useId";
			} else if (role == ROLL_ID_PROMOTER) {
				qry = "update CustomerDetails set status=:stat where userId=:useId";
			}
			// if(role != ROLL_ID_ADMIN_USER)
			// {
			query = session.createQuery(qry.toString());
			query.setParameter("stat", ACTIVE);
			query.setParameter("useId", loginId);
			result = query.executeUpdate();
			if (result <= 0) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NOT_FOUND_AR);
				return map;
			}
			// }
			map = new HashMap<>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_NOT_FOUND);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NOT_FOUND_AR);
			return map;
		}

	}

	@Override
	public Map<String, Object> activateAccount(long loginId, int role, int status) {
		Map<String, Object> map = null;	
		
		List<LoginDetails> userList=null;
			
			Session session = openDBsession();
			String qry = "update LoginDetails l set l.validate=:stat,l.emailVerified=:emVerify where l.userId=:logId and l.roleId=:rol";
			Query query = session.createQuery(qry);
			query.setParameter("stat", status);
			query.setParameter("emVerify", VERIFIED_EMAIL);
			query.setParameter("logId", loginId);
			query.setParameter("rol", role);
			int result = query.executeUpdate();
			if (result > 0) 
			{
				Criteria criteria=session.createCriteria(LoginDetails.class);
				criteria.add(Restrictions.eq("userId", loginId));
				
				userList = criteria.list();
				logger.info(userList+"userListuserListuserListuserListuserListuserListuserList");
				
				if(role == ROLL_ID_CUSTOMER) 
				{
					qry="update CustomerDetails set validate=:stat where userId=:useId";
				}else if(role == ROLL_ID_PROMOTER) 
				{
					qry="update CustomerDetails set validate=:stat where userId=:useId";
				}
				//if(role != ROLL_ID_ADMIN_USER) 
				//{
					query=session.createQuery(qry.toString());
					query.setParameter("stat", status);
					query.setParameter("useId",loginId);
					result=query.executeUpdate();
					if(result <= 0) 
					{
						map = new HashMap<>();
						map.put(ERROR_CODE, -1);
						map.put(ERROR_MESSAGE, MESSAGE_NOT_FOUND);
						map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NOT_FOUND_AR);
						return map;
					}
				//}
				map = new HashMap<>();
				logger.info(userList+"userListuserListuserListuserListuserListuserListuserList");
				map.put("UserList", userList);
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				return map;
			} else {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NOT_FOUND_AR);
				return map;
			}
	}

	/*
	 * @Override public Map<String, Object> changePassword(String oldPass, String
	 * newPass, long userId) { Map<String, Object> map=null; try { Session
	 * session=openDBsession(); String
	 * qry="select um.userId from LoginDetails um where um.userId=:useId"; Query
	 * query=session.createQuery(qry); query.setParameter("useId", userId);
	 * List<Long> logId=(List<Long>) query.list(); if(logId == null ||
	 * logId.isEmpty()) { map=new HashMap<>(); map.put(ERROR_CODE, -5);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_SUCH_USER);
	 * map.put(ERROR_MESSAGE_ARABIC,"لا يوجد مثل هذا المستخدم"); return map; }
	 * qry="update LoginDetails l set l.password=:newp where l.userId=:userId and l.password=:oldp"
	 * ; query=session.createQuery(qry); query.setParameter("newp", newPass);
	 * query.setParameter("oldp", oldPass); query.setParameter("userId", userId);
	 * int result = query.executeUpdate(); if(result > 0) { map=new HashMap<>();
	 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC,MESSAGE_SUCSESS_AR); return
	 * map; }else { map=new HashMap<>(); map.put(ERROR_CODE, -1);
	 * map.put(ERROR_MESSAGE, "given details mistmach");
	 * map.put(ERROR_MESSAGE_ARABIC,MESSAGE_NOT_FOUND_AR); return map; } } catch
	 * (Exception e) { logger.info(e.getMessage()); e.printStackTrace(); throw e; }
	 * }
	 */

	@Override
	public Map<String, Object> changePassword(String oldPass, String newPass, long userId) {
		Map<String, Object> map = null;
		Session session = openDBsession();
		String qry = "update LoginDetails l set l.password=:newp where l.userId=:logId and l.password=:oldp";
		Query query = session.createQuery(qry);
		query.setParameter("newp", newPass);
		query.setParameter("oldp", oldPass);
		query.setParameter("logId", userId);
		
		int result = query.executeUpdate();
		
		qry = "select um from LoginDetails um where um.userId=:logId";
		query = session.createQuery(qry);
		query.setParameter("logId", userId);
	LoginDetails login = (LoginDetails) query.uniqueResult();
		logger.info(login+"loginloginloginloginloginloginloginlogin");
		if (result > 0) 
		{
			map = new HashMap<>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("LOGIN", login);
			return map;
		} else 
		{
			
			
			map = new HashMap<>();
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_NOT_FOUND);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NOT_FOUND_AR);
			return map;
		}
	}

	@Override
	public Map<String, Object> saveAdmin(LoginDetails login, SellerDetails sellerDetails, LoginDetails login2,
			List<Attributes> attriList) {
		Map<String, Object> map = null;
		try {
			Session session = openDBsession();
			String qurey = "select um.userId from LoginDetails um where um.roleId=:pNo";
			Query qry = session.createQuery(qurey);
			qry.setParameter("pNo", ROLL_ID_ADMIN);
			Long logId = (Long) qry.uniqueResult();
			session.flush();
			if (logId == null) {
				try {
					session.save(login);
					session.save(login2);
					sellerDetails.setUserId(login2.getUserId());
					session.save(sellerDetails);
					String q = "select sellerId from SellerDetails where userId=:userId";
					Query qry1 = session.createQuery(q);
					qry1.setParameter("userId", login2.getUserId());
					long s = (long) qry1.uniqueResult();
					String q1 = "update LoginDetails set  entryId=:entryId  where userId=:userId";
					Query qry12 = session.createQuery(q1);
					qry12.setParameter("userId", login2.getUserId());
					qry12.setParameter("entryId", s);
					qry12.executeUpdate();
					session.flush();

					for (Attributes attri : attriList) {
						logger.info(attri);
						session.save(attri);
						session.flush();
					}
				} catch (HibernateException e) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -2);
					map.put(ERROR_MESSAGE, e.getMessage());
					e.printStackTrace();
					return map;
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			throw e;
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, "SUCCESSFULLY ACTIVATE ADMIN AS SELLER ALSO");
		map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		return map;
	}

	@Override
	public Map<String, Object> getGeneralDatas() {
		Map<String, Object> map = null;
		try {
			Session session = openDBsession();
			Map<String, Long> dataMap = new HashMap<>();
			String str = "select count(*) from Shop um where um.status=1";
			Query query = session.createQuery(str);
			long count = (long) query.uniqueResult();
			dataMap.put("stores", count);
			session.flush();
			str = "select count(*) from CustomerDetails cd where cd.status=1";
			query = session.createQuery(str);
			count = (long) query.uniqueResult();
			dataMap.put("customers", count);
			session.flush();
			str = "select count(*) from Order bd where bd.status=1";
			query = session.createQuery(str);
			count = (long) query.uniqueResult();
			dataMap.put("orders", count);
			session.flush();
			str = "select count(*) from Auction bd where bd.status=1";
			query = session.createQuery(str);
			count = (long) query.uniqueResult();
			dataMap.put("auctions", count);
			session.flush();
			map = new HashMap<>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put("DATA", dataMap);
		} catch (HibernateException e) {
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		return map;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> saveOrUpdateDeliveryBoy(DeliveryBoy
	 * deliveryBoy,LoginDetails login)
	 * 
	 * {
	 * 
	 * Map<String, Object> map = null;
	 * 
	 * try { map=new HashMap<>(); Session session=openDBsession(); String
	 * qurey="select um.userId from LoginDetails um where um.phoneNo=:pNo or um.email=:eMail"
	 * ; Query qry=session.createQuery(qurey); qry.setParameter("pNo",
	 * deliveryBoy.getPhoneNumber()); qry.setParameter("eMail", login.getEmail());
	 * List<Long> logId=(List<Long>) qry.list(); if(deliveryBoy.getDeliveryBoyId()>
	 * 0) {
	 * qurey="select um.userId from DeliveryBoy um where um.deliveryBoyId=:cid";
	 * qry=session.createQuery(qurey); qry.setParameter("cid",
	 * deliveryBoy.getDeliveryBoyId()); List<Long> custId=(List<Long>) qry.list();
	 * if(custId == null || custId.isEmpty()) { map.put(ERROR_CODE, -4);
	 * map.put(ERROR_MESSAGE, "No such seller exist in seller table");
	 * map.put(ERROR_MESSAGE_ARABIC,"لا يوجد مثل هذا العميل في جدول العملاء");
	 * return map; } } if(logId == null || logId.isEmpty()){ session.save(login);
	 * session.flush(); deliveryBoy.setUserId(login.getUserId());
	 * session.save(deliveryBoy); session.flush(); }else
	 * if(deliveryBoy.getDeliveryBoyId()> 0){ session.update(deliveryBoy);
	 * session.flush(); login.setUserId(logId.get(0)); session.update(login);
	 * session.flush(); }else{ map.put(ERROR_CODE, -4); map.put(ERROR_MESSAGE,
	 * "Already registered phone number or email");
	 * map.put(ERROR_MESSAGE_ARABIC,"مسجل بالفعل رقم الهاتف أو البريد الإلكتروني");
	 * return map; } //closeDBSession();
	 * 
	 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC,MESSAGE_SUCSESS_AR);
	 * map.put("DELIVERYBOY", deliveryBoy); } catch (HibernateException e) {
	 * //closeDBSession(); map=new HashMap<>(); logger.info(e.getMessage());
	 * if(TESTING){ e.printStackTrace(); } map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, e.getMessage()); throw e; } return map; }
	 */

	/*
	 * @Override public Map<String, Object> sellerLogin(String email,String
	 * password) {
	 * 
	 * Map<String,Object> map = null; try { SellerDetails sellerDetails=new
	 * SellerDetails();
	 * 
	 * LoginDetails login=new LoginDetails(); map=new HashMap<>(); Session
	 * session=openDBsession(); String
	 * qurey="select um.sellerId from SellerDetails um where um.password=:pwd and um.email=:email"
	 * ; Query qry=session.createQuery(qurey); qry.setParameter("pwd",password);
	 * qry.setParameter("email",email); List<Long> logId=(List<Long>) qry.list();
	 * 
	 * if(logId == null || logId.isEmpty()) { map.put(ERROR_CODE, -4);
	 * map.put(ERROR_MESSAGE, "you are not a registred seller");
	 * map.put(ERROR_MESSAGE_ARABIC,"مسجل بالفعل رقم الهاتف أو البريد الإلكتروني");
	 * return map; }
	 * 
	 * else { qurey="select um.userId from LoginDetails um where um.email=:email";
	 * qry=session.createQuery(qurey); qry.setParameter("email",email); List<Long>
	 * log=(List<Long>) qry.list(); if(log== null || log.isEmpty()) {
	 * login.setEmail(email); login.setPassword(password);
	 * login.setRole(ROLL_ID_SHOP);
	 * login.setLast_updated_time(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); login.setEmailVerified(1); session.save(login);
	 * session.flush();
	 * 
	 * } else { login.setLast_updated_time(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); } map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
	 * map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
	 * map.put(ERROR_MESSAGE_ARABIC,MESSAGE_SUCSESS_AR); map.put("SELLER",
	 * sellerDetails);
	 * 
	 * }
	 * 
	 * } catch (HibernateException e) { //closeDBSession(); map=new HashMap<>();
	 * logger.info(e.getMessage()); if(TESTING){ e.printStackTrace(); }
	 * map.put(ERROR_CODE, -3); map.put(ERROR_MESSAGE, e.getMessage()); throw e; }
	 * return map; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllDeliveryBoy(String address, long deliveryBoyId, String deliveryBoyName,
			int firstIndex, int rowPerPage, int status[], String phoneNumber) {
		Map<String, Object> map = null;
		List<DeliveryBoy> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(DeliveryBoy.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from DeliveryBoy o");

			if (deliveryBoyId > 0) {
				flag = false;
				strBfr.append(" where o.deliveryBoyId=" + deliveryBoyId);
				criteria.add(Restrictions.eq("deliveryBoyId", deliveryBoyId));
			}
			if (phoneNumber.length() > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.phoneNumber=" + phoneNumber);
				criteria.add(Restrictions.eq("phoneNumber", phoneNumber));
			}

			if (address.length() > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.address=" + address);
				criteria.add(Restrictions.eq("address", address));
			}

			if (deliveryBoyName.length() > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.deliveryBoyName=" + deliveryBoyName);
				criteria.add(Restrictions.eq("deliveryBoyName", deliveryBoyName));
			}

			if (flag == true) {
				strBfr.replace(28, strBfr.length(), "");
			}
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(DeliveryBoy.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			/*
			 * if (orderId > 0) { criteria = session.createCriteria(Review.class);
			 * criteria.add(Restrictions.eq("orderId", orderId));
			 * //criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
			 * criteria.list();
			 * 
			 * session.flush(); }
			 */
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");

			map.put("DeliveryBoyList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllNotification(int firstIndex, int rowPerPage, int orderStatus[], int readStatus,
			long userId, long notificationId)

	{
		Map<String, Object> map = null;
		List<NotificationDetails> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(NotificationDetails.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from NotificationDetails o");
			if (notificationId > 0) {
				flag = true;
				strBfr.append(" where o.notificationId=" + notificationId);
				criteria.add(Restrictions.eq("notificationId", notificationId));
			}
			if (readStatus == 1 || readStatus == 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where o.readStatus=" + readStatus);
				} else {
					strBfr.append(" and o.readStatus=" + readStatus);
				}
				criteria.add(Restrictions.eq("readStatus", readStatus));
			}

			if (userId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.userId=" + userId);
				criteria.add(Restrictions.eq("userId", userId));
			}

			List<Integer> statAry = null;
			if (orderStatus != null && orderStatus.length > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				Object[] objAr = new Object[orderStatus.length];
				statAry = new ArrayList<>();
				for (int i = 0; i < orderStatus.length; i++) {
					objAr[i] = orderStatus[i];

					logger.info(statAry + "not null---------------------------------()");
					statAry.add(orderStatus[i]);
				}
				criteria.add(Restrictions.in("orderStatus", objAr));

				strBfr.append(" o.orderStatus in (:stat1)");
			}

			Query qry = session.createQuery(strBfr.toString());
			qry.setParameterList("stat1", statAry);
			// qry.setParameterList("stat", statAry1);
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(NotificationDetails.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			/*
			 * if (orderId > 0) { criteria = session.createCriteria(Review.class);
			 * criteria.add(Restrictions.eq("orderId", orderId));
			 * //criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
			 * criteria.list();
			 * 
			 * session.flush(); }
			 */
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");

			map.put("SellerList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllSeller(long sellerId, int firstIndex, int rowPerPage, int stat, String sellerName,
			String phoneNumber, long companyId) {

		Map<String, Object> map = null;
		List<SellerDetails> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(SellerDetails.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from SellerDetails o");
			if (sellerId > 0) {
				flag = true;
				strBfr.append(" where o.sellerId=" + sellerId);
				criteria.add(Restrictions.eq("sellerId", sellerId));
			}
			if (stat == 1 || stat == 0) {
				if (flag == false) {
					strBfr.append(" where o.status=" + stat);
				} else {
					strBfr.append(" and o.status=" + stat);
				}
				criteria.add(Restrictions.eq("status", stat));
			}

			if (companyId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.companyId=" + companyId);
				criteria.add(Restrictions.eq("companyId", companyId));
			}

			if (!sellerName.equals("")) {
				logger.info(sellerName + "___________________________________________");
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" lower(o.sellerName) like '%" + sellerName.toLowerCase() + "%'");
				criteria.add(Restrictions.ilike("sellerName", sellerName));
			}
			if (!phoneNumber.equals("")) {
				logger.info(phoneNumber + "___________________________________________");
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" lower(o.phoneNo) like '%" + phoneNumber.toLowerCase() + "%'");
				criteria.add(Restrictions.ilike("phoneNo", phoneNumber));
			}

			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(SellerDetails.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			/*
			 * if (orderId > 0) { criteria = session.createCriteria(Review.class);
			 * criteria.add(Restrictions.eq("orderId", orderId));
			 * //criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
			 * criteria.list();
			 * 
			 * session.flush(); }
			 */
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");

			map.put("SellerList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@Override
	public Map<String, Object> assignOffer(long offerId, long productId) {
		Map<String, Object> map = null;
		int result = 0;
		try {
			Session session = openDBsession();

			if (offerId == 0) 
			{

				Query qry1 = session.createQuery("update Product set offerId=:offerId where productId=:productId");
				qry1.setParameter("productId", productId);
				qry1.setParameter("offerId", offerId);
				result = qry1.executeUpdate();

				if (result <= 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, "offer cancell failed");
					map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
				} else {
					map = new HashMap<>();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, "offer cancelled ");
					map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				}
			} else {

				DetachedCriteria detachedCriteria2 = DetachedCriteria.forClass(Offer.class);
				detachedCriteria2.add(Restrictions.eq("offerId", offerId));
				List<Offer> dvList = (List<Offer>) hibernateTemplate.findByCriteria(detachedCriteria2);
				if (dvList == null || dvList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, "offerId " + MESSAGE_NOT_FOUND);
					map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
					return map;
				}

				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Product.class);
				detachedCriteria.add(Restrictions.eq("productId", productId));
				List<Product> orderList = (List<Product>) hibernateTemplate.findByCriteria(detachedCriteria);
				if (orderList == null || orderList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, " productId " + MESSAGE_NOT_FOUND);
					map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
					return map;
				}

				Query qry1 = session.createQuery("update Product set offerId=:offerId where productId=:productId");
				qry1.setParameter("productId", productId);
				qry1.setParameter("offerId", offerId);

				result = qry1.executeUpdate();
			}

			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		map = new HashMap<>();
		if (result <= 0) {
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
		} else {
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		}
		return map;
	}

	@Override
	public Map<String, Object> changeDeliveryStatus(long orderId, long deliveryBoyId, int status) {
		Map<String, Object> map = null;// 4 delivered
		int result = 0;
		try {
			if (status == PICKED || status == DELIVERED) {

				Session session = openDBsession();

				DetachedCriteria detachedCriteria2 = DetachedCriteria.forClass(DeliveryBoy.class);
				detachedCriteria2.add(Restrictions.eq("deliveryBoyId", deliveryBoyId));
				List<DeliveryBoy> dvList = (List<DeliveryBoy>) hibernateTemplate.findByCriteria(detachedCriteria2);
				if (dvList == null || dvList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, "deliveryBoyId " + MESSAGE_NOT_FOUND);
					map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
					return map;
				}

				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Order.class);
				detachedCriteria.add(Restrictions.eq("orderId", orderId));
				List<Order> orderList = (List<Order>) hibernateTemplate.findByCriteria(detachedCriteria);
				if (orderList == null || orderList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, "orderId " + MESSAGE_NOT_FOUND);
					map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
					return map;
				}

				Query qry1 = session.createQuery("update Order set status=:status where orderId=:orderId");
				qry1.setParameter("status", status);
				qry1.setParameter("orderId", orderId);
				result = qry1.executeUpdate();
				if (status == 4) {
					Query qry2 = session.createQuery(
							"update DeliveryBoy set isAvailable=:isAvailable where deliveryBoyId=:deliveryBoyId");
					qry2.setParameter("isAvailable", 1);
					qry2.setParameter("deliveryBoyId", deliveryBoyId);
					result = qry2.executeUpdate();
				}

			} else {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "status " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		map = new HashMap<>();
		if (result <= 0) {
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
		} else {
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		}
		return map;
	}

	@Override
	public Map<String, Object> cancellDeriveryBoy(long orderId) {
		Map<String, Object> map = null;
		int result = 0;
		try {
			Session session = openDBsession();

			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Order.class);
			detachedCriteria.add(Restrictions.eq("orderId", orderId));
			List<Order> orderList = (List<Order>) hibernateTemplate.findByCriteria(detachedCriteria);
			if (orderList == null || orderList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "orderId " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}

			Query qry = session.createQuery("update Order set deliveryBoyId=:deliveryBoyId where orderId=:orderId");
			qry.setParameter("orderId", orderId);
			qry.setParameter("deliveryBoyId", CANCELL_DELIVERY);

			result = qry.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		map = new HashMap<>();
		if (result <= 0) {
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
		} else {
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		}
		return map;
	}

	@Override
	public Map<String, Object> getPromocodeDetails(long codeId, long userId, int status, long validFrom, long validTo,
			int currentIndex, int rowPerPage, String promocode) {
		Map<String, Object> map = null;
		List<PromocodeDetails> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(PromocodeDetails.class);
			boolean flag = false;

			long currTime = TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);
			StringBuffer sBuffer1 = new StringBuffer(
					"update PROMOCODE_DETAILS   set status=0 where valid_To <= :currTime");
			SQLQuery qry1 = session.createSQLQuery(sBuffer1.toString());
			qry1.setParameter("currTime", currTime);
			int r = qry1.executeUpdate();
			logger.info(r + "__________________________________r");

	
			StringBuffer strBfr = new StringBuffer("select count(*) from PromocodeDetails o");
			if (codeId > 0) {
				flag = true;
				strBfr.append(" where o.codeId=" + codeId);
				criteria.add(Restrictions.eq("codeId", codeId));
			}
			if (status == 1 || status == 0) {
				if (flag == false) {
					strBfr.append(" where o.status=" + status);
				} else {
					strBfr.append(" and o.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}

			if (userId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" and");
				} else {
					strBfr.append(" where");
				}
				strBfr.append(" o.userId=" + userId);
				criteria.add(Restrictions.eq("userId", userId));
			}

			if (promocode != null && !promocode.equals("")) {
				logger.info(promocode + "promocode_________________________________________");
				if (flag == false) {
					flag = true;
					strBfr.append(" and");

				} else {
					strBfr.append(" and");
				}
				strBfr.append(" lower(o.promocode) like '%" + promocode.toLowerCase() + "%'");
				logger.info(strBfr + "_______________________________________________");
				criteria.add(Restrictions.ilike("promocode", "%" + promocode + "%"));

			}

			Query qry = session.createQuery(strBfr.toString());
			logger.info(qry
					+ "qry---------------------------------------------------------------------------------------------");
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(currentIndex);
			}
			List<Long> ids = criteria.list();
			logger.info(ids
					+ "ids----------------------------------------------------------------------------------------");
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(PromocodeDetails.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");

			map.put("promoList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}

	}

	@Override
	public Map<String, Object> validatePromocode(String promocode, double orderPrice) {
		Map<String, Object> map = null;
		List<PromocodeDetails> data = null;
		List<Review> reviewList = null;
		double totalAmountAfterPromoCode = 0;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(PromocodeDetails.class);
			boolean flag = false;
			long currentDte = TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);
			StringBuffer strBfr = new StringBuffer("select count(*) from PromocodeDetails o");

			if (promocode != null && !promocode.equals("")) {
				logger.info(promocode + "promocode_________________________________________");
				if (flag == false) {
					flag = true;
					strBfr.append(" where");

				} else {
					strBfr.append(" where");
				}
				strBfr.append(" lower(o.promocode) like '%" + promocode.toLowerCase() + "%'");
				logger.info(strBfr + "_______________________________________________");
				criteria.add(Restrictions.ilike("promocode", "%" + promocode + "%"));

			}

			Query qry = session.createQuery(strBfr.toString());
			logger.info(qry
					+ "qry---------------------------------------------------------------------------------------------");
			totalCounts = (long) qry.uniqueResult();
			logger.info(totalCounts
					+ "totalCounts----------------------------------------------------------------------------------------");
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));

			List<Long> ids = criteria.list();

			logger.info(ids
					+ "ids----------------------------------------------------------------------------------------");
			session.flush();
			session.clear();

			if (totalCounts > 0) {
				long id = ids.get(0);
				logger.info(id
						+ "id---------------------------------------------id------------------------------------------------");
				String s = "select flatOffer from PromocodeDetails where codeId =:codeId";
				Query qry2 = session.createQuery(s.toString());
				qry2.setParameter("codeId", id);
				logger.info(qry2
						+ "qry2---------------------------------------------qry2------------------------------------------------");
				double flatOffer = (double) qry2.uniqueResult();
				logger.info(flatOffer
						+ "flatOffer---------------------------------------------flatOffer-----------------------------------------------");
				String s1 = "select percentage from PromocodeDetails where codeId =:codeId";
				Query qry21 = session.createQuery(s1.toString());
				qry21.setParameter("codeId", id);
				logger.info(qry21
						+ "qry21---------------------------------------------qry2------------------------------------------------");
				long percentage = (long) qry21.uniqueResult();
				double p = Double.valueOf(percentage);
				logger.info(p
						+ "p---------------------------------------------p-----------------------------------------------");
				if (flatOffer > 0) {
					totalAmountAfterPromoCode = orderPrice - flatOffer;
					logger.info(totalAmountAfterPromoCode
							+ "totalAmountAfterPromoCode---------------------------------------------totalAmountAfterPromoCode-----------------------------------------------");
				} else {
					totalAmountAfterPromoCode = orderPrice - (orderPrice * p / 100);
					logger.info(totalAmountAfterPromoCode
							+ "totalAmountAfterPromoCode---------------------------------------------totalAmountAfterPromoCode-----------------------------------------------");
				}

			}

			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(PromocodeDetails.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");

			map.put("promoList", data);
			map.put("promocodeamount", totalAmountAfterPromoCode);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	private long ids(int i) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> saveOrUpdateSellerDetails(SellerDetails
	 * sellerDetails, LoginDetails login)
	 * 
	 * {
	 * 
	 * Map<String, Object> map = null;
	 * 
	 * try { map = new HashMap<>(); Session session = openDBsession();
	 * logger.info(sellerDetails.getSellerEmail() +
	 * "EMAIL________________________________________________________________");
	 * String q =
	 * "select um.userId from SellerDetails um where um.sellerId=:sellerId"; Query
	 * q1 = session.createQuery(q); q1.setParameter("sellerId",
	 * sellerDetails.getSellerId()); long userId = (long) q1.uniqueResult();
	 * 
	 * logger.info(userId + "userId__________________________"); String qurey =
	 * "select um.userId from LoginDetails um where um.phoneNo=:pNo or um.email=:email"
	 * ; Query qry = session.createQuery(qurey); qry.setParameter("pNo",
	 * sellerDetails.getSellerPhoneNo()); qry.setParameter("email",
	 * sellerDetails.getSellerEmail()); List<Long> logId = (List<Long>) qry.list();
	 * logger.info(sellerDetails.getUserId() + "__________________________");
	 * logger.info(logId + "logId_______________________________________");
	 * 
	 * if (sellerDetails.getSellerId() > 0) { qurey =
	 * "select um.userId from SellerDetails um where um.sellerId=:cid"; qry =
	 * session.createQuery(qurey); qry.setParameter("cid",
	 * sellerDetails.getSellerId()); List<Long> custId = (List<Long>) qry.list();
	 * logger.info(custId + "custId_______________________________________"); //
	 * long userId=custId.get(0); logger.info(custId +
	 * "custId_______________________________________"); if (custId == null ||
	 * custId.isEmpty()) { map.put(ERROR_CODE, -4); map.put(ERROR_MESSAGE,
	 * "No such seller exist in seller table"); map.put(ERROR_MESSAGE_ARABIC,
	 * "لا يوجد مثل هذا العميل في جدول العملاء"); return map; } }
	 * 
	 * if (sellerDetails.getSellerId() > 0) { sellerDetails.setUserId(userId);
	 * session.update(sellerDetails); session.flush(); String qurey1 =
	 * "UPDATE LoginDetails l SET l.email=:email,l.phoneNo=:phone_no WHERE l.userId=:user_id "
	 * ; Query qry1 = session.createQuery(qurey1);
	 * 
	 * // qry1.setParameter("city",login.getCity() ); //
	 * qry1.setParameter("country",login.getCountry()); qry1.setParameter("email",
	 * sellerDetails.getSellerEmail()); qry1.setParameter("phone_no",
	 * sellerDetails.getSellerPhoneNo());
	 * 
	 * qry1.setParameter("user_id", userId); Integer result = qry1.executeUpdate();
	 * 
	 * }
	 * 
	 * else{ map.put(ERROR_CODE, -4); map.put(ERROR_MESSAGE,
	 * "Already registered phone number or email");
	 * map.put(ERROR_MESSAGE_ARABIC,"مسجل بالفعل رقم الهاتف أو البريد الإلكتروني");
	 * return map; }
	 * 
	 * // closeDBSession();
	 * 
	 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
	 * map.put("SELLER", sellerDetails); } catch (HibernateException e) { //
	 * closeDBSession(); map = new HashMap<>(); logger.info(e.getMessage()); if
	 * (TESTING) { e.printStackTrace(); } map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, e.getMessage()); throw e; } return map; }
	 */

}
