package com.hidile.pental.ctrl.input;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.hidile.pental.constants.Constants;
import com.hidile.pental.model.BasicResponse;
import com.hidile.pental.model.LoggedSession;
import com.hidile.pental.model.input.AddVo;
import com.hidile.pental.model.input.AttributeVo;
import com.hidile.pental.model.input.CategoryVo;
import com.hidile.pental.model.input.CmsVo;
import com.hidile.pental.model.input.CountryVo;
import com.hidile.pental.model.input.FilterVo;
import com.hidile.pental.model.input.GetCmsVo;
import com.hidile.pental.model.input.GetPageVo;
import com.hidile.pental.model.input.GetproductAttributeVo;
import com.hidile.pental.model.input.ManufactureVo;
import com.hidile.pental.model.input.OfferVo;
import com.hidile.pental.model.input.ProductAttributeVo;
import com.hidile.pental.model.input.ProductVo;
import com.hidile.pental.model.input.ReportCommentsVo;
import com.hidile.pental.model.input.ReviewVo;
import com.hidile.pental.model.input.SearchVo;
import com.hidile.pental.model.input.SubCategoryVo;
import com.hidile.pental.service.CommonService;
import com.hidile.pental.service.input.InputService;

@RestController
@RequestMapping("/api/input")
public class InputRestCtrl extends CommonService implements Constants {

	private static final Logger logger = Logger.getLogger(InputRestCtrl.class);
	@Autowired
	private InputService inputService;
	BasicResponse response = null; 
	
	
	@RequestMapping(value = GET_ALL_ADDS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllAdds(@RequestParam("addId") long addId,@RequestParam("sellerId") long sellerId,@RequestParam("addStatus") int addStatus ,@RequestParam("modeId") long modeId ,@RequestParam("arabicOrEnglish") long arabicOrEnglish ,HttpServletRequest req) throws IOException {
		
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res=null;
		/*List<Long> inputAsList =null;
		if(modeId[0] != 0) 
		{
		Long[] inputBoxed = ArrayUtils.toObject(modeId);
		inputAsList = Arrays.asList(inputBoxed);
		}*/
		 res = inputService.getAllAdds(addStatus, addId, sellerId,modeId,arabicOrEnglish);
		 return res;
	}
	
	@RequestMapping(value = CHANGE_RETURN_STATUS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changeReturnStatus(@RequestParam("returnItemId") long returnItemId, @RequestParam("status") int status,
			HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		logger.info("changfe return status_______________________________________________________________");
		if(status == RETURN_CANCELLED)
		{
			
		
      if(session.roleId != ROLL_ID_CUSTOMER)
      {
	res.errorCode = 1;
	res.errorMessage = "customer has only privillage to cancell return";
	
	return res;
}
		}else if(status == RETURN_REJECTED || status == RETURN_ACCEPTED) 
{
			 if(session.roleId != ROLL_ID_SELLER)
		      {
	res.errorCode = 2;
	res.errorMessage  ="seller  is only have privilage to  reject return ";
	return res;
}
}
		res = inputService.changeReturnStatus(returnItemId, status);
		res.bodyJson = getJson(new GetPageVo());
		return res;
	}
	
	@RequestMapping(value = SAVE_ADD, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveAdd(@Valid @NotNull @RequestBody AddVo addVo,Errors errors, HttpServletRequest req)
	{
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 LoggedSession session=(LoggedSession) res.object;
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new AddVo()); 
			   return res;
	        }
         addVo.setOperatingOfficerId(session.userId);
		res = inputService.saveOrUpdateAdd(addVo);
		res.bodyJson=getJson(new AddVo());
		return res;
	} 
	
	
	
	@RequestMapping(value = SAVE_REVIEW, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveOrUpdateReview(@Valid @RequestBody ReviewVo reviewVo,Errors errors, HttpServletRequest req)
	{
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 LoggedSession  session=(LoggedSession) res.object;
		if(reviewVo == null) {
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
			   res.bodyJson=getJson(new CategoryVo());
			   return res;
		}
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new ReviewVo());
			   return res;
	        }
		 reviewVo.setUserId(session.getUserId());
	
		reviewVo.setCompanyId(session.companyId);
		res=inputService.saveReview(reviewVo);
		res.bodyJson=getJson(new ReviewVo());
		return res;
		}
	
	
	
	
	
	
	
	
	@RequestMapping(value = ADD_COMMENTS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addComments(@Valid @RequestBody ReportCommentsVo reviewVo,Errors errors, HttpServletRequest req)
	{
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 LoggedSession  session=(LoggedSession) res.object;
		 if(session.roleId != ROLL_ID_SELLER)
		 {
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage="admn only provision to comments on reveiw";
			   res.bodyJson=getJson(new CategoryVo());
			   return res;
		 }
		if(reviewVo == null) 
		{
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
			   res.bodyJson=getJson(new CategoryVo());
			   return res;
		}
		 if (errors.hasErrors()) 
		 { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new ReviewVo());
			   return res;
	        }
	
	if(reviewVo.getReviewId() <=0)
	{
		res=new BasicResponse();
		   res.errorCode=1;
		   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
		   res.bodyJson=getJson(new CategoryVo());
		   return res;
	}
		
		res=inputService.addComments(reviewVo.getComments(),reviewVo.getReviewId());
		res.bodyJson=getJson(new ReviewVo());
		return res;
		}
	
	
	
	
	
	
	
	@RequestMapping(value = SAVE_SUBCATEGORY, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveSubCategory(@Valid @RequestBody SubCategoryVo subcategoryVo,Errors errors, HttpServletRequest req) {
		
	String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 LoggedSession  session=(LoggedSession) res.object;
		if(subcategoryVo == null) {
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
			   res.bodyJson=getJson(new SubCategoryVo());
			   return res;
		}
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new SubCategoryVo());
			   return res;
	        }
		 subcategoryVo.setUserId(session.userId);
		 subcategoryVo.setCompanyId(session.companyId);
		res = inputService.saveSubCategory(subcategoryVo);
		res.bodyJson=getJson(new SubCategoryVo());
		return res;
	}

	
	
	@RequestMapping(value = ADD_OFFER, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addOffer(@Valid @RequestBody OfferVo offerVo,Errors errors, HttpServletRequest req) 
	{BasicResponse res=null;
		/* String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new CountryVo());
			   return res;
	        }
        */
		 res = inputService.addOffer(offerVo);
		 res.bodyJson=getJson(new OfferVo());
		return res;
	}
	
	
	/*@RequestMapping(value = SAVE_SHOP, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveShop(@Valid @NotNull @RequestBody StoreVo shopVo, Errors errors, HttpServletRequest req)
			throws IOException
			 {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		if (TESTING) {
			logger.info(getJson(shopVo));
		}
		if (shopVo == null) {
			res = new BasicResponse();
			res.errorCode = 1;
			res.errorMessage = MESSAGE_NULL_VALUE + " PASSED";
			res.bodyJson = getJson(new StoreVo());
			return res;
		}
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new StoreVo());
			return res;
		}
		res = inputService.saveShop(shopVo);
		logger.info("return form");
		logger.info(res.errorCode + "" + res.errorMessage);
		res.bodyJson = getJson(new StoreVo());
		return res;
	}*/

	@RequestMapping(value = GET_ALL_PRODUCTS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getProducts(@Valid @RequestBody GetPageVo getProductVo,Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
	
		BasicResponse	res = inputService.getAllProduct(getProductVo.getProductId(), getProductVo.getSellerId(),getProductVo.getUserId(),
				0, getProductVo.getCategoryId(), getProductVo.getSubCategoryId(),getProductVo.getCountryId()
				,getProductVo.getCityId(),getProductVo.getCurrentIndex(), getProductVo.getRowPerPage(),getProductVo.getStat(),getProductVo.getProductName(),getProductVo.getCompanyId(),getProductVo.getNumberOfItemsRemaining(),getProductVo.getOfferId(),getProductVo.getManufacturerId(),getProductVo.getPriceStarts(),getProductVo.getPriceEnds(),getProductVo.getProductAttributeId(),getProductVo.getValue(),getProductVo.getProductNameAr(),getProductVo.getData(),getProductVo.getAttibutesSearch(),getProductVo.getFeatured(),getProductVo.getRank());
		 res.bodyJson=getJson(new GetPageVo());
		return res;
	}
	
	@RequestMapping(value = GET_ALL_PRODUCTS_WITHOUT_OFFER, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getProductswithoutOffer(@Valid @RequestBody GetPageVo getProductVo,Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
		
   /* String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR)
		 {
			 return res;
		 }
		if(getProductVo == null) 
		{
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
		}
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }*/
		 
		BasicResponse res = inputService.getProductswithoutOffer(getProductVo.getRowPerPage(),getProductVo.getCurrentIndex());
		 res.bodyJson=getJson(new GetPageVo());
		return res;
	}
	
@RequestMapping(value = GET_ALL_REVIEWS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
public BasicResponse getAllReview(@RequestParam("firstIndex") int firstIndex,
		@RequestParam("rowPerPage") int rowPerPage, @RequestParam("fatherId") long fatherId,@RequestParam("cityId") long cityId,@RequestParam("countryId") long countryId
		,@RequestParam("customerId") long customerId,@RequestParam("sellerId") long sellerId,@RequestParam("fromDate") long fromDate,@RequestParam("toDate") long toDate, HttpServletRequest req,
		HttpServletResponse resp) throws IOException
{
	BasicResponse res = new BasicResponse();
	if (rowPerPage == 0) {
		res.errorCode = -3;
		res.errorMessage = "SOME IMPORTANT DATA MISSING";
		return res;
	}
	res=inputService.getAllReviews(fatherId,cityId,countryId,customerId,sellerId,firstIndex,rowPerPage,fromDate,toDate);
	logger.info(getJson(res));
	return res;
	
}
@RequestMapping(value = GET_ALL_SUBCATEGORIES, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllSubcategoris(@RequestParam("mainCategoryId") long mainCategoryId, @RequestParam("categoryId") long categoryId,@RequestParam("companyId") long companyId,
			HttpServletRequest req, HttpServletResponse resp) throws IOException 
{

		BasicResponse res=null;
		
		res = inputService.getAllSubcategories(mainCategoryId, categoryId,companyId);
		return res;
	}

@RequestMapping(value = GET_ALL_CATEGORIES, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
public BasicResponse getAllCategory(@RequestParam("categoryId") long categoryId,@RequestParam("mainCategoryId") long mainCategoryId,@RequestParam("status") int status,@RequestParam("companyId") long companyId, HttpServletRequest req,
		HttpServletResponse resp) throws IOException 
{
	
	 BasicResponse res = inputService.getAllCategory(categoryId,mainCategoryId,status, companyId);
	return res;
}



	
	/*@RequestMapping(value = GET_ALL_SHOPS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getShops(@Valid @NotNull @RequestBody GetPageVo getPageVo, HttpServletRequest req,
			HttpServletResponse resp) throws IOException {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		if (TESTING) {
			logger.info(getJson(getPageVo));
		}
		res = inputService.getAllShops(getPageVo.getShopId(), getPageVo.getCategoryId(), getPageVo.getSubCategoryId(),
				getPageVo.getCurrentIndex(), getPageVo.getRowPerPage(), getPageVo.getFromDate(), getPageVo.getToDate());

		return res;
	}*/

	@RequestMapping(value = GET_ALL_PRODUCTS_BY_USER_ID, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllProductsByUserId(@Valid @NotNull @RequestBody GetPageVo getPageVo,
			HttpServletRequest req) throws IOException 
	{
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		if (TESTING) {
			logger.info(getJson(getPageVo));
		}
		if (getPageVo == null) {
			res = new BasicResponse();
			res.errorCode = 1;
			res.errorMessage = MESSAGE_NULL_VALUE + " PASSED";
			res.bodyJson = getJson(new GetPageVo());
			return res;
		}
		if (getPageVo.getUserId() <= 0) {
			res = new BasicResponse();
			res.errorCode = -2;
			res.errorMessage = "No such user";
			return res;
		}
		/*
		 * res = inputService.getAllProductsByUserId(getPageVo.getUserId(),
		 * getPageVo.getCurrentIndex(), getPageVo.getRowPerPage());
		 */
		return res;
	}

	@RequestMapping(value = SAVE_CATEGORY, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveCategory(@Valid  @RequestBody CategoryVo categoryVo, Errors errors,HttpServletRequest req)
	{ 
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 LoggedSession  session=(LoggedSession) res.object;
		if(categoryVo == null) {
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
			   res.bodyJson=getJson(new CategoryVo());
			   return res;
		}
		if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new CategoryVo()); 
			   return res;
	        }
		categoryVo.setCompanyId(session.companyId);
		res = inputService.saveCategory(categoryVo);
		res.bodyJson=getJson(new CategoryVo());
		return res;
	}
	
	
	@RequestMapping(value = SAVE_CMS, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveCms(@Valid  @RequestBody CmsVo categoryVo, Errors errors,HttpServletRequest req)
	{ 
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 LoggedSession  session=(LoggedSession) res.object;
		if(categoryVo == null) {
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
			   res.bodyJson=getJson(new CategoryVo());
			   return res;
		}*/
		 BasicResponse res= null;
		if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new CategoryVo()); 
			   return res;
	        }
		
		res = inputService.saveCms(categoryVo);
		res.bodyJson=getJson(new CategoryVo());
		return res;
	}
	

	
	@RequestMapping(value = SAVE_MANUFACTURE, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse svaeManufacture(@Valid  @RequestBody ManufactureVo manufactureVo, Errors errors,HttpServletRequest req)
	{ 
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 LoggedSession  session=(LoggedSession) res.object;
		 if(session.getRoleId() != ROLL_ID_SELLER)
		 {
			 res.errorCode=1;
				res.errorMessage="ONLY SELLER CAN ADD MANUFACTURE";
				//res.errorMessageArabic="أوامر الطلب"+MESSAGE_NOT_FOUND_AR;
				return res; 
		 }
		 logger.info(session.getUserId());
			 int r=(int) session.getUserId();
		if(manufactureVo == null) {
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
			   res.bodyJson=getJson(new ManufactureVo());
			   return res;
		}
		if (errors.hasErrors()) 
		{ 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new ManufactureVo()); 
			   return res;
	        }
		manufactureVo.setCompanyId(session.companyId);
		res = inputService.svaeManufacture(manufactureVo,r);
		res.bodyJson=getJson(new ManufactureVo());
		return res;
	}
	
	
	
	
	
	@RequestMapping(value = SAVE_PRODUCT, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveProduct(@Valid @NotNull @RequestBody ProductVo productVo, Errors errors,
			HttpServletRequest req) 
	{
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) 
		{
			return res;
		} LoggedSession session=(LoggedSession) res.object;
		if (productVo == null) {
			res = new BasicResponse();
			res.errorCode = 1;
			res.errorMessage = MESSAGE_NULL_VALUE + " PASSED";
			res.bodyJson = getJson(new ProductVo());
			return res;
		}
		if (TESTING) 
		{
			logger.info(getJson(productVo));
		}
		if (errors.hasErrors()) 
		{
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new ProductVo());
			return res;
		}
		productVo.setSellerId(session.entryId);
		logger.info(productVo.getSellerId()+"seller id_____________________________________________________________________");
		productVo.setUserId(session.userId);
		productVo.setCompanyId(session.companyId);
		res = inputService.saveProduct(productVo);
		res.bodyJson = getJson(new ProductVo());
		return res;
	}

	@RequestMapping(value = SEARCH_DATA, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse searchData(@Valid @NotNull @RequestBody SearchVo searchVo, HttpServletRequest req,
			HttpServletResponse resp) 
	{
		/*String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}*/
		if (TESTING) {
			logger.info(getJson(searchVo));
		}
		BasicResponse res = inputService.searchData(searchVo.getData(), searchVo.getMode(), searchVo.currentIndex,
				searchVo.rowPerPage, searchVo.latitude, searchVo.longitude);
		logger.info(res.errorMessage);
		res.bodyJson = getJson(new SearchVo());
		return res;
	}
	
	@RequestMapping(value = FILTER_DATA, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse filterData(@Valid @NotNull @RequestBody FilterVo searchVo, HttpServletRequest req,
			HttpServletResponse resp) 
	{
		/*String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}*/
		if (TESTING) 
		{
			logger.info(getJson(searchVo));
		}
		BasicResponse res = inputService.filterData( searchVo.getMode(), searchVo.currentIndex,
				searchVo.rowPerPage,searchVo.getCategoryId());
		logger.info(res.errorMessage);
		res.bodyJson = getJson(new SearchVo());
		return res;
	}
	
	
	
	
	
	

	@RequestMapping(value = GET_ALL_DETAILS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllOverallDetails(@RequestParam("userId") long userId, @RequestParam("id") long id,
			@RequestParam("productOrShop") int prodOrShop, HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		if (id == 0) {
			res = new BasicResponse();
			res.errorCode = 4;
			res.errorMessage = MESSAGE_NULL_VALUE;
			return res;
		}
		res = inputService.getAllDetails(userId, id, prodOrShop);
		return res;
	}
	
	@RequestMapping(value = UPDATE_STOCK, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse updateStock(@RequestParam("productId") long productId, @RequestParam("productQuantity") long productQuantity,
			 HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		/*String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}*/
		if (productId == 0) 
		{
			BasicResponse res = new BasicResponse();
			res.errorCode = 4;
			res.errorMessage = "please provide product id";
			return res;
		}
		BasicResponse res = inputService.updateStock(productId,productQuantity);
		return res;
	}

	/*@RequestMapping(value = GET_ALL_SHOPS_BY_SELLER_ID, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllShopsBySellerId(@Valid @NotNull @RequestBody GetPageVo getPageVo, HttpServletRequest req)
			throws IOException {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		if (TESTING) {
			logger.info(getJson(getPageVo));
		}
		LoggedSession session = (LoggedSession) res.object;
		if (getPageVo == null) {
			res = new BasicResponse();
			res.errorCode = 1;
			res.errorMessage = MESSAGE_NULL_VALUE + " PASSED";
			res.bodyJson = getJson(new GetPageVo());
			return res;
		}
		if (session.roleId == ROLL_ID_SHOP) {
			getPageVo.setUserId(session.userId);
		}
		res = inputService.getAllShopsBySellerId(getPageVo.getSellerId(), getPageVo.getCurrentIndex(),
				getPageVo.getRowPerPage());
		return res;
	}*/

	/*@RequestMapping(value = ADD_COUNTRTY, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addCountry(@Valid @RequestBody CountryVo countryVo, Errors errors, HttpServletRequest req) {
		//String headerToken = req.getHeader(TOKEN);
		BasicResponse res =new  BasicResponse();
				//checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		if (TESTING) {
			logger.info(getJson(countryVo));
		}
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new CountryVo());
			return res;
		}

		res = inputService.saveOrUpdateCountry(countryVo);
		res.bodyJson = getJson(new CountryVo());
		return res;
	}

	@RequestMapping(value = ADD_CITY, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addCity(@Valid @RequestBody CityVo cityVo, Errors errors, HttpServletRequest req) {
		//String headerToken = req.getHeader(TOKEN);
		BasicResponse res = new BasicResponse();
				//checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		if (TESTING) {
			logger.info(getJson(cityVo));
		}
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new AuctionVo());
			return res;
		}

		res = inputService.saveOrUpdateCity(cityVo);
		res.bodyJson = getJson(new CityVo());
		return res;
	}
*/
	@RequestMapping(value = GET_ALL_CITY, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllCity(@Valid @RequestBody GetPageVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res=null;
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
		 
		 res = inputService.getAllCity(getOrderVo.getCountryId(),getOrderVo.getCityId(),getOrderVo.getCityName(),
				 getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage(),getOrderVo.getCitusStatus());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	@RequestMapping(value = GET_ALL_OFFER, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllOffer(@Valid @RequestBody OfferVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res=null;
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
		 
		 res = inputService.getAllOffer(getOrderVo.getOfferId(),getOrderVo.getOfferEnds(),getOrderVo.getOfferStarts(),getOrderVo.getStatus(),getOrderVo.getOfferName(),getOrderVo.getOfferNameAr(),
				 getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	@RequestMapping(value = GET_ALL_PRODUCT_ATTRIBUTES, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAttributes(@Valid @RequestBody GetproductAttributeVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res=null;
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
		 
		 
		 res = inputService.getAttributes(getOrderVo.getAttributeName(),getOrderVo.getAttributeNameAr(),
				 getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage(),getOrderVo.getStatus());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	@RequestMapping(value = GET_ALL_CMS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllCms(@Valid @RequestBody GetCmsVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res=null;
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
		 
		 res = inputService.getAllCms(getOrderVo.getCmsId(),getOrderVo.getRowPerPage(),getOrderVo.getCurrentIndex());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	
	@RequestMapping(value = GET_ALL_MANUFACTURE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getallManufacture(@Valid @RequestBody ManufactureVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res=null;
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
		 
		 res = inputService.getallManufacture(getOrderVo.getManufacturerId(),getOrderVo.getManufacturerName(),getOrderVo.getStatus(),getOrderVo.getCurrentIndex(),getOrderVo.getRowPerPage(),getOrderVo.getCompanyId()
				);
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	

	@RequestMapping(value = GET_ALL_COUNTRY, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getContry(@RequestParam("countryId") long countryId,@RequestParam("status") int status, HttpServletRequest req,
			HttpServletResponse resp) throws IOException {
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res=null;
		 
		 res = inputService.getAllCountry(countryId,status);
		return res;
	}
	
	
	
	@RequestMapping(value = READ_NOTIFICATION, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse readNotification( HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) 
		{
			return res;
		}
	
		LoggedSession session = (LoggedSession) res.object;
		 if(res.errorCode != ERROR_CODE_NO_ERROR) 
		 {
			 return res;
		 }
		
		 
		 res = inputService.readNotification(session.getUserId());
		return res;
	}
	
	
	
	
	
	
	
	
	@RequestMapping(value = GOOGLE_AUTHENTICATION, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse googleAuth(@RequestParam("key") String key,@RequestParam("email") String email, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
	
		BasicResponse res=null;
		final String uri = "https://www.google.com?key="+key;
        RestTemplate restTemplate = new RestTemplate();
        String  result = restTemplate.getForObject(uri, String.class);
        logger.info(result+"result_______________________________result______________");
		// res = inputService.googleAuth(key,email);
       
		return res;
	}
	
	@RequestMapping(value = CHANGE_PRODUCT_STATUS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changeProductStatus(@RequestParam("productId") long productId,@RequestParam("status") int status,HttpServletRequest req,HttpServletResponse resp) throws IOException {
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new BasicResponse();
				 checkTokenOfUser(headerToken);
		if(res.errorCode != ERROR_CODE_NO_ERROR) 
		{
			 return res;
		}
		/* if (errors.hasErrors()) 
		 * { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }*/
		 
		 res = inputService.changeProductStatus(productId,status);
		 //res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	@RequestMapping(value = CHANGE_SHIPPING_STATUS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changeShipppingStatus(@RequestParam("shippingId") long shippingId,@RequestParam("status") int status,HttpServletRequest req,HttpServletResponse resp) throws IOException {
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new BasicResponse();
				 checkTokenOfUser(headerToken);
		if(res.errorCode != ERROR_CODE_NO_ERROR) 
		{
			 return res;
		}
		/* if (errors.hasErrors()) 
		 * { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }*/
		 
		 res = inputService.changeShipppingStatus(shippingId,status);
		 //res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
		
	
	
	@RequestMapping(value = SAVE_ATTRIBUTE, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveAttribute(@Valid @NotNull @RequestBody AttributeVo attributeVo,Errors errors, HttpServletRequest req)
	{
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 LoggedSession session=(LoggedSession) res.object;
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new AttributeVo()); 
			   return res;
	        }
		 attributeVo.setOperatingOfficerId(session.userId);
		res = inputService.saveOrUpdateAttribute(attributeVo);
		res.bodyJson=getJson(new AttributeVo());
		return res;
	} 

	@RequestMapping(value = GET_ALL_ATTRIBUTES, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllAttribute(@RequestParam("attributeId") long attributeId,@RequestParam("status") int status ,HttpServletRequest req) throws IOException {
		
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res = inputService.getAllAttribute(attributeId, status);
		 return res;
	}
	
	
	@RequestMapping(value = GET_ALL_ATTRIBUTES_VALUES, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllAttributevalues( @RequestBody GetproductAttributeVo attributeVo,Errors errors, HttpServletRequest req) throws IOException {
		
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res = inputService.getAllAttributevalues(attributeVo.getAttributeId(), attributeVo.getStatus(),attributeVo.getCurrentIndex(),attributeVo.getRowPerPage());
		 return res;
	}
	


	/*
	 * @RequestMapping(value = ADD_CATEGORY, method = RequestMethod.POST, consumes =
	 * MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * addCategory(@Valid @RequestBody CategoryVo categoryVo,Errors errors,
	 * HttpServletRequest req) { BasicResponse res = new BasicResponse();
	 * if(categoryVo == null) { res=new BasicResponse(); res.errorCode=1;
	 * res.errorMessage=MESSAGE_NULL_VALUE +" PASSED"; res.bodyJson=getJson(new
	 * CategoryVo()); return res; } if (errors.hasErrors()) { res=new
	 * BasicResponse(); res.errorCode=2;
	 * res.errorMessage=errors.getAllErrors().stream().map(x ->
	 * x.getDefaultMessage()).collect(Collectors.joining(","));
	 * res.bodyJson=getJson(new CategoryVo()); return res; } res =
	 * inputService.saveOrUpdateCategory(categoryVo); res.bodyJson=getJson(new
	 * CategoryVo()); return response; }
	 */
	/*
	 * @RequestMapping(value = SAVE_LIKE_OR_SHARE, method = RequestMethod.POST,
	 * consumes = MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * saveLIkeOrShares(@RequestBody LikeAndShareVO likesOrshares,
	 * HttpServletRequest req, HttpServletResponse resp) { BasicResponse res = new
	 * BasicResponse(); LoggedSession session = getLogedUserFromSession(req); if
	 * (session == null) { // resp.sendRedirect(GENERAL_FIRST_URL); res.errorCode =
	 * 4; res.errorMessage = MESSAGE_PLEASE_LOGIN; return res; }
	 * likesOrshares.userId = session.userId; res =
	 * inputService.saveLikeOrShare(likesOrshares); return res; }
	 */

	/*
	 * @RequestMapping(value = GET_ALL_CATEGORY_FROM_MAINCATEGORY, method =
	 * RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE) public
	 * BasicResponse getCategoryFromMain(@RequestParam("mainCategoryId") long
	 * mainCategoryId) { return
	 * inputService.getAllCategoryFromMainCatgoryId(mainCategoryId); }
	 */

	/*
	 * @RequestMapping(value =GET_VIEW_BOOKING_DETAILS,method =
	 * RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE) public
	 * BasicResponse getBookingDetailsList(@RequestBody ReportVo reportVo){
	 * if(reportVo.fromDate == null || reportVo.toDate == null){ BasicResponse
	 * res=new BasicResponse(); res.errorCode=2;
	 * res.errorMessage=MESSAGE_NULL_VALUE; return res; } long
	 * fromDate=TimeUtils.instance().getMilSecFromDateStrAsShort(reportVo.
	 * fromDate); long
	 * toDate=TimeUtils.instance().getMilSecFromDateStrAsShort(reportVo.toDate); //
	 * logger.info(reportVo.toString()); return
	 * reportService.getBookingDetailsReportFromMode(fromDate,
	 * toDate,reportVo.dataMap, reportVo.mode,reportVo.status); }
	 * 
	 */

	/*
	 * @RequestMapping(value = GET_ALL_SHOPS, method = RequestMethod.POST, produces
	 * = MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * getShops(@RequestBody GetPageVo getPageVo, HttpServletRequest req,
	 * HttpServletResponse resp) throws IOException {
	 * 
	 * if(getLogedUserFromSession(req) == null){
	 * resp.sendRedirect(GENERAL_FIRST_URL); }
	 * 
	 * BasicResponse res = inputService.getAllShops(getPageVo.shopId,
	 * getPageVo.mainCategory, getPageVo.categoryId, getPageVo.subCategoryId,
	 * getPageVo.currentIndex, getPageVo.rowPerPage); return res; }
	 */

	/*
	 * @RequestMapping(value = GET_ALL_SHOPS_BY_USER_ID, method =
	 * RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE) public
	 * BasicResponse getAllShopsByUserId(@RequestBody GetPageVo getPageVo,
	 * HttpServletRequest req) throws IOException { BasicResponse res =null;
	 * LoggedSession session = getLogedUserFromSession(req);
	 * 
	 * res = inputService.getAllShopsByUserId(getPageVo.userId,
	 * getPageVo.currentIndex,getPageVo.rowPerPage); return res; }
	 */

	/*
	 * @RequestMapping(value = GET_ALL_MAINCATEGORIES, method = RequestMethod.GET,
	 * produces = MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * getAllMaincategories(HttpServletRequest req, HttpServletResponse resp) throws
	 * IOException {
	 * 
	 * if(getLogedUserFromSession(req) == null){
	 * resp.sendRedirect(GENERAL_FIRST_URL); }
	 * 
	 * BasicResponse res = inputService.getAllMainCategoryFromId(); return res; }
	 */
	/*
	 * @RequestMapping(value = REQUEST_ITEMS, method = RequestMethod.POST, produces
	 * = MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * requestItems(@Valid @RequestBody RequestItemsVo requestItemsVo,Errors errors,
	 * HttpServletRequest req) { String headerToken=req.getHeader(TOKEN);
	 * BasicResponse res=checkTokenOfUser(headerToken); if(res.errorCode !=
	 * ERROR_CODE_NO_ERROR) { return res; } LoggedSession session=(LoggedSession)
	 * res.object; if(requestItemsVo == null) { res=new BasicResponse();
	 * res.errorCode=1; res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
	 * res.bodyJson=getJson(new CategoryVo()); return res; } if (errors.hasErrors())
	 * { res=new BasicResponse(); res.errorCode=2;
	 * res.errorMessage=errors.getAllErrors().stream().map(x ->
	 * x.getDefaultMessage()).collect(Collectors.joining(","));
	 * res.bodyJson=getJson(new RequestItemsVo()); return res; }
	 * requestItemsVo.setUserId(session.userId);
	 * requestItemsVo.setStoreId(session.entryId);
	 * res=inputService.saveRequestItem(requestItemsVo); res.bodyJson=getJson(new
	 * RequestItemsVo()); return res; }
	 * 
	 * 
	 * @RequestMapping(value =UPDATE_REQUEST_ITEMS, method = RequestMethod.GET,
	 * produces = MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * updaterequestItems(@RequestParam("requestId") long
	 * requestItemId,@RequestParam("status") int status, HttpServletRequest req) {
	 * String headerToken=req.getHeader(TOKEN); BasicResponse
	 * res=checkTokenOfUser(headerToken); if(res.errorCode != ERROR_CODE_NO_ERROR) {
	 * return res; }
	 * 
	 * res=inputService.updateRequestItem(requestItemId,status);
	 * 
	 * return res; }
	 * 
	 * 
	 * @RequestMapping(value = GET_ALL_ITEM_REQUEST, method = RequestMethod.GET,
	 * produces = MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * getAllItemRequest(@RequestParam("requestItemId") long
	 * requestItemId,@RequestParam("userId") long userId, HttpServletRequest req,
	 * HttpServletResponse resp) throws IOException { String
	 * headerToken=req.getHeader(TOKEN); BasicResponse
	 * res=checkTokenOfUser(headerToken); if(res.errorCode != ERROR_CODE_NO_ERROR) {
	 * return res; }
	 * 
	 * res = inputService.getAllItemRequest(requestItemId,userId); return res; }
	 */

	/*
	 * @RequestMapping(value = SAVE_MAINCATEGORY, method = RequestMethod.POST,
	 * produces = MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * getBooked(@RequestBody CategoryVo categoryVo, HttpServletRequest req) throws
	 * IOException { BasicResponse res = null; LoggedSession session =
	 * getLogedUserFromSession(req);
	 * 
	 * res = inputService.saveMainCategory(categoryVo); return res; }
	 */

	/*
	 * @RequestMapping(value = DELETE_REVIEW, method = RequestMethod.GET, produces =
	 * MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * deleteReview(@RequestParam("reviewId") long
	 * reviewId, @RequestParam("subOrMain") int subOrMain, HttpServletRequest req,
	 * HttpServletResponse resp) throws IOException { BasicResponse res = new
	 * BasicResponse(); LoggedSession session = getLogedUserFromSession(req); if
	 * (session == null) { // resp.sendRedirect(GENERAL_FIRST_URL); res.errorCode =
	 * 4; res.errorMessage = MESSAGE_PLEASE_LOGIN; return res; } if (reviewId == 0)
	 * { res.errorCode = -3; res.errorMessage = "SOME IMPORTANT DATA MISSING";
	 * return res; } return inputService.deleteReview(reviewId,
	 * subOrMain,session.userId); // } }
	 */
	
	/*public BasicResponse getShops(@Valid @NotNull @RequestBody GetPageVo getPageVo, BindingResult bindingResult, HttpServletRequest req) {
	String headerToken=req.getHeader(TOKEN);
	 BasicResponse res=checkTokenOfUser(headerToken);
	 if(res.errorCode != ERROR_CODE_NO_ERROR) 
	 {
		 return res;
	 }
	 if(TESTING) {
			logger.info(getJson(getPageVo));
		}
    res = inputService.getAllShops(getPageVo.getShopId(), getPageVo.getCategoryId(), getPageVo.getSubCategoryId(), getPageVo.getCurrentIndex(), getPageVo.getRowPerPage(),getPageVo.getFromDate(),getPageVo.getToDate());
}*/
	/*
	public BasicResponse saveOrUpdateReview(@Valid @NotNull @RequestBody ReviewVo reviewVo,Errors errors, HttpServletRequest req)
	{
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR)
		 {
			 return res;
		 }
		 if(TESTING) {
				logger.info(getJson(reviewVo));
			}
		 LoggedSession  session=(LoggedSession) res.object;
		if(reviewVo == null) {
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
			   res.bodyJson=getJson(new CategoryVo());
			   return res;
		}
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new ReviewVo());
			   return res;
	        }
		 reviewVo.setUserId(session.userId);
		res=inputService.saveReview(reviewVo);
		res.bodyJson=getJson(new ReviewVo());
		return res;*/

}
