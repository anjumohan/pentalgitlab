/**
 * 
 */
package com.hidile.pental.ctrl.input;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.hidile.pental.constants.Constants;
import com.hidile.pental.model.BasicResponse;
import com.hidile.pental.service.CommonService;
import com.hidile.pental.service.input.InputService;
import com.hidile.pental.utils.TimeUtils;

/**
 * @author Shibina EC
 * @email shibina@sociohub.in
 */
@RestController
@RequestMapping("/api")
public class UploadCtrl extends CommonService implements Constants {

	private static final Logger logger = Logger.getLogger(UploadCtrl.class);
	@Autowired
	private InputService inputService;

	@RequestMapping(value = URL_FILE_UPLOAD,headers=("content-type=multipart/*"), method = RequestMethod.POST)
	public BasicResponse upload(@RequestParam("file") MultipartFile file, @RequestParam("type") String type,
			HttpServletRequest req) throws IOException {
		BasicResponse res = new BasicResponse();
		byte[] bytes;
		if (!file.isEmpty()) {
			bytes = file.getBytes();
			String filename = file.getOriginalFilename();
			
			String name = filename.replace(" ", "_");
			logger.info(name+"---->"+filename);
			
			String extension = FilenameUtils.getExtension(filename);
			name = name.replace("." + extension, "");
			String imageNameToSave = name + "_" + TimeUtils.instance().getCurrentTime(0) + "." + extension;

			if (bytes != null) {
				res = ftpFileUploadServer(bytes, imageNameToSave, type);
			}
		}
		return res;
	}

	public BasicResponse ftpFileUploadServer(byte[] bytes, String imageNameToSave, String type) {
		BasicResponse res = null;

		String server = "ftp.sociohub.in";
		int port = 21;
		String user = "hidile@sociohub.in";
		String pass = "London@2016";
		FTPClient ftpClient = new FTPClient();
		try {

			ftpClient.connect(server, port);
			ftpClient.login(user, pass);
			ftpClient.enterLocalPassiveMode();
             
			String firstRemoteFile = "jewelry/" + imageNameToSave;
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			InputStream inputStream1 = new ByteArrayInputStream(bytes);
			System.out.println("Start uploading  file");
			boolean done = ftpClient.storeFile(firstRemoteFile, inputStream1);
			inputStream1.close();
			logger.info(done);
			if (done) {
				res = new BasicResponse();
				System.out.println("The file is uploaded successfully.");
				res.errorCode = 1;
				res.errorMessage = "Success";
				res.setObject("http://sociohub.in/sociohub.in/hidile/jewelry/" + imageNameToSave);
			}else{
				res=new BasicResponse();
				res.errorCode = -3;
				res.errorMessage = "failed";
			}
		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
			res = new BasicResponse();
			res.errorCode = -5;
			res.errorMessage = ex.getMessage();
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return res;

	}
	
	@RequestMapping(value = URL_FILE_UPLOAD_PENTAL, headers = ("content-type=multipart/*"), method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
    public BasicResponse uploadToServer(@RequestParam("file") MultipartFile file, HttpServletRequest req,
            HttpServletResponse response) throws IOException {

        String path = "C:";
        // response.setContentType("text/html;charset=UTF-8");
        String imageNameToSave="";
        File newFile = null;
    //    String path = req.getSession().getServletContext().getRealPath("");
        File f = new File(path + File.separator + "pentalimage");
        if (!f.exists()) {

            f.mkdirs();
        }
        String filename = file.getOriginalFilename();
        String name = filename.replace(" ", "_");
        String extension = FilenameUtils.getExtension(filename);
        name = name.replace("." + extension, "");
        String fileNameToSave = name + "_" + TimeUtils.instance().getCurrentTime(0) + "." + extension;
        OutputStream out = null;
        InputStream filecontent = null;
    //    final ServletOutputStream writer = response.getOutputStream();
        try {
            imageNameToSave = path + File.separator + "pentalimage" + File.separator + fileNameToSave;
            newFile = new File(imageNameToSave);
            out = new FileOutputStream(newFile);
            filecontent = file.getInputStream();

            int read = 0;
            final byte[] bytes = new byte[1024];
            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            // writer.println("New file " + imageNameToSave + " created at " +
            // path);
        //    writer.println(fileNameToSave);

        } catch (FileNotFoundException fne) {
/*//            writer.println("You either did not specify a file to upload or are "
//                    + "trying to upload a file to a protected or nonexistent " + "location.");
//            writer.println("<br/> ERROR: " + fne.getMessage());
*/
        } finally {
            if (out != null) {
                out.close();
            }
            if (filecontent != null) {
                filecontent.close();
            }
            /*if (writer != null) {
                writer.close();
            }*/

        }
    //   newFile.delete();
        BasicResponse res = new BasicResponse();
    //    System.out.println("The file is uploaded successfully.");
        res.errorCode = 0;
        res.errorMessage = "Success";
        logger.info(fileNameToSave + "-------------img name to save");
        res.object=fileNameToSave;
    //    res.setObjectData(fileNameToSave);
        logger.info(res.getObject()+"=================/////");
        return res;

        
    }
	
	
	@RequestMapping(value = URL_FILE_UPLOAD_TO_AWS,headers=("content-type=multipart/*"), method = RequestMethod.POST)
	public BasicResponse uploadToAWS(@RequestParam("file") MultipartFile file,
			HttpServletRequest req) throws IOException {
		AWSCredentials credentials = new BasicAWSCredentials("AKIAJT4ZP3OM7HDEDJBA", "wEkvaCrn+2O62ipBncCZWFjuX5O6kPdI4kCBvcFW");
		
		//AWSCredentials credentials = new BasicAWSCredentials("AKIAI5YHP3DC7DN4PVHA", "W89JLYeJqG1Q+C5ueNcgNbDYpmpp1vKsAK/UdXhy");
		
		
		
		AmazonS3 s3client = new AmazonS3Client(credentials);
		String bucketName = "projectdatas";
		String folderName="leansouq";
		String filename = file.getOriginalFilename();
		String name = filename.replace(" ", "_");
		logger.info(name+"---->"+filename);
		
		String extension = FilenameUtils.getExtension(filename);
		name = name.replace("." + extension, "");
		String imageNameToSave = "JW_PIC" + "_" + TimeUtils.instance().getCurrentTime(0) + "." + extension;
		String saveLocation = folderName + "/" + imageNameToSave;
		File fileToUpload=multipartToFile(file);
		/*PutObjectResult re=*/s3client.putObject(new PutObjectRequest(bucketName, saveLocation, 
				fileToUpload).withCannedAcl(CannedAccessControlList.PublicRead));
		/*try {
			re.wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		BasicResponse res = new BasicResponse();
		logger.info("The file is uploaded successfully.");
		if(fileToUpload.delete()) {
			logger.info("File deleted successfully");
		}
		res.errorCode = 0;
		res.errorMessage = "Success";
		res.setObject(imageNameToSave);
		return res;
	}
	
	public File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException 
	{
	    File convFile = new File( multipart.getOriginalFilename());
	    multipart.transferTo(convFile);
	    return convFile;
	}
	
}
