package com.hidile.pental.ctrl.user;

import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hidile.pental.constants.Constants;
import com.hidile.pental.entity.user.PromocodeDetails;
import com.hidile.pental.model.AddCustomer;
import com.hidile.pental.model.BasicResponse;
import com.hidile.pental.model.CompanyVo;
import com.hidile.pental.model.GetCustomerVo;
import com.hidile.pental.model.GetNotificationVo;
import com.hidile.pental.model.GetPromocodeDetails;
import com.hidile.pental.model.LoggedSession;
import com.hidile.pental.model.PromocodeDetailsVo;
import com.hidile.pental.model.UpdateSeller;
import com.hidile.pental.model.input.CategoryVo;
import com.hidile.pental.model.input.GetPageVo;
import com.hidile.pental.model.input.SellerVo;
import com.hidile.pental.model.order.AuctionDetailsVo;
import com.hidile.pental.service.CommonService;
import com.hidile.pental.service.GeneralService;


@RestController
@RequestMapping("/api/user")
public class GeneralRestCtrl extends CommonService implements Constants{
	private static final Logger logger = Logger.getLogger(GeneralRestCtrl.class);
	
	@Autowired
	private GeneralService generalService;
	
	
	@RequestMapping(value = ACTIVATION, method = RequestMethod.GET)
	public BasicResponse activateadmin(HttpServletRequest req) 
	       {
        
	BasicResponse	response = null;
  /*      logger.info("ownerVo-----"+getJson(new OrderVo()));
        logger.info("reviewVo-----"+getJson(new ReviewVo()));
        logger.info("usermodel----"+getJson(new OrderDetailsVo()));
        logger.info("addcustomer----"+getJson(new AddCustomer()));
        logger.info("bookingdetialsvo---"+getJson(new AuctionVo()));
        logger.info("featurevo----"+getJson(new FeatureVo()));
        logger.info("getpagevo-----"+getJson(new GetPageVo()));
        logger.info("palacepricevo----"+getJson(new PalacePriceVo()));
        logger.info("palacevo-----"+getJson(new ShopV()));
        logger.info("searchvo------"+getJson(new SearchVo()));
        logger.info("reportvo------"+getJson(new ReportVo()));*/
        LoggedSession loggedSession=new LoggedSession();
        loggedSession.email="admin@hidile.com";
        loggedSession.phone="9388468585";
        loggedSession.password="hidile@123";
        
        response=generalService.saveAdmin(loggedSession);
        logger.info(getJson(response));
		return response;

	}
	
	@RequestMapping(value = GET_CURRENT_USER, method = RequestMethod.GET,produces =  MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getCurrentUser(@RequestParam("") long userId,HttpServletRequest req) {
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		LoggedSession session = (LoggedSession) res.object;//getLogedUserFromSession(req);
			res.errorCode=ERROR_CODE_NO_ERROR;
			res.errorMessage=MESSAGE_SUCSESS;
			res.object=session;

         return res;
	}
	
	@RequestMapping(value = UPDATE_CUSTOMER, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse updateCustomer(@Valid @NotNull @RequestBody AddCustomer addUser, Errors errors,
			HttpServletRequest req) {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new AddCustomer());
			return res;
		}
		res = generalService.updateCustomer(addUser, session.roleId);
		res.bodyJson = getJson(new AddCustomer());
		return res;
	}
	
	
	@RequestMapping(value = AUTHENTICATION_USER, method = RequestMethod.POST,consumes =  MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse authenticate(@Valid @NotNull @RequestBody LoggedSession addUser,Errors errors,HttpServletRequest req) {
		BasicResponse res = new BasicResponse();
		logger.info("accepts data in authenticate");
		if (errors.hasErrors())
		{ 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new LoggedSession());
			   return res;
	        }
		logger.info("anju------------------------------------------------------------------------");
		res=generalService.authenticate(addUser.email, addUser.password,addUser.roleId);
		if(res.errorCode == ERROR_CODE_NO_ERROR) 
		{
			LoggedSession session=(LoggedSession) res.object;
			String compactToken=createToken(session);
			session.token=compactToken;
			res.object=session;
		}
		res.bodyJson=getJson(new LoggedSession());
		return res;
	}
	
	@RequestMapping(value = AUTHENTICATION_SOCIAL,method= RequestMethod.POST,consumes =  MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value=HttpStatus.OK)
	public void soialAuthenticate(@RequestBody Object obj,HttpServletRequest req) {
	//	BasicResponse res = userService.authenticate(addUser.customerName, addUser.phoneNumber);
		logger.info("hai"+obj.toString());

		/*if (res.errorCode == ERROR_CODE_NO_ERROR) {

			LoggedSession session = (LoggedSession) res.object;
			initSession(req, session);
			return res;
		} else {
			return res;

		}*/

	}
	
	
	/*@RequestMapping(value = CHANGE_PASSWORD, method = RequestMethod.POST,consumes =  MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changePassword(@Valid @RequestBody LoggedSession addUser,Errors errors,HttpServletRequest req) {
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		if(addUser == null) {
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
			   res.bodyJson=getJson(new LoggedSession());
			   return res;
		}
		
		if(TESTING)
		  logger.info(getJson(res));
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new LoggedSession());
			   return res;
	        }
		 res=generalService.changePassword(addUser.oldPassword, addUser.password, addUser.userId);
		 res.bodyJson=getJson(new LoggedSession());
		return res;

	}*/
	
	@RequestMapping(value = CHANGE_PASSWORD, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changePassword(@RequestBody LoggedSession addUser, HttpServletRequest req) {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		LoggedSession obj = (LoggedSession) res.object;
		res = generalService.changePassword(obj.password, addUser.getPassword(), obj.userId);
		if (TESTING)
			logger.info(getJson(res));
		if(res.errorCode == 0)
			 {
			 LoggedSession session=(LoggedSession) res.object;
				String compactToken=createToken(session);
				logger.info(compactToken+"compactToken__________________________compactToken______________________compactToken_______________compactToken");
				session.token=compactToken;
				
			res.object=session;
			 }

		return res;

	}

	
	 @RequestMapping(value =SAVE_CUSTOMER,method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	    public BasicResponse saveCustomer(@Valid @RequestBody AddCustomer addUser,Errors errors,HttpServletRequest req)
	 { 
		 BasicResponse res=null;
		 if(addUser == null) 
			{
				 res=new BasicResponse();
				   res.errorCode=1;
				   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
				   res.bodyJson=getJson(new AddCustomer());
				   return res;
			}
			 if (errors.hasErrors()) 
			 { 
				   res=new BasicResponse();
				   res.errorCode=2;
				   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
				   res.bodyJson=getJson(new AddCustomer());
				   return res;
		        }
			
			 res=generalService.saveCustomer(addUser);
			 res.bodyJson=getJson(new AddCustomer());
			 if(res.errorCode == 0)
			 {
			 LoggedSession session=(LoggedSession) res.object;
				String compactToken=createToken(session);
				logger.info(compactToken+"compactToken__________________________compactToken______________________compactToken_______________compactToken");
				session.token=compactToken;
				
			res.object=session;
			 }
			 return res;
	 }
	 
	 @RequestMapping(value =SAVE_PROMOTERS,method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	    public BasicResponse savePromoters(@Valid @RequestBody AddCustomer addUser,Errors errors,HttpServletRequest req)
	 { 
		 BasicResponse res=null;
		 if(addUser == null) 
			{
				 res=new BasicResponse();
				   res.errorCode=1;
				   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
				   res.bodyJson=getJson(new AddCustomer());
				   return res;
			}
			 if (errors.hasErrors()) { 
				   res=new BasicResponse();
				   res.errorCode=2;
				   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
				   res.bodyJson=getJson(new AddCustomer());
				   return res;
		        }
			
			 res=generalService.savePromoters(addUser);
			 res.bodyJson=getJson(new AddCustomer());
			/* LoggedSession session=(LoggedSession) res.object;
				String compactToken=createToken(session);
				session.token=compactToken;
			res.object=session;*/
			 return res;
	 }
	
	 
	 @RequestMapping(value =GENERATE_PROMOCODE,method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	    public BasicResponse generatePromocode(@Valid @RequestBody PromocodeDetailsVo addUser,Errors errors,HttpServletRequest req)
	 { 
		 BasicResponse res=null;
		 if(addUser == null) 
			{
				 res=new BasicResponse();
				   res.errorCode=1;
				   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
				   res.bodyJson=getJson(new AddCustomer());
				   return res;
			}
			 if (errors.hasErrors()) { 
				   res=new BasicResponse();
				   res.errorCode=2;
				   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
				   res.bodyJson=getJson(new PromocodeDetailsVo());
				   return res;
		        }
			
			 res=generalService.generatePromocode(addUser);
			 res.bodyJson=getJson(new PromocodeDetailsVo());
			 return res;
	 }
	 
	 @RequestMapping(value =SAVE_PROMOCODE_DETAILS,method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	    public BasicResponse savePromocode(@Valid @RequestBody PromocodeDetailsVo addUser,Errors errors,HttpServletRequest req)
	 { 
		 BasicResponse res=null;
		 if(addUser == null) 
			{
				 res=new BasicResponse();
				   res.errorCode=1;
				   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
				   res.bodyJson=getJson(new AddCustomer());
				   return res;
			}
			 if (errors.hasErrors()) { 
				   res=new BasicResponse();
				   res.errorCode=2;
				   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
				   res.bodyJson=getJson(new PromocodeDetailsVo());
				   return res;
		        }
			
			 res=generalService.savePromocode(addUser);
			 res.bodyJson=getJson(new PromocodeDetailsVo());
			 return res;
	 }
	 
	 
	/*@RequestMapping(value = GET_ALL_CUSTOMERS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse getAllCustomers(@RequestParam("customerId") long customerId, HttpServletRequest req,
				HttpServletResponse resp) throws IOException {
			String headerToken=req.getHeader(TOKEN);
			 BasicResponse res=checkTokenOfUser(headerToken);
			 if(res.errorCode != ERROR_CODE_NO_ERROR) {
				 return res;
			 }
			 LoggedSession  session=(LoggedSession) res.object;
			 if(session.roleId != ROLL_ID_ADMIN) {
				   res.errorCode=1;
				   res.errorMessage="only admin are allowed to view";
				  return res;
			 }
			 
			 res = generalService.getAllCustomers(customerId);
			return res;
		} */
	 
	 /*@RequestMapping(value = GET_ALL_CUSTOMERS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse getAllCustomers(@RequestBody GetCustomerVo getPageVo, HttpServletRequest req) throws IOException
		{
			String headerToken=req.getHeader(TOKEN);
			 BasicResponse res=checkTokenOfUser(headerToken);
			 if(res.errorCode != ERROR_CODE_NO_ERROR) {
				 return res;
			 }
			 LoggedSession  session=(LoggedSession) res.object;
			 if(session.roleId != ROLL_ID_ADMIN) 
			 {
				   res.errorCode=1;
				   res.errorMessage="only admin are allowed to view";
				  return res;
			 }
			 
			 
			 
		 BasicResponse
		 
		 res = generalService.getAllCustomers(getPageVo.getCustomerId(),getPageVo.getCityId(),getPageVo.getCountryId()
				 ,getPageVo.getUserId(),getPageVo.getStatus(),getPageVo.getFromDate(),getPageVo.getToDate(),getPageVo.getCurrentIndex(),getPageVo.getRowPerPage(),getPageVo.getCustomerName());
		res.bodyJson=getJson(new GetCustomerVo());
		return res;
		} */
	 
	 
	 @RequestMapping(value = GET_ALL_CUSTOMERS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse getAllCustomers(@RequestBody GetCustomerVo getPageVo, HttpServletRequest req) throws IOException
		{
			/*String headerToken=req.getHeader(TOKEN);
			 BasicResponse res=checkTokenOfUser(headerToken);
			 if(res.errorCode != ERROR_CODE_NO_ERROR) {
				 return res;
			 }
			 LoggedSession  session=(LoggedSession) res.object;
			 if(session.roleId != ROLL_ID_ADMIN) {
				   res.errorCode=1;
				   res.errorMessage="only admin are allowed to view";
				  return res;
			 }*/
			 
			BasicResponse res = generalService.getAllCustomers(getPageVo.getCustomerId(),getPageVo.getCityId(),getPageVo.getCountryId()
					 ,getPageVo.getUserId(),getPageVo.getStatus(),getPageVo.getFromDate(),getPageVo.getToDate(),getPageVo.getCurrentIndex(),getPageVo.getRowPerPage(),getPageVo.getCompanyId(),getPageVo.getCustomerName(),getPageVo.getEmailId(),getPageVo.getPhoneNo(),getPageVo.getCustomerNameAr());
			res.bodyJson=getJson(new GetCustomerVo());
			return res;
		}
	 
	 @RequestMapping(value = GET_ALL_PROMOTERS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse getAllPromoters(@RequestBody GetCustomerVo getPageVo, HttpServletRequest req) throws IOException
		{
			/*String headerToken=req.getHeader(TOKEN);
			 BasicResponse res=checkTokenOfUser(headerToken);
			 if(res.errorCode != ERROR_CODE_NO_ERROR) {
				 return res;
			 }
			 LoggedSession  session=(LoggedSession) res.object;
			 if(session.roleId != ROLL_ID_ADMIN) {
				   res.errorCode=1;
				   res.errorMessage="only admin are allowed to view";
				  return res;
			 }*/
			 
			BasicResponse res = generalService.getAllPromoters(getPageVo.getCustomerId(),getPageVo.getCityId(),getPageVo.getCountryId()
					 ,getPageVo.getUserId(),getPageVo.getStatus(),getPageVo.getFromDate(),getPageVo.getToDate(),getPageVo.getCurrentIndex(),getPageVo.getRowPerPage(),getPageVo.getCompanyId(),getPageVo.getCustomerName(),getPageVo.getEmailId(),getPageVo.getPhoneNo(),getPageVo.getCustomerNameAr());
			res.bodyJson=getJson(new GetCustomerVo());
			return res;
		}
	 
	 
	 @RequestMapping(value = GET_PROMOCODE_DETAILS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse getPromocodeDetails(@RequestBody GetPromocodeDetails getPageVo, HttpServletRequest req) throws IOException
		{
			/*String headerToken=req.getHeader(TOKEN);
			 BasicResponse res=checkTokenOfUser(headerToken);
			 if(res.errorCode != ERROR_CODE_NO_ERROR) {
				 return res;
			 }
			 LoggedSession  session=(LoggedSession) res.object;
			 if(session.roleId != ROLL_ID_ADMIN) {
				   res.errorCode=1;
				   res.errorMessage="only admin are allowed to view";
				  return res;
			 }*/
			 
			BasicResponse res = generalService.getPromocodeDetails(getPageVo.getCodeId(),getPageVo.getUserId(),
					getPageVo.getStatus(),getPageVo.getValidFrom(),getPageVo.getValidTo(),getPageVo.getCurrentIndex(),getPageVo.getRowPerPage(),getPageVo.getPromocode());
			res.bodyJson=getJson(new GetCustomerVo());
			return res;
		}
	 
	 
	 
	 @RequestMapping(value = VALIDATE_PROMOCODE_DETAILS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse validatePromocode(@RequestBody GetPromocodeDetails getPageVo, HttpServletRequest req) throws IOException
		{
			/*String headerToken=req.getHeader(TOKEN);
			 BasicResponse res=checkTokenOfUser(headerToken);
			 if(res.errorCode != ERROR_CODE_NO_ERROR) {
				 return res;
			 }
			 LoggedSession  session=(LoggedSession) res.object;
			 if(session.roleId != ROLL_ID_ADMIN) {
				   res.errorCode=1;
				   res.errorMessage="only admin are allowed to view";
				  return res;
			 }*/
			 
			BasicResponse res = generalService.validatePromocode(getPageVo.getPromocode(),getPageVo.getOrderPrice());
			res.bodyJson=getJson(new GetCustomerVo());
			return res;
		}
	  
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 @RequestMapping(value = GET_USER, method = RequestMethod.GET,produces =  MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse getUser(@RequestParam("userId") long userId,HttpServletRequest req) {
			/*String headerToken=req.getHeader(TOKEN);
			 BasicResponse res=checkTokenOfUser(headerToken);
			 if(res.errorCode != ERROR_CODE_NO_ERROR) {
				 return res;
			 }
			LoggedSession session = (LoggedSession) res.object;//getLogedUserFromSession(req);
	       if(session.roleId != ROLL_ID_ADMIN) 
	       {
	    	   res.errorCode=3;
	    	   res.errorMessage="Only admin allowed to get user login details";
	    	   return res;
	       }*/
		 BasicResponse res=generalService.getUserFromUserId(userId);

	         return res;
		}
	 
	 
	 
	
	 /*@RequestMapping(value = GET_USER, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse getUser(HttpServletRequest req) {
			String headerToken = req.getHeader(TOKEN);
			BasicResponse res = checkTokenOfUser(headerToken);
			if (res.errorCode != ERROR_CODE_NO_ERROR) {
				return res;
			}
			// logger.info(res.errorCode);
			LoggedSession session = (LoggedSession) res.object;// getLogedUserFromSession(req);
			if (session == null) {
				res.errorCode = 2;
				res.errorMessage = "Some problem in send from keramart";
			} else if (session.userId <= 0) {
				res.errorCode = 3;
				res.errorMessage = "Logination invalid";
				res.object = session.userName;
			} else {
				res.errorCode = ERROR_CODE_NO_ERROR;
				res.errorMessage = MESSAGE_SUCSESS;
				res.object = session.userId;

			}
			return res;
		}
	*/
	
	/*
	@RequestMapping(value = ADD_DELIVERY_BOY, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveOrUpdateDeliveryBoy(@Valid @NotNull @RequestBody DeliveryBoyVo deliveryBoyVo,Errors errors, HttpServletRequest req) 
	{
		 //String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new BasicResponse();
				 //checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new DeliveryBoyVo());
			   return res;
	        }
        
		 res = generalService.saveOrUpdateDeliveryBoy(deliveryBoyVo);
		 res.bodyJson=getJson(new DeliveryBoyVo());
		return res;
	}	*/
	
	/* @RequestMapping(value = UPDATE_SELLER_DETAILS, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse UpdateSellerDetails(@Valid @NotNull @RequestBody UpdateSeller sellerDetailsVo, Errors errors,
				HttpServletRequest req) {
			
			 * String headerToken=req.getHeader(TOKEN); BasicResponse
			 * res=checkTokenOfUser(headerToken); if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 * return res; }
			 
			
			 * if (errors.hasErrors()) { res=new BasicResponse(); res.errorCode=2;
			 * res.errorMessage=errors.getAllErrors().stream().map(x ->
			 * x.getDefaultMessage()).collect(Collectors.joining(","));
			 * res.bodyJson=getJson(new UpdateSeller()); return res; }
			 

			BasicResponse res = generalService.saveOrUpdateSellerDetails(sellerDetailsVo);
			res.bodyJson = getJson(new UpdateSeller());
			return res;
		}*/
	@RequestMapping(value = ADD_SELLER_DETAILS, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveOrUpdateSellerDetails(@Valid @NotNull @RequestBody SellerVo sellerDetailsVo,Errors errors, HttpServletRequest req) 
	{
		BasicResponse res=null;
		 /*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 LoggedSession session = (LoggedSession) res.object;
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		*/
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new SellerVo());
			   return res;
	        }
			
			
		/*	logger.info(session.companyId+"companyId___________________________________________");
			sellerDetailsVo.setCompanyId(session.companyId);*/
		 res = generalService.saveOrUpdateSellerDetails(sellerDetailsVo);
		 res.bodyJson=getJson(new SellerVo());
		return res;
	}
	
	
	@RequestMapping(value = ADD_COMPANY_DETAILS, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse saveComapnay(@Valid @NotNull @RequestBody CompanyVo companyVo,Errors errors, HttpServletRequest req) 
	{
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		if (session.roleId != ROLL_ID_ADMIN) {
			res.errorCode = 1;
			res.errorMessage = "only ADMIN are allowed to add";
			res.bodyJson = getJson(new AuctionDetailsVo());
			return res;
		}
		
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new CompanyVo());
			   return res;
	        }
        
		 res = generalService.saveComapnay(companyVo);
		 res.bodyJson=getJson(new CompanyVo());
		return res;
	}
	
	
	
	
	
	
	
	 
	/*@RequestMapping(value = SELLER_LOGIN, method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse sellerLogin(@Valid @RequestParam("email") String email,@RequestParam("password") String password, HttpServletRequest req) 
	{
		 //String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new BasicResponse();
				 //checkTokenOfUser(headerToken);
		
		 res = generalService.sellerLogin(email,password);
		 res.bodyJson=getJson(new SellerDetailsVo());
		return res;
	}*/
	
	@RequestMapping(value = LOGOUT_USER, method = RequestMethod.GET,produces =  MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse logoutUser(HttpServletRequest req) {
		BasicResponse res = new BasicResponse();
		LoggedSession session = null; //setLogedUserFromSession(req);
         if(session == null){
        	 res.errorCode=ERROR_CODE_NO_ERROR;
 			res.errorMessage=MESSAGE_SUCSESS;
         } else
         {
                     res.errorCode=2;
			res.errorMessage="Some happened during logout";
		}
         return res;
	}
	
	
	@RequestMapping(value = GET_ALL_DELIVERY_BOY, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllDeliveryBoy(@Valid @RequestBody GetPageVo getDeliveryBoyVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
		 
		 res = generalService.getAllDeliveryBoy(getDeliveryBoyVo.getAddress(), getDeliveryBoyVo.getDeliveryBoyId(),getDeliveryBoyVo.getDeliveryBoyName(),
				 getDeliveryBoyVo.getCurrentIndex(), getDeliveryBoyVo.getRowPerPage(),getDeliveryBoyVo.getStatus(),getDeliveryBoyVo.getPhoneNumber());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	
	
	@RequestMapping(value = ASSIGNING_OFFER, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse assignOffer(@RequestParam("offerId") long offerId,@RequestParam("productId") long productId,HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		 BasicResponse res=new BasicResponse();
				
		 
		 res = generalService.assignOffer(offerId,productId);
		
		 return res;
	}
	
	
	@RequestMapping(value = CHANGE_DELIVERY_STATUS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changeDeliveryStatus(@RequestParam("orderId") long orderId,@RequestParam("deliveryBoyId") long deliveryBoyId,@RequestParam("status") int status,HttpServletRequest req,HttpServletResponse resp) throws IOException {
		//String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new BasicResponse();
				 //checkTokenOfUser(headerToken);
		/* if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }*/
		 
		 res = generalService.changeDeliveryStatus(orderId,deliveryBoyId,status);
		// res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = CANCELL_DELIVERY_BOY, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse cancellDeriveryBoy(@RequestParam("orderId") long orderId,HttpServletRequest req,HttpServletResponse resp) throws IOException {
String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 
		 res = generalService.cancellDeriveryBoy(orderId);
		
		 return res;
	}
	
	
	@RequestMapping(value = GET_ALL_NOTIFICATION, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllNotification(@Valid @RequestBody GetNotificationVo getSellersVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) 
		 {
			 return res;
		 }*/
		BasicResponse res=null;
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
		
		 res = generalService.getAllNotification(getSellersVo.getCurrentIndex(), getSellersVo.getRowPerPage(),getSellersVo.getOrderStatus(),getSellersVo.getReadStatus(),getSellersVo.getUserId(),getSellersVo.getNotificationId());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	
	
	
	
	

	@RequestMapping(value = GET_ALL_SELLERS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllSeller(@Valid @RequestBody GetPageVo getSellersVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) 
		 {
			 return res;
		 }*/
		BasicResponse res=null;
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
		
		 res = generalService.getAllSeller(getSellersVo.getSellerId(),
				 getSellersVo.getCurrentIndex(), getSellersVo.getRowPerPage(),getSellersVo.getStat(),getSellersVo.getSellerName(),getSellersVo.getPhoneNumber(),getSellersVo.getCompanyId());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	@RequestMapping(value = CHANGE_SELLER_STATUS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changeSellerStatus(@RequestParam("sellerId") long sellerId,@RequestParam("status") int status,HttpServletRequest req,HttpServletResponse resp) throws IOException {
		//String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new BasicResponse();
				 //checkTokenOfUser(headerToken);
		/* if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }*/
		 
		 res = generalService.changeSellerStatus(sellerId,status);
		// res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	@RequestMapping(value = CHANGE_CUSTOMER_STATUS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changeCustomerStatus(@RequestParam("customerId") long customerId,@RequestParam("status") int status,HttpServletRequest req,HttpServletResponse resp) throws IOException {
		//String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new BasicResponse();
				 //checkTokenOfUser(headerToken);
		/* if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }*/
		 
		 res = generalService.changeCustomerStatus(customerId,status);
		// res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	@RequestMapping(value = CHANGE_PROMOCODE_STATUS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changePromocodeStatus(@RequestParam("codeId") long codeId,@RequestParam("status") int status,HttpServletRequest req,HttpServletResponse resp) throws IOException {
		//String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new BasicResponse();
				 //checkTokenOfUser(headerToken);
		/* if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }*/
		 
		 res = generalService.changePromocodeStatus(codeId,status);
		// res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	
}
	
	
	
	
	
	
	
	 
		/* @RequestMapping(value =SAVE_USER,method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
		    public BasicResponse saveUser(@RequestBody AddEmployeeVo addUser,HttpServletRequest req){
			BasicResponse res=new BasicResponse();
			 if( addUser.name == null ||  addUser.password == null || addUser.phoneNo == null){
				 res.errorCode=5;
				 res.errorMessage=MESSAGE_NULL_VALUE;
				 return res;
			 }
			 logger.info("get here the data :");
			 return generalService.saveUser(addUser);
		 }
		 
		 @RequestMapping(value =GET_ALL_USERS,method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
		    public BasicResponse getUsers(@RequestParam("userId") long userId){
			 return generalService.getAllUsers(userId);
		 }
		 
		 @RequestMapping(value =GET_ALL_ROLES,method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
		    public Map<Integer,String> getAllRoles(){
			 return generalService.getAllrolls();
		 }
		 
		 @RequestMapping(value =GET_ALL_CUSTOMERS,method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
		    public BasicResponse getCustomers(@RequestParam("customerId") long customerId){
			 return generalService.getAllCustomers(customerId);
		 }
		 
		 @RequestMapping(value =GET_DASHBOARD_DETAILS,method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
		    public BasicResponse getDashboardDetails(){
			 return generalService.getDashDetails();
		 }*/
		 
		/*@RequestMapping(value = AUTHENTICATION_USER, method = RequestMethod.POST,consumes =  MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse postlogin(@RequestBody LoggedSession session,HttpServletRequest req)
		{
			BasicResponse res = new BasicResponse();
		//	logger.info(res.errorCode);
	        if(session == null || session.phone == null || session.password == null){
	        	res.errorCode=-2;
				res.errorMessage=MESSAGE_NULL_VALUE;
				return res;
	        }
				initSession(req, session);
				Misceleneous.instance().session=session;
				res.errorCode=ERROR_CODE_NO_ERROR;
				res.errorMessage=MESSAGE_SUCSESS;
				res.object=session.userId;
				return res;
		


		}*/
	
	 	

