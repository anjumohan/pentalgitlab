package com.hidile.pental.ctrl.order;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hidile.pental.constants.Constants;
import com.hidile.pental.model.BasicResponse;
import com.hidile.pental.model.LoggedSession;
import com.hidile.pental.model.input.CityVo;
import com.hidile.pental.model.input.CountryVo;
import com.hidile.pental.model.input.GetPageVo;
import com.hidile.pental.model.input.ReturnItemsVo;
import com.hidile.pental.model.order.AuctionDetailsVo;
import com.hidile.pental.model.order.CardDetailsVo;
import com.hidile.pental.model.order.OrderVo;
import com.hidile.pental.model.order.ShippingVo;
import com.hidile.pental.model.order.TaxVo;
import com.hidile.pental.service.CommonService;
import com.hidile.pental.service.order.OrderService;

@RestController
@RequestMapping("/api/order")
public class OrderContrlr extends CommonService implements Constants {
	private static final Logger logger = Logger.getLogger(OrderContrlr.class);
	@Autowired
	private OrderService orderService;
	
	
	
	

	/*@RequestMapping(value = ADD_ORDER, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addOrder(@Valid @RequestBody OrderVo orderVo, Errors errors, HttpServletRequest req) {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		logger.info("hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		if (res.errorCode != ERROR_CODE_NO_ERROR) 
		{
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		if (session.roleId != ROLL_ID_CUSTOMER) 
		{
			res.errorCode = 1;
			res.errorMessage = "only customers are allowed to add";
			res.bodyJson = getJson(new OrderVo());
			return res;
		}
		orderVo.setCustomerId(session.getUserId());
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new OrderVo());
			return res;
		}
		orderVo.setCustomerId(session.userId);
		orderVo.setOperatingOfficerId(session.userId);
		orderVo.setCompanyId(session.companyId);
			res = orderService.saveOrUpdateOrder(orderVo);
		res.bodyJson = getJson(new OrderVo());
		return res;
	}*/
	
	
/*	@RequestMapping(value = ADD_NEW_ORDER, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addOrder(@Valid @NotNull @RequestBody OrderVo orderVo, Errors errors, HttpServletRequest req)
	{
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR)

		{
			res.errorCode = 1;
			res.errorMessage = "error";
			res.bodyJson = getJson(new OrderVo());
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		if (session.roleId != ROLL_ID_CUSTOMER) {
			res.errorCode = 1;
			res.errorMessage = "only customers are allowed to add";
			res.bodyJson = getJson(new OrderVo());
			return res;
		}
		orderVo.setCustomerId(session.getUserId());
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new OrderVo());
			return res;
		}
		orderVo.setCustomerId(session.userId);
		res = orderService.addOrder(orderVo);
		res.bodyJson = getJson(new OrderVo());
		return res;
	}*/
	
	@RequestMapping(value = ADD_TAX, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addTax(@Valid @RequestBody TaxVo taxVo, Errors errors, HttpServletRequest req) {
		//String headerToken = req.getHeader(TOKEN);
		BasicResponse res =new  BasicResponse();
				//checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR)
		{
			return res;
		}
		if (TESTING) {
			logger.info(getJson(taxVo));
		}
		if (errors.hasErrors())
		{
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new TaxVo());
			return res;
		}

		res = orderService.saveOrUpdateTax(taxVo);
		res.bodyJson = getJson(new TaxVo());
		return res;
	}
	
	
	@RequestMapping(value = GET_SHIPPING_BY_ORDERID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getShippingByOrder(@RequestParam("orderId") long orderId, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
	}*/
		BasicResponse res=null;
		
		 
		 res = orderService.getShippingByOrder(orderId);
		return res;
	}
	
	
	@RequestMapping(value = GET_ALL_TAX, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getTax(@Valid @RequestBody GetPageVo getPageVo, Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException {

//String headerToken = req.getHeader(TOKEN);
		BasicResponse res = new BasicResponse();
				//checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		if (TESTING) {
			logger.info(getJson(getPageVo));
		}
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new GetPageVo());
			return res;
		}

		res = orderService.getTax(getPageVo.getCountryId(), getPageVo.getCityId(),getPageVo.getTaxId(),
				getPageVo.getCurrentIndex(), getPageVo.getRowPerPage(),getPageVo.getStat());
		res.bodyJson = getJson(new GetPageVo());
		return res;
	}
	
	
	@RequestMapping(value = DELETE_TAX, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse deleteTax(@RequestParam("taxId") long taxId, HttpServletRequest req)
	{
		BasicResponse res=null;
		
		 res = orderService.deletetax(taxId);
		
		return res;
	}
	
	
	/*@RequestMapping(value = CHANGE_ORDER_STATUS, method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changeStatus(@RequestParam("orderId") long orderId,
			@RequestParam("orderlyStatus") int orderlyStatus, @RequestParam("orderDetailsIds") long[] orderIds,
			HttpServletRequest req) {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		if (orderIds == null || orderIds.length == 0) {
			res.errorCode = 1;
			res.errorMessage = "Order ids " + MESSAGE_NOT_FOUND;
			res.errorMessageArabic = "أوامر الطلب" + MESSAGE_NOT_FOUND_AR;
			return res;
		}
		if (orderId <= 0 || orderlyStatus <= 0) {
			res.errorCode = 1;
			res.errorMessage = "orderid or status is not passed";
			res.errorMessageArabic = "لا يتم تمرير أورديرد أو الوضع";
			return res;
		}
		Long[] inputBoxed = ArrayUtils.toObject(orderIds);
		List<Long> inputAsList = Arrays.asList(inputBoxed);
		res = orderService.changeStatusOfOrder(orderlyStatus, orderId, session.userId, inputAsList);
		return res;

	}*/
	
	@RequestMapping(value = CHANGE_ORDER_STATUS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse changeStatus(@RequestParam("orderId") long orderId, @RequestParam("orderlyStatus") int orderlyStatus,@RequestParam("orderDetailsIds[]") long[] orderIds, HttpServletRequest req) 
	{ 
		 String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 LoggedSession  session=(LoggedSession) res.object;
		if(orderIds == null || orderIds.length == 0)
		{
			res.errorCode=2;
			res.errorMessage="Order ids "+MESSAGE_NOT_FOUND;
			res.errorMessageArabic="أوامر الطلب"+MESSAGE_NOT_FOUND_AR;
			return res;
		}
		if(orderlyStatus <= 0) {
			res.errorCode=1;
			res.errorMessage="orderid or status is not passed";
			res.errorMessageArabic="لا يتم تمرير أورديرد أو الوضع";
			return res;
		}
		List<Long> inputAsList =null;
		if(orderIds[0] != 0) 
		{
		Long[] inputBoxed = ArrayUtils.toObject(orderIds);
		inputAsList = Arrays.asList(inputBoxed);
		}
		logger.info("isndiet");
	    res = orderService.changeStatusOfOrder(orderlyStatus, orderId, session.userId, inputAsList,session.entryId);
	    if(res.errorCode == -25) {
	    	String msg="";
		    if(session.roleId == ROLL_ID_SELLER) {
		    	 msg="Seller is only have privilage to confirm,picked,delivary or reject";
		    }else if(session.roleId == ROLL_ID_CUSTOMER) {
		    	 msg="Customer is only have privilage to order";
		    }
		    res.errorMessage=msg;
	    }
		return res;
	
}
	
	@RequestMapping(value = ADD_SHIPPING_DETAILS, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addShippingDetails(@Valid @RequestBody ShippingVo shippingVo, Errors errors,
			HttpServletRequest req) 
	{
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) 
		{
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		/*if (session.roleId == ROLL_ID_CUSTOMER) 
		{
			shippingVo.setCustomerId(session.entryId);
			logger.info(shippingVo.getCustomerId()+"custmrId------------------------------");
		}*/
		if (TESTING) {
			logger.info(getJson(shippingVo));
		}
		if (errors.hasErrors()) 
		{
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new ShippingVo());
			return res;
		}
		shippingVo.setCustomerId(session.entryId);
		res = orderService.addShippingDetails(shippingVo);
		res.bodyJson = getJson(new ShippingVo());
		return res;
	}
	
	@RequestMapping(value = ADD_CARD_DETAILS, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addCardDetails(@Valid @RequestBody CardDetailsVo shippingVo, Errors errors,
			HttpServletRequest req) 
	{
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		
		
		LoggedSession session = (LoggedSession) res.object;
		if (session.roleId == ROLL_ID_CUSTOMER) {
			shippingVo.setCustomerId(session.entryId);
			logger.info(shippingVo.getCustomerId()+"custmrId------------------------------");
		}
		if (TESTING)
		{
			logger.info(getJson(shippingVo));
		}
		if (errors.hasErrors()) 
		{
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new CardDetailsVo());
			return res;
		}

		res = orderService.addCardDetails(shippingVo);
		res.bodyJson = getJson(new CardDetailsVo());
		return res;
	}

	@RequestMapping(value = DELETE_SHIPPING, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse deleteShipping(@RequestParam("shippingId") long shippingId,HttpServletRequest req,
			HttpServletResponse resp) throws IOException {
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res=null;
		 
		 res = orderService.deleteShipping(shippingId);
		return res;
	}
	
	@RequestMapping(value = DELETE_CARD, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse deleteCard(@RequestParam("cardId") long cardId,HttpServletRequest req,
			HttpServletResponse resp) throws IOException {
		/*String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }*/
		BasicResponse res=null;
		 
		 res = orderService.deleteCard(cardId);
		return res;
	}
	
	
	
	
	@RequestMapping(value = GET_ALL_SHIPPING_DETAILS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllShippingDetails(@Valid @RequestBody GetPageVo getPageVo, Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException {

		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		LoggedSession session=(LoggedSession) res.object;
		if (TESTING) {
			logger.info(getJson(getPageVo));
		}
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			//res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			res.bodyJson = getJson(new GetPageVo());
			return res;
		}
		long customerId=getPageVo.getCustomerId();
        if(session.roleId == ROLL_ID_CUSTOMER) {
        	customerId=session.entryId;
        }
        logger.info(getPageVo.getStat());
		res = orderService.getAllShippingDetails(customerId,getPageVo.getShippingId(),
				getPageVo.getCurrentIndex(), getPageVo.getRowPerPage(),getPageVo.getStat());
		res.bodyJson = getJson(new GetPageVo());
		return res;
	}
	
	@RequestMapping(value = GET_CARD_DETAILS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getCardDetails(@Valid @RequestBody CardDetailsVo getPageVo, Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{

		/*String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) 
		{
			return res;
		}
		LoggedSession session=(LoggedSession) res.object;
		if (TESTING) {
			logger.info(getJson(getPageVo));
		}
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			//res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			res.bodyJson = getJson(new GetPageVo());
			return res;
		}
		long customerId=getPageVo.getCustomerId();
        if(session.roleId == ROLL_ID_CUSTOMER) {
        	customerId=session.entryId;
        }*/
		BasicResponse res = orderService.getCardDetails(getPageVo.getCardId(),getPageVo.getCustomerId(),getPageVo.getCurrentIndex(), getPageVo.getRowPerPage());
		res.bodyJson = getJson(new GetPageVo());
		return res;
	}
	
	
	@RequestMapping(value = ADD_COUNTRTY, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addCountry(@Valid @RequestBody CountryVo countryVo,Errors errors, HttpServletRequest req) 
	{BasicResponse res=null;
		/* String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new CountryVo());
			   return res;
	        }
        */
		 res = orderService.saveOrUpdateCountry(countryVo);
		 res.bodyJson=getJson(new CountryVo());
		return res;
	}
	
	@RequestMapping(value = ADD_CITY, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addCity(@Valid @RequestBody CityVo cityVo,Errors errors, HttpServletRequest req) 
	{BasicResponse res=null;
		/* String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new CityVo());
			   return res;
	        }
        */
		 res = orderService.saveOrUpdateCity(cityVo);
		 res.bodyJson=getJson(new CityVo());
		return res;
	}
	
	
	
	
	
	

	/*
	 * @RequestMapping(value = CHANGE_AUCTION_STATUS, method = RequestMethod.GET,
	 * consumes = MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * changeStatus(@RequestParam("auctionId") long
	 * auctionId, @RequestParam("auctionStatus") int auctionStat, HttpServletRequest
	 * req) { String headerToken=req.getHeader(TOKEN); BasicResponse
	 * res=checkTokenOfUser(headerToken); if(res.errorCode != ERROR_CODE_NO_ERROR) {
	 * return res; } LoggedSession session=(LoggedSession) res.object;
	 * if(session.roleId != ROLL_ID_ADMIN || session.roleId != ROLL_ID_SHOP) {
	 * res.errorCode=1; res.errorMessage="customers are not allowed to change";
	 * res.bodyJson=getJson(new AuctionDetailsVo()); return res; } if(auctionId <= 0
	 * || auctionStat <= 0) { res.errorCode=1;
	 * res.errorMessage="auctionid or status is not passed";
	 * res.errorMessageArabic="مزاد أو الحالة لا يتم تمريرها"; return res; } res =
	 * orderService.changeStatusOfAuction(auctionStat, auctionId, session.userId);
	 * return res;
	 * 
	 * }
	 * 
	 * 
	 * @RequestMapping(value = SAVE_BID, method = RequestMethod.POST, produces =
	 * MediaType.APPLICATION_JSON_VALUE) public BasicResponse saveBid(@RequestBody
	 * AuctionDetailsVo auctionDetailsVo,Errors errors, HttpServletRequest
	 * req,HttpServletResponse resp) throws IOException {
	 * 
	 * String headerToken=req.getHeader(TOKEN); BasicResponse
	 * res=checkTokenOfUser(headerToken); if(res.errorCode != ERROR_CODE_NO_ERROR) {
	 * return res; } LoggedSession session=(LoggedSession) res.object;
	 * if(session.roleId != ROLL_ID_CUSTOMER) { res.errorCode=1;
	 * res.errorMessage="only customers are allowed to add";
	 * res.bodyJson=getJson(new AuctionDetailsVo()); return res; } if
	 * (errors.hasErrors()) { res=new BasicResponse(); res.errorCode=2;
	 * res.errorMessage=errors.getAllErrors().stream().map(x ->
	 * x.getDefaultMessage()).collect(Collectors.joining(","));
	 * res.bodyJson=getJson(new AuctionDetailsVo()); return res; }
	 * 
	 * auctionDetailsVo.setUserId(session.userId); res =
	 * orderService.saveAuctionDetails(auctionDetailsVo); res.bodyJson=getJson(new
	 * AuctionDetailsVo()); return res; }
	 * 
	 * @RequestMapping(value = DELETE_BID, method = RequestMethod.GET, produces =
	 * MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * deleteBid(@RequestParam("auctionId") long
	 * auctionId,@RequestParam("auctionDetailsId") long auctionDetailsId,
	 * HttpServletRequest req) {
	 * 
	 * String headerToken=req.getHeader(TOKEN); BasicResponse
	 * res=checkTokenOfUser(headerToken); if(res.errorCode != ERROR_CODE_NO_ERROR) {
	 * return res; } LoggedSession session=(LoggedSession) res.object;
	 * if(session.roleId != ROLL_ID_CUSTOMER) { res.errorCode=1;
	 * res.errorMessage="only customers are allowed to add";
	 * res.bodyJson=getJson(new AuctionDetailsVo()); return res; } res =
	 * orderService.deleteAuctionDetails(auctionId,auctionDetailsId);
	 * 
	 * return res; }
	 */

	/*
	 * @RequestMapping(value = ADD_AUCTION, method = RequestMethod.POST, consumes =
	 * MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * addAuction(@Valid @RequestBody AuctionVo auctionVo,Errors errors,
	 * HttpServletRequest req) { String headerToken=req.getHeader(TOKEN);
	 * BasicResponse res=checkTokenOfUser(headerToken); if(res.errorCode !=
	 * ERROR_CODE_NO_ERROR) { return res; } LoggedSession session=(LoggedSession)
	 * res.object; if (errors.hasErrors()) { res=new BasicResponse();
	 * res.errorCode=2; res.errorMessage=errors.getAllErrors().stream().map(x ->
	 * x.getDefaultMessage()).collect(Collectors.joining(","));
	 * res.bodyJson=getJson(new AuctionVo()); return res; }
	 * 
	 * auctionVo.setUserId(session.userId); res =
	 * orderService.saveOrUpdateAuction(auctionVo); res.bodyJson=getJson(new
	 * AuctionVo()); return res; }
	 */

	/*
	 * @RequestMapping(value = ADD_CART, method = RequestMethod.POST, consumes =
	 * MediaType.APPLICATION_JSON_VALUE) public BasicResponse
	 * addtoCart(@Valid @RequestBody CartAndFavouritesVo cartAndFavouritesVo,
	 * HttpServletRequest req) {
	 * 
	 * response = orderService.saveOrUpdateCart(cartAndFavouritesVo); return
	 * response; }
	 */

	@RequestMapping(value = RETURN_PRODUCT, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addReturnProduct(@RequestBody List<ReturnItemsVo> returnItemsVo, Errors errors,
			HttpServletRequest req)
	{
	String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
	if (res.errorCode != ERROR_CODE_NO_ERROR) {
			res.bodyJson=getJson(new ReturnItemsVo());
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		returnItemsVo.get(0).setOperatingOfficerId(session.userId);
		returnItemsVo.get(0).setCompanyId(session.companyId);
		 res = orderService.addReturnProduct(returnItemsVo);
		
		return res;
	}
	
	
	
	
	
	@RequestMapping(value = GET_ALL_RETURN_PRODUCTS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllReturnProducts(@Valid @RequestBody GetPageVo getPageVo, Errors errors, HttpServletRequest req)
	{
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		
		if (errors.hasErrors()) 
		{
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new GetPageVo());
			return res;
		}

		res = orderService.getAllReturnProducts(getPageVo.getReturnItemId(),getPageVo.getOrderDetailsId(),getPageVo.getRowPerPage(),getPageVo.getCurrentIndex(),getPageVo.getStoreId(),getPageVo.getStat());
		res.bodyJson = getJson(new GetPageVo());
		
		return res;
		
	}

}
