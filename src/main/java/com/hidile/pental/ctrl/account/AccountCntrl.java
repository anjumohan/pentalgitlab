package com.hidile.pental.ctrl.account;

import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hidile.pental.constants.Constants;
import com.hidile.pental.ctrl.input.InputRestCtrl;
import com.hidile.pental.model.BasicResponse;
import com.hidile.pental.model.LoggedSession;
import com.hidile.pental.model.account.ExpenseVo;
import com.hidile.pental.model.account.IncomeExpenseHeadVo;
import com.hidile.pental.model.account.IncomeVo;
import com.hidile.pental.service.CommonService;
import com.hidile.pental.service.account.AccountService;

@RestController
@RequestMapping("/api/account")
public class AccountCntrl extends CommonService implements Constants {
	
	private static final Logger logger = Logger.getLogger(InputRestCtrl.class);
	@Autowired
	private AccountService accountService;
	BasicResponse response = null;
	
	
	@RequestMapping(value = ADD_INCOME_EXPENSE_HEAD, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addExpenseeHead(@Valid @RequestBody IncomeExpenseHeadVo incExpHedModel, Errors errors, HttpServletRequest req) {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new IncomeExpenseHeadVo());
			return res;
		}

		incExpHedModel.setOperatingOfficer(session.userId);
		res = accountService.saveorUpdateIncomeExpenseHead(incExpHedModel);
		res.bodyJson = getJson(new IncomeExpenseHeadVo());
		return res;
	}
	
	
	@RequestMapping(value = GET_INCOME_EXPENSE_HEAD, method = RequestMethod.GET)
	public BasicResponse getIncomeExpenseHead(@RequestParam("incomeOrExpense") int incomeOrExpense,@RequestParam("incomExpnsHeadId") long incomExpnsHeadId,
			@RequestParam("status") int status, HttpServletRequest req) {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;

		}
		LoggedSession session = (LoggedSession) res.object;
		res = accountService.getIncomeExpenseHead(incomeOrExpense,incomExpnsHeadId,status);
		return res;
	}
	
	
	@RequestMapping(value = ADD_INCOME, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addIncome(@Valid @RequestBody IncomeVo incomeModel, Errors errors, HttpServletRequest req) {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new IncomeVo());
			return res;
		}

		incomeModel.setOperatingOfficer(session.userId);
		res = accountService.saveorUpdateIncome(incomeModel);
		res.bodyJson = getJson(new IncomeVo());
		return res;
	}
	
	@RequestMapping(value = ADD_EXPENSE, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse addExpense(@Valid @RequestBody ExpenseVo expenseModel, Errors errors, HttpServletRequest req) 
	{
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
		if (errors.hasErrors())
		{
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new ExpenseVo());
			return res;
		}

		expenseModel.setOperatingOfficer(session.userId);
		res = accountService.saveorUpdateExpense(expenseModel);
		res.bodyJson = getJson(new ExpenseVo());
		return res;
	}
	

	
	@RequestMapping(value = GET_INCOME, method = RequestMethod.GET)
	public BasicResponse getIncomes(@RequestParam("currentIndex") int currentIndex,
			@RequestParam("rowPerPage") int rowPerPage, @RequestParam("incomeId") long incomeId,
			@RequestParam("status") int status,HttpServletRequest req) {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;

		}
		LoggedSession session = (LoggedSession) res.object;
		res = accountService.getIncomes(currentIndex, rowPerPage,incomeId,status);
		return res;
	}
	
	
	@RequestMapping(value = GET_EXPENSE, method = RequestMethod.GET)
	public BasicResponse getExpenses(@RequestParam("currentIndex") int currentIndex,
			@RequestParam("rowPerPage") int rowPerPage,@RequestParam("expenseId") long expenseId,
			@RequestParam("status") int status, HttpServletRequest req) {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;

		}
		LoggedSession session = (LoggedSession) res.object;
		res = accountService.getExpenses(currentIndex, rowPerPage,expenseId,status);
		return res;
	}
}
