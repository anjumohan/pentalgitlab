package com.hidile.pental.ctrl.report;


import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.util.json.JSONException;
import com.hidile.pental.constants.Constants;
import com.hidile.pental.model.BasicResponse;
import com.hidile.pental.model.LoggedSession;
import com.hidile.pental.model.input.GetPageVo;
import com.hidile.pental.model.input.GetTaxVo;
import com.hidile.pental.model.input.OfferVo;
import com.hidile.pental.model.order.GetTransactionVo;
import com.hidile.pental.model.order.OrderDetailsVo;
import com.hidile.pental.model.order.OrderNewVos;
import com.hidile.pental.model.order.OrderVo;
import com.hidile.pental.service.CommonService;
import com.hidile.pental.service.input.InputService;
import com.hidile.pental.service.order.OrderService;
import com.hidile.pental.service.report.ReportService;


@RestController
@RequestMapping("/api/report")


public class ReportCtrl extends CommonService implements Constants {

	@Autowired
	InputService inputService;
	@Autowired
	OrderService orderService;
	@Autowired
	ReportService reportService;
	private Logger logger = Logger.getLogger(ReportCtrl.class);
	
	
	@RequestMapping(value = ADD_NEW_ORDER, method = RequestMethod.POST, consumes ="application/json",headers = "content-type=application/json")
	public BasicResponse addOrder(@Valid @NotNull @RequestBody OrderNewVos orderVo, Errors errors, HttpServletRequest request) throws JSONException
	{
		//BasicResponse res = new BasicResponse();
		
	String headerToken = request.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR)

		{
			
			return res;
		}
		LoggedSession session = (LoggedSession) res.object;
	//logger.info(session.getRoleId()+"roleId________________________****************************************************");
		
		orderVo.setCustomerId(session.getUserId());
		if (errors.hasErrors()) 
		{
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new OrderNewVos());
			return res;
		}
		orderVo.setCustomerId(session.userId);
		orderVo.setCountryCode(session.countryCode);
		orderVo.setCustomerPhone(session.phone);
		res = reportService.addOrder(orderVo);
		res.bodyJson = getJson(new OrderNewVos());
		return  res;
	}
//@ExceptionHandler(value = Exception.class)  
	@RequestMapping(value = GET_ALL_ORDERS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllOrder(@Valid @RequestBody GetPageVo getOrderVo, Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{

		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		LoggedSession session = (LoggedSession) res.object;
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		// BasicResponse res=null;
		if (errors.hasErrors()) {
			res = new BasicResponse();
			res.errorCode = 2;
			res.errorMessage = errors.getAllErrors().stream().map(x -> x.getDefaultMessage())
					.collect(Collectors.joining(","));
			res.bodyJson = getJson(new GetPageVo());
			return res;
		}
		if (session.getRoleId() == ROLL_ID_CUSTOMER) 
		{
			getOrderVo.setCustomerId(session.getUserId());
		}
		res = reportService.getAllOrder(getOrderVo.getOrderId(), getOrderVo.getStoreId(), getOrderVo.getCustomerId(),
				getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage(), getOrderVo.getStatus(),
				getOrderVo.getFromDate(), getOrderVo.getToDate(), getOrderVo.getDeliveryDate(),
				getOrderVo.getServiceOrProduct(), getOrderVo.getDeliveryBoyId(), getOrderVo.getOrderListId());
		res.bodyJson = getJson(new GetPageVo());
	
		return res;
	}
	
	@RequestMapping(value = GET_ALL_ORDER_DETAILS_BY, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllOrdersDetails(@Valid @RequestBody GetPageVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) 
		 { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	      }
		 
		 res = reportService.getAllOrdersDetails(getOrderVo.getOrderId(), getOrderVo.getOrderDetailsId(), getOrderVo.getCustomerId(), getOrderVo.getFromDate(), getOrderVo.getToDate(), getOrderVo.getSellerId(), getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage(),getOrderVo.getCompanyId());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	@RequestMapping(value = BEST_PURCHASED_PRODUCTS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse bestPurchasedProducts(@Valid @RequestBody OrderDetailsVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		
		 
		BasicResponse res = reportService.bestPurchasedProducts(getOrderVo.getOrderId(), getOrderVo.getOrderListId(), getOrderVo.getCustomerId(), getOrderVo.getFromDate(), getOrderVo.getToDate(), getOrderVo.getSellerId(), getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage(),getOrderVo.getCompanyId(),getOrderVo.getCategoryId(),getOrderVo.getSubCategoryId());
		 res.bodyJson=getJson(new OrderDetailsVo());	
		 return res;
	}

	
	@RequestMapping(value = COMMISSION_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse commissionReport(@Valid @RequestBody GetPageVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		
		BasicResponse	res = reportService.commissionReport(getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage(),getOrderVo.getStat(),getOrderVo.getLastUpdatedTime(),getOrderVo.getOrderId(),getOrderVo.getUserId(),getOrderVo.getPriceStarts(),getOrderVo.getPriceEnds(),getOrderVo.getFromDate(),getOrderVo.getToDate(),getOrderVo.getTransactionMode(),getOrderVo.gettMode());
		 res.bodyJson=getJson(new GetPageVo());
		 return res;
	}

	
	
	
	@RequestMapping(value = GET_ALL_ADD_SATUS, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllAdd(@Valid @RequestBody GetPageVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
  /* String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
	        */
		 
		BasicResponse  res = reportService.getAllAdd(getOrderVo.getStatus(),getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage(),getOrderVo.getCompanyId(),getOrderVo.getModeId(),getOrderVo.getModeIds(),getOrderVo.getArabicOrEnglish(),getOrderVo.getStat());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	
	@RequestMapping(value = GET_ALL_SALES_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllSalesReport(@Valid @RequestBody GetPageVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
         //String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new  BasicResponse();
				 //checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR)
		 {
			 
			 
			 
			 return res;
		 }
		 if (errors.hasErrors()) 
		 { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	     }
		 
		// res = reportService.getAllSalesReport(getOrderVo.getStoreId(),getOrderVo.getCustomerId(),getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage(),getOrderVo.getDeliveryBoyId(),getOrderVo.getCategoryId(),getOrderVo.getProductId(),getOrderVo.getSellerId(),getOrderVo.getFromDate(),getOrderVo.getToDate(),getOrderVo.getOrderId(),getOrderVo.getSubCategoryId(),getOrderVo.getTransactionId());
		 res = 	 reportService.getAllSalesReport(getOrderVo.getStoreId(), getOrderVo.getCustomerId(),
					getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage(), getOrderVo.getDeliveryBoyId(),
					getOrderVo.getCategoryId(), getOrderVo.getProductId(), getOrderVo.getSellerId(),
					getOrderVo.getFromDate(), getOrderVo.getToDate(), getOrderVo.getSubCategoryId(),
					getOrderVo.getTranMode());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	@RequestMapping(value = GET_ALL_PROMOCODE_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse promocodeReport(@Valid @RequestBody GetPageVo getOrderVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
         //String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new  BasicResponse();
				 //checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR)
		 {
			 return res;
		 }
		 if (errors.hasErrors()) 
		 { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	     }
		 
		 res = reportService.getPromocodeReport(getOrderVo.getCurrentIndex(), getOrderVo.getRowPerPage(),getOrderVo.getFromDate(),getOrderVo.getToDate(),getOrderVo.getCodeId(),getOrderVo.getUserId(),getOrderVo.getPromoterId());
		
		 return res;
	}
	
	@RequestMapping(value = MONTHLY_REVENUE_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse monthlyRevenueReport(@Valid @RequestBody GetTransactionVo getTran,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
         //String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new  BasicResponse();
				 //checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR)
		 {
			 return res;
		 }
		 if (errors.hasErrors()) 
		 { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	     }
		 
		 res = reportService.monthlyRevenueReport(getTran.getCurrentIndex(), getTran.getRowPerPage(),getTran.getFromDate(),getTran.getToDate(),getTran.getCodeId());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	
	@RequestMapping(value = YEARLY_REVENUE_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse yearlyRevenueReport(@Valid @RequestBody GetTransactionVo getTran,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
         //String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new  BasicResponse();
				 //checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR)
		 {
			 return res;
		 }
		 if (errors.hasErrors()) 
		 { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	     }
		 
		 res = reportService.yearlyRevenueReport(getTran.getCurrentIndex(), getTran.getRowPerPage(),getTran.getFromDate(),getTran.getToDate(),getTran.getCodeId());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}
	
	@RequestMapping(value = WEEKLY_REVENUE_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse weeklyRevenueReport(@Valid @RequestBody GetTransactionVo getTran,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
         //String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=new  BasicResponse();
				 //checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR)
		 {
			 return res;
		 }
		 if (errors.hasErrors()) 
		 { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	     }
		 
		 res = reportService.weeklyRevenueReport(getTran.getCurrentIndex(), getTran.getRowPerPage(),getTran.getFromDate(),getTran.getToDate(),getTran.getCodeId());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	}

	@RequestMapping(value = GET_ALL_DETAILS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getAllOverallDetails(HttpServletRequest req,
			HttpServletResponse resp) throws IOException {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			//res.bodyJson=getJson(new GetBookVo());
			return res;
		}
		LoggedSession session=(LoggedSession) res.object;
		long userId=0;
		if(session.roleId == ROLL_ID_ADMIN) {
			userId=session.userId;
		}else if(session.roleId == ROLL_ID_SELLER) {
			userId=session.userId;
		}
		res = reportService.getAllDetails(userId,session.roleId);
		logger.info(getJson(res));
		return res;
	}

	
	
	@RequestMapping(value = LOW_STOCK_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse lowStockReport(@Valid @RequestBody GetPageVo getProductVo,Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
	
		 BasicResponse	res = reportService.lowStockReport(getProductVo.getCurrentIndex(), getProductVo.getRowPerPage(),getProductVo.getStat(),getProductVo.getNumberOfItemsRemaining(),getProductVo.getNumberStarts(),getProductVo.getNumberEnds());
		 res.bodyJson=getJson(new GetPageVo());
		 return res;
	}
	
	@RequestMapping(value = TRANSACTION_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse transactionReport(@Valid @RequestBody GetPageVo getProductVo,Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
	
		 BasicResponse	res = reportService.transactionReport(getProductVo.getCurrentIndex(), getProductVo.getRowPerPage(),getProductVo.getStat(),getProductVo.getLastUpdatedTime(),getProductVo.getOrderId(),getProductVo.getUserId(),getProductVo.getPriceStarts(),getProductVo.getPriceEnds(),getProductVo.getFromDate(),getProductVo.getToDate(),getProductVo.getTransactionMode(),getProductVo.gettMode(),getProductVo.getiNoR(),getProductVo.getBankOrCash());
		 res.bodyJson=getJson(new GetPageVo());
		 return res;
	}
	
	
	@RequestMapping(value = INCOME_EXPENCE_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse incomeExpenceReport(@Valid @RequestBody GetPageVo getProductVo,Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
	
		 BasicResponse	res = reportService.incomeExpenceReport(getProductVo.getCurrentIndex(), getProductVo.getRowPerPage(),getProductVo.getStat(),getProductVo.getLastUpdatedTime(),getProductVo.getOrderId(),getProductVo.getUserId(),getProductVo.getPriceStarts(),getProductVo.getPriceEnds(),getProductVo.getFromDate(),getProductVo.getToDate(),getProductVo.getTransactionMode(),getProductVo.gettMode());
		 res.bodyJson=getJson(new GetPageVo());
		 return res;
	}
	
	
	@RequestMapping(value = TAX_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse taxReport(@Valid @RequestBody GetTaxVo getProductVo,Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
	
		 BasicResponse	res = reportService.taxReport(getProductVo.getCurrentIndex(), getProductVo.getRowPerPage(),getProductVo.getStat(),getProductVo.getLastUpdatedTime(),getProductVo.getOrderId(),getProductVo.getPriceStarts(),getProductVo.getPriceEnds(),getProductVo.getFromDate(),getProductVo.getToDate(),getProductVo.getTaxId());
		 res.bodyJson=getJson(new GetTaxVo());
		 return res;
	}
	
	
	
	
	
	@RequestMapping(value = PRODUCT_PURCHASE_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse productPurchase(@Valid @RequestBody GetPageVo getProductVo,Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
	
		BasicResponse	res = reportService.getAllProduct(getProductVo.getProductId(), getProductVo.getSellerId(),getProductVo.getUserId(),
				0, getProductVo.getCategoryId(), getProductVo.getSubCategoryId(),getProductVo.getCountryId()
				,getProductVo.getCityId(),getProductVo.getCurrentIndex(), getProductVo.getRowPerPage(),getProductVo.getStat(),getProductVo.getCompanyId(),getProductVo.getNumberOfItemsRemaining(),getProductVo.getOfferId(),getProductVo.getManufacturerId(),getProductVo.getPriceStarts(),getProductVo.getPriceEnds(),getProductVo.getProductAttributeId()
				
				);
		 res.bodyJson=getJson(new GetPageVo());
		return res;
	}
	
	
	
	
	
	
	
	/*@RequestMapping(value = PRODUCT_PURCHASE_REPORT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse productPurchaseReport(@Valid @RequestBody GetPageVo getProductVo,Errors errors, HttpServletRequest req,
			HttpServletResponse resp) throws IOException 
	{
		
    String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR)
		 {
			 return res;
		 }
		if(getProductVo == null) 
		{
			 res=new BasicResponse();
			   res.errorCode=1;
			   res.errorMessage=MESSAGE_NULL_VALUE +" PASSED";
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
		}
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
		 
		BasicResponse	res = reportService.productPurchaseReport(getProductVo.getCurrentIndex(), getProductVo.getRowPerPage());
		 res.bodyJson=getJson(new GetPageVo());
		return res;
	}*/
	
	
	

	
	/*@RequestMapping(value = GET_ALL_AUCTION, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getService(@Valid @RequestBody GetPageVo getAuctionVo,Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
		 
		 res = reportService.getAllAuctionFrom(getAuctionVo.getAuctionId(),getAuctionVo.getCustomerId(),getAuctionVo.getAuctionDetaiilsId(), getAuctionVo.getRowPerPage(),getAuctionVo.getCurrentIndex(),getAuctionVo.getStatus(),getAuctionVo.getFromDate(),getAuctionVo.getToDate());
		 res.bodyJson=getJson(new GetPageVo());	
		 return res;
	} 
	
	@RequestMapping(value = GET_ALL_AUCTION_BY, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getServiceAuction(@Valid @RequestBody GetPageVo auctionDetailsVo , Errors errors, HttpServletRequest req,HttpServletResponse resp) throws IOException {
		
		String headerToken=req.getHeader(TOKEN);
		 BasicResponse res=checkTokenOfUser(headerToken);
		 if(res.errorCode != ERROR_CODE_NO_ERROR) {
			 return res;
		 }
		 if (errors.hasErrors()) { 
			   res=new BasicResponse();
			   res.errorCode=2;
			   res.errorMessage=errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(","));
			   res.bodyJson=getJson(new GetPageVo());
			   return res;
	        }
			
	
			res = reportService.getAllAuctionDetailsBy(auctionDetailsVo.getAuctionId(),auctionDetailsVo.getAuctionDetaiilsId(),auctionDetailsVo.getCustomerId(),auctionDetailsVo.getCurrentIndex(),auctionDetailsVo.getRowPerPage(),auctionDetailsVo.getFromDate(),auctionDetailsVo.getToDate());
			 res.bodyJson=getJson(new GetPageVo());
			 return res;
		}*/

	/*@RequestMapping(value = URL_MONTH_EACH_DAY_STATUS_BOOKING, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getDateStatus(@RequestParam("month") int month, @RequestParam("year") int year,
			@RequestParam("palaceId") long palaceId, HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		BasicResponse res = new BasicResponse();
		if (month <= 0 || year <= 0 || palaceId < 0) {
			res.errorCode = -3;
			res.errorMessage = "INCORRECT DATA INPUTED";
			return res;
		}
		res=reportService.getBookingDetailsOfMonth(month, year, palaceId);
		logger.info(getJson(res));
		return res;
	}

	@RequestMapping(value = URL_REPORT_BOOKING, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse search(@RequestBody ReportVo reportVo) {
		BasicResponse res = new BasicResponse();
		logger.info(getJson(reportVo));
		if (reportVo.rowPerPage <= 0) {
			res.errorCode = 5;
			res.errorMessage = MESSAGE_NULL_VALUE;
			return res;
		}
		res= reportService.getBookingDate(reportVo.ownerId, reportVo.palaceId,reportVo.customerId, reportVo.fromDate, reportVo.toDate, reportVo.status
				, reportVo.country, reportVo.state, reportVo.city,reportVo.firstIndex,reportVo.rowPerPage,reportVo.bookFromDate,reportVo.bookToDate);
		logger.info(getJson(res));
		return res;
	}

	@RequestMapping(value = GET_BOOKING_CUSTOMER, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse getCustomerBooings(@RequestParam("customerId") long customerId,
			@RequestParam("status") int status, @RequestParam("bookingId") long bookingId, HttpServletRequest req,
			HttpServletResponse resp) throws IOException {
		BasicResponse res = new BasicResponse();
		if (customerId <= 0) {
			res.errorCode = -3;
			res.errorMessage = "INCORRECT DATA INPUTED";
			return res;
		}
		res=inputService.getBookingOfCustomer(customerId, status, bookingId);
		logger.info(getJson(res));
		return res;
	}
	
	
	
	
	 @RequestMapping(value = GET_ALL_DETAILS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public BasicResponse getAllOverallDetails(@RequestParam("ownerId") long ownerId, HttpServletRequest req, HttpServletResponse resp)
				throws IOException {
			BasicResponse res = new BasicResponse();
			LoggedSession session = getLogedUserFromSession(req);
			if (session == null) {
				// resp.sendRedirect(GENERAL_FIRST_URL);
				res.errorCode = -1;
				res.errorMessage = "Please login";
				return res;
			}
			if (userId == 0) {
				userId = session.userId;
			}
			res = reportService.getAllDetails(ownerId);
			logger.info(getJson(res));
			return res;
		}*/

}
