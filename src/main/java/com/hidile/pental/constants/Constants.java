package com.hidile.pental.constants;

import java.security.Key;

import io.jsonwebtoken.impl.crypto.MacProvider;

public interface Constants extends InputConst,ReportConst,UserConst,Errors,RegexConst,OrderConst,TransConst{

	
	public static final int ERROR_CODE_NO_ERROR=0;
	public static final String ERROR_MESSAGE="ERROR MESSAGE";
	public static final String ERROR_MESSAGE_ARABIC="ERROR MESSAGE ARABIC";
	public static final String ERROR_CODE="ERROR CODE";
	public static final String SESSION_USER = "USER_SESSION";
	public static final int TAKE_TIME_SOURSE=0;
	public static final String DATA="Data";
	public static final String TOTAL_COUNT="totalcount";
	public static final double ROUND_OFF_DECMAL_TO = 100.0;
	public static final double ROUND_OFF_DECIMAL_TO_ONE = 10.0;


	
	public static final String BASIC_URL="http://18.221.8.43:7076";
	
	public static final String TOKEN="Bearer";
	public static final Key key = MacProvider.generateKey();
	
	
	//---------------PENDING TASK SCREEN NAVIGATING CONSTANTS---------------------
	
	
    public static final boolean TESTING=true;
    public static final String FIRST_URL="../login";
    public static final String GENERAL_FIRST_URL="/login";
    public static final String INITIAL_URL="";
    
    
  //------------------  URL CONSTANTS---------------------------------------------
   
    
  //----------------GET VIEW URLS-------------------------------------------  
    public static final String GET_VIEW_LOGIN_SCREEN="/login";
    public static final String GET_VIEW_HOME="/home";
    public static final String SAVE_DEFAULT_ADMIN="/adddefaultuser";
  
    
  //---------------------  PATH CONSTANTS --------------------------------------------
    
    public static final String VIEW_LOGIN_SCREEN="commons/login";
    public static final String VIEW_HOME_SCREEN="commons/home";
    
    //------------------  USUAL CONSTANTS---------------------------------------------
    
	public static final String SUBSCRIBE_MODE_NAME_DAILY="DAILY";
	public static final String SUBSCRIBE_MODE_NAME_MONTHLY="MONTHLY";
	public static final String SUBSCRIBE_MODE_NAME_WEEKLY="WEEKLY";
	public static final String SUBSCRIBE_MODE_NAME_YEARLY="YEARLY";
	public static final String SUBSCRIBE_MODE_NAME_TWO_MONTHS="IN TWO MONTHS";
	public static final String SUBSCRIBE_MODE_NAME_THREE_MONTHS="IN THREE MONTHS";
	public static final String SUBSCRIBE_MODE_NAME_TWO_WEEKS="IN TWO WEEKS";
	public static final String SUBSCRIBE_MODE_NAME_THREE_WEEKS="IN THREE WEEKS";
	//---------------------------------------mail


	public static final int PHONE=1;
	public static final int WEB=2;
	
	
	
	
	public static final int SUBSCRIBE_MODE_DAILY=1;
	public static final int SUBSCRIBE_MODE_WEEKLY=2;
	public static final int SUBSCRIBE_MODE_MONTHLY=3;
	public static final int SUBSCRIBE_MODE_YEARLY=4;
	public static final int SUBSCRIBE_MODE_TWO_WEEKS = 5;
	public static final int SUBSCRIBE_MODE_THREE_WEEKS = 6;
	public static final int SUBSCRIBE_MODE_TWO_MONTHS = 7;
	public static final int SUBSCRIBE_MODE_THREE_MONTHS = 8;
	public static final int MESSAGE_NOTFOUND= -9;
	
	//--------------------------language-----------------------------
	public static final int ARABIC=1;
	public static final int ENGLISH=2;
	
	public static final String TOKEN_ERROR="Token is null or empty";
	public static final String TOKEN_ERROR_AR="الرمز فارغ أو فارغ";
	public static final String JWT_SIGNATURE_ERROR_AR="توقيع جوت غير صحيح";
	public static final String JSON_TO_OBJECT_ERROR="Ù„Ø§ ÙŠÙ…ÙƒÙ† Ø§Ø³ØªØ®Ø±Ø§Ø¬ Ø§Ù„Ù…Ø³ØªØ®Ø¯Ù… Ù�ÙŠ Ù‡Ø°Ø§ Ø§Ù„ØªÙˆÙ‚ÙŠØ¹";
	public static final String MESSAGER_INCORRECT_DATA="Incorrect data input from client";
	public static final String MESSAGER_INCORRECT_DATA_AR="إدخال بيانات غير صحيح من العميل";
	
}
