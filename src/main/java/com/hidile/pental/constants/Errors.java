package com.hidile.pental.constants;

public interface Errors {
	/*public static final String SUCESS_KEY_MESSAGE="SUCCESS_MESSAGE";
	public static final String FAIL_KEY_MESSAGE="FAIL_MESSAGE";
	public static final String ERROR_KEY_MESSAGE="ERROR_MESSAGE";
	public static final String ERROR_KEY_CODE="ErrorCode";
	public static final String MESSAGE_UPDATED_SUCESSFULLY = "Updated successfully";
	public static final String MESSAGE_UPDATED_SUCESSFULLY_AR ="تم التحديث بنجاح";

	public static final int ERROR_CODE_NO_ERROR = 0;
	public static final int ERROR_CODE_LOGIN_FAIL=-12;
	public static final String ERROR_MESSAGE_LOGIN_FAIL="LOGIN FAILED";
	public static final String ERROR_MESSAGE = "ERROR MESSAGE";
	public static final String ERROR_MESSAGE_ARABIC="ERROR MESSAGE ARABIC";
	public static final String ERROR_CODE = "ERROR CODE";
	public static final String MESSAGE_SUCSESS = "SUCCESSFULL";
	public static final String MESSAGE_SUCSESS_AR = "نجاح";
	public static final String MESSAGE_NO_SUCH_USER = "NO SUCH USER";
	public static final String MESSAGE_DUBLICATE_DATA_AR = "Ø¨ÙŠØ§Ù†Ø§Øª Ù…ÙƒØ±Ø±Ø©";
	public static final String MESSAGE_DUBLICATE_DATA = "Dublicate data";
	public static final String MESSAGE_NOT_FOUND = "NOT FOUND";
	public static final String MESSAGE_NOT_FOUND_AR = "ØºÙŠØ± Ù…Ø¹Ø«ÙˆØ± Ø¹Ù„ÙŠÙ‡";
	public static final String MESSAGE_SOME_ERROR_OCCURED = "SOME ERROR OCCURED";
	public static final String MESSAGE_UPDATE_FAILED = "UPDATE FAILED";
	public static final String MESSAGE_UPDATE_FAILED_AR = "Ù�Ø´Ù„ Ø§Ù„ØªØ­Ø¯ÙŠØ«";
	public static final String MESSAGE_SAVING_FAILED = "SAVING FAILED";
	public static final String MESSAGE_NULL_VALUE = "NULL VALUE";
	public static final String MESSAGE_NULL_VALUE_AR="Ù‚ÙŠÙ…Ø© Ù�Ø§Ø±ØºØ©";
	public static final String MESSAGE_NO_DATA_TO_SHOW = "NO DATA TO SHOW";
	public static final String MESSAGE_NO_DATA_TO_SHOW_AR = "Ù†Ùˆ Ø¨ÙŠØ§Ù†Ø§Øª Ø§Ù„Ù…Ø¹Ø±Ø¶";
	public static final boolean TESTING = true;
	public static final String NULL_VALUE_PASSED_ERROR = "NULL_VALUE_PASSED_ERROR";
	public static final String MESSAGE_NOT_ACTIVE = "NOT AN ACTIVE USER";
	public static final String MESSAGE_MAIL_ID_EXISTS = "EMAIL ALREADY EXISTS";
	public static final String MESSAGE_USERNAME_EXISTS = "USER NAME ALREADY EXISTS";
	public static final String MESSAGE_PRODUCTNAME_EXISTS = "PRODUCT  ALREADY EXISTS";
	public static final String MESSAGE_NOT_LOGIN = "USER IS NOT LOGIN";
	public static final String MESSAGE_NAME_EXISTS = " ALREADY EXISTS";
	public static final String MESSAGE_PLEASE_LOGIN="PLEASE LOGIN";
	public static final String MESSAGE_NO_SUCH_USER_AR="Ù„Ø§ ÙŠÙˆØ¬Ø¯ Ù…Ø«Ù„ Ù‡Ø°Ø§ Ø§Ù„Ù…Ø³ØªØ®Ø¯Ù…";
	public static final String MESSAGE_SUCCESS = "SUCCESSFULL";
	public static final String MESSAGE_SOME_IMPORTANT_DATA_MISSING="SOME IMPORTANT DATA MISSING";

	public static final String INCORRECT_EMAIL = "INCORRECT EMAIL";
	public static final String ALPHANUMERICS_ONLY = "USERNAME MUST BE ALPHANUMERIC";
	public static final String MINIMUM_LENGHTH = "USERNAME MUST BE MINIMUM LENGHT 6 AND MAXIMUM 12";
	public static final String PASSWORD_LENGHTH = "PASSWORD MUST BE MINIMUM LENGHT 6";
	
	
	//------------COMMON ERROR MESSAGES---
	public static final int ERROR_CODE_SUCCESS =0;
	public static final int ERROR_CODE_FAIL =-1;
	public static final int ERROR_CODE_SESSION_EXPIRED =-2;
	public static final String ERROR_MESSAGE_SESSION_EXPIRED="Session expired.";*/
	public static final String SUCESS_KEY_MESSAGE = "SUCCESS_MESSAGE";
	public static final String FAIL_KEY_MESSAGE = "FAIL_MESSAGE";
	public static final String ERROR_KEY_MESSAGE = "ERROR_MESSAGE";
	public static final String ERROR_KEY_CODE = "ErrorCode";
	public static final int ERROR_CODE_NO_ERROR = 0;
	public static final int ERROR_CODE_LOGIN_FAIL = -12;
	public static final String ERROR_MESSAGE_LOGIN_FAIL = "LOGIN FAILED";
	public static final String ERROR_MESSAGE = "ERROR MESSAGE";
	public static final String ERROR_MESSAGE_ARABIC = "ERROR MESSAGE ARABIC";
	public static final String ERROR_CODE = "ERROR CODE";
	public static final String MESSAGE_SUCSESS = "SUCCESSFULL";
	public static final String MESSAGE_SUCSESS_AR = "نجاح";
	public static final String MESSAGE_NO_SUCH_USER = "NO SUCH USER";
	public static final String MESSAGE_DUBLICATE_DATA_AR = "بيانات تكرارية";
	public static final String MESSAGE_DUBLICATE_DATA = "Dublicate data";
	public static final String MESSAGE_NOT_FOUND = "NOT FOUND";
	public static final String MESSAGE_NOT_FOUND_AR = "غير معثور عليه";
	public static final String MESSAGE_SOME_ERROR_OCCURED = "SOME ERROR OCCURED";
	public static final String MESSAGE_UPDATE_FAILED = "UPDATE FAILED";
	public static final String MESSAGE_UPDATE_FAILED_AR = "فشل التحديث";
	public static final String MESSAGE_SAVING_FAILED = "SAVING FAILED";
	public static final String MESSAGE_NULL_VALUE = "NULL VALUE";
	public static final String MESSAGE_NATIONAL_ID_NUMBER_FORMAT_INCORRECT = "NATIONAL ID MUST STARTS WITH 1";
	public static final String MESSAGE_IQAMA_ID_NUMBER_FORMAT_INCORRECT = "IQAMA ID MUST STARTS WITH 2";
	public static final String MESSAGE_NULL_VALUE_AR = "قيمة فارغة";
	public static final String MESSAGE_NO_DATA_TO_SHOW = "NO DATA TO SHOW";
	public static final String MESSAGE_NO_DATA_TO_SHOW_AR = "لا توجد بيانات لعرضها";
	public static final boolean TESTING = true;
	public static final String NULL_VALUE_PASSED_ERROR = "NULL_VALUE_PASSED_ERROR";
	public static final String MESSAGE_NOT_ACTIVE = "NOT AN ACTIVE USER";
	public static final String MESSAGE_MAIL_ID_EXISTS = "EMAIL ALREADY EXISTS";
	public static final String MESSAGE_USERNAME_EXISTS = "USER NAME ALREADY EXISTS";
	public static final String MESSAGE_PRODUCTNAME_EXISTS = "PRODUCT  ALREADY EXISTS";
	public static final String MESSAGE_NOT_LOGIN = "USER IS NOT LOGIN";
	public static final String MESSAGE_NAME_EXISTS = " ALREADY EXISTS";
	public static final String MESSAGE_PLEASE_LOGIN = "PLEASE LOGIN";
	public static final String MESSAGE_NO_SUCH_USER_AR = "لا يوجد مثل هذا المستخدم";
	public static final String MESSAGE_SUCCESS = "SUCCESSFULL";
	public static final String MESSAGE_SOME_IMPORTANT_DATA_MISSING = "SOME IMPORTANT DATA MISSING";
	public static final String MESSAGE_UPDATED_SUCESSFULLY = "Updated successfully";
	public static final String MESSAGE_UPDATED_SUCESSFULLY_AR = "تم التحديث بنجاح";

	public static final String INCORRECT_EMAIL = "INCORRECT EMAIL";
	public static final String ALPHANUMERICS_ONLY = "USERNAME MUST BE ALPHANUMERIC";
	public static final String MINIMUM_LENGHTH = "USERNAME MUST BE MINIMUM LENGHT 6 AND MAXIMUM 12";
	public static final String PASSWORD_LENGHTH = "PASSWORD MUST BE MINIMUM LENGHT 6";
	public static final String MESSAGE_NO_SUCH_CUSTOMER_EXIST = "No such customer exist in customer table";
	public static final String MESSAGE_NO_SUCH_CUSTOMER_EXIST_AR = "لا يوجد مثل هذا العميل موجود في جدول العملاء";
	public static final String MESSAGE_NO_SUCH_PRODUCT_AVAILABLE="No such product available";
	public static final String MESSAGE_NO_SUCH_PRODUCT_AVAILABLE_AR="لا يوجد مثل هذا المنتج متاح";
	public static final String MESSAGE_ALREADY_REGISTERED_PHONE_OR_EMAIL = "Already registered phone number or email";
	public static final String MESSAGE_ALREADY_REGISTERED_PHONE_OR_EMAIL_AR =  "بالفعل رقم الهاتف المسجل أو البريد الإلكتروني";
	
	public static final String MESSAGE_INVALID_PHONE_OR_EMAIL = "Invalid phone number,email or password";
	public static final String MESSAGE_INVALID_PHONE_OR_EMAIL_AR = "رقم الهاتف غير صالح أو البريد الإلكتروني أو كلمة المرور";
	public static final String MESSAGE_COLOR_ALREADY_IN_PRESENT = "Color already in present";
	public static final String MESSAGE_COLOR_ALREADY_IN_PRESENT_AR = "اللون بالفعل في الوقت الحاضر";
	public static final String MESSAGE_SAME_CATEGORY_NAME_EXIST = "Same category name already exist";
	
	public static final String MESSAGE_SAME_CATEGORY_NAME_EXIST_AR = "اسم الفئة نفسه موجود بالفعل";
	public static final String MESSAGE_NO_SUCH_CATEGORY = "No such category";
	public static final String MESSAGE_NO_SUCH_CATEGORY_AR = "لا يوجد مثل هذه الفئة";
	public static final String MESSAGE_NO_SUCH_STORE = "No such store";
	
	public static final String MESSAGE_NO_SUCH_STORE_AR = "لا يوجد متجر من هذا القبيل";
	public static final String MESSAGE_SOME_ATTRIBUTE_NOT_AVAILABLE = "Some attribute are not available or duplicate attribute applied in single product";
	public static final String MESSAGE_SOME_ATTRIBUTE_NOT_AVAILABLE_AR = "بعض السمات غير متاحة أو سمة مكررة مطبقة في منتج واحد";
	public static final String MESSAGE_NO_SUCH_PRODUCT_ADDED_BEFORE = "No such product added before";

	public static final String MESSAGE_NO_SUCH_PRODUCT_ADDED_BEFORE_AR = "لا أضيف هذا المنتج من قبل";
	
	
	public static final String MESSAGE_FEATURES_UPDATE_FAILED = "Features updation failed";
	
	public static final String MESSAGE_FEATURES_UPDATE_FAILED_AR = "ملامح فشل التحديث";
	public static final String MESSAGE_PRODUCT_ATTRIBUTE_FAILED = "product attribute updation failed";
	public static final String MESSAGE_PRODUCT_ATTRIBUTE_FAILED_AR = "فشل تحديث سمة المنتج";
	public static final String MESSAGE_PRODUCT_UPDATE_FAILED = "Product update failed";
	public static final String MESSAGE_PRODUCT_UPDATE_FAILED_AR ="فشل تحديث المنتج";
	public static final String MESSAGE_ALREADY_REVIEWED_ITEM = "Already reviewed item";
	
	public static final String MESSAGE_ALREADY_REVIEWED_ITEM_AR = "البند استعرض بالفعل";
	
	public static final String MESSAGE_SUB_CATEGORY_NAME_ALREADY_EXIST = "Sub category name already exist";
	public static final String MESSAGE_SUB_CATEGORY_NAME_ALREADY_EXIST_AR = "اسم الفئة الفرعية موجود بالفعل";
	public static final String MESSAGE_COUNTRY_FORMAT_INCORRECT = "country format incorrect";
	public static final String MESSAGE_COUNTRY_FORMAT_INCORRECT_AR = "تنسيق البلد غير صحيح";
	public static final String MESSAGE_CATEGORY_FORMAT_INCORRECT = "category format incorrect";
	
	public static final String MESSAGE_CITY_FORMAT_INCORRECT_AR ="تنسيق المدينة غير صحيح";
	public static final String MESSAGE_CITY_FORMAT_INCORRECT = "city format incorrect";
	
	public static final String MESSAGE_PRICE_FORMAT_INCORRECT_AR ="تنسيق السعر غير صحيح";
	public static final String MESSAGE_PRICE_FORMAT_INCORRECT = "price format incorrect";
	
	public static final String MESSAGE_SUB_CATEGORY_FORMAT_INCORRECT_AR ="تنسيق الفئة الفرعية غير صحيح";
	public static final String MESSAGE_SUB_CATEGORY_FORMAT_INCORRECT = "subcategory format incorrect";
	
	public static final String MESSAGE_CATEGORY_FORMAT_INCORRECT_AR ="تنسيق الفئة غير صحيح";
	public static final String MESSAGE_NO_SUCH_SUBCATEGORY = "No such subcategory";
	public static final String MESSAGE_NO_SUCH_SUBCATEGORY_AR = "لا يوجد مثل هذه الفئة الفرعية";
	
	public static final String MESSAGE_NO_SUCH_COUNTRY = "No such country";
	public static final String MESSAGE_NO_SUCH_COUNTRY_AR = "لا يوجد مثل هذا البلد";
	public static final String MESSAGE_NO_SUCH_CITY = "No such city";
	public static final String MESSAGE_NO_SUCH_CITY_AR = "لا يوجد مثل هذه المدينة";
	
	public static final String MESSAGE_NO_SUCH_ACTIVE_AUCTION = "No such active auction";
	public static final String MESSAGE_NO_SUCH_ACTIVE_AUCTION_AR = "لا يوجد مثل هذا المزاد النشط";
	public static final String MESSAGE_BID_AMOUNT_SHOULD_BE_GREATER_THAN = "Bid amount should be greater than ";
	public static final String MESSAGE_BID_AMOUNT_SHOULD_BE_GREATER_THAN_AR = "يجب أن يكون مبلغ العطاء أكبر من";
	
	
	public static final String MESSAGE_AUCTION_NOT_FOUND = "Auction not found";
	public static final String MESSAGE_AUCTION_NOT_FOUND_AR = "المزاد غير موجود";
	public static final String MESSAGE_AUCTION_UPDATE_FAILED = "Auction update failed";
	public static final String MESSAGE_ORDER_NOT_FOUND = "Order not found";
	public static final String MESSAGE_ORDER_NOT_FOUND_AR = "لم يتم العثور على الطلب";
	
	
	
	public static final String MESSAGE_AUCTION_DETAILS_DELETING_FAILED_AR = "تفاصيل المزاد المحذوفة";
	public static final String MESSAGE_AUCTION_DETAILS_DELETING_FAILED = "Auction details deleteing failed";
	
	
	
	
	public static final String MESSAGE_NO_SUCH_AUCTION_FOR_THIS_AUCTION_ID="No such auction found with this auction id";
	public static final String MESSAGE_NO_SUCH_AUCTION_FOR_THIS_AUCTION_ID_AR="لم يتم العثور على هذا المزاد مع معرف المزاد هذا";
	public static final String MESSGE_AUCTION_UPDATE_FAILED="Auction update failed";

	//------------COMMON ERROR MESSAGES---
	public static final int ERROR_CODE_SUCCESS = 0;
	public static final int ERROR_CODE_FAIL = -1;
	public static final int ERROR_CODE_SESSION_EXPIRED = -2;
	public static final String ERROR_MESSAGE_SESSION_EXPIRED = "Session expired.";
	
	//invalidlogindata
	//
	
	
	
	
}
