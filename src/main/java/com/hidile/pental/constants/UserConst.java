package com.hidile.pental.constants;

public interface UserConst {
	public static final String ACTIVATION="/activate";
	public static final String ACTIVATE_CUSTOMER="/adfadsf3534543jljlk";
	public static final String CHANGE_PASSWORD="/changepassword";
	public static final String SAVE_CUSTOMER="/savecustomer";
	public static final String SAVE_PROMOTERS="/savepromoters";
	public static final String GENERATE_PROMOCODE="/generatepromocode";
	public static final String SAVE_PROMOCODE_DETAILS="/savepromocodedetails";
	
	public static final int VERIFIED_EMAIL=1;
	public static final int NON_VERIFIED_EMAIL=0;
	
	
	//---------------------------------role constants------------------------------------
	public static final int ROLL_ID_SELLER = 10;
	public static final int ROLL_ID_COMPANY =20;
	public static final int ROLL_ID_ADMIN = 7;
	public  static final int ROLL_ID_CUSTOMER = 51;
	public static final int ROLL_ID_MANAGER = 52;
	public static final int ROLL_ID_PROMOTER=54;
	public static final int ROLL_ID_DELIVERY_BOY = 53;
	
	public static final String ROLL_NAME_ADMIN ="Admin";
	public static final String ROLL_NAME_SHOP ="Shop";
	public static final String ROLL_NAME_CUSTOMER ="Customer";
	public static final String ROLL_NAME_MANAGER ="Manager";
	public static final String ROLL_NAME_SELLER ="Seller";
	public static final String ROLL_NAME_DELIVERY_BOY ="Delivery Boy";
	
	
	
	
	
	
	
	
	//---------------------------activateon contatnt--------------------------------------
	public static final String  PREFIX_CUSTOMER_ACTIVATION="xbua14uiorqe56iuui";
	public static final String  SUFFIX_CUSTOMER_ACTIVATION="vv897mbkfh";
	public static final String  PREFIX_OWNER_ACTIVATION="owmjhui55nnvnliud";
	public static final String  SUFFIX_OWNER_ACTIVATION="ppzxnbvcdryuuik";
	
	public static final String REDIRECT_URL="http://pentalsaudi.com/app/emailverify";
	//public static final String REDIRECT_URL="http://localhost:8080/app/emailverify";

}
