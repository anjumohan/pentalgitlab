package com.hidile.pental.constants;

public interface OrderConst {

	//--------------------booking status----------------------------
		public static final int ORDERED=1;
		public static final int CONFIRMED=2;
		public static final int READY_TO_DELIVERY=6;
		public static final int REJECTED=3;
		public static final int DELIVERED=4;
		public static final int CANCELED=5;
		public static final int PICKED=7;
		public static final int DELIVERY_BOY_NOT_AVAILABLE=0;
		public static final String STRING_ORDERED="Ordered";
		public static final String STRING_CONFIRMED="Confirmed";
		public static final String STRING_REJECTED="Rejected";
		public static final String STRING_CANCELED="Canceled";
		public static final String STRING_DELIVERED="Delivered";
		public static final String STRING_PICKED="Picked";
		public static final String STRING_READY_TO_DELIVERY="Ready to delivary";
		
		
		public static final String STRING_ORDERED_AR="تم الطلب";
		public static final String STRING_CONFIRMED_AR="مؤكد";
		public static final String STRING_REJECTED_AR="مرفوض";
		public static final String STRING_CANCELED_AR="ألغيت";
		public static final String STRING_DELIVERED_AR="تم الاستلام";
		public static final String STRING_PICKED_AR="اختار";
		public static final String STRING_READY_TO_DELIVERY_AR="على استعداد ل";
		
		
		
		//---------------------auction constants
		public static final int AUC_PRESENTED=1;
		public static final int AUC_STARTED=2;
		public static final int AUC_REJECTED=3;
		public static final int AUC_SALED=3;
		public static final int AUC_CANCELED=4;
		
		public static final String AUC_STRING_ORDERED="Presented";
		public static final String AUC_STRING_STARTED="Activated";
		public static final String AUC_STRING_REJECTED="Rejected";
		public static final String AUC_STRING_SALED="Saled";
		public static final String AUC_STRING_DELIVERED="Deleted";
		//-------------------return constants
		
		public static final int RETURN_REQUESTED=10;
		public static final int RETURN_ACCEPTED=11;
		public static final int RETURN_REJECTED=12;//store
		public static final int RETURN_CANCELLED=13;//customer
		public static final int RETURN_COMPLETED=15;
		
		public static final String STRING_RETURN_REQUESTED="Return Applied";
		public static final String STRING_RETURN_ACCEPTED="Return Accepted";
		public static final String STRING_RETURN_REJECTED="Return Rejected";
		public static final String STRING_RETURN_CANCELLED="Return Rejected";
		public static final String STRING_RETURN_COMPLETED="Returned";
		
		public static final String STRING_RETURN_REQUESTED_AR="عاد";
		public static final String STRING_RETURN_ACCEPTED_AR="العودة المقبولة";
		public static final String STRING_RETURN_REJECTED_AR="العودة رفضت";
		public static final String STRING_RETURN_CANCELLED_AR="العودة رفضت";
		public static final String STRING_RETURN_COMPLETED_AR="تمت الاستعادة";
		
		
		public static final int IS_ONLINE=1;
		public static final int CASH_ON_DELIVERY=2;
	
}
