package com.hidile.pental.constants;

public interface RegexConst {
  public static final String REGEX_EMAIL="^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
  public static final String REGEX_NAME_LENGTH = "^.{0,50}$";
  public static final String REGEX_NAME ="[a-zA-Z0-9]+\\.?";
  public static final String REGEX_NUMBER = "^\\d{1,16}$";
  public static final String REGEX_PRICE ="^[.]?[0-9]{1,16}(\\.[0-9]{1,2})?$";
  public static final String REGEX_NOTE = "^.{0,500}$";
  public static final String REGEX_PECENTAGE ="^[.]?[0-9]{1,2}(\\.[0-9]{1,2})?$";
  public static final String REGEX_PHONE ="^[0-9\\+]{1,}[0-9\\-]{2,15}$";
  public static final String REGEX_ALPHANUMERIC ="^[a-zA-Z0-9]{1,50}+$";
  //public static final String REGEX_PASSWORD = "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$";
 // public static final String REGEX_PASSWORD = "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$";
  public static final String REGEX_PASSWORD =  "((?=.*\\d)(?=.*[a-z]).{8,30})";
  public static final String REGEX_WEBADDRESS = "^((https?|ftp|smtp):\\/\\/)?(www.)?[a-z0-9]+\\.[a-z]+(\\/[a-zA-Z0-9#]+\\/?)*$";
}
