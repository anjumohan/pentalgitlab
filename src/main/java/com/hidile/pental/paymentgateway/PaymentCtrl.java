package com.hidile.pental.paymentgateway;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/payment")
public class PaymentCtrl {

	
	@RequestMapping(value = "requestcheckout", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	private String request() throws IOException 
	{
		URL url = new URL("https://test.oppwa.com/v1/checkouts");

		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setDoInput(true);
		conn.setDoOutput(true);

		String data = ""
			+ "authentication.userId=8a82941865fc22f80166071f8e710f2b"
			+ "&authentication.password=yrkE5s3FRF"
			+ "&authentication.entityId=8a82941865fc22f80166071ff3fa0f2f"
			+ "&amount=92.00"
			+ "&currency=SAR"
			+ "&paymentType=DB";

		DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
		wr.writeBytes(data);
		wr.flush();
		wr.close();
		int responseCode = conn.getResponseCode();
		InputStream is;

		if (responseCode >= 400) is = conn.getErrorStream();
		else is = conn.getInputStream();

		return IOUtils.toString(is);
	}
	
	
	
	
	
	@RequestMapping(value = "checkcheckout", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	private String checkCheckOut(@RequestParam("id") String Id) throws IOException {
		URL url = new URL("https://test.oppwa.com/v1/checkouts/"+Id+"/payment" +
			"?authentication.userId=8a8294174d0595bb014d05d829e701d1" +
			"&authentication.password=9TnJPc2n9h" +
			"&authentication.entityId=8a8294174d0595bb014d05d82e5b01d2");

		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		int responseCode = conn.getResponseCode();
		InputStream is;

		if (responseCode >= 400) is = conn.getErrorStream();
		else is = conn.getInputStream();

		return IOUtils.toString(is);
	}
	
	
	
	
	
}
