package com.hidile.pental.service.account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transaction;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hidile.pental.constants.Constants;
import com.hidile.pental.dao.account.AccountDao;
import com.hidile.pental.entity.account.Expense;
import com.hidile.pental.entity.account.Income;
import com.hidile.pental.entity.account.IncomeExpenseHead;
import com.hidile.pental.entity.order.Transactions;
import com.hidile.pental.model.BasicResponse;
import com.hidile.pental.model.account.ExpenseVo;
import com.hidile.pental.model.account.IncomeExpenseHeadVo;
import com.hidile.pental.model.account.IncomeVo;
import com.hidile.pental.service.GeneralService;
import com.hidile.pental.utils.TimeUtils;

@Transactional
@Service("accountService")
public class AccountServiceImpl implements AccountService, Constants{
	private static Logger logger = Logger.getLogger(AccountServiceImpl.class);
	
	@Autowired
	private AccountDao accountDao;

	@Autowired
	private GeneralService generalService;

	@Autowired
	private ModelMapper modelMapper;

	
	@Override
	public BasicResponse saveorUpdateIncomeExpenseHead(IncomeExpenseHeadVo incExpHedModel) {


		BasicResponse response = new BasicResponse();
		Map<String, Object> map = null;
		List<IncomeExpenseHead> headList = new ArrayList<>();
		IncomeExpenseHead head = new IncomeExpenseHead();
		head = modelMapper.map(incExpHedModel, IncomeExpenseHead.class);
		head.setLast_updated_time(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		head.setBusiness_type(0);
		
		map = accountDao.saveorUpdateIncomeExpenseHead(head);
		response = new BasicResponse();
		response.errorCode = (int) map.get(ERROR_CODE);
		response.errorMessage = (String) map.get(ERROR_MESSAGE);
		return response;
		
		
	
	
	}


	@Override
	public BasicResponse getIncomeExpenseHead(int incomeOrExpense, long incomExpnsHeadId, int status) {


		IncomeExpenseHeadVo expenseModel = null;
		BasicResponse resp = null;
		Map<String, Object> expenseMap = new HashMap<>();
		
		List<IncomeExpenseHeadVo> expenseModelList = new ArrayList<>();
		BasicResponse res = new BasicResponse();
		expenseMap = accountDao.getExpenseIncomeHead(incomeOrExpense,incomExpnsHeadId, status);
		if (res.errorCode == 0) {
			List<IncomeExpenseHead> expenseList = (List<IncomeExpenseHead>) expenseMap.get("HeadList");
			if (expenseList != null) {
				for (IncomeExpenseHead expenseDetails : expenseList) {
					expenseModel = new IncomeExpenseHeadVo();
					expenseModel = modelMapper.map(expenseDetails, IncomeExpenseHeadVo.class);
					expenseModelList.add(expenseModel);
				}
			}
			resp = new BasicResponse();
			resp.errorCode = 0;
			resp.errorMessage = MESSAGE_SUCCESS;
			resp.object = expenseModelList;
		} else {
			resp = new BasicResponse();
			resp.errorCode = -8;
			resp.errorMessage = FAIL_KEY_MESSAGE;
		}
		return resp;
	
	
	}


	@Override
	public BasicResponse saveorUpdateIncome(IncomeVo incomeModel) {

		BasicResponse response = new BasicResponse();
		Map<String, Object> map = null;
		List<Transactions> transList = new ArrayList<>();
		List<Income> incomeList = new ArrayList<>();
		Income income = new Income();
		income = modelMapper.map(incomeModel, Income.class);
		income.setLast_updated_time(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		income.setBusiness_type(0);
		income.setUserId(incomeModel.getOperatingOfficer());
		income.setStatus(ACTIVE);
		incomeList.add(income);
		
		Transactions feeTran = new Transactions();
		feeTran.setBankOrCash(0);
		feeTran.setCmd(incomeModel.getCmd());
		feeTran.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		feeTran.setNetAmount(incomeModel.getAmount());
		feeTran.setDiscount(0);
		feeTran.setOperatingOfficer(incomeModel.getOperatingOfficer());
		feeTran.setStatus(ACTIVE);
		feeTran.setToWhoId(incomeModel.getOperatingOfficer());
		feeTran.setTransactionMode(TRAN_MODE_INCOME);
		feeTran.setCompanyId(0);
		feeTran.setInOrOut(0);
		feeTran.setIsCleared(0);
		feeTran.setNote("income added");
		feeTran.setPairedId(incomeModel.getIncomExpnsHeadId());
		feeTran.setReciptNo("0");
		
		
		
		
		
		
		
		
		//feeTran.setReciptNo("1");
		transList.add(feeTran);

		map = accountDao.saveorUpdateIncome(incomeList,transList);
		response = new BasicResponse();
		response.errorCode = (int) map.get(ERROR_CODE);
		response.errorMessage = (String) map.get(ERROR_MESSAGE);
		return response;
	
	}


	@Override
	public BasicResponse saveorUpdateExpense(ExpenseVo expenseModel) {


		BasicResponse response = new BasicResponse();
		Map<String, Object> map = null;
		List<Transactions> transList = new ArrayList<>();
		List<Expense> expenseList = new ArrayList<>();
		Expense expense = new Expense();
		expense = modelMapper.map(expenseModel, Expense.class);
		expense.setLast_updated_time(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		expense.setBusiness_type(0);
		expense.setUserId(expenseModel.getOperatingOfficer());
		expenseList.add(expense);
		
		Transactions feeTran = new Transactions();
		feeTran.setBankOrCash(0);
		feeTran.setCmd(0);
		feeTran.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		feeTran.setNetAmount(expenseModel.getAmount());
		feeTran.setDiscount(0);
		feeTran.setOperatingOfficer(expenseModel.getOperatingOfficer());
		feeTran.setStatus(ACTIVE);
		feeTran.setToWhoId(expenseModel.getOperatingOfficer());
		feeTran.setTransactionMode(TRAN_MODE_EXPENSE);
		feeTran.setCompanyId(0);
		feeTran.setInOrOut(0);
		feeTran.setIsCleared(0);
		feeTran.setReciptNo("0");
		feeTran.setNote("expence added");
		feeTran.setPairedId(expenseModel.getIncomExpnsHeadId());
		
		transList.add(feeTran);
		
		map = accountDao.saveorUpdateExpense(expenseList,transList);
		response = new BasicResponse();
		response.errorCode = (int) map.get(ERROR_CODE);
		response.errorMessage = (String) map.get(ERROR_MESSAGE);
		return response;
	
	
	}


	
	@Override
	public BasicResponse getIncomes(int currentIndex, int rowPerPage, long incomeId, int status) {

		IncomeVo incomeModel = null;
		BasicResponse resp = null;
		Map<String, Object> incomMap = new HashMap<>();
		List<IncomeVo> incomeModelList = new ArrayList<>();
		Map<Long,String> headMap = getAllIncExpHeadNmaes();
	//	Map<Long, String> incomeeHeadMap = getAllIncomeExpenseHead(schoolId);
		BasicResponse res = new BasicResponse();
		incomMap = accountDao.getIncomes(currentIndex, rowPerPage, incomeId, status);
		if (res.errorCode == 0) {
			List<Income> incomeList = (List<Income>) incomMap.get("IncomeList");
			if (incomeList != null) {
				for (Income incomeDetails : incomeList) {
					incomeModel = new IncomeVo();
					incomeModel = modelMapper.map(incomeDetails, IncomeVo.class);
					
				//	incomeModel.setIncomeName(headMap.get(incomeModel.getIncomExpnsHeadId()));
					incomeModel.setIncomeExpenseName(headMap.get(incomeModel.getIncomExpnsHeadId()));
					incomeModelList.add(incomeModel);
				}
			}
			resp = new BasicResponse();
			resp.errorCode = 0;
			resp.errorMessage = MESSAGE_SUCCESS;
			resp.object = incomeModelList;
		} else {
			resp = new BasicResponse();
			resp.errorCode = -8;
			resp.errorMessage = FAIL_KEY_MESSAGE;
		}
		return resp;
	
	}


	


	private Map<Long, String> getAllIncExpHeadNmaes() {


		List<IncomeExpenseHead> brandList = null;
		Map<Long, String> allserviceMap = null;
		brandList = accountDao.getAllIncExpHeadNmaesList(0);
		if (brandList != null && !brandList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (IncomeExpenseHead item : brandList) {
				allserviceMap.put(item.getIncomExpnsHeadId(), item.getIncomeExpenseName());
			}
			return allserviceMap;
		} else {
			return null;
		}
	
	
	}


	@Override
	public BasicResponse getExpenses(int currentIndex, int rowPerPage, long expenseId, int status) {

		ExpenseVo expenseModel = null;
		BasicResponse resp = null;
		Map<String, Object> expenseMap = new HashMap<>();
		Map<Long,String> headMap = getAllIncExpHeadNmaes();
	//	Map<Long, String> expenseHeadMap = getAllIncomeExpenseHead(schoolId);
		List<ExpenseVo> expenseModelList = new ArrayList<>();
		BasicResponse res = new BasicResponse();
		expenseMap = accountDao.getExpenses(currentIndex, rowPerPage, expenseId, status);
		if (res.errorCode == 0) {
			List<Expense> expenseList = (List<Expense>) expenseMap.get("ExpenseList");
			if (expenseList != null) {
				for (Expense expenseDetails : expenseList) {
					expenseModel = new ExpenseVo();
					expenseModel = modelMapper.map(expenseDetails, ExpenseVo.class);
				//	expenseModel.setExpenseName(expenseHeadMap.get(expenseModel.getIncomExpnsHeadId()));
					expenseModel.setIncomeExpenseName(headMap.get(expenseModel.getIncomExpnsHeadId()));
					expenseModelList.add(expenseModel);
				}
			}
			resp = new BasicResponse();
			resp.errorCode = 0;
			resp.errorMessage = MESSAGE_SUCCESS;
			resp.object = expenseModelList;
		} else {
			resp = new BasicResponse();
			resp.errorCode = -8;
			resp.errorMessage = FAIL_KEY_MESSAGE;
		}
		return resp;
	
	}

}
