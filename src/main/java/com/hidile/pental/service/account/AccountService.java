package com.hidile.pental.service.account;

import com.hidile.pental.model.BasicResponse;
import com.hidile.pental.model.account.ExpenseVo;
import com.hidile.pental.model.account.IncomeExpenseHeadVo;
import com.hidile.pental.model.account.IncomeVo;

public interface AccountService {

	BasicResponse saveorUpdateIncomeExpenseHead(IncomeExpenseHeadVo incExpHedModel);

	BasicResponse getIncomeExpenseHead(int incomeOrExpense, long incomExpnsHeadId, int status);

	BasicResponse saveorUpdateIncome(IncomeVo incomeModel);

	BasicResponse saveorUpdateExpense(ExpenseVo expenseModel);

	BasicResponse getIncomes(int currentIndex, int rowPerPage, long incomeId, int status);

	BasicResponse getExpenses(int currentIndex, int rowPerPage, long expenseId, int status);

}
