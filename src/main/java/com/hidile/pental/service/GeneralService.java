package com.hidile.pental.service;

import java.util.Map;

import com.hidile.pental.entity.input.Offer;
import com.hidile.pental.entity.input.OfferDetails;
import com.hidile.pental.entity.input.SellerDetails;
import com.hidile.pental.entity.order.TaxManagement;
import com.hidile.pental.entity.user.CustomerDetails;
import com.hidile.pental.model.AddCustomer;
import com.hidile.pental.model.BasicResponse;
import com.hidile.pental.model.CompanyVo;
import com.hidile.pental.model.DeliveryBoyVo;
import com.hidile.pental.model.LoggedSession;
import com.hidile.pental.model.PromocodeDetailsVo;
import com.hidile.pental.model.input.SellerVo;



public interface GeneralService {
	public BasicResponse saveCustomer(AddCustomer usersignup);
	public BasicResponse  savePromoters(AddCustomer usersignup);
	public BasicResponse checkWeatherEmailPresent(String email,int role,int phoneOrWeb);
	  public BasicResponse activateCustomer(long userId,int status);
	  public BasicResponse  validateCustomer(long userId,int status);
	public BasicResponse generatePromocode(PromocodeDetailsVo addUser);
	public BasicResponse savePromocode(PromocodeDetailsVo addUser);
	//public BasicResponse saveOrUpdateDeliveryBoy(DeliveryBoyVo deliveryBoyVo);
	 public Map<Integer, String> getAllrolls();
	  //public BasicResponse getAllCustomers(long customerId);
	 public BasicResponse getAllCustomers(long customerId,long cityId,long countryId,long userId,int orderBy,long fromDate,long toDate,int currentIndex,int rowPerPage,int status);
	 public Map<Integer, String> getSalaryModes();
	 public BasicResponse saveOrUpdateSellerDetails(SellerVo sellerDetailsVo);
	 //public Map<String, Object> saveOrUpdateSellerDetails(SellerDetails sellerDetails,LoginDetails login);
	 public BasicResponse saveComapnay(CompanyVo companyVo); 
	 //public BasicResponse sellerLogin(String email,String password);
	 public Long getIdFromName(String name, Map<Long, String> map);
	 public CustomerDetails getCustomerFromUserId(long customerId);
	 public TaxManagement getTax(long taxId);
	 public Offer getOffer(long offerId);
	 public SellerDetails  getSellerFromUserId(long customerId); 
	// public BasicResponse getAllCustomers(long customerId,long userId,long fromDate,long toDate,int status,int firstIndex,int rowPerPage);
	 public BasicResponse getDashDetails();
	 //public BasicResponse getAllCustomers(long customerId,long cityId,long countryId,long userId,int status,long fromDate,long toDate,int currentIndex,int rowPerPage,String customerName);
	 public Map<Long, String> getAllCustomerMap();
	 public Map<Long, String[]> getAllCustomerMap1() ;
	 public BasicResponse getAllNotification(int firstIndex, int rowPerPage,int orderStatus[],int readStatus,long userId,long notificationId);
	public BasicResponse saveAdmin(LoggedSession logSession);
	 public BasicResponse getCustomerFrom(String userName,String phoneNo);
	 public BasicResponse authenticate(String userName,String passWord,int role);
	 public BasicResponse changePassword(String oldPass,String newPass,long userId);
	 public BasicResponse getAllDeliveryBoy(String address,long deliveryBoyId,String deliveryBoyName, int firstIndex, int rowPerPage,int status[],String phoneNumber);
	 public BasicResponse assignOffer(long offerId,long productId);
	 public BasicResponse changeDeliveryStatus(long orderId,long deliveryBoyId,int status);
	 public BasicResponse cancellDeriveryBoy(long orderId);
	 public BasicResponse changeSellerStatus(long sellerId,int status);
	 public BasicResponse changeCustomerStatus(long customerId,int status);
	 public BasicResponse changePromocodeStatus(long codeId,int status);
//public BasicResponse getAllSeller(long sellerId,int firstIndex, int rowPerPage,int status);
	 public BasicResponse getAllSeller(long sellerId,int firstIndex, int rowPerPage,int stat,String sellerName,String phoneNumber,long companyId);
public BasicResponse updateCustomer(AddCustomer usersignup,int role);
public BasicResponse getUserFromUserId(long userId);
public BasicResponse getPromocodeDetails(long codeId,long userId,int status,long validFrom,long validTo,int currentIndex,int rowPerPage,String promocode);
public BasicResponse validatePromocode(String promocode,double orderPrice);
public BasicResponse getAllPromoters(long customerId,long cityId,long countryId,long userId,int status,long fromDate,long toDate,int currentIndex,int rowPerPage,long companyId,String customerName,String emailid,String phoneNo,String customerNameAr);
public BasicResponse getAllCustomers(long customerId,long cityId,long countryId,long userId,int status,long fromDate,long toDate,int currentIndex,int rowPerPage,long companyId,String customerName,String emailid,String phoneNo,String customerNameAr);
}
