package com.hidile.pental;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

public class SuperDao extends HibernateDaoSupport{



protected Transaction tx;
protected Session session;


public void openDBsession(){
	if(session == null || !session.isOpen()){
		session=getHibernateTemplate().getSessionFactory().openSession();
	}else{
		session.flush();
		session.clear();
	}
	tx=session.beginTransaction();
}


public void closeDBSession(){
	tx.commit();
	tx=null;
	session.flush();
	session.close();
}


public void closeDBwithoutSave(){
	tx.rollback();
	tx=null;
	session.flush();
	session.close();
}
}
