package com.hidile.pental.utils;

import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import com.hidile.pental.constants.Constants;

public class TimeUtils implements Constants{
	
	
	public static final String DEFAULT_DATE_FORMAT = "dd-MM-yyyy:hh:mm a";
	public static final String DEFAULT_SHORT_DATE_FORMAT = "dd-MM-yyyy";
	public static final String DATE_FORMAT_YEAR = "yyyy-MM-dd";
	
	
	private static TimeUtils _self;
	protected TimeUtils (){}
	
	public static TimeUtils instance(){
		if(_self == null){
			_self = new TimeUtils();
		}
		return  _self;
	}
	
	
//	public static void main(String[] args) {
//		long d = (long) 1422469860000L;
//		System.out.println(new Date(d));
//		
//		System.out.println(today[0]+" "+today[1]);
//	
//		String endday = today[0]+":11:59 PM";
//	
//		long endLong = instance().getMilSecFromDateStr(endday);
//		stday = instance().getDateStrFromMilSec(stLong);
//		endday = instance().getDateStrFromMilSec(endLong);
//		System.out.println(stLong+"  "+endLong);
//		
//		System.out.println(stday+"  "+endday);
//		
//		
//	}
	public long getCurrentTime(int mode){
		if(mode == 0){
		return new Date().getTime();
		}else{
			String TIME_SERVER = "time-a.nist.gov";   
 	       NTPUDPClient timeClient = new NTPUDPClient();
 	      long returnTime=0;
		try {
			InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
			TimeInfo timeInfo = timeClient.getTime(inetAddress);
	 	    returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
		} catch (Exception e) {
		//	DialogsNeeded.instance().getErorrDialog("Internet connection is not available");
			if(TESTING){
			e.printStackTrace();
			}
			return 0;
		}
 	       return new Date(returnTime).getTime();
		}
	}
	
	
	
	/*public LocalDate getLocalDateFromMilliSec(long milliSec){
		Date date=new Date();
		date.setTime(milliSec);
		LocalDate localDate=date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return localDate;
		
	}
	
	public long getMilliSecFromLocalDate(LocalDate localDate){
		Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		long milliSec=date.getTime();
		return milliSec;
	}*/
	
	public long getStartOfCurrentMonth(){
		Calendar c=Calendar.getInstance();
		 c.set(Calendar.DAY_OF_MONTH, 1);
		 long miliSec=c.getTimeInMillis();
		 return miliSec;
	}
	
	public long getStartTimeOfGiveTime(long daytime){
		String[] today = instance(). getDayAndTimeFrom(daytime);
		String stday = today[0]+":00:00 AM";
		long stLong = instance().getMilSecFromDateStr(stday);
		return stLong;
	}
	
	public long getEndTimeOfGiveTime(long daytime){
		String[] today = instance(). getDayAndTimeFrom(daytime);
		String stday = today[0]+":11:59 PM";
		long stLong = instance().getMilSecFromDateStr(stday);
		return stLong;
	}
	
	
	public long getStartOfCurrentDay(){
		String[] today = instance(). getDayAndTimeFrom(instance().getCurrentTime(TAKE_TIME_SOURSE));
		String stday = today[0]+":00:00 AM";
		long stLong = instance().getMilSecFromDateStr(stday);
		return stLong;
	}
	
	
	
	public Long getMilSecFromDateStr(String dateStr) {
		Long milSec = null;
		try {
			
			SimpleDateFormat formatter = new SimpleDateFormat(
					DEFAULT_DATE_FORMAT);
			Date convertedDate = (Date) formatter.parse(dateStr);
			return convertedDate.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return milSec;
	}

	public Long getMilSecFromDateFormatYear(String dateStr) throws Exception {
        Long milSec = null;
        try {

            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_YEAR );
            Date convertedDate = (Date) formatter.parse(dateStr);
            
            return convertedDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return milSec;
    }
	
	public Long getMilSecFromDateStrAsShort(String dateStr) {
		Long milSec = null;
		try {
			
			SimpleDateFormat formatter = new SimpleDateFormat(
					DEFAULT_SHORT_DATE_FORMAT);
			Date convertedDate = (Date) formatter.parse(dateStr);
			return convertedDate.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return milSec;
	}
	
	
	public String[] getDayAndTimeFrom(long milsec) {
		String dateStr = getDateStrFromMilSec(milsec);
		if (dateStr != null) {
			// dd-MM-yyyy:hh:mm a
			dateStr = dateStr.trim();
			String[] dateArray = new String[2];
			dateArray[0] = dateStr.substring(0, 10);
			dateArray[1] = dateStr.substring(11);
			return dateArray;
		} else {
			return null;
		}
	}

	public String getDateStrFromMilSec(long milsec) {
		String dateStr = null;

		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					DEFAULT_DATE_FORMAT);
			dateStr = (formatter.format(new Date(milsec)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dateStr;
	}
	
	

	/**
	 * <p>
	 * dd-MM-yyyy
	 * </p>
	 * 
	 * @param milsec
	 * @return
	 */
	public String getDateStrFromMilSecAsShort(long milsec) {
		String dateStr = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					DEFAULT_SHORT_DATE_FORMAT);
			dateStr = (formatter.format(new Date(milsec)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dateStr;
	}

//	public long getUkCurrentTime() {
//		TimeZone london = TimeZone.getTimeZone("Europe/London");
//		long now = System.currentTimeMillis();
//		return now + london.getOffset(now);
//	}

	public String getIntervelFromCurrentTime(long pasttime) {
		long dif = getCurrentTime(TAKE_TIME_SOURSE) - pasttime;
		int oneMin = 60 * 1000;
		int oneHr = 60 * oneMin;
		int oneDay = 24 * oneHr;
		String intervewl = "";
		if (dif < oneMin) {
			intervewl = "Just Now";
		} else if (dif > oneMin && dif < oneHr) {
			StringBuffer sb = new StringBuffer();
			sb.append(String.valueOf(dif / oneMin));
			sb.append(" Mins");
			intervewl = sb.toString();
			sb = null;
		} else if (dif > oneHr && dif < oneDay) {
			StringBuffer sb = new StringBuffer();
			sb.append(String.valueOf(dif / oneHr));
			sb.append(".");
			sb.append(String.valueOf((dif % oneHr) / oneMin));
			sb.append(" Hrs");
			intervewl = sb.toString();
			sb = null;
		} else if (dif > oneDay) {
			StringBuffer sb = new StringBuffer();
			sb.append(String.valueOf(dif / oneDay));
			sb.append(" Day(s)");
			// sb.append(" Day(s) &");
			// sb.append(String.valueOf((dif%oneDay)/oneHr));
			// sb.append(" Hrs");
			intervewl = sb.toString();
			sb = null;
		}
		return intervewl;
	}
	public long getEndOfCurrentDay() {
		long stLong=0;
			String[] today = instance().getDayAndTimeFrom(instance().getCurrentTime(TAKE_TIME_SOURSE));
			String stday = today[0] + ":11:59 PM";
			stLong = instance().getMilSecFromDateStr(stday);
		return stLong;
	}
	/*public long getStartOfCurrentDay(){
		String[] today = instance(). getDayAndTimeFrom(instance().getCurrentTime(TAKE_TIME_SOURSE));
		String stday = today[0]+":00:00 AM";
		long stLong = instance().getMilSecFromDateStr(stday);
		return stLong;
	}*/

	/**
	 * @param endTime
	 * @param startTime
	 * @return int [2] time <b> time[0] hr and time[1] min</b>
	 * 
	 */
	public int[] getIntervelBetweenTwoTime(long endTime, long startTime) {
		long dif = endTime - startTime;
		int oneMin = 60 * 1000;
		int oneHr = 60 * oneMin;
		long oneDay=oneHr*24;
		// int oneDay = 24*oneHr;
		int[] times = new int[3];
		times[0] = 0;
		times[1] = 0;
		times[2] = 0;
		if (dif < oneMin) {
			times[0] = (int) (dif/60);
		} else if (dif > oneMin && dif < oneHr) {
			times[1] = (int) dif / oneMin;
		} else if (dif > oneHr && dif < oneDay) {
			times[0] = (int) ((dif % oneHr) / oneMin);
			times[1] = (int) (dif / oneHr);
		} else if(dif > oneDay){
			times[2]=(int) (dif/oneDay);
		}
		return times;
	}

	public long getIntervelInMinutesBetweenTwoTime(long endTime, long startTime) {
		long dif = endTime - startTime;
		System.out.println("" + dif + " = " + endTime + " - " + startTime);
		long oneMinuteMil = 60000;
		long min = (dif / oneMinuteMil);
		return min;
	}

	public String convertingMinToHrString(long mins) {
		StringBuffer sb = new StringBuffer();
		if (mins <= 0) {
			sb.append("00:00 Min(s)");
		} else if (mins < 60) {
			if (mins <= 9) {
				sb.append("0");
			}
			sb.append(mins);
			sb.append(" Min(s)");
		} else {
			long hr = mins / 60;
			long min = mins % 60;
			if (hr <= 9) {
				sb.append("0");
			}
			sb.append(hr);
			sb.append(" Hr(s) ");
			if (min <= 9) {
				sb.append("0");
			}
			sb.append(min);
			sb.append(" Min(s)");
		}
		return sb.toString();
	}
	
	@SuppressWarnings("deprecation")
	public String getTimeFromDate(long millisec){
		Date date=new Date(millisec);
		String time=date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
		return time;
	}
	
	public int getDayFromDate(long millisec){
		int day=new Date(millisec).getDate();
		return day;
	}
	
	public Calendar getCalendar(int day, int month, int year) {
	    Calendar date = Calendar.getInstance();
	    date.set(Calendar.YEAR, year);
	    date.set(Calendar.MONTH, month);
	    date.set(Calendar.DAY_OF_MONTH, day);

	    return date;
	}
	
	
	public long increaseDayBy(long millisec,int days) {
		Calendar c = Calendar.getInstance(); 
		c.setTimeZone(TimeZone.getTimeZone("UTC"));
		c.setTimeInMillis(millisec);
		c.add(Calendar.DATE, 1);
		return c.getTimeInMillis();
	}
}
