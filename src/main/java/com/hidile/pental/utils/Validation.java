//package com.kerlon.biz.util;
package com.hidile.pental.utils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.hidile.pental.constants.Constants;




public class Validation implements Constants{

	private static Validation _self;
	public static final Logger logger = Logger.getLogger(Validation.class);
	
	public Validation(){
	}
	
	public static Validation instance(){
		if(_self == null){
			_self=new Validation();
		}
		return _self;
	}
	
	
	
	/**this the method to validate given email is valid or not
	 * @param data
	 * @return boolean true or false
	 */
	public boolean emailValidator(String data){
		boolean b = false;
		try {
            String emailregex = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
            b = data.matches(emailregex);
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
		return b;
    }
	
	
	public boolean commnonNameLengthValidation(String data){
		 String regx = "^.{0,50}$";
		    Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
		    Matcher matcher = pattern.matcher(data);
		    return matcher.find();
		
	}

	/**this method to validate name is correct or not
	 * @param data
	 * @return boolean true or false
	 */
	public boolean nameValidation(String data){
		String regx ="[a-zA-Z0-9]+\\.?";
		  // String regx = "^[a-zA-Z ]+$";
		    Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
		    Matcher matcher = pattern.matcher(data);
		 // logger.info("inside name validation "+matcher.find());
		  
		    return matcher.find();
	}
	
	
	/**this method validate wheather number or not
	 * @param data
	 * @return true or false
	 */
	public boolean numberValidation(String data){
		String regx="^\\d{1,16}$";
	    Pattern pattern = Pattern.compile(regx);
	    Matcher matcher = pattern.matcher(data);
	    return matcher.find();
	}
	
	public boolean accountnumberValidation(String data){
		String regx="^\\d{1,16}$";
	    Pattern pattern = Pattern.compile(regx);
	    Matcher matcher = pattern.matcher(data);
	    return matcher.find();
	}
	
	/**this method validate range of password
	 * @param data
	 * @return true or false
	 */
	public boolean rangeValidation(String data){
		String regx = "{6,10}";
	    Pattern pattern = Pattern.compile(regx);
	    Matcher matcher = pattern.matcher(data);
	    return matcher.find();
	}
	public boolean minmaxValidation(String data){
		String regx = "{6,12}";
	    Pattern pattern = Pattern.compile(regx);
	    Matcher matcher = pattern.matcher(data);
	    return matcher.find();
	}
	/**
	 * @param it check whether data is empty or not
	 * @return true or false
	 */
	public boolean isNotEmpty(String data){
		if(!"".equals(data) || !data.isEmpty()){
		return true;
		}
		return false;
	}
	
	/**
	 * @param it check decimal numbers and numerical numbers
	 * @return true or false
	 */
	public boolean priceValidation(String data){
		String regx = "^[.]?[0-9]{1,16}(\\.[0-9]{1,2})?$";
		Pattern pattern = Pattern.compile(regx);
	    Matcher matcher = pattern.matcher(data);
	    return matcher.find();
		
	}
	
	public boolean priceValidationIncludingDecimel(String data){
		String regx = "\\d+(\\.\\d{1,2})?";
	//	String regx="[0-9]+(\\.[0-9][0-9]?)?";
		Pattern pattern = Pattern.compile(regx);
	    Matcher matcher = pattern.matcher(data);
	    return matcher.find();
		
	}
	
	public boolean noteLengthValidation(String data){
		 String regx = "^.{0,250}$";
		    Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
		    Matcher matcher = pattern.matcher(data);
		    return matcher.find();
		
	}
//	protected boolean  dateExceedValidation(String data){
//		long passdate=Long.parseLong(data);
//		if(TimeUtils.instance().getMilliSecFromLocalDate(date.getValue())==passdate){
//		return false;
//	}
//	}
	
	public boolean onOrAfterdateValidation(long data){
	long on=TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);
	if(on > data){
		return true;
	}
	return false;
	}
	
	public boolean percentValidation(String data){
		String regx = "^[.]?[0-9]{1,2}(\\.[0-9]{1,2})?$";
		Pattern pattern = Pattern.compile(regx);
	    Matcher matcher = pattern.matcher(data);
	    return matcher.find();
	}
	
	
	
	public boolean phoneValidation(String data){
		String regx = "^[0-9\\+]{1,}[0-9\\-]{2,15}$";
		Pattern pattern = Pattern.compile(regx);
	    Matcher matcher = pattern.matcher(data);
	    return matcher.find();
	}
	
	public boolean alphaNumericValidation(String data){
		String regx = "^[a-zA-Z0-9]{1,50}+$";
		Pattern pattern = Pattern.compile(regx);
	    Matcher matcher = pattern.matcher(data);
	    return matcher.find();
	}

	
	
	public boolean beforeDateValidation(long date) throws Exception{
		long on=TimeUtils.instance().getStartOfCurrentDay()-86400000;
		if(date < on){
			return true;
		}
		return false;
	}
	
	public boolean afterDateValidation(long date) throws Exception{
		long on=TimeUtils.instance().getStartOfCurrentDay();
		on=TimeUtils.instance().getEndTimeOfGiveTime(on);
		if(date > on){
			return true;
		}
		return false;
	}

	public boolean nonMilitarytimeValidaton(String data){
		String regx = "^[01]?\\d:[0-5]\\d:(am|pm)?$";
		Pattern pattern = Pattern.compile(regx);
	    Matcher matcher = pattern.matcher(data);
	    return matcher.find();
	}

}
