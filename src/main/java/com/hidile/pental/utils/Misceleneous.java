package com.hidile.pental.utils;

import java.text.DecimalFormat;

import com.hidile.pental.model.LoggedSession;



public class Misceleneous {
 
	private static Misceleneous _self;
	
	public static Misceleneous instance(){
		if(_self == null){
			_self=new Misceleneous();
		}
		return _self;
	}
	
	public static LoggedSession session=new LoggedSession();
	
	public String convertFromScientificNotation(double number) {
		// Check if in scientific notation
		if (String.valueOf(number).toLowerCase().contains("e")) {
			DecimalFormat formatter = new DecimalFormat();
			formatter.setMaximumFractionDigits(25);
			String data = formatter.format(number);
			String string[] = data.split(",");
			int i = 0;
			data = "";
			while (i < string.length) {
				data = data.concat(string[i]);
				i++;
			}
			return data;
		} else
			return String.valueOf(number);
	}
}
