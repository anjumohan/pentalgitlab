/**
 * 
 */
package com.hidile.pental.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;



@Entity
@Table(name = "LOGIN", uniqueConstraints = { @UniqueConstraint(columnNames = "USER_ID") })
public class LoginDetails implements Serializable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USER_ID", unique = false, nullable = true)
	private long userId;

	@Column(name = "EMAIL", unique = false, nullable = true)
	private String email;
	
	@Column(name = "PHONE_NO", unique = false, nullable = true)
	private String phoneNo;

	@Column(name = "PASSWORD", unique = false, nullable = true)
	private String password;

	@Column(name = "ENTRY_ID", unique = false, nullable = true)
	private long entryId;

	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = true)
	private long last_updated_time;

	@Column(name = "STATUS", unique = false, nullable = true)
	private int status;

	@Column(name = "ROLE", unique = false, nullable = true)
	private int roleId;
	
	@Column(name = "TOKEN", unique = false, nullable = true)
	private String token;
	
	@Column(name = "EMAIL_VERIFIED", unique = false, nullable = true)
	private int emailVerified;
	
	@Column(name = "COUNTRY", unique = false, nullable = false)
	private long country;
	
	@Column(name = "CITY", unique = false, nullable = true)
	private long city;

	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	
	@Column(name = "DEVICE_ID", unique = false, nullable = true)
	private long deviceId;
	

	@ColumnDefault("'00'")
	@Column(name ="VALIDATE",unique =false,nullable =false)
	private int validate;
	
	@Column(name = "COUNTRY_CODE",unique = false,nullable = true)
	private int countryCode;
	
	
	
	
	
	public int getValidate() {
		return validate;
	}

	public void setValidate(int validate) {
		this.validate = validate;
	}
	
	
	public int getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;

	}

	public long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getCity() {
		return city;
	}

	public void setCity(long city) {
		this.city = city;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getCountry() {
		return country;
	}

	public void setCountry(long country) {
		this.country = country;
	}

	


	

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public long getEntryId() {
		return entryId;
	}

	public void setEntryId(long entryId) {
		this.entryId = entryId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public long getLast_updated_time() {
		return last_updated_time;
	}

	public void setLast_updated_time(long last_updated_time) {
		this.last_updated_time = last_updated_time;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}



	public int getEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(int emailVerified) {
		this.emailVerified = emailVerified;
	}
	
	

}
