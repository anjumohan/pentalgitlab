package com.hidile.pental.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "NOTIFICATION",uniqueConstraints ={@UniqueConstraint(columnNames = "NOTIFICATION_ID")})
public class NotificationDetails implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "NOTIFICATION_ID", unique = true, nullable = false)
	private long notificationId;
	
	@Column(name = "USER_ID", unique = false, nullable = false)
	private long userId;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DESCRIPTION", unique = false, nullable = false)
	private String description;
	
	@Column(name = "ORDER_STATUS", unique = false, nullable = false)
	private int orderStatus;
	
	
	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;
	
	
	
	@Column(name = "READ_STATUS", unique = false, nullable = false)
	private int readStatus;



	public long getNotificationId() {
		return notificationId;
	}



	public void setNotificationId(long notificationId) {
		this.notificationId = notificationId;
	}



	public long getUserId() {
		return userId;
	}



	public void setUserId(long userId) {
		this.userId = userId;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public int getOrderStatus() {
		return orderStatus;
	}



	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}



	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}



	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}



	public int getReadStatus() {
		return readStatus;
	}



	public void setReadStatus(int readStatus) {
		this.readStatus = readStatus;
	}
	
	
	
	
	
	
	
	


}
