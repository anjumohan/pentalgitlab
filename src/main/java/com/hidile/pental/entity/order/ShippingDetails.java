package com.hidile.pental.entity.order;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.hidile.pental.entity.user.CustomerDetails;



@Entity
@Table(name = "SHIPPING_TABLE", uniqueConstraints = { @UniqueConstraint(columnNames = "SHIPPING_ID") })
public class ShippingDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SHIPPING_ID", unique = true, nullable = false)
	private long shippingId;

	@Column(name = "CUSTOMER_ID", unique = false, nullable = false)
	private long customerId;
	
	@Column(name = "TO_DELIVER", unique = false, nullable = true)
	private String toDeliver;

	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;

	@Column(name = "COUNTRY_ID", unique = false, nullable = true)
	private long countryId;

	@Column(name = "CITY_ID", unique = false, nullable = true)
	private long cityId;

	@Column(name = "STATUS", unique = false, nullable = false)
	private int status;

	@Column(name = "ADDRESS_1", unique = false, nullable = false)
	private String address1;
	
	

	@Column(name = "ADDRESS_2", unique = false, nullable = false)
	private String address2;

	

	@Column(name = "ADDRESS_AR_1", unique = false, nullable = true)
	private String addressAr1;
	

	@Column(name = "ADDRESS_AR_2", unique = false, nullable = true)
	private String address2Ar;

	@Column(name = "PINCODE", unique = false, nullable = false)
	private String pincode;

	@Column(name = "LANDMARK", unique = false, nullable = false)
	private String landMark;

	@Column(name = "LANDMARK_AR", unique = false, nullable = true)
	private String landMarkAr;

	@Column(name = "PHONE_NO", unique = false, nullable = true)
	private String phoneNumber;
	
	@Column(name = "DELIVERY_NAME", unique = false, nullable = true)
	private String deliveryName;
	

	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "CUSTOMER_ID", nullable = true, insertable = false, updatable = false)
	private CustomerDetails customerDetails;

	
	
	
	
	
	

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getToDeliver() {
		return toDeliver;
	}

	public void setToDeliver(String toDeliver) {
		this.toDeliver = toDeliver;
	}

	public String getAddressAr1() {
		return addressAr1;
	}

	public void setAddressAr1(String addressAr1) {
		this.addressAr1 = addressAr1;
	}

	public String getAddress2Ar() {
		return address2Ar;
	}

	public void setAddress2Ar(String address2Ar) {
		this.address2Ar = address2Ar;
	}

	public String getLandMarkAr() {
		return landMarkAr;
	}

	public void setLandMarkAr(String landMarkAr) {
		this.landMarkAr = landMarkAr;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}

	public long getShippingId() {
		return shippingId;
	}

	public void setShippingId(long shippingId) {
		this.shippingId = shippingId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getLandMark() {
		return landMark;
	}

	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

}
