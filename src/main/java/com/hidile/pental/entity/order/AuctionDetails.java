package com.hidile.pental.entity.order;
/*package com.hidile.pental.entity.order;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.hidile.pental.entity.input.Store;

@Entity
@Table(name = "AUCTION_DETAILS",uniqueConstraints ={@UniqueConstraint(columnNames = "AUCTION_DETAILS_ID")})
public class AuctionDetails
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUCTION_DETAILS_ID",unique = true,nullable = false)
	private long auctionDetailsId;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@Column(name ="OPERATING_OFFICER_ID",unique =false,nullable =false)
	private long operatingOfficerId;
	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
	@Column(name = "BID_AMOUNT",unique = false,nullable = false)
	private double bidAmount;
	
	@Column(name = "LAST_BID_AMOUNT",unique = false,nullable = false)
	private double lastBidAmount;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name = "AUCTION_ID",unique = false,nullable = false)
	private long auctionId;
	
	@ManyToOne( fetch = FetchType.EAGER)  
	@JoinColumn(name="AUCTION_ID", nullable = true, insertable = false, updatable = false)
	private Auction auction;
	
	

	public long getAuctionDetailsId() {
		return auctionDetailsId;
	}

	public void setAuctionDetailsId(long auctionDetailsId) {
		this.auctionDetailsId = auctionDetailsId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public Auction getAuction() {
		return auction;
	}

	public void setAuction(Auction auction) {
		this.auction = auction;
	}

	public long getAuctionId() {
		return auctionId;
	}

	public void setAuctionId(long auctionId) {
		this.auctionId = auctionId;
	}

	

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public double getBidAmount() {
		return bidAmount;
	}

	public void setBidAmount(double bidAmount) {
		this.bidAmount = bidAmount;
	}

	public double getLastBidAmount() {
		return lastBidAmount;
	}

	public void setLastBidAmount(double lastBidAmount) {
		this.lastBidAmount = lastBidAmount;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
*/