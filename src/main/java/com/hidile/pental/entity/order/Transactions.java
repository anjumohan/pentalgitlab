package com.hidile.pental.entity.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "TRANSACTION", uniqueConstraints = { @UniqueConstraint(columnNames = "TRANSACTION_ID") })
public class Transactions implements Serializable, Cloneable 
{

	/**
	* 
		 */
	private static final long serialVersionUID = -1737516571705459293L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TRANSACTION_ID", unique = true, nullable = false)
	private long transactionId;

	@Column(name = "TRANSACTION_MODE", unique = false, nullable = false)
	private int transactionMode;

	@Column(name = "OPRATING_OFFICERID", unique = false, nullable = false)
	private long operatingOfficer;

	@Column(name = "TO_WHO_ID", unique = false, nullable = false)
	private long toWhoId;

	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;

	@Column(name = "RECIPT_NO", unique = false, nullable = false)
	private String reciptNo;

	@Column(name = "NOTE", unique = false, nullable = false)
	private String note;

	@Column(name = "NET_AMOUNT", unique = false, nullable = false)
	private double netAmount;
	
	@Column(name = "DISCOUNT", unique = false, nullable = false)
	private double discount;

	@Column(name = "IN_OR_OUT", unique = false, nullable = false)
	private int inOrOut;

	@Column(name = "ISCLEARED", unique = false, nullable = false)
	private int isCleared;

	@Column(name = "PAIRED_ID", unique = false, nullable = false)
	private long pairedId;

	@Column(name = "BANK_OR_CASH", unique = false, nullable = false)
	private int bankOrCash;

	@Column(name = "CMD", unique = false, nullable = false)
	private long cmd;

	@Column(name = "STATUS", unique = false, nullable = false)
	private int status;
	
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	

	
	
	

	public int getIsCleared() {
		return isCleared;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public int getTransactionMode() {
		return transactionMode;
	}

	public void setTransactionMode(int transactionMode) {
		this.transactionMode = transactionMode;
	}

	public long getOperatingOfficer() {
		return operatingOfficer;
	}

	public void setOperatingOfficer(long operatingOfficer) {
		this.operatingOfficer = operatingOfficer;
	}

	public long getToWhoId() {
		return toWhoId;
	}

	public void setToWhoId(long toWhoId) {
		this.toWhoId = toWhoId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	
	

	public String getReciptNo() {
		return reciptNo;
	}

	public void setReciptNo(String reciptNo) {
		this.reciptNo = reciptNo;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}


	
	public int getInOrOut() {
		return inOrOut;
	}

	public void setInOrOut(int inOrOut) {
		this.inOrOut = inOrOut;
	}

	public long getPairedId() {
		return pairedId;
	}

	public void setPairedId(long pairedId) {
		this.pairedId = pairedId;
	}

	public int getBankOrCash() {
		return bankOrCash;
	}

	public void setBankOrCash(int bankOrCash) {
		this.bankOrCash = bankOrCash;
	}

	public long getCmd() {
		return cmd;
	}

	public void setCmd(long cmd) {
		this.cmd = cmd;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setIsCleared(int isCleared) {
		this.isCleared = isCleared;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	
	
}
