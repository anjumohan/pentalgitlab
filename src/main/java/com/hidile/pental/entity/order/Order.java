package com.hidile.pental.entity.order;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "ORDER_TABLE", uniqueConstraints = { @UniqueConstraint(columnNames = "ORDER_ID") })
public class Order implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ORDER_ID", unique = true, nullable = false)
	private long orderId;

	@Column(name = "ORDER_STATUS", unique = false, nullable = false)
	private int orderstatus;

	@Column(name = "IS_COD", unique = false, nullable = true)
	private int cod;

	@Column(name = "IS_ONLINE", unique = false, nullable = true)
	private int online;

	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;

	@Column(name = "PROMO_CODE_ID", unique = false, nullable = true)
	private long promocodeId;

	@Column(name = "MODE_ID", unique = false, nullable = false)
	private int modeId;

	@Column(name = "DESCRIPTION", unique = false, nullable = false)
	private String description;

	@Column(name = "STATUS", unique = false, nullable = false)
	private int status;

	@Column(name = "OPERATING_OFFICER_ID", unique = false, nullable = false)
	private long operatingOfficerId;

	@Column(name = "SELLER_ID", unique = false, nullable = false)
	private long sellerId;

	@Column(name = "CUSTOMER_ID", unique = false, nullable = false)
	private long customerId;

	@Column(name = "DELIVERY_DATE", unique = false, nullable = false)
	private long deliveryDate;

	@Column(name = "EXPECTED_DELIVERY_DATE", unique = false, nullable = false)
	private long expectedDeliveryDate;

	@Column(name = "NETPRICE", unique = false, nullable = false)
	private double netPrice;

	@Column(name = "LATITUDE", unique = false, nullable = false)
	private double latitude;

	@Column(name = "LONGITUDE", unique = false, nullable = false)
	private double longitude;

	@Column(name = "NUMBER_OF_ITEMS", unique = false, nullable = false)
	private long numberOfItems;

	@Column(name = "TAX", unique = false, nullable = false)
	private double tax;

	@Column(name = "SHIPPING_ID", unique = false, nullable = false)
	private long shippingId;
	
	@ColumnDefault("'00'")
	@Column(name = "TOTAL_SHIPPING_CHARGE", unique = false, nullable = true)
	private double totalShippingCharge;
	
	@ColumnDefault("'00'")
	@Column(name = "TOTAL_INSTALLATION_CHARGE", unique = false, nullable = true)
	private double totalInstallationCharge;

	@Column(name = "PICK_UP_TIME", unique = false, nullable = false)
	private long pickUpTime;

	@Column(name = "TRANSACTION_ID", unique = false, nullable = false)
	private long transactionId;

	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	
	@ColumnDefault("'00'")
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "PAYMENT_ID", unique = false, nullable = true)
	private String paymentId;
	
	@ColumnDefault("'00'")
	@Column(name = "PAYMENT_STATUS", unique = false, nullable = true)
	private int paymentStatus;
	
	

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "order", cascade = CascadeType.ALL)
	private Set<OrderDetails> orderDetails;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "SHIPPING_ID", nullable = true, insertable = false, updatable = false)
	private ShippingDetails shippingDetails;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "TRANSACTION_ID", nullable = true, insertable = false, updatable = false)
	private Transactions transaction;
	
	
	
	
	
	
	

	public int getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public double getTotalInstallationCharge() {
		return totalInstallationCharge;
	}

	public void setTotalInstallationCharge(double totalInstallationCharge) {
		this.totalInstallationCharge = totalInstallationCharge;
	}

	public double getTotalShippingCharge() {
		return totalShippingCharge;
	}

	public void setTotalShippingCharge(double totalShippingCharge) {
		this.totalShippingCharge = totalShippingCharge;
	}

	public long getPromocodeId() {
		return promocodeId;
	}

	public void setPromocodeId(long promocodeId) {
		this.promocodeId = promocodeId;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public int getOnline() {
		return online;
	}

	public void setOnline(int online) {
		this.online = online;
	}

	public ShippingDetails getShippingDetails() {
		return shippingDetails;
	}

	public void setShippingDetails(ShippingDetails shippingDetails) {
		this.shippingDetails = shippingDetails;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getShippingId() {
		return shippingId;
	}

	public void setShippingId(long shippingId) {
		this.shippingId = shippingId;
	}

	public int getModeId() {
		return modeId;
	}

	public void setModeId(int modeId) {
		this.modeId = modeId;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getPickUpTime() {
		return pickUpTime;
	}

	public void setPickUpTime(long pickUpTime) {
		this.pickUpTime = pickUpTime;
	}

	public Transactions getTransaction() {
		return transaction;
	}

	public void setTransaction(Transactions transaction) {
		this.transaction = transaction;
	}

	public Set<OrderDetails> getOrderDetails() {
		return orderDetails;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setOrderDetails(Set<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	/*
	 * public long getDeliveryBoyId() { return deliveryBoyId; }
	 * 
	 * public void setDeliveryBoyId(long deliveryBoyId) { this.deliveryBoyId =
	 * deliveryBoyId; }
	 */

	public double getNetPrice() {
		return netPrice;
	}

	public void setNetPrice(double netPrice) {
		this.netPrice = netPrice;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public long getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(long numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public long getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(long expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public int getOrderstatus() {
		return orderstatus;
	}

	public void setOrderstatus(int orderstatus) {
		this.orderstatus = orderstatus;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public long getSellerId() {
		return sellerId;
	}

	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(long deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

}
