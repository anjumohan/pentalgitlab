package com.hidile.pental.entity.order;
/*package com.hidile.pental.entity.order;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.hidile.pental.entity.order.Auction;
@Entity
@Table(name = "CS_FEATURE_AND_PHOTOS_AUCTION",uniqueConstraints ={@UniqueConstraint(columnNames = "FEATURE_PHOTO_ID")})
public class FeaturesAndPhotosAuction implements Serializable 
{

*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;

public FeaturesAndPhotosAuction()
{}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FEATURE_PHOTO_ID",unique = true,nullable = false)
	private long featurePhotoId;
	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
	@Column(name = "MODE_ID",unique = false,nullable = false)
	private int modeId;
	
	@Column(name ="DATA",unique =false,nullable =true)
	private String data;
	
	@Column(name ="EXTRA_DATA",unique =false,nullable =true)
	private String extraData;
	
	@Column(name ="EXTRA_DATA_NUM",unique =false,nullable =true)
	private double extraDataNum;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@Column(name ="AUCTION_ID",unique =false,nullable =false)
	private long auctionId;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="AUCTION_ID", nullable = true, insertable = false, updatable = false)
	private Auction  auction;

	public long getFeaturePhotoId() {
		return featurePhotoId;
	}

	public void setFeaturePhotoId(long featurePhotoId) {
		this.featurePhotoId = featurePhotoId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getModeId() {
		return modeId;
	}

	public void setModeId(int modeId) {
		this.modeId = modeId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getExtraData() {
		return extraData;
	}

	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}

	public double getExtraDataNum() {
		return extraDataNum;
	}

	public void setExtraDataNum(double extraDataNum) {
		this.extraDataNum = extraDataNum;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getAuctionId() {
		return auctionId;
	}

	public void setAuctionId(long auctionId) {
		this.auctionId = auctionId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Auction getAuction() {
		return auction;
	}

	public void setAuction(Auction auction) {
		this.auction = auction;
	}
	
	
	
	
	
		
	}
	
	

*/