package com.hidile.pental.entity.order;
/*package com.hidile.pental.entity.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "CART_AND_FAVOURITES",uniqueConstraints ={@UniqueConstraint(columnNames = "CART_ID")})
public class CartAndFavourites implements Serializable
{
	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="CART_ID",unique =true,nullable =true)
	private long cartId;
	
	
	@Column(name = "ITEM_QUANTITY",unique = false,nullable = false)
	private int itemQuantity;
	
	@Column(name = "ITEM_NAME",unique = false,nullable = false)
	private String itemName;
	

	@Column(name = "ITEM_NETPRICE",unique = false,nullable = false)
	private double itemNetPrice;
	
	
	@Column(name = "ITEM_PRICE",unique = false,nullable = false)
	private long itemPrice;
	
	
	@Column(name = "SELLER_NAME",unique = false,nullable = false)
	private String sellerName;
	
	
	@Column(name ="ORDER_STATUS",unique =false,nullable =false)
	private int orderStatus;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;

	
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}


	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}


	public long getCartId() {
		return cartId;
	}


	public void setCartId(long cartId) {
		this.cartId = cartId;
	}


	

	public int getItemQuantity() {
		return itemQuantity;
	}


	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public double getItemNetPrice() {
		return itemNetPrice;
	}


	public void setItemNetPrice(double itemNetPrice) {
		this.itemNetPrice = itemNetPrice;
	}


	public long getItemPrice() {
		return itemPrice;
	}


	public void setItemPrice(long itemPrice) {
		this.itemPrice = itemPrice;
	}


	public String getSellerName() {
		return sellerName;
	}


	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}


	public int getOrderStatus() {
		return orderStatus;
	}


	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
		
}
*/