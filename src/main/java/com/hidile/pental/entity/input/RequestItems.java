package com.hidile.pental.entity.input;
/*package com.hidile.pental.entity.input;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "REQUEST_ITEMS",uniqueConstraints ={@UniqueConstraint(columnNames = "REQUEST_ITEM_ID")})
public class RequestItems  implements Serializable 
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REQUEST_ITEM_ID",unique = true,nullable = false)
	private long requestItemId;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name ="CREATED_TIME",unique =false,nullable =false)
	private long createdTime;
	

	@Column(name ="OPERATING_OFFICER_ID",unique =false,nullable =false)
	private long operatingOfficerId;
	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
	@Column(name = "STORE_ID",unique = false,nullable = false)
	private long storeId;
	
	
	@Column(name ="PHONE_NUMBER",unique =false,nullable =false)
	private String phoneNumber;
	
	@Column(name ="EMAIL_ID",unique =false,nullable =false)
	private String emailId;
	
	

	
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public long getRequestItemId() {
		return requestItemId;
	}

	public void setRequestItemId(long requestItemId) {
		this.requestItemId = requestItemId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}
	
	
	
	
	
	
	
	
}
*/