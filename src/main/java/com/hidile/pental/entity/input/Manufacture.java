package com.hidile.pental.entity.input;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "MANUFACTURER", uniqueConstraints = { @UniqueConstraint(columnNames = "MANUFACTURER_ID") })
public class Manufacture implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2694261013002519012L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MANUFACTURER_ID", unique = true, nullable = false)
	private long manufacturerId;

	@Column(name = "MANUFACTURER_NAME", unique = false, nullable = false)
	private String manufacturerName;
	
	@Column(name = "MANUFACTURER_NAME_AR", unique = false, nullable = true)
	private String manufacturerNameAr;

	@Column(name = "BUSINESS_TYPE", unique = false,nullable = false)
	private String businessType;
	
	@Column(name = "STATUS", unique = false,nullable = false)
	private int status;
	
	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;
	
	@Column(name ="OPRATING_OFFICERID",unique =false,nullable =false)
	private long operatingOfficer;
	
	@Column(name ="BATCH_PROCESSING_ID",unique =false,nullable = true)
	private long batchProcessingId;
	
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	
	@Column(name = "ImageName",unique = false,nullable = true)
	private String imageName;
	
	
	
	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getManufacturerNameAr() {
		return manufacturerNameAr;
	}

	public void setManufacturerNameAr(String manufacturerNameAr) {
		this.manufacturerNameAr = manufacturerNameAr;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getOperatingOfficer() {
		return operatingOfficer;
	}

	public void setOperatingOfficer(long operatingOfficer) {
		this.operatingOfficer = operatingOfficer;
	}

	public long getBatchProcessingId() {
		return batchProcessingId;
	}

	public void setBatchProcessingId(long batchProcessingId) {
		this.batchProcessingId = batchProcessingId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	
	
}
