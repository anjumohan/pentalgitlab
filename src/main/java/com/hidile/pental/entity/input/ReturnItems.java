package com.hidile.pental.entity.input;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

import com.hidile.pental.entity.order.OrderDetails;
import com.hidile.pental.entity.order.Transactions;


@Entity
@Table(name = "RETURN_ITEMS",uniqueConstraints =  {@UniqueConstraint(columnNames = "RETURN_ITEM_ID")})
public class ReturnItems implements Serializable
{
private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RETURN_ITEM_ID",unique = true,nullable = false)
	private long returnItemId;	
	
	@Column(name = "ORDER_LIST_ID", unique = false, nullable = false)
	private long orderListId;
	
	@Column(name = "OLD_ORDER_LIST_ID", unique = false, nullable = true)
	private long oldOrderListId;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "RETURN_REASON", unique = false, nullable = false)
	private String returnReason;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "COMMENTS", unique = false, nullable = true)
	private String comments;
	
	@Column(name = "TRANSACTION_ID", unique = false, nullable = false)
	private long transactionId;
	
	@Column(name = "ITEM_QUANTITY",unique = false,nullable = false)
	private long itemQuantity;
	
	@Column(name = "ITEM_NETPRICE",unique = false,nullable = false)
	private double itemNetPrice;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@Column(name ="OPERATING_OFFICER_ID",unique =false,nullable =false)
	private long operatingOfficerId;
	
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="ORDER_LIST_ID", nullable = true, insertable = false, updatable = false)
	private OrderDetails orderDetails;

	public long getOldOrderListId() {
		return oldOrderListId;
	}

	public void setOldOrderListId(long oldOrderListId) {
		this.oldOrderListId = oldOrderListId;
	}

	public long getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(long itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	
	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public double getItemNetPrice() {
		return itemNetPrice;
	}

	public void setItemNetPrice(double itemNetPrice) {
		this.itemNetPrice = itemNetPrice;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getReturnItemId() {
		return returnItemId;
	}

	public void setReturnItemId(long returnItemId) {
		this.returnItemId = returnItemId;
	}

	public String getReturnReason() {
		return returnReason;
	}

	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	

	public long getOrderListId() {
		return orderListId;
	}

	public void setOrderListId(long orderListId) {
		this.orderListId = orderListId;
	}

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	
	

}
