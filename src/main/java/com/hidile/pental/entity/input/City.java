package com.hidile.pental.entity.input;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "CITY",uniqueConstraints ={@UniqueConstraint(columnNames = "CITY_ID")})
public class City implements Serializable 
{

private static final long serialVersionUID = 1L;
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CITY_ID",unique = true,nullable = false)
	private long cityId;
	
	@Column(name = "CITY_NAME",unique = false,nullable = false)
	private String cityName;
	
	@Column(name = "CITY_NAME_AR",unique = false,nullable = true)
	private String cityNameAr;
	
	@Column(name = "CITY_STATUS",unique = false,nullable = false)
	private int citusStatus;
	
	@Column(name = "CITY_FLAG",unique = false,nullable = false)
	private int cityFlag;
	
	@Column(name = "COUNTRY_ID",unique = false,nullable = false)
	private long countryId;
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="COUNTRY_ID", nullable = true, insertable = false, updatable = false)
	private Country country;
	

	public String getCityNameAr() {
		return cityNameAr;
	}

	public void setCityNameAr(String cityNameAr) {
		this.cityNameAr = cityNameAr;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	
	

	public int getCitusStatus() {
		return citusStatus;
	}

	public void setCitusStatus(int citusStatus) {
		this.citusStatus = citusStatus;
	}

	public int getCityFlag() {
		return cityFlag;
	}

	public void setCityFlag(int cityFlag) {
		this.cityFlag = cityFlag;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
