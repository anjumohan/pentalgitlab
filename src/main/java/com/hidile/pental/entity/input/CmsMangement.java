package com.hidile.pental.entity.input;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "CMS_MANAGEMENT",uniqueConstraints ={@UniqueConstraint(columnNames = "CMS_ID")})
public class CmsMangement implements Serializable 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CMS_ID",unique = true,nullable = false)
	private long cmsId;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "TERMS_AND_CONDITIONS", unique = false, nullable = false)
	private String termsAndConditions;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "TERMS_AND_CONDITIONS_AR", unique = false, nullable = true)
	private String termsAndConditionsAr;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "ABOUT_US", unique = false, nullable = false)
	private String aboutUs;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "ABOUT_US_AR", unique = false, nullable = true)
	private String aboutUsAr;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "FAQS", unique = false, nullable = false)
	private String faqs;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "FAQS_AR", unique = false, nullable = true)
	private String faqsAr;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "PRIVACY_POLICY", unique = false, nullable = false)
	private String privacyPolicy;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "PRIVACY_POLICY_AR", unique = false, nullable = true)
	private String privacyPolicyAr;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "CONTACT_US", unique = false, nullable = false)
	private String contactUs;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "CONTACT_US_AR", unique = false, nullable = true)
	private String contactUsAr;
	
	
	
	

	public String getTermsAndConditionsAr() {
		return termsAndConditionsAr;
	}

	public void setTermsAndConditionsAr(String termsAndConditionsAr) {
		this.termsAndConditionsAr = termsAndConditionsAr;
	}

	public String getAboutUsAr() {
		return aboutUsAr;
	}

	public void setAboutUsAr(String aboutUsAr) {
		this.aboutUsAr = aboutUsAr;
	}

	public String getFaqsAr() {
		return faqsAr;
	}

	public void setFaqsAr(String faqsAr) {
		this.faqsAr = faqsAr;
	}

	public String getPrivacyPolicyAr() {
		return privacyPolicyAr;
	}

	public void setPrivacyPolicyAr(String privacyPolicyAr) {
		this.privacyPolicyAr = privacyPolicyAr;
	}

	public String getContactUsAr() {
		return contactUsAr;
	}

	public void setContactUsAr(String contactUsAr) {
		this.contactUsAr = contactUsAr;
	}

	public long getCmsId() {
		return cmsId;
	}

	public void setCmsId(long cmsId) {
		this.cmsId = cmsId;
	}

	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public String getAboutUs() {
		return aboutUs;
	}

	public void setAboutUs(String aboutUs) {
		this.aboutUs = aboutUs;
	}

	public String getFaqs() {
		return faqs;
	}

	public void setFaqs(String faqs) {
		this.faqs = faqs;
	}

	public String getPrivacyPolicy() {
		return privacyPolicy;
	}

	public void setPrivacyPolicy(String privacyPolicy) {
		this.privacyPolicy = privacyPolicy;
	}

	public String getContactUs() {
		return contactUs;
	}

	public void setContactUs(String contactUs) {
		this.contactUs = contactUs;
	}
	
	
	
	
	
	
	
}
