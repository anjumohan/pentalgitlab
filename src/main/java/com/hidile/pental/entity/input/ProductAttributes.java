package com.hidile.pental.entity.input;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "PRODUCT_ATTRIBUTES",uniqueConstraints ={@UniqueConstraint(columnNames = "PRODUCT_ATTRIBUTE_ID")})
public class ProductAttributes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRODUCT_ATTRIBUTE_ID",unique = true,nullable = false)
	private long productAttributeId;
	
	@Column(name ="PRODUCT_ID",unique =false,nullable =false)
	private long productId;
	
	@Column(name = "ATTRIBUTE_ID",unique = false,nullable = false)
	private long attributeId;
	
	@Column(name = "VALUE",unique = false,nullable = false)
	private String value;
	
	@Column(name = "VALUE_AR",unique = false,nullable = true)
	private String valueAr;
	
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="PRODUCT_ID", nullable = true, insertable = false, updatable = false)
	private Product product;

	
	public String getValueAr() {
		return valueAr;
	}

	public void setValueAr(String valueAr) {
		this.valueAr = valueAr;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public long getProductAttributeId() {
		return productAttributeId;
	}

	public void setProductAttributeId(long productAttributeId) {
		this.productAttributeId = productAttributeId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(long attributeId) {
		this.attributeId = attributeId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	
	
	
}
