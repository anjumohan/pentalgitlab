package com.hidile.pental.entity.input;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "COUNTRY",uniqueConstraints ={@UniqueConstraint(columnNames = "COUNTRY_ID")})
public class Country implements Serializable
{
private static final long serialVersionUID = 1L;
	
	
	

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name ="COUNTRY_ID",unique =true,nullable =true)
private long countryId;
	
	@Column(name = "COUNTRY_NAME",unique = false,nullable = false)
	private String countryName;
	
	@Column(name = "COUNTRY_NAME_AR",unique = false,nullable = true)
	private String countryNameAr;
	
	
	@Column(name = "COUNTRY_STATUS",unique = false,nullable = false)
	private int countryStatus;
	
	@Column(name = "COUNTRY_FLAG",unique = false,nullable = false)
	private String countryFlag;
	
	@Column(name = "CURRENCY",unique = false,nullable = true)
	private String currency;
	
	@Column(name = "COUNTRY_CODE",unique = false,nullable = true)
	private int countryCode;
	

	public int getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryNameAr() {
		return countryNameAr;
	}

	public void setCountryNameAr(String countryNameAr) {
		this.countryNameAr = countryNameAr;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}


	

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getCountryStatus() {
		return countryStatus;
	}

	public void setCountryStatus(int countryStatus) {
		this.countryStatus = countryStatus;
	}

	public String getCountryFlag() {
		return countryFlag;
	}

	public void setCountryFlag(String countryFlag) {
		this.countryFlag = countryFlag;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}
