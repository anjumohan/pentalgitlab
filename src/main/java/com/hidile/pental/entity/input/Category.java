package com.hidile.pental.entity.input;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "CATEGORY",uniqueConstraints ={@UniqueConstraint(columnNames = "CATEGORY_ID")})
public class Category implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CATEGORY_ID",unique = true,nullable = false)
	private long categoryId;
	
	@Column(name = "CATEGORY_NAME",unique = false,nullable = false)
	private String categoryName;
	
	@Column(name = "CATEGORY_NAME_ARABIC",unique = false,nullable = false)
	private String categoryNameAr;
	
	@Column(name = "COLOR",unique = false,nullable = false)
	private String color;
	
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name = "CREATED_DATE",unique = false,nullable = false)
	private long createdDate;
	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DiSCRIPTION",unique = false,nullable = false)
	private String discription;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DiSCRIPTION_AR",unique = false,nullable = true)
	private String discriptionAr;
	
	
	@Column(name = "ImageName",unique = false,nullable = false)
	private String imageName;
	
	/*@Column(name = "IconImage",unique = false,nullable = false)
	private String iconImage;
	*/
	@Column(name ="OPERATING_OFFICER_ID",unique =false,nullable =false)
	private long operatingOfficerId;
	
	@Column(name ="COMMISION",unique =false,nullable =false)
	private double commision;
	
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	

	@OneToMany(fetch = FetchType.EAGER, mappedBy="category" ,cascade = CascadeType.ALL)
	private Set<SubCategory> subCategorys;
	
	@Column(name ="RANK",unique =false,nullable =false)
	private int  rank;

	
	
	
	
	
	public String getDiscriptionAr() {
		return discriptionAr;
	}

	public void setDiscriptionAr(String discriptionAr) {
		this.discriptionAr = discriptionAr;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	/*public String getIconImage() {
		return iconImage;
	}

	public void setIconImage(String iconImage) {
		this.iconImage = iconImage;
	}
*/
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getCategoryNameAr() {
		return categoryNameAr;
	}

	public void setCategoryNameAr(String categoryNameAr) {
		this.categoryNameAr = categoryNameAr;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public Set<SubCategory> getSubCategorys() {
		return subCategorys;
	}

	public void setSubCategorys(Set<SubCategory> subCategorys) {
		this.subCategorys = subCategorys;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String description) {
		this.discription = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public double getCommision() {
		return commision;
	}

	public void setCommision(double commision) {
		this.commision = commision;
	}

	
	

}

