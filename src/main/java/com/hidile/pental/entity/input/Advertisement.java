package com.hidile.pental.entity.input;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "ADVERTISEMENT",uniqueConstraints ={@UniqueConstraint(columnNames = "ADD_ID")})
public class Advertisement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ADD_ID",unique = true,nullable = false)
	private long addId;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@Column(name ="OPERATING_OFFICER_ID",unique =false,nullable =false)
	private long operatingOfficerId;
	
	@Column(name ="ADD_STATUS",unique =false,nullable =true)
	private int addStatus;
	
	@Column(name ="STATUS",unique =false,nullable =true)
	private int status;
	
	@Column(name ="ADD_EXPIRY_DATE",unique =false,nullable =true)
	private long addExpirtyDate;
	
	@Column(name ="imgaeName",unique =false,nullable =true)
	private String imageName;
	
	@Column(name = "SELLER_ID",unique = false,nullable = true)
	private long sellerId;
	
	@Column(name = "TITILE",unique = false,nullable = true)
	private String titile;
	
	@Column(name = "TITILE_AR",unique = false,nullable = true)
	private String titileAr;
	
	@Column(name = "SUB_TITILE",unique = false,nullable = true)
	private String subTitile;
	
	@Column(name = "SUB_TITILE_AR",unique = false,nullable = true)
	private String subTitileAr;
	
	@Column(name = "LINK",unique = false,nullable = true)
	private String link;
	
	@Column(name = "TEXT",unique = false,nullable = true)
	private String text;
	
	@Column(name = "TEXT_AR",unique = false,nullable = true)
	private String textAr;
	
	@Column(name = "COMPANY_ID",unique = false,nullable = true)
	private long companyId;
	
	@Column(name = "MODE_ID",unique = false,nullable = true)
	private long modeId;
	
	
	@Column(name = "HEIGHT",unique = false,nullable = true)
	private double height;
	
	@ColumnDefault("'00'")
	@Column(name = "ARABIC_OR_ENGLISH",unique = false,nullable = true)
	private long arabicOrEnglish;
	
	@ColumnDefault("'00'")
	@Column(name = "RANK",unique = false,nullable = true)
	private int  rank;
	
		
	
	
	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public long getArabicOrEnglish() {
		return arabicOrEnglish;
	}

	public void setArabicOrEnglish(long arabicOrEnglish) {
		this.arabicOrEnglish = arabicOrEnglish;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public long getModeId() {
		return modeId;
	}

	public void setModeId(long modeId) {
		this.modeId = modeId;
	}

	public String getTitileAr() {
		return titileAr;
	}

	public void setTitileAr(String titileAr) {
		this.titileAr = titileAr;
	}

	public String getSubTitileAr() {
		return subTitileAr;
	}

	public void setSubTitileAr(String subTitileAr) {
		this.subTitileAr = subTitileAr;
	}

	public String getTextAr() {
		return textAr;
	}

	public void setTextAr(String textAr) {
		this.textAr = textAr;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getTitile() {
		return titile;
	}

	public void setTitile(String titile) {
		this.titile = titile;
	}

	public String getSubTitile() {
		return subTitile;
	}

	public void setSubTitile(String subTitile) {
		this.subTitile = subTitile;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getAddId() {
		return addId;
	}

	public void setAddId(long addId) {
		this.addId = addId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public int getAddStatus() {
		return addStatus;
	}

	public void setAddStatus(int addStatus) {
		this.addStatus = addStatus;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getAddExpirtyDate() {
		return addExpirtyDate;
	}

	public void setAddExpirtyDate(long addExpirtyDate) {
		this.addExpirtyDate = addExpirtyDate;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public long getSellerId() {
		return sellerId;
	}

	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}

	
	

}
