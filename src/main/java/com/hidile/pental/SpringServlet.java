package com.hidile.pental;
/*package com.hidile.pental.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@PropertySource("classpath:/jdbc.properties")
@ComponentScan(basePackages={"com.hidile.pental.ctrl","com.hidile.pental.service","com.hidile.pental.dao"})
@EnableTransactionManagement
@EnableWebMvc
public class SpringServlet extends WebMvcConfigurerAdapter{

	 @Value("${hibernate.dialect}") String dialect;
	 @Value("${hibernate.show_sql}") String showSql;
	 @Value("${hibernate.hbm2ddl.auto}") String hibernateAuto;
	 @Value("${db.driver}") String driverClass;
	 @Value("${db.url}") String dbUrl;
	 @Value("${db.username}") String dbUsername;
	 @Value("${db.password}") String dbPassword;

	
	@Bean(name = "dataSource")
	public DataSource getDataSource() {
	
	    BasicDataSource dataSource = new BasicDataSource();
	    dataSource.setDriverClassName(driverClass);
	    dataSource.setUrl(dbUrl);
	    dataSource.setUsername(dbUsername);
	    dataSource.setPassword(dbPassword);
	 
	    return dataSource;
	}
	
	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource) {
	 
	    LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
	 
	    sessionBuilder.scanPackages("com.hidile.pental.entity");
	    sessionBuilder.addProperties(getHibernateProperties());
	    return sessionBuilder.buildSessionFactory();
	}
	
	
	private Properties getHibernateProperties() {
	    Properties properties = new Properties();
	    properties.put("hibernate.show_sql", showSql);
	    properties.put("hibernate.dialect", dialect);
	    properties.put("hibernate.hbm2ddl.auto",hibernateAuto);
	    return properties;
	}
	
	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(
	        SessionFactory sessionFactory) {
	    HibernateTransactionManager transactionManager = new HibernateTransactionManager(
	            sessionFactory);
	 
	    return transactionManager;
	}
	
	@Autowired
	@Bean(name = "hibernateTemplate")
	public HibernateTemplate getHibernateTemplate(SessionFactory sessionFactory){
		HibernateTemplate hibernateTemplate=new HibernateTemplate(sessionFactory);
		return hibernateTemplate;
	}
	
	 @Bean
	  public InternalResourceViewResolver getViewResolver() {
	    InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	    viewResolver.setPrefix("/WEB-INF/pages/");
	    viewResolver.setSuffix(".jsp");
	    return viewResolver;
	  }

	  @Override
	  public void addViewControllers(ViewControllerRegistry registry) {
		  registry.addViewController("/").setViewName("forward:/index.html");
	  }
	  
	  
	// equivalents for <mvc:resources/> tags
	    @Override
	    public void addResourceHandlers(ResourceHandlerRegistry registry) {
	        registry.addResourceHandler("/resources/**").addResourceLocations("/lib/").setCachePeriod(31556926);
	        registry.addResourceHandler("/public/**").addResourceLocations("/public/").setCachePeriod(31556926);
	    }

	    // equivalent for <mvc:default-servlet-handler/> tag
	    @Override
	    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
	        configurer.enable();
	    }
}
*/