package com.hidile.pental.model.shifting;

public class DelivaryModel {
	public String counterName;
	public long counterId;
	public long itemId;
	public String initial_reading;
	public double sellPrice;
	public String itemName;
	public double purchasePrice;
	
	
	
	
	public DelivaryModel(String counterName,long counterId, long itemId,
			String initial_reading, double sellPrice, String itemName,double purPrice) {
		super();
		this.counterName = counterName;
		this.counterId = counterId;
		this.itemId = itemId;
		this.initial_reading = initial_reading;
		this.sellPrice = sellPrice;
		this.itemName = itemName;
		this.purchasePrice= purPrice;
	}
	public String getCounterName() {
		return counterName;
	}
	public void setCounterName(String counterName) {
		this.counterName = counterName;
	}
	public long getItemId() {
		return itemId;
	}
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	public String getInitial_reading() {
		return initial_reading;
	}
	public void setInitial_reading(String initial_reading) {
		this.initial_reading = initial_reading;
	}
	public double getSellPrice() {
		return sellPrice;
	}
	public void setSellPrice(double sellPrice) {
		this.sellPrice = sellPrice;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getPurchasePrice() {
		return purchasePrice;
	}
	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	public long getCounterId() {
		return counterId;
	}
	public void setCounterId(long counterId) {
		this.counterId = counterId;
	}
	
	
	
}
