package com.hidile.pental.model.shifting;

public class CounterInfo {
	public long terminalId;
	public double initialReading;
	public double finalReading;
	public double unitPrice;
	public double lossAmount;
	public double netReading;
	public double purchaseprice;
	public long itemId;
	public String itemName;
	public double payingAmt;

	public long userId;

	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public long getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(long terminalId) {
		this.terminalId = terminalId;
	}

	public double getInitialReading() {
		return initialReading;
	}

	public void setInitialReading(double initialReading) {
		this.initialReading = initialReading;
	}

	public double getFinalReading() {
		return finalReading;
	}

	public void setFinalReading(double finalReading) {
		this.finalReading = finalReading;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public double getLossAmount() {
		return lossAmount;
	}

	public void setLossAmount(double lossAmount) {
		this.lossAmount = lossAmount;
	}


	public double getNetReading() {
		return netReading;
	}

	public void setNetReading(double netReading) {
		this.netReading = netReading;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public double getPayingAmt() {
		return payingAmt;
	}

	public void setPayingAmt(double payingAmt) {
		this.payingAmt = payingAmt;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public double getPurchaseprice() {
		return purchaseprice;
	}

	public void setPurchaseprice(double purchaseprice) {
		this.purchaseprice = purchaseprice;
	}
	
	
}
