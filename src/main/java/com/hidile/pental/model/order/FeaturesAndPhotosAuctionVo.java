package com.hidile.pental.model.order;

public class FeaturesAndPhotosAuctionVo 
{
	private long featurePhotoId;
	private long userId;
	private int modeId;
	private String data;
	private String extraData;
	private double extraDataNum;
	private long lastUpdatedTime;
	private long auctionId;
	private int status;
	
	
	public long getFeaturePhotoId() {
		return featurePhotoId;
	}
	public void setFeaturePhotoId(long featurePhotoId) {
		this.featurePhotoId = featurePhotoId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getModeId() {
		return modeId;
	}
	public void setModeId(int modeId) {
		this.modeId = modeId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getExtraData() {
		return extraData;
	}
	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}
	public double getExtraDataNum() {
		return extraDataNum;
	}
	public void setExtraDataNum(double extraDataNum) {
		this.extraDataNum = extraDataNum;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(long auctionId) {
		this.auctionId = auctionId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
