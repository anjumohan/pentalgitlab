package com.hidile.pental.model.order;

public class TransactionModel 
{
	private long transactionId;
	private int transactionMode;
	private long operatingOfficer;
	private long toWhoId;
	private long lastUpdatedTime;
	private String lastUpdatedTimeString;
	private double totalCommisiion;
	private String note;
	private double netAmount;
	private int inOrOut;
	private double discount;
	private int isCleared;
	private long pairedId;
	private int bankOrCash;
	private long cmd;
	private int status;
	private String tranMode;
	private String cashOrBank;
	private double monthlyTotal;
	private long monthyDate;
	private long time;
	private String month;
	private long productId;
	private String productName;
	private long sumOfOrderId;
	private double totalNetAmount;
	private long categoryId;
	private String categoryName;
	private long subCategoryId;
	private String subCategoryName;
	private long customerId;
	private String customerName;
	private String categoryNameAr;
	private String subCategoryNameAr;
	private String customerNameAr;
	private String productNameAr;
	private long sellerId;
	private String reciptNo;
	
	
	
	public double getTotalCommisiion() {
		return totalCommisiion;
	}
	public void setTotalCommisiion(double totalCommisiion) {
		this.totalCommisiion = totalCommisiion;
	}
	public String getReciptNo() {
		return reciptNo;
	}
	public void setReciptNo(String reciptNo) {
		this.reciptNo = reciptNo;
	}
	public long getSellerId() {
		return sellerId;
	}
	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}
	public String getProductNameAr() {
		return productNameAr;
	}
	public void setProductNameAr(String productNameAr) {
		this.productNameAr = productNameAr;
	}
	public String getCategoryNameAr() {
		return categoryNameAr;
	}
	public void setCategoryNameAr(String categoryNameAr) {
		this.categoryNameAr = categoryNameAr;
	}
	public String getSubCategoryNameAr() {
		return subCategoryNameAr;
	}
	public void setSubCategoryNameAr(String subCategoryNameAr) {
		this.subCategoryNameAr = subCategoryNameAr;
	}
	public String getCustomerNameAr() {
		return customerNameAr;
	}
	public void setCustomerNameAr(String customerNameAr) {
		this.customerNameAr = customerNameAr;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public long getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public long getSumOfOrderId() {
		return sumOfOrderId;
	}
	public void setSumOfOrderId(long sumOfOrderId) {
		this.sumOfOrderId = sumOfOrderId;
	}
	public double getTotalNetAmount() {
		return totalNetAmount;
	}
	public void setTotalNetAmount(double totalNetAmount) {
		this.totalNetAmount = totalNetAmount;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public double getMonthlyTotal() {
		return monthlyTotal;
	}
	public void setMonthlyTotal(double monthlyTotal) {
		this.monthlyTotal = monthlyTotal;
	}
	public long getMonthyDate() {
		return monthyDate;
	}
	public void setMonthyDate(long monthyDate) {
		this.monthyDate = monthyDate;
	}
	public String getCashOrBank() {
		return cashOrBank;
	}
	public void setCashOrBank(String cashOrBank) {
		this.cashOrBank = cashOrBank;
	}
	public String getTranMode() {
		return tranMode;
	}
	public void setTranMode(String tranMode) {
		this.tranMode = tranMode;
	}
	public String getLastUpdatedTimeString() {
		return lastUpdatedTimeString;
	}
	public void setLastUpdatedTimeString(String lastUpdatedTimeString) {
		this.lastUpdatedTimeString = lastUpdatedTimeString;
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public int getTransactionMode() {
		return transactionMode;
	}
	public void setTransactionMode(int transactionMode) {
		this.transactionMode = transactionMode;
	}
	public long getOperatingOfficer() {
		return operatingOfficer;
	}
	public void setOperatingOfficer(long operatingOfficer) {
		this.operatingOfficer = operatingOfficer;
	}
	public long getToWhoId() {
		return toWhoId;
	}
	public void setToWhoId(long toWhoId) {
		this.toWhoId = toWhoId;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public double getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}
	public int getInOrOut() {
		return inOrOut;
	}
	public void setInOrOut(int inOrOut) {
		this.inOrOut = inOrOut;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public int getIsCleared() {
		return isCleared;
	}
	public void setIsCleared(int isCleared) {
		this.isCleared = isCleared;
	}
	
	public long getPairedId() {
		return pairedId;
	}
	public void setPairedId(long pairedId) {
		this.pairedId = pairedId;
	}
	
	public long getCmd() {
		return cmd;
	}
	public void setCmd(long cmd) {
		this.cmd = cmd;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getBankOrCash() {
		return bankOrCash;
	}
	public void setBankOrCash(int bankOrCash) {
		this.bankOrCash = bankOrCash;
	}
	
	
	
	
	
	
	
	
	
	
	
}

