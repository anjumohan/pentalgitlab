package com.hidile.pental.model.order;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.hidile.pental.constants.Constants;
import com.hidile.pental.model.input.FeatureVo;

public class AuctionVo implements Constants
{

	private long auctionId;
	private long lastUpdatedTime;
	private String lastUpdatedTimeString;
	private long operatingOfficerId;
	private int auctionStatus;
	private int status;
	@NotNull
	@NotEmpty
	@Pattern(regexp=REGEX_NAME_LENGTH)
	private String itemName;
	@NotNull
	private double itemNetPrice;
	@NotNull
	@Min(value=0,message="auction expiry time should not be null")
	private long auctionExpiryTime;
	private String auctionExpiryTimeString;
	private double lastBidAmt;
	private int noOfBids;
	private String acutionDateString;
	private long acutionDate;
	private double basicBidAmt;
	private double maxBidAmt;
	@Min(value=1,message="city id should not be zero or less")
	private long cityId;
	@Min(value=1,message="country id should not be zero or less")
	private long countryId;
	private long userId;
	@NotNull(message="auction features and photos is empty")
	@NotEmpty(message="auction images and features are empty")
	private List<FeatureVo> featureAndPhotos=new ArrayList<FeatureVo>();
	@NotEmpty(message="item name arabic is empty")
	private String itemNameAr="";
	@NotEmpty(message="Description is empty")
	private String description="";
	@NotEmpty(message="Address is empty")
	private String address="";
	@Pattern(regexp=REGEX_PHONE,message="phone number format is incorrect")
	private String mobile="";
	@Pattern(regexp=REGEX_NOTE,message="note should not exceed 250")
	private String descriptionAr="";
	private long auctionDate;
	@Pattern(regexp=REGEX_NAME,message="Name format is incorrect")
	private String ownerName;
	
	

	public String getItemNameAr() {
		return itemNameAr;
	}
	public void setItemNameAr(String itemNameAr) {
		this.itemNameAr = itemNameAr;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getDescriptionAr() {
		return descriptionAr;
	}
	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr = descriptionAr;
	}
	public long getAuctionDate() {
		return auctionDate;
	}
	public void setAuctionDate(long auctionDate) {
		this.auctionDate = auctionDate;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public List<FeatureVo> getFeatureAndPhotos() {
		return featureAndPhotos;
	}
	public void setFeatureAndPhotos(List<FeatureVo> featureAndPhotos) {
		this.featureAndPhotos = featureAndPhotos;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getAcutionDate() {
		return acutionDate;
	}
	public String getLastUpdatedTimeString() {
		return lastUpdatedTimeString;
	}
	public void setLastUpdatedTimeString(String lastUpdatedTimeString) {
		this.lastUpdatedTimeString = lastUpdatedTimeString;
	}
	public String getAuctionExpiryTimeString() {
		return auctionExpiryTimeString;
	}
	public void setAuctionExpiryTimeString(String auctionExpiryTimeString) {
		this.auctionExpiryTimeString = auctionExpiryTimeString;
	}
	public String getAcutionDateString() {
		return acutionDateString;
	}
	public void setAcutionDateString(String acutionDateString) {
		this.acutionDateString = acutionDateString;
	}
	public double getBasicBidAmt() {
		return basicBidAmt;
	}
	public void setBasicBidAmt(double basicBidAmt) {
		this.basicBidAmt = basicBidAmt;
	}
	public double getMaxBidAmt() {
		return maxBidAmt;
	}
	public void setMaxBidAmt(double maxBidAmt) {
		this.maxBidAmt = maxBidAmt;
	}
	public void setAcutionDate(long acutionDate) {
		this.acutionDate = acutionDate;
	}
	
	public double getLastBidAmt() {
		return lastBidAmt;
	}
	public void setLastBidAmt(double lastBidAmt) {
		this.lastBidAmt = lastBidAmt;
	}
	public int getNoOfBids() {
		return noOfBids;
	}
	public void setNoOfBids(int noOfBids) {
		this.noOfBids = noOfBids;
	}
	public long getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(long auctionId) {
		this.auctionId = auctionId;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

   
	public int getAuctionStatus() {
		return auctionStatus;
	}
	public void setAuctionStatus(int auctionStatus) {
		this.auctionStatus = auctionStatus;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getItemNetPrice() {
		return itemNetPrice;
	}
	public void setItemNetPrice(double itemNetPrice) {
		this.itemNetPrice = itemNetPrice;
	}
	public long getAuctionExpiryTime() {
		return auctionExpiryTime;
	}
	public void setAuctionExpiryTime(long auctionExpiryTime) {
		this.auctionExpiryTime = auctionExpiryTime;
	}
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	
	
	
}
