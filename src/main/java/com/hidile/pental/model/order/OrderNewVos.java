package com.hidile.pental.model.order;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.hidile.pental.entity.order.Transactions;



public class OrderNewVos
{
	private long orderId;
	private int orderstatus;
	private int cod;
	private int online;
	private long lastUpdatedTime;
	private long promocodeId;
	private int modeId;
	private String description;
	private int status;
	private long operatingOfficerId;
	private long sellerId;
	private long customerId;
	private long deliveryDate;
	private long expectedDeliveryDate;
	private double netPrice;
	private double latitude;
	private double longitude;
	private long numberOfItems;
	private double tax;
	private long shippingId;
	private long  pickUpTime;
	private long transactionId;
	private long companyId;
	private String customerPhone;
	private double totalShippingCharge;
	private String paymentId;
	@NotNull(message="order details is null")
	@NotEmpty(message="order details is empty")
	private List<OrderDetailsVo> orderDetailsVo=new ArrayList<>();
	private Transactions transaction;
	
	private int countryCode;
	private String promocode;
	private long codeId;
	private double minimumAmount;
	private double totalInstallationCharge;
	private long validFrom;
	private long validTo;
	private double flatOffer;
	private long percentage;
	private double promoReportPrice;
	private double flatOffer1;
	private long percentage1;
	private long promorterId;
	
	

	public long getPromorterId() {
		return promorterId;
	}
	public void setPromorterId(long promorterId) {
		this.promorterId = promorterId;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public int getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	public double getTotalInstallationCharge() {
		return totalInstallationCharge;
	}
	public void setTotalInstallationCharge(double totalInstallationCharge) {
		this.totalInstallationCharge = totalInstallationCharge;
	}
	public double getTotalShippingCharge() {
		return totalShippingCharge;
	}
	public void setTotalShippingCharge(double totalShippingCharge) {
		this.totalShippingCharge = totalShippingCharge;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getPromocode() {
		return promocode;
	}
	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}
	public long getCodeId() {
		return codeId;
	}
	public void setCodeId(long codeId) {
		this.codeId = codeId;
	}
	public double getMinimumAmount() {
		return minimumAmount;
	}
	public void setMinimumAmount(double minimumAmount) {
		this.minimumAmount = minimumAmount;
	}
	public long getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(long validFrom) {
		this.validFrom = validFrom;
	}
	public long getValidTo() {
		return validTo;
	}
	public void setValidTo(long validTo) {
		this.validTo = validTo;
	}
	public double getFlatOffer() {
		return flatOffer;
	}
	public void setFlatOffer(double flatOffer) {
		this.flatOffer = flatOffer;
	}
	public long getPercentage() {
		return percentage;
	}
	public void setPercentage(long percentage) {
		this.percentage = percentage;
	}
	public double getPromoReportPrice() {
		return promoReportPrice;
	}
	public void setPromoReportPrice(double promoReportPrice) {
		this.promoReportPrice = promoReportPrice;
	}
	public double getFlatOffer1() {
		return flatOffer1;
	}
	public void setFlatOffer1(double flatOffer1) {
		this.flatOffer1 = flatOffer1;
	}
	public long getPercentage1() {
		return percentage1;
	}
	public void setPercentage1(long percentage1) {
		this.percentage1 = percentage1;
	}
	public List<OrderDetailsVo> getOrderDetailsVo() {
		return orderDetailsVo;
	}
	public void setOrderDetailsVo(List<OrderDetailsVo> orderDetailsVo) {
		this.orderDetailsVo = orderDetailsVo;
	}
	public Transactions getTransaction() {
		return transaction;
	}
	public void setTransaction(Transactions transaction) {
		this.transaction = transaction;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public int getOrderstatus() {
		return orderstatus;
	}
	public void setOrderstatus(int orderstatus) {
		this.orderstatus = orderstatus;
	}
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}
	public int getOnline() {
		return online;
	}
	public void setOnline(int online) {
		this.online = online;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getPromocodeId() {
		return promocodeId;
	}
	public void setPromocodeId(long promocodeId) {
		this.promocodeId = promocodeId;
	}
	public int getModeId() {
		return modeId;
	}
	public void setModeId(int modeId) {
		this.modeId = modeId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}
	public long getSellerId() {
		return sellerId;
	}
	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public long getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(long deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public long getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}
	public void setExpectedDeliveryDate(long expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
	public double getNetPrice() {
		return netPrice;
	}
	public void setNetPrice(double netPrice) {
		this.netPrice = netPrice;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public long getNumberOfItems() {
		return numberOfItems;
	}
	public void setNumberOfItems(long numberOfItems) {
		this.numberOfItems = numberOfItems;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public long getShippingId() {
		return shippingId;
	}
	public void setShippingId(long shippingId) {
		this.shippingId = shippingId;
	}
	public long getPickUpTime() {
		return pickUpTime;
	}
	public void setPickUpTime(long pickUpTime) {
		this.pickUpTime = pickUpTime;
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	
	
}
