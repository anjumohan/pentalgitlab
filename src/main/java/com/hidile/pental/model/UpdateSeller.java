package com.hidile.pental.model;

import com.hidile.pental.constants.Constants;

public class UpdateSeller implements Constants 
{

	public String sellerName;
	public String sellerEmail;
	public String sellerPhoneNo;
	public String sellerAddress;
	public String sellerWebAddress;
	public long sellerId;
	public int sellerStatus;
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public String getSellerEmail() {
		return sellerEmail;
	}
	public void setSellerEmail(String sellerEmail) {
		this.sellerEmail = sellerEmail;
	}
	public String getSellerPhoneNo() {
		return sellerPhoneNo;
	}
	public void setSellerPhoneNo(String sellerPhoneNo) {
		this.sellerPhoneNo = sellerPhoneNo;
	}
	public String getSellerAddress() {
		return sellerAddress;
	}
	public void setSellerAddress(String sellerAddress) {
		this.sellerAddress = sellerAddress;
	}
	public String getSellerWebAddress() {
		return sellerWebAddress;
	}
	public void setSellerWebAddress(String sellerWebAddress) {
		this.sellerWebAddress = sellerWebAddress;
	}
	public long getSellerId() {
		return sellerId;
	}
	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}
	public int getSellerStatus() {
		return sellerStatus;
	}
	public void setSellerStatus(int sellerStatus) {
		this.sellerStatus = sellerStatus;
	}
	
	
}
