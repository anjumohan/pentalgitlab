package com.hidile.pental.model.account;

import javax.persistence.Column;

public class ExpenseVo {
	
	private long expenseId;
	private int business_type;
	private long last_updated_time;
	private int ispayable;
	private long cmd;
	private int status;
	private long operatingOfficer;
	private double amount;
	private long transactionId;
	private long incomExpnsHeadId;
	private long entered_date;
	private long bill_no;
	private long date_expense;
	private int tran_status;
	private long userId;
	
	
	private int transactionMode;
	private long toWhoId;
	private long lastUpdatedTime;
	private String lastUpdatedTimeString;
	private String reciptNo;
	private String note;
	private double netAmount;
	private int inOrOut;
	private double discount;
	private int isCleared;
	private long pairedId;
	private int bankOrCash;
	private String tranMode;
	private String cashOrBank;
	private double monthlyTotal;
	private long monthyDate;
	private long time;
	private String month;
	private long productId;
	private String productName;
	private long sumOfOrderId;
	private double totalNetAmount;
	private long categoryId;
	private String categoryName;
	private long subCategoryId;
	private String subCategoryName;
	private long customerId;
	private String customerName;
	private String categoryNameAr;
	private String subCategoryNameAr;
	private String customerNameAr;
	private String productNameAr;
	private long sellerId;
	private String incomeExpenseName;
	
	
	
	public String getIncomeExpenseName() {
		return incomeExpenseName;
	}
	public void setIncomeExpenseName(String incomeExpenseName) {
		this.incomeExpenseName = incomeExpenseName;
	}
	public long getExpenseId() {
		return expenseId;
	}
	public void setExpenseId(long expenseId) {
		this.expenseId = expenseId;
	}
	public int getBusiness_type() {
		return business_type;
	}
	public void setBusiness_type(int business_type) {
		this.business_type = business_type;
	}
	public long getLast_updated_time() {
		return last_updated_time;
	}
	public void setLast_updated_time(long last_updated_time) {
		this.last_updated_time = last_updated_time;
	}
	public int getIspayable() {
		return ispayable;
	}
	public void setIspayable(int ispayable) {
		this.ispayable = ispayable;
	}
	public long getCmd() {
		return cmd;
	}
	public void setCmd(long cmd) {
		this.cmd = cmd;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getOperatingOfficer() {
		return operatingOfficer;
	}
	public void setOperatingOfficer(long operatingOfficer) {
		this.operatingOfficer = operatingOfficer;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public long getIncomExpnsHeadId() {
		return incomExpnsHeadId;
	}
	public void setIncomExpnsHeadId(long incomExpnsHeadId) {
		this.incomExpnsHeadId = incomExpnsHeadId;
	}
	public long getEntered_date() {
		return entered_date;
	}
	public void setEntered_date(long entered_date) {
		this.entered_date = entered_date;
	}
	public long getBill_no() {
		return bill_no;
	}
	public void setBill_no(long bill_no) {
		this.bill_no = bill_no;
	}
	public long getDate_expense() {
		return date_expense;
	}
	public void setDate_expense(long date_expense) {
		this.date_expense = date_expense;
	}
	public int getTran_status() {
		return tran_status;
	}
	public void setTran_status(int tran_status) {
		this.tran_status = tran_status;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getTransactionMode() {
		return transactionMode;
	}
	public void setTransactionMode(int transactionMode) {
		this.transactionMode = transactionMode;
	}
	public long getToWhoId() {
		return toWhoId;
	}
	public void setToWhoId(long toWhoId) {
		this.toWhoId = toWhoId;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public String getLastUpdatedTimeString() {
		return lastUpdatedTimeString;
	}
	public void setLastUpdatedTimeString(String lastUpdatedTimeString) {
		this.lastUpdatedTimeString = lastUpdatedTimeString;
	}
	public String getReciptNo() {
		return reciptNo;
	}
	public void setReciptNo(String reciptNo) {
		this.reciptNo = reciptNo;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public double getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}
	public int getInOrOut() {
		return inOrOut;
	}
	public void setInOrOut(int inOrOut) {
		this.inOrOut = inOrOut;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public int getIsCleared() {
		return isCleared;
	}
	public void setIsCleared(int isCleared) {
		this.isCleared = isCleared;
	}
	public long getPairedId() {
		return pairedId;
	}
	public void setPairedId(long pairedId) {
		this.pairedId = pairedId;
	}
	public int getBankOrCash() {
		return bankOrCash;
	}
	public void setBankOrCash(int bankOrCash) {
		this.bankOrCash = bankOrCash;
	}
	public String getTranMode() {
		return tranMode;
	}
	public void setTranMode(String tranMode) {
		this.tranMode = tranMode;
	}
	public String getCashOrBank() {
		return cashOrBank;
	}
	public void setCashOrBank(String cashOrBank) {
		this.cashOrBank = cashOrBank;
	}
	public double getMonthlyTotal() {
		return monthlyTotal;
	}
	public void setMonthlyTotal(double monthlyTotal) {
		this.monthlyTotal = monthlyTotal;
	}
	public long getMonthyDate() {
		return monthyDate;
	}
	public void setMonthyDate(long monthyDate) {
		this.monthyDate = monthyDate;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public long getSumOfOrderId() {
		return sumOfOrderId;
	}
	public void setSumOfOrderId(long sumOfOrderId) {
		this.sumOfOrderId = sumOfOrderId;
	}
	public double getTotalNetAmount() {
		return totalNetAmount;
	}
	public void setTotalNetAmount(double totalNetAmount) {
		this.totalNetAmount = totalNetAmount;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public long getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCategoryNameAr() {
		return categoryNameAr;
	}
	public void setCategoryNameAr(String categoryNameAr) {
		this.categoryNameAr = categoryNameAr;
	}
	public String getSubCategoryNameAr() {
		return subCategoryNameAr;
	}
	public void setSubCategoryNameAr(String subCategoryNameAr) {
		this.subCategoryNameAr = subCategoryNameAr;
	}
	public String getCustomerNameAr() {
		return customerNameAr;
	}
	public void setCustomerNameAr(String customerNameAr) {
		this.customerNameAr = customerNameAr;
	}
	public String getProductNameAr() {
		return productNameAr;
	}
	public void setProductNameAr(String productNameAr) {
		this.productNameAr = productNameAr;
	}
	public long getSellerId() {
		return sellerId;
	}
	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}
	
	
	
	
	

}
