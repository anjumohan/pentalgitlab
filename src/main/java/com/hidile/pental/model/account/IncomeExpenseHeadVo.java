package com.hidile.pental.model.account;

import javax.persistence.Column;

public class IncomeExpenseHeadVo {
	
	private long incomExpnsHeadId;
	private String incomeExpenseName;
	private int business_type;
	private long last_updated_time;
	private long operatingOfficer;
	private String year;
	private int incomeOrExpense;
	private int status;
	private String date;
	
	
	
	
	public long getIncomExpnsHeadId() {
		return incomExpnsHeadId;
	}
	public void setIncomExpnsHeadId(long incomExpnsHeadId) {
		this.incomExpnsHeadId = incomExpnsHeadId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getIncomeExpenseName() {
		return incomeExpenseName;
	}
	public void setIncomeExpenseName(String incomeExpenseName) {
		this.incomeExpenseName = incomeExpenseName;
	}
	public int getBusiness_type() {
		return business_type;
	}
	public void setBusiness_type(int business_type) {
		this.business_type = business_type;
	}
	public long getLast_updated_time() {
		return last_updated_time;
	}
	public void setLast_updated_time(long last_updated_time) {
		this.last_updated_time = last_updated_time;
	}
	public long getOperatingOfficer() {
		return operatingOfficer;
	}
	public void setOperatingOfficer(long operatingOfficer) {
		this.operatingOfficer = operatingOfficer;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public int getIncomeOrExpense() {
		return incomeOrExpense;
	}
	public void setIncomeOrExpense(int incomeOrExpense) {
		this.incomeOrExpense = incomeOrExpense;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	

}
