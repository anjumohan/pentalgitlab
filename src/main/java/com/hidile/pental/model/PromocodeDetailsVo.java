package com.hidile.pental.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import com.hidile.pental.constants.Constants;
public class PromocodeDetailsVo implements Constants
{

	private long codeId;
	private long userId;
	private String promocode;
	private double flatOffer;
	private long percentage;
	private long validFrom;
	private long validTo;
	private int status;
	private double minimumAmount;
	 
	 
	 
	 
	public double getMinimumAmount() {
		return minimumAmount;
	}
	public void setMinimumAmount(double minimumAmount) {
		this.minimumAmount = minimumAmount;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(long validFrom) {
		this.validFrom = validFrom;
	}
	public long getValidTo() {
		return validTo;
	}
	public void setValidTo(long validTo) {
		this.validTo = validTo;
	}
	public long getCodeId() {
		return codeId;
	}
	public void setCodeId(long codeId) {
		this.codeId = codeId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getPromocode() {
		return promocode;
	}
	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}
	public double getFlatOffer() {
		return flatOffer;
	}
	public void setFlatOffer(double flatOffer) {
		this.flatOffer = flatOffer;
	}
	public long getPercentage() {
		return percentage;
	}
	public void setPercentage(long percentage) {
		this.percentage = percentage;
	}

	
	
	
	
	
	
}
