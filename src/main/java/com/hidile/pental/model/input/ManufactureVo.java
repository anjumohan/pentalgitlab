package com.hidile.pental.model.input;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ManufactureVo {
	
	private long manufacturerId;
	private String manufacturerName;
	private String businessType;
	private long lastUpdatedTime;
	private long operatingOfficer;
	private long batchProcessingId;
	private int status;
	@NotNull(message="row per page is null")
	@Min(value=1,message="minimum row per page should be 1")
	private int rowPerPage;
	@NotNull(message="row per page is null")
	@Min(value=0,message="minimum current index should be 0")
	private int currentIndex;
	public long companyId;
	private String manufacturerNameAr;
	private String imageName;
	
	
	
	
	
	
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getManufacturerNameAr() {
		return manufacturerNameAr;
	}
	public void setManufacturerNameAr(String manufacturerNameAr) {
		this.manufacturerNameAr = manufacturerNameAr;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	public long getManufacturerId() {
		return manufacturerId;
	}
	public void setManufacturerId(long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	public String getManufacturerName() {
		return manufacturerName;
	}
	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}
	
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getOperatingOfficer() {
		return operatingOfficer;
	}
	public void setOperatingOfficer(long operatingOfficer) {
		this.operatingOfficer = operatingOfficer;
	}
	public long getBatchProcessingId() {
		return batchProcessingId;
	}
	public void setBatchProcessingId(long batchProcessingId) {
		this.batchProcessingId = batchProcessingId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	

}
