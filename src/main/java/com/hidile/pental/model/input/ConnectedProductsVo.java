package com.hidile.pental.model.input;

public class ConnectedProductsVo {
	private long connectedProductsId;
	private long productId;
	private int modeId;
	private long relatedProductId;
	
	private long lastUpdatedTime;
	public long getConnectedProductsId() {
		return connectedProductsId;
	}
	public void setConnectedProductsId(long connectedProductsId) {
		this.connectedProductsId = connectedProductsId;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public int getModeId() {
		return modeId;
	}
	public void setModeId(int modeId) {
		this.modeId = modeId;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getRelatedProductId() {
		return relatedProductId;
	}
	public void setRelatedProductId(long relatedProductId) {
		this.relatedProductId = relatedProductId;
	}
	
	
	

}
