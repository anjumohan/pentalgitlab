package com.hidile.pental.model.input;

public class AttributeVo 
{
	private long attributeId;
	private String attributeName;
	private String attributeNameAr;
	private long createdDate;
	private long operatingOfficerId;
	private int status;
	
	
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}
	public long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(long attributeId) {
		this.attributeId = attributeId;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public String getAttributeNameAr() {
		return attributeNameAr;
	}
	public void setAttributeNameAr(String attributeNameAr) {
		this.attributeNameAr = attributeNameAr;
	}
	public long getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
