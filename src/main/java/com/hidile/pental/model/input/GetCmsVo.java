package com.hidile.pental.model.input;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class GetCmsVo {
	private long cmsId;
	private String termsAndConditions;
	private String aboutUs;
	private String faqs;
	private String privacyPolicy;
	private String contactUs;
	@NotNull(message="row per page is null")
	@Min(value=1,message="minimum row per page should be 1")
	private int rowPerPage;
	@NotNull(message="row per page is null")
	@Min(value=0,message="minimum current index should be 0")
	private int currentIndex;
	private String termsAndConditionsAr;
	private String faqsAr;
	private String aboutUsAr;
	private String privacyPolicyAr;
	private String contactUsAr;
	
	
	
	
	
	
	public String getTermsAndConditionsAr() {
		return termsAndConditionsAr;
	}
	public void setTermsAndConditionsAr(String termsAndConditionsAr) {
		this.termsAndConditionsAr = termsAndConditionsAr;
	}
	public String getFaqsAr() {
		return faqsAr;
	}
	public void setFaqsAr(String faqsAr) {
		this.faqsAr = faqsAr;
	}
	public String getAboutUsAr() {
		return aboutUsAr;
	}
	public void setAboutUsAr(String aboutUsAr) {
		this.aboutUsAr = aboutUsAr;
	}
	public String getPrivacyPolicyAr() {
		return privacyPolicyAr;
	}
	public void setPrivacyPolicyAr(String privacyPolicyAr) {
		this.privacyPolicyAr = privacyPolicyAr;
	}
	public String getContactUsAr() {
		return contactUsAr;
	}
	public void setContactUsAr(String contactUsAr) {
		this.contactUsAr = contactUsAr;
	}
	public long getCmsId() {
		return cmsId;
	}
	public void setCmsId(long cmsId) {
		this.cmsId = cmsId;
	}
	public String getTermsAndConditions() {
		return termsAndConditions;
	}
	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}
	public String getAboutUs() {
		return aboutUs;
	}
	public void setAboutUs(String aboutUs) {
		this.aboutUs = aboutUs;
	}
	public String getFaqs() {
		return faqs;
	}
	public void setFaqs(String faqs) {
		this.faqs = faqs;
	}
	public String getPrivacyPolicy() {
		return privacyPolicy;
	}
	public void setPrivacyPolicy(String privacyPolicy) {
		this.privacyPolicy = privacyPolicy;
	}
	public String getContactUs() {
		return contactUs;
	}
	public void setContactUs(String contactUs) {
		this.contactUs = contactUs;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	
	
	
}
