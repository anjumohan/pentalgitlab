package com.hidile.pental.model.input;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class CountryVo 
{

	private long countryId;
	@NotNull(message="country is null")
	private String countryName;
    @Min(value=0,message="minimum value of country status is 0")
    @Max(value=1,message="maximum value of country status is 1")
	private int countryStatus;
    @NotNull(message="country flag should be in string")
    @NotEmpty(message="country flag is empty'")
	private String countryFlag;
    private String lastUpdatedTimeString;
	@NotNull(message="currency is null")
    private String currency;
	private String countryNameAr;
	private int countryCode;
	
	
	public int getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	public String getCountryNameAr() {
		return countryNameAr;
	}
	public void setCountryNameAr(String countryNameAr) {
		this.countryNameAr = countryNameAr;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public int getCountryStatus() {
		return countryStatus;
	}
	public void setCountryStatus(int countryStatus) {
		this.countryStatus = countryStatus;
	}
	public String getCountryFlag() {
		return countryFlag;
	}
	public void setCountryFlag(String countryFlag) {
		this.countryFlag = countryFlag;
	}
	public String getLastUpdatedTimeString() {
		return lastUpdatedTimeString;
	}
	public void setLastUpdatedTimeString(String lastUpdatedTimeString) {
		this.lastUpdatedTimeString = lastUpdatedTimeString;
	}
    
    
    
	
	
	
}
