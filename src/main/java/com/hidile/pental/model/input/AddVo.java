package com.hidile.pental.model.input;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

public class AddVo {

	private long addId;
	private long lastUpdatedTime;
	private long operatingOfficerId;
	private int addStatus;
	private int status;
	@Min(value=1,message="expiry date should be greater than zero")
	private long addExpirtyDate;
	@NotEmpty(message="image names should not be empty")
	private String imageName="";

	private long sellerId;
	private String titile;
	private String subTitile;
	private String link;
	private String text;
	private long companyId;
	private String textAr;
	private String subTitileAr;
	private String titileAr;
	private long modeId;
	private double height;
	private long arabicOrEnglish;
	private int rank;
	
	
	
	



	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public long getArabicOrEnglish() {
		return arabicOrEnglish;
	}

	public void setArabicOrEnglish(long arabicOrEnglish) {
		this.arabicOrEnglish = arabicOrEnglish;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public long getModeId() {
		return modeId;
	}

	public void setModeId(long modeId) {
		this.modeId = modeId;
	}

	public String getTextAr() {
		return textAr;
	}

	public void setTextAr(String textAr) {
		this.textAr = textAr;
	}

	public String getSubTitileAr() {
		return subTitileAr;
	}

	public void setSubTitileAr(String subTitileAr) {
		this.subTitileAr = subTitileAr;
	}

	public String getTitileAr() {
		return titileAr;
	}

	public void setTitileAr(String titileAr) {
		this.titileAr = titileAr;
	}

	public long getSellerId() {
		return sellerId;
	}

	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}

	public String getTitile() {
		return titile;
	}

	public void setTitile(String titile) {
		this.titile = titile;
	}

	public String getSubTitile() {
		return subTitile;
	}

	public void setSubTitile(String subTitile) {
		this.subTitile = subTitile;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getAddId() {
		return addId;
	}

	public void setAddId(long addId) {
		this.addId = addId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public int getAddStatus() {
		return addStatus;
	}

	public void setAddStatus(int addStatus) {
		this.addStatus = addStatus;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getAddExpirtyDate() {
		return addExpirtyDate;
	}

	public void setAddExpirtyDate(long addExpirtyDate) {
		this.addExpirtyDate = addExpirtyDate;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}


	
	

}
