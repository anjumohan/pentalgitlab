package com.hidile.pental.model.input;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class ProductVo {


	private String productName;

	@NotEmpty(message = "product name is empty")
	private String productNameAr="";
	@NotNull
	@Min(value=1,message="category id is zero or less")
	private long categoryId;
	@NotNull
	private long subCategoryId;
	private int itemDimeansion;
	private long ratingPercentage;
	private double offerRatePercentage;
	private int typeOfFeature;
	private double maximumPromoOffer;
	@NotNull(message="description arabic is null")
	@NotEmpty(message = "description arabic is empty")
	private String descriptionAr="";
	@NotNull
	@Min(value=1,message="price value is zero or less")
	private double price;
	private int status;
	@NotNull
	@Min(value=1,message="seller id is zero or less")
	private long sellerId;
	private String sellerName;
	private String sellerNameAr;
	private long totalSaledItems;
	private String sellerAddress;
	@Min(value=0,message="price value should be minimum zero")

	private long productId;
	private String returnPolicy;
	private String returnPolicyAr;
	private String description;
    private long shares;
	private double rating;
	private double taxAmount;
	private long numberOfItems;
	private long numberOfItemsRemaining;
	private long productOrShop;
	private String createdDateString;
	private long createdDate;
	private String lastUpdatedTimeString;
	private long lastUpdatedTime;
	private int delivaryStatus;
	private long userId;
	private String categoryName;
	private String subcategoryName;
	private String categoryNameAr;
	private String subcategoryNameAr;
	private double totalAmount;
	private long mainCateogoryId;
	private String mainCateogoryName;
	@Min(value=1,message="product quantity is zero")
	private long productQuantity;
	private long noOfReviews;
	@NotNull(message="features and photos are null")
	@NotEmpty(message="features or photos is empty")
	@Valid
	private List<FeatureVo> featureOrPhotos=new ArrayList<>();
	/*@NotNull(message="ConnectedProductsVo are null")
	@NotEmpty(message="ConnectedProductsVo is empty")
	@Valid*/
	private List<ConnectedProductsVo> connectedProductsVo=new ArrayList<>();
	
	private List<OfferDetailsVo> offerDetailsVo=new ArrayList<>();
	private String manufacturerName;
	private String manufacturerNameAr;
	private String businessType;
	private long taxId;
	private String phone;
	private String city;
	private String country;
	private long offerId;
	@NotNull(message="out of stock should not be null")
	private int isOutOfStock;
	public long companyId;
	private int  serviceOrProduct;
    private List<ReviewVo> reviewVos;
    private String imageName;
	private double shippingCharge;
	private String cityName;
	private String countryName;
	private double latitude;
	private double longitude;
	private String contactName;
	private String telephoneNo;
	@NotEmpty(message="product attribute is empty")
	private List<ProductAttributeVo> productAttributeVos=new ArrayList<>();
	private long manufacturerId;
	private String cityNameAr;
	private String countryNameAr;
	private String offerName;
	private String offerNameAr;
    private String offerStarts;
	private String offerEnds;
	private double offerRate;
	private double offerPercentage;
	private String offerImage;	
	private String offerDescription;	
	private String offerDescriptionAr;
	private List<OfferVo> offerVos=new ArrayList<>();
	private double totalOfferAmount;
	private double sellingPrice;
	private double purchasePrice;
	private double totalAmountIncludingShipping;
	private double taxPercentage;
	private String wrranty;
	private double installationCharge;
	private String wrrantyAr;
	private double offerByPromocode;
    private double offerToPromoter;
	private int featured;
	private int rank;
	private double commission;
	
	
	
	
	
	public double getMaximumPromoOffer() {
		return maximumPromoOffer;
	}

	public void setMaximumPromoOffer(double maximumPromoOffer) {
		this.maximumPromoOffer = maximumPromoOffer;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getFeatured() {
		return featured;
	}

	public void setFeatured(int featured) {
		this.featured = featured;
	}

	public double getOfferByPromocode() {
		return offerByPromocode;
	}

	public void setOfferByPromocode(double offerByPromocode) {
		this.offerByPromocode = offerByPromocode;
	}

	public double getOfferToPromoter() {
		return offerToPromoter;
	}

	public void setOfferToPromoter(double offerToPromoter) {
		this.offerToPromoter = offerToPromoter;
	}

	public String getWrrantyAr() {
		return wrrantyAr;
	}

	public void setWrrantyAr(String wrrantyAr) {
		this.wrrantyAr = wrrantyAr;
	}

	public double getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(double installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getWrranty() {
		return wrranty;
	}

	public void setWrranty(String wrranty) {
		this.wrranty = wrranty;
	}

	public double getTaxPercentage() {
		return taxPercentage;
	}

	public void setTaxPercentage(double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}

	public double getTotalAmountIncludingShipping() {
		return totalAmountIncludingShipping;
	}

	public void setTotalAmountIncludingShipping(double totalAmountIncludingShipping) {
		this.totalAmountIncludingShipping = totalAmountIncludingShipping;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}



	public double getOfferRatePercentage() {
		return offerRatePercentage;
	}

	public void setOfferRatePercentage(double offerRatePercentage) {
		this.offerRatePercentage = offerRatePercentage;
	}

	public String getSellerNameAr() {
		return sellerNameAr;
	}

	public void setSellerNameAr(String sellerNameAr) {
		this.sellerNameAr = sellerNameAr;
	}

	public long getRatingPercentage() {
		return ratingPercentage;
	}

	public void setRatingPercentage(long ratingPercentage) {
		this.ratingPercentage = ratingPercentage;
	}

	public long getTotalSaledItems() {
		return totalSaledItems;
	}

	public void setTotalSaledItems(long totalSaledItems) {
		this.totalSaledItems = totalSaledItems;
	}

	public double getTotalOfferAmount() {
		return totalOfferAmount;
	}

	public void setTotalOfferAmount(double totalOfferAmount) {
		this.totalOfferAmount = totalOfferAmount;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public List<OfferVo> getOfferVos() {
		return offerVos;
	}

	public void setOfferVos(List<OfferVo> offerVos) {
		this.offerVos = offerVos;
	}

	public String getManufacturerNameAr() {
		return manufacturerNameAr;
	}

	public void setManufacturerNameAr(String manufacturerNameAr) {
		this.manufacturerNameAr = manufacturerNameAr;
	}

	public String getOfferImage() {
		return offerImage;
	}

	public void setOfferImage(String offerImage) {
		this.offerImage = offerImage;
	}

	public String getOfferDescription() {
		return offerDescription;
	}

	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}

	public String getOfferDescriptionAr() {
		return offerDescriptionAr;
	}

	public void setOfferDescriptionAr(String offerDescriptionAr) {
		this.offerDescriptionAr = offerDescriptionAr;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getOfferNameAr() {
		return offerNameAr;
	}

	public void setOfferNameAr(String offerNameAr) {
		this.offerNameAr = offerNameAr;
	}

	public String getOfferStarts() {
		return offerStarts;
	}

	public void setOfferStarts(String offerStarts) {
		this.offerStarts = offerStarts;
	}

	public String getOfferEnds() {
		return offerEnds;
	}

	public void setOfferEnds(String offerEnds) {
		this.offerEnds = offerEnds;
	}

	public double getOfferRate() {
		return offerRate;
	}

	public void setOfferRate(double offerRate) {
		this.offerRate = offerRate;
	}

	

	public double getOfferPercentage() {
		return offerPercentage;
	}

	public void setOfferPercentage(double offerPercentage) {
		this.offerPercentage = offerPercentage;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getCategoryNameAr() {
		return categoryNameAr;
	}

	public void setCategoryNameAr(String categoryNameAr) {
		this.categoryNameAr = categoryNameAr;
	}

	public String getSubcategoryNameAr() {
		return subcategoryNameAr;
	}

	public void setSubcategoryNameAr(String subcategoryNameAr) {
		this.subcategoryNameAr = subcategoryNameAr;
	}

	public String getCityNameAr() {
		return cityNameAr;
	}

	public void setCityNameAr(String cityNameAr) {
		this.cityNameAr = cityNameAr;
	}

	public String getCountryNameAr() {
		return countryNameAr;
	}

	public void setCountryNameAr(String countryNameAr) {
		this.countryNameAr = countryNameAr;
	}

	public long getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public List<OfferDetailsVo> getOfferDetailsVo() {
		return offerDetailsVo;
	}

	public void setOfferDetailsVo(List<OfferDetailsVo> offerDetailsVo) {
		this.offerDetailsVo = offerDetailsVo;
	}

	public String getReturnPolicy() {
		return returnPolicy;
	}

	public void setReturnPolicy(String returnPolicy) {
		this.returnPolicy = returnPolicy;
	}

	public String getReturnPolicyAr() {
		return returnPolicyAr;
	}

	public void setReturnPolicyAr(String returnPolicyAr) {
		this.returnPolicyAr = returnPolicyAr;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public List<ProductAttributeVo> getProductAttributeVos() {
		return productAttributeVos;
	}

	public void setProductAttributeVos(List<ProductAttributeVo> productAttributeVos) {
		this.productAttributeVos = productAttributeVos;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getTelephoneNo() {
		return telephoneNo;
	}

	public void setTelephoneNo(String telephoneNo) {
		this.telephoneNo = telephoneNo;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getSellerAddress() {
		return sellerAddress;
	}

	public void setSellerAddress(String sellerAddress) {
		this.sellerAddress = sellerAddress;
	}

	public double getShippingCharge() {
		return shippingCharge;
	}

	public void setShippingCharge(double shippingCharge) {
		this.shippingCharge = shippingCharge;
	}

	public long getTaxId() {
		return taxId;
	}

	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}

	public List<ConnectedProductsVo> getConnectedProductsVo() {
		return connectedProductsVo;
	}

	
	
	
	public int getItemDimeansion() {
		return itemDimeansion;
	}

	public void setItemDimeansion(int itemDimeansion) {
		this.itemDimeansion = itemDimeansion;
	}

	public int getTypeOfFeature() {
		return typeOfFeature;
	}

	public void setTypeOfFeature(int typeOfFeature) {
		this.typeOfFeature = typeOfFeature;
	}

	public void setConnectedProductsVo(List<ConnectedProductsVo> connectedProductsVo) {
		this.connectedProductsVo = connectedProductsVo;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<ReviewVo> getReviewVos() {
		return reviewVos;
	}

	public void setReviewVos(List<ReviewVo> reviewVos) {
		this.reviewVos = reviewVos;
	}

	public int getServiceOrProduct() {
		return serviceOrProduct;
	}

	public void setServiceOrProduct(int serviceOrProduct) {
		this.serviceOrProduct = serviceOrProduct;
	}

	public long getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(long numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public long getNumberOfItemsRemaining() {
		return numberOfItemsRemaining;
	}

	public void setNumberOfItemsRemaining(long numberOfItemsRemaining) {
		this.numberOfItemsRemaining = numberOfItemsRemaining;
	}

	

	
	
	
	public long getSellerId() {
		return sellerId;
	}

	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}


	
	

	public String getProductNameAr() {
		return productNameAr;
	}

	public void setProductNameAr(String productNameAr) {
		this.productNameAr = productNameAr;
	}

	public String getDescriptionAr() {
		return descriptionAr;
	}

	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr = descriptionAr;
	}

	public int getIsOutOfStock() {
		return isOutOfStock;
	}

	public void setIsOutOfStock(int isOutOfStock) {
		this.isOutOfStock = isOutOfStock;
	}

	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getCategoryId() {
		return categoryId;
	}
	
	

	public long getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(long productQuantity) {
		this.productQuantity = productQuantity;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public String getCreatedDateString() {
		return createdDateString;
	}

	public void setCreatedDateString(String createdDateString) {
		this.createdDateString = createdDateString;
	}

	public String getLastUpdatedTimeString() {
		return lastUpdatedTimeString;
	}

	public void setLastUpdatedTimeString(String lastUpdatedTimeString) {
		this.lastUpdatedTimeString = lastUpdatedTimeString;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public long getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}



	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getShares() {
		return shares;
	}

	public void setShares(long shares) {
		this.shares = shares;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public long getProductOrShop() {
		return productOrShop;
	}

	public void setProductOrShop(long productOrShop) {
		this.productOrShop = productOrShop;
	}


	public int getDelivaryStatus() {
		return delivaryStatus;
	}

	public void setDelivaryStatus(int delivaryStatus) {
		this.delivaryStatus = delivaryStatus;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSubcategoryName() {
		return subcategoryName;
	}

	public void setSubcategoryName(String subcategoryName) {
		this.subcategoryName = subcategoryName;
	}

	public long getMainCateogoryId() {
		return mainCateogoryId;
	}

	public void setMainCateogoryId(long mainCateogoryId) {
		this.mainCateogoryId = mainCateogoryId;
	}

	public String getMainCateogoryName() {
		return mainCateogoryName;
	}

	public void setMainCateogoryName(String mainCateogoryName) {
		this.mainCateogoryName = mainCateogoryName;
	}

	public long getNoOfReviews() {
		return noOfReviews;
	}

	public void setNoOfReviews(long noOfReviews) {
		this.noOfReviews = noOfReviews;
	}

	public List<FeatureVo> getFeatureOrPhotos() {
		return featureOrPhotos;
	}

	public void setFeatureOrPhotos(List<FeatureVo> featureOrPhotos) {
		this.featureOrPhotos = featureOrPhotos;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}


}