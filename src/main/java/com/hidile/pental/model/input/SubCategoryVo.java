package com.hidile.pental.model.input;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;


import org.hibernate.validator.constraints.NotEmpty;

import com.hidile.pental.constants.Constants;



public class SubCategoryVo implements Constants
{
	
	private long subCategoryId;
	@NotNull(message="subcategory name is null")
	@NotEmpty(message="subcategory name is empty")
	//@Pattern(regexp=REGEX_NAME,message="name validation format failed")
	private String subCategoryName="";
	@NotNull(message="subcategory name arabic is null")
	@NotEmpty(message="subcategory name arabic is empty")
	private String subCategoryNameAr="";
	@Max(value=1,message="maximum value for status is 1")
	private int status;
	private long categoryId;
	@NotNull(message="image name is null")
	@NotEmpty(message="image name is empty")
	private String imageName="";
	private long createdDate;
	private String lastUpdatedTimeString="";
	private long lastUpdatedTime;
	private String createdDateString="";
	@NotNull(message="description is null")
	@NotEmpty(message="description is empty")
	private String discription="";
	private String catName;
	private String catNameAr;
	private String iconImage;
	public long companyId;
	public long userId;
	private String discriptionAr;
	
	
	public String getCatNameAr() {
		return catNameAr;
	}
	public void setCatNameAr(String catNameAr) {
		this.catNameAr = catNameAr;
	}
	public String getDiscriptionAr() {
		return discriptionAr;
	}
	public void setDiscriptionAr(String discriptionAr) {
		this.discriptionAr = discriptionAr;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getIconImage() {
		return iconImage;
	}
	public void setIconImage(String iconImage) {
		this.iconImage = iconImage;
	}
	public long getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getSubCategoryNameAr() {
		return subCategoryNameAr;
	}
	public void setSubCategoryNameAr(String subCategoryNameAr) {
		this.subCategoryNameAr = subCategoryNameAr;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public long getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}
	public String getLastUpdatedTimeString() {
		return lastUpdatedTimeString;
	}
	public void setLastUpdatedTimeString(String lastUpdatedTimeString) {
		this.lastUpdatedTimeString = lastUpdatedTimeString;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public String getCreatedDateString() {
		return createdDateString;
	}
	public void setCreatedDateString(String createdDateString) {
		this.createdDateString = createdDateString;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	
	

	
	
	
	
	

}
