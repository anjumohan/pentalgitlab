package com.hidile.pental.model.input;

import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class SearchVo {
	@NotNull(message="")
	public Map<Integer, String> data = new HashMap<Integer, String>();
	public int mode;
	@Min(value=0,message="")
	public int currentIndex;
	@Min(value=1,message="")
	public int rowPerPage;
	@Min(value=0,message="")
	public double latitude;
	@Min(value=0,message="")
	public double longitude;

	public Map<Integer, String> getData() {
		return data;
	}

	public void setData(Map<Integer, String> data) {
		this.data = data;
	}
	
	

	public int getMode() {
		return mode;
	}

	
	public void setMode(int mode) {
		this.mode = mode;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	public int getRowPerPage() {
		return rowPerPage;
	}

	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "SearchVo [data=" + data + ", mode=" + mode + ", firstResult=" + currentIndex + ", rowPerPage="
				+ rowPerPage + "]";
	}

}
