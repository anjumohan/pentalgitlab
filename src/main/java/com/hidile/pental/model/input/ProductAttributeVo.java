package com.hidile.pental.model.input;

public class ProductAttributeVo {
	private long productAttributeId;
	private long productId;
	private long attributeId;
	private int status;
	private String proudctName;
	private String attributeName;
	private String attributeNameAr;
	private String value;
	private String valueAr;
	private long lastUpdatedTime;
	
	
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public String getValueAr() {
		return valueAr;
	}
	public void setValueAr(String valueAr) {
		this.valueAr = valueAr;
	}
	public long getProductAttributeId() {
		return productAttributeId;
	}
	public void setProductAttributeId(long productAttributeId) {
		this.productAttributeId = productAttributeId;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(long attributeId) {
		this.attributeId = attributeId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getProudctName() {
		return proudctName;
	}
	public void setProudctName(String proudctName) {
		this.proudctName = proudctName;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public String getAttributeNameAr() {
		return attributeNameAr;
	}
	public void setAttributeNameAr(String attributeNameAr) {
		this.attributeNameAr = attributeNameAr;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
