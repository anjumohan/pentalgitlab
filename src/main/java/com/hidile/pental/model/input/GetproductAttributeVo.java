package com.hidile.pental.model.input;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class GetproductAttributeVo {
	private long productAttributeId;
	private long attributeId;
	private long productId;

	private int status;
	
	private String proudctName;
	private String attributeName;
	private String attributeNameAr;
	private long createdDate;
	private String value;
	private String valueAr;
	@NotNull(message="row per page is null")
	@Min(value=1,message="minimum row per page should be 1")
	private int rowPerPage;
	@NotNull(message="row per page is null")
	@Min(value=0,message="minimum current index should be 0")
	private int currentIndex;
	
	
	
	public long getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}
	public long getProductAttributeId() {
		return productAttributeId;
	}
	public void setProductAttributeId(long productAttributeId) {
		this.productAttributeId = productAttributeId;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(long attributeId) {
		this.attributeId = attributeId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getProudctName() {
		return proudctName;
	}
	public void setProudctName(String proudctName) {
		this.proudctName = proudctName;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public String getAttributeNameAr() {
		return attributeNameAr;
	}
	public void setAttributeNameAr(String attributeNameAr) {
		this.attributeNameAr = attributeNameAr;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getValueAr() {
		return valueAr;
	}
	public void setValueAr(String valueAr) {
		this.valueAr = valueAr;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	
	
	
}
