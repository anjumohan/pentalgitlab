package com.hidile.pental.model.input;

public class ReturnItemsVo 
{
	private long returnItemId;	
	private long orderListId;
	private String returnReason;
	private String comments;
	private long transactionId;
	private long storeId;
	private long itemQuantity;
	private double itemNetPrice;
	private int status;
	private long lastUpdatedTime;
	private long operatingOfficerId;
	private String operatingOfficerName;
	private long customerId;
	private String storeName;
	private long companyId;
	private long orderId;
	public String customerName="";
	public String custEmail="";
	public String phoneNo="";
	
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public long getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(long itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public double getItemNetPrice() {
		return itemNetPrice;
	}
	public void setItemNetPrice(double itemNetPrice) {
		this.itemNetPrice = itemNetPrice;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}
	public String getOperatingOfficerName() {
		return operatingOfficerName;
	}
	public void setOperatingOfficerName(String operatingOfficerName) {
		this.operatingOfficerName = operatingOfficerName;
	}
	
	
	
	
	public long getStoreId() {
		return storeId;
	}
	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public long getReturnItemId() {
		return returnItemId;
	}
	public void setReturnItemId(long returnItemId) {
		this.returnItemId = returnItemId;
	}
	public long getOrderListId() {
		return orderListId;
	}
	public void setOrderListId(long orderListId) {
		this.orderListId = orderListId;
	}
	public String getReturnReason() {
		return returnReason;
	}
	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	

}
