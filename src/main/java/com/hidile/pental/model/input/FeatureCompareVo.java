package com.hidile.pental.model.input;

import java.util.Comparator;

import com.hidile.pental.entity.input.FeaturesAndPhotosProduct;
import com.hidile.pental.service.input.InputServiceImpl;
import com.sun.media.jfxmedia.logging.Logger;

public class FeatureCompareVo implements Comparator<FeaturesAndPhotosProduct> 
{
	
	    public int compare(FeaturesAndPhotosProduct m1, FeaturesAndPhotosProduct m2) 
	    { 
	    	
	        if (m1.getLastUpdatedTime() < m2.getLastUpdatedTime()) return -1;
	        
	        if (m1.getLastUpdatedTime() > m2.getLastUpdatedTime()) return 1; 
	        else return 0; 
	    } 
	 
}
