package com.hidile.pental.model;

import javax.validation.constraints.Min;

public class GetPromocodeDetails {
	@Min(value=0,message="minimum current index should be 0")
	private int currentIndex;
	@Min(value=1,message="minimum row per page should be 1")
	private int rowPerPage;
	private long codeId;
	private long userId;
	private String promocode;
	private String customerName;
	private String customerNameAr;
	private double flatOffer;
	private long percentage;
	private long validFrom;
	private long validTo;
	private int status;
	private double minimumAmount;
	private double orderPrice;
	private double promocodeamount;
	
	
	
	
	
	public double getPromocodeamount() {
		return promocodeamount;
	}

	public void setPromocodeamount(double promocodeamount) {
		this.promocodeamount = promocodeamount;
	}

	public double getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(double orderPrice) {
		this.orderPrice = orderPrice;
	}

	public double getMinimumAmount() {
		return minimumAmount;
	}

	public void setMinimumAmount(double minimumAmount) {
		this.minimumAmount = minimumAmount;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}
	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNameAr() {
		return customerNameAr;
	}

	public void setCustomerNameAr(String customerNameAr) {
		this.customerNameAr = customerNameAr;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public long getCodeId() {
		return codeId;
	}
	public void setCodeId(long codeId) {
		this.codeId = codeId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getPromocode() {
		return promocode;
	}
	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}
	public double getFlatOffer() {
		return flatOffer;
	}
	public void setFlatOffer(double flatOffer) {
		this.flatOffer = flatOffer;
	}
	public long getPercentage() {
		return percentage;
	}
	public void setPercentage(long percentage) {
		this.percentage = percentage;
	}
	public long getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(long validFrom) {
		this.validFrom = validFrom;
	}
	public long getValidTo() {
		return validTo;
	}
	public void setValidTo(long validTo) {
		this.validTo = validTo;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	
	

}
