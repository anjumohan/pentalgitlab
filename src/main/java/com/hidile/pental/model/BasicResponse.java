package com.hidile.pental.model;




public class BasicResponse {
	
	public int errorCode;
	public String errorMessage;
	public String errorMessageArabic;
	public Object object;
	public long totalRecords;
	public String bodyJson;
	
	

	public String getErrorMessageArabic() {
		return errorMessageArabic;
	}
	public void setErrorMessageArabic(String errorMessageArabic) {
		this.errorMessageArabic = errorMessageArabic;
	}
	public String getBodyJson() {
		return bodyJson;
	}
	public void setBodyJson(String bodyJson) {
		this.bodyJson = bodyJson;
	}
	public long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode)   {
			this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	

}
